-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		gmaxparedes
-- Create date: 08/07/2020
-- Description:	Crear detalle de baseamount
--
-- =============================================
CREATE PROCEDURE PRAL_INSE_BASEAMOUNT_DETAILS 
	-- Add the parameters for the stored procedure here
	@CODE VARCHAR(20),
	@DATE DATETIME,
	@CONTROL_TYPE INT = NULL,
	@POS_TYPE INT = NULL,
	@TYPE_AGEN INT = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DECLARE @T TABLE(ID INT IDENTITY(1,1), CODE VARCHAR(20), [Type] INT, RepAuthTypeFilter INT, GLPostType INT, WorkTypeFilter NVARCHAR(10), County NVARCHAR(3))
	DECLARE @Resultado bit, @UltimaLinea int=0, @Reg int, @Contador int=1

	INSERT INTO @T(CODE, [Type], RepAuthTypeFilter, GLPostType, WorkTypeFilter, County)
		SELECT CODE, [Type], ReportingAuthorityType, GLPostType, WorkTypeFilter, County FROM DBO.PayrollControl WHERE 
			GLPostType = CASE WHEN (@POS_TYPE = 0) THEN
						GLPostType ELSE @POS_TYPE END
			AND		
			[Type] = CASE WHEN (@CONTROL_TYPE = 0) THEN
						[Type] ELSE @CONTROL_TYPE END
			AND		
			[ReportingAuthorityType] = CASE WHEN (@TYPE_AGEN = 0) THEN
						[ReportingAuthorityType] ELSE @TYPE_AGEN END
			AND Code NOT IN (SELECT ControlCodeFilter FROM dbo.BaseAmountDetail WHERE ControlCodeFilter=@CODE and EffectiveDate = @DATE)


	IF ((SELECT TOP 1 CODE FROM @T) IS NOT NULL)
	BEGIN
		SET @UltimaLinea=ISNULL(
						(SELECT MAX(LineNumber) from dbo.BaseAmountDetail WHERE BaseAmountCode=@CODE
							AND EffectiveDate = @DATE)
						,0)
		


		SET @Reg = isnull((SELECT COUNT(CODE) FROM @T) ,0)

		WHILE @Contador <= @Reg
		BEGIN
			SET @UltimaLinea+=1;

			INSERT INTO dbo.BaseAmountDetail(BaseAmountCode, EffectiveDate, LineNumber, ControlCodeFilter, TypeFilter, RepAuthTypeFilter,GLPostTypeFilter, WorkTypeFilter, CountyFilter)
				SELECT @CODE, @DATE, @UltimaLinea, CODE, [Type], RepAuthTypeFilter, GLPostType, WorkTypeFilter, County 
				FROM @T WHERE 
					ID = @Contador

			SET @Contador+=1;
		END

		UPDATE dbo.BaseAmount
		SET
			NumberofOccurrences= ISNULL((SELECT COUNT(CODE) FROM @T),0)
		WHERE
			Code = @CODE AND EffectiveDate = @DATE
		SET @Resultado=1
		
	END

	SET @Resultado=0
	SELECT @Resultado

END
GO
