USE Planilla
GO
/* Insertar datos sistema */
INSERT INTO Global.GLB_SISTEMAS(CD_CODI_SIST, CD_EMPRESA, DS_NOMB_SIST, CD_ESTADO_SISTEMA, USUA_CREA)
VALUES
('GLOBAL', '0001', 'Global', 1, 'Admin')

/* insertar menus de sistema */
INSERT INTO Global.GLB_MENUS_SISTEMAS (CD_CODI_MENU, CD_EMPRESA, DS_DESC_MENU, CD_ESTADO_MENU, USUA_CREA)
VALUES
('Consultas', '0001', 'Consultas del sistema', 1 , 'Admin')
INSERT INTO Global.GLB_MENUS_SISTEMAS (CD_CODI_MENU, CD_EMPRESA, DS_DESC_MENU, CD_ESTADO_MENU, USUA_CREA)
VALUES
('Procesos', '0001', 'Procesos del sistema', 1 , 'Admin')
INSERT INTO Global.GLB_MENUS_SISTEMAS (CD_CODI_MENU, CD_EMPRESA, DS_DESC_MENU, CD_ESTADO_MENU, USUA_CREA)
VALUES
('Tablas', '0001', 'Tablas del sistema', 1 , 'Admin')

/* Crear perfil de usuario */
INSERT INTO Global.GLB_PERFILES_USUARIOS (CD_CODIGO_PERFIL, CD_EMPRESA, DS_NOMBRE_PERFIL, CD_ESTADO_PERFIL,
USUA_CREA)
VALUES
('Administrador', '0001', 'Administrador de sistema.', 1, 'Admin')

/* Crear usuario */
INSERT INTO Global.USUARIOS (CD_CODIGO_USUARIO, DS_NOMBRE_USUARIO, CD_CLAVE_USUARIO, CD_DIRE_EMAIL,
CD_ESTADO_USUARIO, CD_EMPRESA_DEFECTO, USUA_CREA)
VALUES
('A0001', 'Admin', '2Bfq/uv9JWpfD2wFwPCWTw==', 'admin@admin.com', 'A', '0001', 'Admin')
/* Relacionar usuario perfil */
INSERT INTO Global.GLB_USUARIOS_PERFIL(CD_CODIGO_USUARIO, CD_EMPRESA, CD_CODIGO_PERFIL, USUA_CREA)
VALUES
('A0001', '0001', 'Administrador', 'Admin')
/* Insertar opciones del sistema 

Opciones mantenimiento de usuarios*/
INSERT INTO Global.GLB_OPCIONES_SISTEMA(CD_CODI_OPCI_SIST, CD_EMPRESA, DS_CODI_OPCI_SIST, DS_NOMBRE_FORMA, 
CD_INDI_REFRE_CAMP, DS_LINK_URL, CD_INDI_NECE_EMPR, CD_INDI_NECE_PERI_CONT, CD_INDI_VALI_PERI_CONT, 
CD_ESTADO_OPCION_SISTEMA, USUA_CREA)
VALUES
(
'MttnUsers', '0001', 'Mttn usuario', 'Usuarios', 'N', '/GlobalUsuarios',
'S', 'N', 'N', 1, 'Admin'
)
/* Crear usuarios */
--INSERT INTO Global.GLB_OPCIONES_SISTEMA(CD_CODI_OPCI_SIST, CD_EMPRESA, DS_CODI_OPCI_SIST, DS_NOMBRE_FORMA, 
--CD_INDI_REFRE_CAMP, DS_LINK_URL, CD_INDI_NECE_EMPR, CD_INDI_NECE_PERI_CONT, CD_INDI_VALI_PERI_CONT, 
--CD_ESTADO_OPCION_SISTEMA, USUA_CREA)
--VALUES
--(
--'CrearUsuario', '0001', 'Crear usuario', 'Crear usuario', 'N', '~/Views/GlobalUsuarios/Create.cshtml',
--'S', 'N', 'N', 1, 'Admin'
--)

/* Configurar opciones de sistema */
INSERT INTO Global.GLB_CONF_OPCI_SIST (CD_CODI_SIST, CD_CODI_MENU, CD_CODI_OPCI_SIST, CD_EMPRESA, 
NM_SEQU_ORDE_SIST, NM_SEQU_ORDE_MENU, NM_SEQU_ORDE_OPCI, USUA_CREA, Estado)
VALUES
(
'GLOBAL', 'Tablas', 'MttnUsers', '0001', 1, 1, 1, 'Admin', 1
)
/* Relacionar opciones de sistema con perfil de usuario */

INSERT INTO Global.GLB_OPCI_SIST_PERFIL(CD_CODIGO_PERFIL, CD_EMPRESA, CD_CODI_SIST, CD_CODI_MENU,
CD_CODI_OPCI_SIST, USUA_CREA, Estado)
VALUES
(
'Administrador', '0001', 'GLOBAL', 'Tablas', 'MttnUsers', 'Admin', 1
)