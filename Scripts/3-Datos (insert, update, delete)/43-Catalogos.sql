USE [Planilla]
GO
SET DATEFORMAT DMY;
SET IDENTITY_INSERT [dbo].[PayrollTypeReportingAuthority] ON 

INSERT [dbo].[PayrollTypeReportingAuthority] ([ID_TYPE], [DESCRIPCION], [USUA_CREA], [FECH_CREA], [USUA_ACTUA], [FECH_ACTUA]) VALUES (1, N'Otros', N'A0001', CAST(N'2020-06-22 11:07:37.817' AS DateTime), NULL, NULL)
SET IDENTITY_INSERT [dbo].[PayrollTypeReportingAuthority] OFF
SET IDENTITY_INSERT [dbo].[PayrollPostType] ON 

INSERT [dbo].[PayrollPostType] ([ID_POST], [DESCRIPCION], [USUA_CREA], [FECH_CREA], [USUA_ACTUA], [FECH_ACTUA]) VALUES (1, N'Do not post to G/L', N'A0001', CAST(N'2020-06-22 17:06:34.567' AS DateTime), NULL, NULL)
INSERT [dbo].[PayrollPostType] ([ID_POST], [DESCRIPCION], [USUA_CREA], [FECH_CREA], [USUA_ACTUA], [FECH_ACTUA]) VALUES (2, N'Post Expense and Cash to G/L', N'A0001', CAST(N'2020-06-22 17:06:54.660' AS DateTime), NULL, NULL)
INSERT [dbo].[PayrollPostType] ([ID_POST], [DESCRIPCION], [USUA_CREA], [FECH_CREA], [USUA_ACTUA], [FECH_ACTUA]) VALUES (3, N'Post Liability and Cash to G/L', N'A0001', CAST(N'2020-06-22 17:07:00.400' AS DateTime), NULL, NULL)
INSERT [dbo].[PayrollPostType] ([ID_POST], [DESCRIPCION], [USUA_CREA], [FECH_CREA], [USUA_ACTUA], [FECH_ACTUA]) VALUES (4, N'Post Expanse and Liability to G/L', N'A0001', CAST(N'2020-06-22 17:07:05.983' AS DateTime), NULL, NULL)
SET IDENTITY_INSERT [dbo].[PayrollPostType] OFF
SET IDENTITY_INSERT [dbo].[PayrollControlType] ON 

INSERT [dbo].[PayrollControlType] ([ID_TYPE], [DESCRIPCION], [USUA_CREA], [FECH_CREA], [USUA_ACTUA], [FECH_ACTUA]) VALUES (1, N'Ingresos', N'', CAST(N'2020-06-19 15:04:38.783' AS DateTime), NULL, NULL)
INSERT [dbo].[PayrollControlType] ([ID_TYPE], [DESCRIPCION], [USUA_CREA], [FECH_CREA], [USUA_ACTUA], [FECH_ACTUA]) VALUES (2, N'Deducción', N'', CAST(N'2020-06-19 15:05:03.437' AS DateTime), NULL, NULL)
INSERT [dbo].[PayrollControlType] ([ID_TYPE], [DESCRIPCION], [USUA_CREA], [FECH_CREA], [USUA_ACTUA], [FECH_ACTUA]) VALUES (3, N'Horas', N'', CAST(N'2020-06-19 15:05:13.623' AS DateTime), NULL, NULL)
INSERT [dbo].[PayrollControlType] ([ID_TYPE], [DESCRIPCION], [USUA_CREA], [FECH_CREA], [USUA_ACTUA], [FECH_ACTUA]) VALUES (4, N'Varios', N'', CAST(N'2020-06-19 15:08:22.897' AS DateTime), NULL, NULL)
INSERT [dbo].[PayrollControlType] ([ID_TYPE], [DESCRIPCION], [USUA_CREA], [FECH_CREA], [USUA_ACTUA], [FECH_ACTUA]) VALUES (5, N'Pago neto', N'', CAST(N'2020-06-19 15:08:36.703' AS DateTime), NULL, NULL)
SET IDENTITY_INSERT [dbo].[PayrollControlType] OFF
INSERT [dbo].[PayrollReportingAuthority] ([Code], [Type], [Name], [County], [Locality], [DateFilter], [GlobalDimension1Filter], [GlobalDimension2Filter], [PayrollControlTypeFilter], [GLPostTypeFilter], [WorkTypeFilter], [Amount], [TaxableAmount], [UnemploymentInsurance], [StateNoCode], [TaxingEntityCode], [ParentRepAuthCode], [EditStatus], [EmployerNoFilter]) VALUES (N'PRA-01', 1, N'Prueba', N'SS', N'SS', CAST(N'2020-06-23' AS Date), N'0', N'0', 1, 2, N'1', CAST(100.00000000000000000000 AS Decimal(38, 20)), CAST(50.00000000000000000000 AS Decimal(38, 20)), 1, N'0', N'0', N'0', NULL, N'0')
SET IDENTITY_INSERT [dbo].[PayrollControlMonthlySchedule] ON 

INSERT [dbo].[PayrollControlMonthlySchedule] ([ID_CALENDARIO], [DESCRIPCION], [USUA_CREA], [FECH_CREA], [USUA_ACTUA], [FECH_ACTUA]) VALUES (1, N'Each pay period', N'A0001', CAST(N'2020-06-23 08:59:16.440' AS DateTime), NULL, NULL)
INSERT [dbo].[PayrollControlMonthlySchedule] ([ID_CALENDARIO], [DESCRIPCION], [USUA_CREA], [FECH_CREA], [USUA_ACTUA], [FECH_ACTUA]) VALUES (2, N'1st pay period', N'A0001', CAST(N'2020-06-23 08:59:26.550' AS DateTime), NULL, NULL)
INSERT [dbo].[PayrollControlMonthlySchedule] ([ID_CALENDARIO], [DESCRIPCION], [USUA_CREA], [FECH_CREA], [USUA_ACTUA], [FECH_ACTUA]) VALUES (3, N'2nd pay period', N'A0001', CAST(N'2020-06-23 08:59:35.800' AS DateTime), NULL, NULL)
INSERT [dbo].[PayrollControlMonthlySchedule] ([ID_CALENDARIO], [DESCRIPCION], [USUA_CREA], [FECH_CREA], [USUA_ACTUA], [FECH_ACTUA]) VALUES (4, N'3rd pay period', N'A0001', CAST(N'2020-06-23 08:59:41.793' AS DateTime), NULL, NULL)
INSERT [dbo].[PayrollControlMonthlySchedule] ([ID_CALENDARIO], [DESCRIPCION], [USUA_CREA], [FECH_CREA], [USUA_ACTUA], [FECH_ACTUA]) VALUES (5, N'4th pay period', N'A0001', CAST(N'2020-06-23 08:59:52.047' AS DateTime), NULL, NULL)
INSERT [dbo].[PayrollControlMonthlySchedule] ([ID_CALENDARIO], [DESCRIPCION], [USUA_CREA], [FECH_CREA], [USUA_ACTUA], [FECH_ACTUA]) VALUES (6, N'1st an 3rd pay periods', N'A0001', CAST(N'2020-06-23 08:59:59.670' AS DateTime), NULL, NULL)
INSERT [dbo].[PayrollControlMonthlySchedule] ([ID_CALENDARIO], [DESCRIPCION], [USUA_CREA], [FECH_CREA], [USUA_ACTUA], [FECH_ACTUA]) VALUES (7, N'2nd and 4th pay periods', N'A0001', CAST(N'2020-06-23 09:00:05.787' AS DateTime), NULL, NULL)
INSERT [dbo].[PayrollControlMonthlySchedule] ([ID_CALENDARIO], [DESCRIPCION], [USUA_CREA], [FECH_CREA], [USUA_ACTUA], [FECH_ACTUA]) VALUES (8, N'Last pay period', N'A0001', CAST(N'2020-06-23 09:00:11.403' AS DateTime), NULL, NULL)
INSERT [dbo].[PayrollControlMonthlySchedule] ([ID_CALENDARIO], [DESCRIPCION], [USUA_CREA], [FECH_CREA], [USUA_ACTUA], [FECH_ACTUA]) VALUES (9, N'1st and 2nd pay periods', N'A0001', CAST(N'2020-06-23 09:00:21.793' AS DateTime), NULL, NULL)
INSERT [dbo].[PayrollControlMonthlySchedule] ([ID_CALENDARIO], [DESCRIPCION], [USUA_CREA], [FECH_CREA], [USUA_ACTUA], [FECH_ACTUA]) VALUES (10, N'1st and 4th pay periods', N'A0001', CAST(N'2020-06-23 09:00:26.793' AS DateTime), NULL, NULL)
SET IDENTITY_INSERT [dbo].[PayrollControlMonthlySchedule] OFF
INSERT [dbo].[PayrollControlGroup] ([Code], [Name], [PayControls], [IsGroupUsed], [PayControlFilter]) VALUES (N'13AVO', N'Pago de treceava', 2, 1, N'Pay')
INSERT [dbo].[PayrollControlGroup] ([Code], [Name], [PayControls], [IsGroupUsed], [PayControlFilter]) VALUES (N'14AVO', N'Pago de catorceava', 2, 1, NULL)
INSERT [dbo].[PayrollControlGroup] ([Code], [Name], [PayControls], [IsGroupUsed], [PayControlFilter]) VALUES (N'ANTICIPOS', NULL, 1, 1, NULL)
INSERT [dbo].[PayrollControlGroup] ([Code], [Name], [PayControls], [IsGroupUsed], [PayControlFilter]) VALUES (N'COMISION', N'Pago comisionista', 1, 0, NULL)
INSERT [dbo].[PayrollControlGroup] ([Code], [Name], [PayControls], [IsGroupUsed], [PayControlFilter]) VALUES (N'CONFIDENCI', N'Pago de salarios confidencia', 21, 0, NULL)
INSERT [dbo].[PayrollControlGroup] ([Code], [Name], [PayControls], [IsGroupUsed], [PayControlFilter]) VALUES (N'CONSTSUELD', N'Constancia de sueldo', 9, 0, NULL)
INSERT [dbo].[PayrollControlGroup] ([Code], [Name], [PayControls], [IsGroupUsed], [PayControlFilter]) VALUES (N'EM IHSS', N'EM IHSS REPORTE', 3, 0, NULL)
INSERT [dbo].[PayrollControlGroup] ([Code], [Name], [PayControls], [IsGroupUsed], [PayControlFilter]) VALUES (N'EM IHSS PT', N'EM IHSS PATRONAL', 3, 0, NULL)
INSERT [dbo].[PayrollControlGroup] ([Code], [Name], [PayControls], [IsGroupUsed], [PayControlFilter]) VALUES (N'FIJO', N'Pago de salario fijo', 14, 0, NULL)
INSERT [dbo].[PayrollPostingGroup] ([Code], [EarningsAccount], [BankAccountNo], [LiabilityAccount], [ExpenseAccount], [DateFilter], [DetailExists], [CurrentEffectiveDate]) VALUES (N'PAYNETO', N'1413863-15', NULL, NULL, NULL, CAST(N'2020-06-28 00:00:00.000' AS DateTime), 0, CAST(N'2020-07-28 00:00:00.000' AS DateTime))
INSERT [dbo].[PayrollPostingGroup] ([Code], [EarningsAccount], [BankAccountNo], [LiabilityAccount], [ExpenseAccount], [DateFilter], [DetailExists], [CurrentEffectiveDate]) VALUES (N'PG-01', N'0510-0001-0011', NULL, NULL, NULL, CAST(N'2020-06-23 00:00:00.000' AS DateTime), 0, CAST(N'2020-01-01 00:00:00.000' AS DateTime))
INSERT [dbo].[PayrollControlGroupControl] ([PayrollControlGroupCode], [PayControlCode], [PayControlName], [PayrollControlGroupName], [MonthlySchedule], [PayrollPostingGroup]) VALUES (N'13AVO', N'100%INCAP', N'Pagar 100% incapacidad', N'Pago de treceava', 1, NULL)
INSERT [dbo].[PayrollControlGroupControl] ([PayrollControlGroupCode], [PayControlCode], [PayControlName], [PayrollControlGroupName], [MonthlySchedule], [PayrollPostingGroup]) VALUES (N'13AVO', N'FACTPROD', N'Factura por producción', N'Pago de treceava', NULL, N'PG-01')
INSERT [dbo].[PayrollControlGroupControl] ([PayrollControlGroupCode], [PayControlCode], [PayControlName], [PayrollControlGroupName], [MonthlySchedule], [PayrollPostingGroup]) VALUES (N'FIJO', N'100%INCA', N'Pago 100% incapacidad', N'Pago de salario fijo', 1, NULL)
INSERT [dbo].[PayrollControlGroupControl] ([PayrollControlGroupCode], [PayControlCode], [PayControlName], [PayrollControlGroupName], [MonthlySchedule], [PayrollPostingGroup]) VALUES (N'FIJO', N'34%INCA', N'Pago 34% incapacidad', N'Pago de salario fijo', 1, NULL)
INSERT [dbo].[PayrollControlGroupControl] ([PayrollControlGroupCode], [PayControlCode], [PayControlName], [PayrollControlGroupName], [MonthlySchedule], [PayrollPostingGroup]) VALUES (N'FIJO', N'AUVAC', N'Vacaciones', N'Pago de salario fijo', 1, NULL)
INSERT [dbo].[PayrollControlGroupControl] ([PayrollControlGroupCode], [PayControlCode], [PayControlName], [PayrollControlGroupName], [MonthlySchedule], [PayrollPostingGroup]) VALUES (N'FIJO', N'FACTPROD', N'Factura por prod', N'Pago de salario fijo', NULL, N'PG-01')
INSERT [dbo].[PayrollControlGroupControl] ([PayrollControlGroupCode], [PayControlCode], [PayControlName], [PayrollControlGroupName], [MonthlySchedule], [PayrollPostingGroup]) VALUES (N'FIJO', N'HRNORMAL', N'Horas normales', N'Pago de salario fijo', 1, NULL)
SET IDENTITY_INSERT [dbo].[DirectDepositTypeContent] ON 

INSERT [dbo].[DirectDepositTypeContent] ([ID_CONTENT], [DESCRIPCION], [USUA_CREA], [FECH_CREA], [USUA_ACTUA], [FECH_ACTUA]) VALUES (1, N'Header', N'', CAST(N'2020-06-03 11:21:07.600' AS DateTime), NULL, NULL)
INSERT [dbo].[DirectDepositTypeContent] ([ID_CONTENT], [DESCRIPCION], [USUA_CREA], [FECH_CREA], [USUA_ACTUA], [FECH_ACTUA]) VALUES (2, N'Detail', N'', CAST(N'2020-06-03 11:22:36.317' AS DateTime), NULL, NULL)
SET IDENTITY_INSERT [dbo].[DirectDepositTypeContent] OFF
INSERT [dbo].[DirectDepositSections] ([DirectDepositLayoutCode], [SectionType], [LineNumber], [SequenceNumber], [Description], [IncTotalLineCount], [IncTotalHeaderCount], [IncTotalDetailCount], [IncTotalFooterCount], [IncTotalCreditCount], [IncTotalDebitCount], [IncTotalBatchCount], [IncBatchDetailCount], [ResetBatchTotals]) VALUES (N'FICOHSA', 1, 1, 0, N'TRANSBNKEL', 1, 0, 1, 1, 1, 1, 1, 1, 1)
INSERT [dbo].[DirectDepositSections] ([DirectDepositLayoutCode], [SectionType], [LineNumber], [SequenceNumber], [Description], [IncTotalLineCount], [IncTotalHeaderCount], [IncTotalDetailCount], [IncTotalFooterCount], [IncTotalCreditCount], [IncTotalDebitCount], [IncTotalBatchCount], [IncBatchDetailCount], [ResetBatchTotals]) VALUES (N'FICOHSA', 1, 2, 1, N'PRUEBA', 1, 1, 1, 0, 0, 0, 1, 0, 0)
INSERT [dbo].[DirectDepositSections] ([DirectDepositLayoutCode], [SectionType], [LineNumber], [SequenceNumber], [Description], [IncTotalLineCount], [IncTotalHeaderCount], [IncTotalDetailCount], [IncTotalFooterCount], [IncTotalCreditCount], [IncTotalDebitCount], [IncTotalBatchCount], [IncBatchDetailCount], [ResetBatchTotals]) VALUES (N'FICOHSA', 1, 3, 2, N'a', 0, 0, 0, 0, 0, 0, 0, 0, 0)
INSERT [dbo].[DirectDepositSections] ([DirectDepositLayoutCode], [SectionType], [LineNumber], [SequenceNumber], [Description], [IncTotalLineCount], [IncTotalHeaderCount], [IncTotalDetailCount], [IncTotalFooterCount], [IncTotalCreditCount], [IncTotalDebitCount], [IncTotalBatchCount], [IncBatchDetailCount], [ResetBatchTotals]) VALUES (N'FICOHSA', 1, 4, 3, N'prueba 3', 0, 0, 0, 0, 0, 0, 1, 0, 0)
INSERT [dbo].[DirectDepositSections] ([DirectDepositLayoutCode], [SectionType], [LineNumber], [SequenceNumber], [Description], [IncTotalLineCount], [IncTotalHeaderCount], [IncTotalDetailCount], [IncTotalFooterCount], [IncTotalCreditCount], [IncTotalDebitCount], [IncTotalBatchCount], [IncBatchDetailCount], [ResetBatchTotals]) VALUES (N'FICOHSA', 2, 5, 4, N'Prueba 4', 1, 1, 0, 0, 0, 0, 0, 0, 0)
INSERT [dbo].[DirectDepositSections] ([DirectDepositLayoutCode], [SectionType], [LineNumber], [SequenceNumber], [Description], [IncTotalLineCount], [IncTotalHeaderCount], [IncTotalDetailCount], [IncTotalFooterCount], [IncTotalCreditCount], [IncTotalDebitCount], [IncTotalBatchCount], [IncBatchDetailCount], [ResetBatchTotals]) VALUES (N'FICOHSA', 2, 6, 5, N'Prueba 5', 0, 0, 1, 1, 0, 0, 0, 0, 0)
SET IDENTITY_INSERT [dbo].[DirectDepositDateFormat] ON 

INSERT [dbo].[DirectDepositDateFormat] ([ID_DATE_FORMAT], [DESCRIPCION], [USUA_CREA], [FECH_CREA], [USUA_ACTUA], [FECH_ACTUA]) VALUES (1, N'dd/MM/yyyy', N'A0001', CAST(N'2020-06-10 09:51:19.383' AS DateTime), NULL, NULL)
INSERT [dbo].[DirectDepositDateFormat] ([ID_DATE_FORMAT], [DESCRIPCION], [USUA_CREA], [FECH_CREA], [USUA_ACTUA], [FECH_ACTUA]) VALUES (2, N'yyyy/MM/dd', N'A0001', CAST(N'2020-06-10 09:51:36.167' AS DateTime), NULL, NULL)
SET IDENTITY_INSERT [dbo].[DirectDepositDateFormat] OFF
SET IDENTITY_INSERT [dbo].[DirectDepositLineType] ON 

INSERT [dbo].[DirectDepositLineType] ([ID_LINE_TYPE], [DESCRIPCION], [USUA_CREA], [FECH_CREA], [USUA_ACTUA], [FECH_ACTUA]) VALUES (1, N'Campo', N'A0001', CAST(N'2020-06-08 11:28:39.990' AS DateTime), NULL, NULL)
INSERT [dbo].[DirectDepositLineType] ([ID_LINE_TYPE], [DESCRIPCION], [USUA_CREA], [FECH_CREA], [USUA_ACTUA], [FECH_ACTUA]) VALUES (2, N'Literal', N'A0001', CAST(N'2020-06-08 11:29:48.540' AS DateTime), NULL, NULL)
SET IDENTITY_INSERT [dbo].[DirectDepositLineType] OFF
SET IDENTITY_INSERT [dbo].[DirectDepositTimeFormat] ON 

INSERT [dbo].[DirectDepositTimeFormat] ([ID_TIME_FORMAT], [DESCRIPCION], [USUA_CREA], [FECH_CREA], [USUA_ACTUA], [FECH_ACTUA]) VALUES (1, N'24', N'A0001', CAST(N'2020-06-10 09:55:55.723' AS DateTime), NULL, NULL)
INSERT [dbo].[DirectDepositTimeFormat] ([ID_TIME_FORMAT], [DESCRIPCION], [USUA_CREA], [FECH_CREA], [USUA_ACTUA], [FECH_ACTUA]) VALUES (2, N'12', N'A0001', CAST(N'2020-06-10 09:56:02.867' AS DateTime), NULL, NULL)
SET IDENTITY_INSERT [dbo].[DirectDepositTimeFormat] OFF
SET IDENTITY_INSERT [dbo].[DirectDepositTypeJustification] ON 

INSERT [dbo].[DirectDepositTypeJustification] ([ID_JUSTIFICATION], [DESCRIPCION], [USUA_CREA], [FECH_CREA], [USUA_ACTUA], [FECH_ACTUA]) VALUES (1, N'Derecha', N'A0001', CAST(N'2020-06-10 09:50:46.443' AS DateTime), NULL, NULL)
INSERT [dbo].[DirectDepositTypeJustification] ([ID_JUSTIFICATION], [DESCRIPCION], [USUA_CREA], [FECH_CREA], [USUA_ACTUA], [FECH_ACTUA]) VALUES (2, N'Izquierda', N'A0001', CAST(N'2020-06-10 09:51:00.880' AS DateTime), NULL, NULL)
SET IDENTITY_INSERT [dbo].[DirectDepositTypeJustification] OFF
SET IDENTITY_INSERT [dbo].[DirectDepositTypeLayout] ON 

INSERT [dbo].[DirectDepositTypeLayout] ([ID_LAYOUT], [DESCRIPCION], [USUA_CREA], [FECH_CREA], [USUA_ACTUA], [FECH_ACTUA]) VALUES (1, N'CPA', N'', CAST(N'2020-06-03 13:37:09.273' AS DateTime), NULL, NULL)
SET IDENTITY_INSERT [dbo].[DirectDepositTypeLayout] OFF
SET IDENTITY_INSERT [dbo].[DirectDepositTypeToExport] ON 

INSERT [dbo].[DirectDepositTypeToExport] ([ID_TYPE_EXPORT], [DESCRIPCION], [USUA_CREA], [FECH_CREA], [USUA_ACTUA], [FECH_ACTUA]) VALUES (1, N'Texto', N'A0001', CAST(N'2020-06-08 11:14:03.160' AS DateTime), NULL, NULL)
INSERT [dbo].[DirectDepositTypeToExport] ([ID_TYPE_EXPORT], [DESCRIPCION], [USUA_CREA], [FECH_CREA], [USUA_ACTUA], [FECH_ACTUA]) VALUES (2, N'Cantidad', N'A0001', CAST(N'2020-06-08 11:14:11.787' AS DateTime), NULL, NULL)
SET IDENTITY_INSERT [dbo].[DirectDepositTypeToExport] OFF
SET IDENTITY_INSERT [dbo].[PayFactorAnual] ON 

INSERT [dbo].[PayFactorAnual] ([ID_FAC_ANUAL], [DESCRIPCION], [USUA_CREA], [FECH_CREA], [USUA_ACTU], [FECH_ACTU]) VALUES (1, N'Periodos generados', N'Admin', CAST(N'2020-05-20 16:59:20.240' AS DateTime), N'A0001', CAST(N'2020-06-02 11:32:05.757' AS DateTime))
INSERT [dbo].[PayFactorAnual] ([ID_FAC_ANUAL], [DESCRIPCION], [USUA_CREA], [FECH_CREA], [USUA_ACTU], [FECH_ACTU]) VALUES (2, N'Prueba', N'A0001', CAST(N'2020-06-02 11:27:18.963' AS DateTime), N'A0001', CAST(N'2020-06-02 11:29:30.567' AS DateTime))
SET IDENTITY_INSERT [dbo].[PayFactorAnual] OFF
SET IDENTITY_INSERT [dbo].[PayFrecuency] ON 

INSERT [dbo].[PayFrecuency] ([PayFrequency], [Description], [USUA_CREA], [FECH_CREA], [USUA_ACTU], [FECH_ACTU]) VALUES (1, N'Semanal', N'Admin', CAST(N'2020-05-21 13:39:13.023' AS DateTime), N'A0001', CAST(N'2020-06-01 14:22:46.530' AS DateTime))
INSERT [dbo].[PayFrecuency] ([PayFrequency], [Description], [USUA_CREA], [FECH_CREA], [USUA_ACTU], [FECH_ACTU]) VALUES (2, N'Quincenal', N'Admin', CAST(N'2020-05-21 13:39:13.183' AS DateTime), N'A0001', CAST(N'2020-06-01 14:19:13.560' AS DateTime))
INSERT [dbo].[PayFrecuency] ([PayFrequency], [Description], [USUA_CREA], [FECH_CREA], [USUA_ACTU], [FECH_ACTU]) VALUES (3, N'Semimensual', N'Admin', CAST(N'2020-05-21 13:39:13.240' AS DateTime), N'A0001', CAST(N'2020-05-29 16:57:03.733' AS DateTime))
INSERT [dbo].[PayFrecuency] ([PayFrequency], [Description], [USUA_CREA], [FECH_CREA], [USUA_ACTU], [FECH_ACTU]) VALUES (4, N'Mensual', N'Admin', CAST(N'2020-05-21 13:39:13.267' AS DateTime), N'A0001', CAST(N'2020-06-01 08:32:20.530' AS DateTime))
INSERT [dbo].[PayFrecuency] ([PayFrequency], [Description], [USUA_CREA], [FECH_CREA], [USUA_ACTU], [FECH_ACTU]) VALUES (5, N'Bimensual', N'Admin', CAST(N'2020-05-21 13:39:13.313' AS DateTime), N'A0001', CAST(N'2020-06-01 08:32:41.210' AS DateTime))
INSERT [dbo].[PayFrecuency] ([PayFrequency], [Description], [USUA_CREA], [FECH_CREA], [USUA_ACTU], [FECH_ACTU]) VALUES (6, N'Trimestral', N'Admin', CAST(N'2020-05-21 13:39:13.317' AS DateTime), NULL, NULL)
INSERT [dbo].[PayFrecuency] ([PayFrequency], [Description], [USUA_CREA], [FECH_CREA], [USUA_ACTU], [FECH_ACTU]) VALUES (7, N'Semianual', N'Admin', CAST(N'2020-05-21 13:39:13.330' AS DateTime), NULL, NULL)
INSERT [dbo].[PayFrecuency] ([PayFrequency], [Description], [USUA_CREA], [FECH_CREA], [USUA_ACTU], [FECH_ACTU]) VALUES (8, N'Anual', N'Admin', CAST(N'2020-05-21 13:39:13.333' AS DateTime), NULL, NULL)
INSERT [dbo].[PayFrecuency] ([PayFrequency], [Description], [USUA_CREA], [FECH_CREA], [USUA_ACTU], [FECH_ACTU]) VALUES (10, N'Prueba', N'A0001', CAST(N'2020-06-02 10:30:20.167' AS DateTime), NULL, NULL)
SET IDENTITY_INSERT [dbo].[PayFrecuency] OFF
SET IDENTITY_INSERT [dbo].[PayrollControlAbsenceCode] ON 

INSERT [dbo].[PayrollControlAbsenceCode] ([ID_ABSENCE], [DESCRIPCION], [USUA_CREA], [FECH_CREA], [USUA_ACTUA], [FECH_ACTUA]) VALUES (1, N'Vacación', N'A0001', CAST(N'2020-06-22 18:05:11.420' AS DateTime), N'A0001', CAST(N'2020-06-22 18:05:28.033' AS DateTime))
INSERT [dbo].[PayrollControlAbsenceCode] ([ID_ABSENCE], [DESCRIPCION], [USUA_CREA], [FECH_CREA], [USUA_ACTUA], [FECH_ACTUA]) VALUES (2, N'Incapacidad', N'A0001', CAST(N'2020-06-22 18:05:20.790' AS DateTime), NULL, NULL)
SET IDENTITY_INSERT [dbo].[PayrollControlAbsenceCode] OFF
SET IDENTITY_INSERT [dbo].[PayrollControlArrearsCode] ON 

INSERT [dbo].[PayrollControlArrearsCode] ([ID_ARREARS], [DESCRIPCION], [USUA_CREA], [FECH_CREA], [USUA_ACTUA], [FECH_ACTUA]) VALUES (1, N'Prueba', N'A0001', CAST(N'2020-06-23 10:10:11.740' AS DateTime), NULL, NULL)
INSERT [dbo].[PayrollControlArrearsCode] ([ID_ARREARS], [DESCRIPCION], [USUA_CREA], [FECH_CREA], [USUA_ACTUA], [FECH_ACTUA]) VALUES (2, N'Segunda Prueba', N'A0001', CAST(N'2020-06-23 10:10:21.797' AS DateTime), NULL, NULL)
SET IDENTITY_INSERT [dbo].[PayrollControlArrearsCode] OFF
SET IDENTITY_INSERT [dbo].[PayrollControlComment] ON 

INSERT [dbo].[PayrollControlComment] ([ID_COMMENT], [CodePayControl], [COMMENT], [USUA_CREA], [FECH_CREA], [USUA_ACTUA], [FECH_ACTUA]) VALUES (2, N'PC-01', N'esto es una prueba', N'A0001', CAST(N'2020-06-26 14:35:12.933' AS DateTime), N'', CAST(N'2020-07-01 16:32:16.963' AS DateTime))
INSERT [dbo].[PayrollControlComment] ([ID_COMMENT], [CodePayControl], [COMMENT], [USUA_CREA], [FECH_CREA], [USUA_ACTUA], [FECH_ACTUA]) VALUES (3, N'PC-01', N'hola ed', N'A0001', CAST(N'2020-06-26 14:35:35.207' AS DateTime), N'A0001', CAST(N'2020-06-26 15:11:12.130' AS DateTime))
INSERT [dbo].[PayrollControlComment] ([ID_COMMENT], [CodePayControl], [COMMENT], [USUA_CREA], [FECH_CREA], [USUA_ACTUA], [FECH_ACTUA]) VALUES (6, N'PC-02', N'Prueba', N'', CAST(N'2020-07-01 16:48:21.447' AS DateTime), NULL, NULL)
INSERT [dbo].[PayrollControlComment] ([ID_COMMENT], [CodePayControl], [COMMENT], [USUA_CREA], [FECH_CREA], [USUA_ACTUA], [FECH_ACTUA]) VALUES (7, N'PC-02', N'Esto es una prueba más realizada para pay control', N'', CAST(N'2020-07-01 16:48:53.770' AS DateTime), NULL, NULL)
SET IDENTITY_INSERT [dbo].[PayrollControlComment] OFF
SET IDENTITY_INSERT [dbo].[PayrollControlNetPayTest] ON 

INSERT [dbo].[PayrollControlNetPayTest] ([ID_NETPAYTEST], [DESCRIPCION], [USUA_CREA], [FECH_CREA], [USUA_ACTUA], [FECH_ACTUA]) VALUES (1, N'Do not test', N'A0001', CAST(N'2020-06-23 09:18:23.627' AS DateTime), NULL, NULL)
INSERT [dbo].[PayrollControlNetPayTest] ([ID_NETPAYTEST], [DESCRIPCION], [USUA_CREA], [FECH_CREA], [USUA_ACTUA], [FECH_ACTUA]) VALUES (2, N'Test and adjust amount', N'A0001', CAST(N'2020-06-23 09:18:29.457' AS DateTime), NULL, NULL)
INSERT [dbo].[PayrollControlNetPayTest] ([ID_NETPAYTEST], [DESCRIPCION], [USUA_CREA], [FECH_CREA], [USUA_ACTUA], [FECH_ACTUA]) VALUES (3, N'Arrears tracking', N'A0001', CAST(N'2020-06-23 09:18:34.370' AS DateTime), NULL, NULL)
SET IDENTITY_INSERT [dbo].[PayrollControlNetPayTest] OFF
SET IDENTITY_INSERT [dbo].[PayrollControlPrintOnCheck] ON 

INSERT [dbo].[PayrollControlPrintOnCheck] ([ID_PRINT], [DESCRIPCION], [USUA_CREA], [FECH_CREA], [USUA_ACTUA], [FECH_ACTUA]) VALUES (1, N'Current Pay Amount', N'A0001', CAST(N'2020-06-22 17:41:35.773' AS DateTime), NULL, NULL)
INSERT [dbo].[PayrollControlPrintOnCheck] ([ID_PRINT], [DESCRIPCION], [USUA_CREA], [FECH_CREA], [USUA_ACTUA], [FECH_ACTUA]) VALUES (2, N'No Print', N'A0001', CAST(N'2020-06-22 17:41:46.670' AS DateTime), NULL, NULL)
SET IDENTITY_INSERT [dbo].[PayrollControlPrintOnCheck] OFF
INSERT [Global].[GLB_TIPOS_DE_IMPUESTOS] ([CD_CODIGO_IMPUESTO], [CD_PAIS], [CD_EMPRESA], [DS_DESCRIPCION_IMPUESTO], [VL_PORCENTAJE_1], [VL_PORCENTAJE_2], [USUA_CREA], [FECH_CREA], [USUA_ACTU], [FECH_ACTU], [CD_CUENTA_CONTABLE]) VALUES (N'IVA_13', N'SV', N'0001', N'IVA', CAST(13.00 AS Numeric(12, 2)), NULL, N'A0001', CAST(N'2020-06-22 08:52:40.113' AS DateTime), NULL, CAST(N'2020-06-22 08:52:40.113' AS DateTime), NULL)
INSERT [Global].[GLB_TIPOS_DE_IMPUESTOS] ([CD_CODIGO_IMPUESTO], [CD_PAIS], [CD_EMPRESA], [DS_DESCRIPCION_IMPUESTO], [VL_PORCENTAJE_1], [VL_PORCENTAJE_2], [USUA_CREA], [FECH_CREA], [USUA_ACTU], [FECH_ACTU], [CD_CUENTA_CONTABLE]) VALUES (N'Renta_SV', N'SV', N'0001', N'Impuesto sobre la renta', CAST(10.00 AS Numeric(12, 2)), NULL, N'A0001', CAST(N'2020-06-22 08:57:31.387' AS DateTime), N'A0001', CAST(N'2020-06-22 08:57:39.523' AS DateTime), NULL)
