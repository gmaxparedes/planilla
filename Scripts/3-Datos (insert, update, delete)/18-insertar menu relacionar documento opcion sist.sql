

/* Insertar opciones del sistema 

Opciones Ciclo de pago*/
INSERT INTO Global.GLB_OPCIONES_SISTEMA(CD_CODI_OPCI_SIST, CD_EMPRESA, DS_CODI_OPCI_SIST, DS_NOMBRE_FORMA, 
CD_INDI_REFRE_CAMP, DS_LINK_URL, CD_INDI_NECE_EMPR, CD_INDI_NECE_PERI_CONT, CD_INDI_VALI_PERI_CONT, 
CD_ESTADO_OPCION_SISTEMA, USUA_CREA)
VALUES
(
'RelDocSit', '0001', 'Relacion', 'Relacionar documentos', 'N', '/RelacionDocsOpcSist',
'S', 'N', 'N', 1, 'Admin'
)

/* Configurar opciones de sistema */
select * from Global.GLB_CONF_OPCI_SIST
INSERT INTO Global.GLB_CONF_OPCI_SIST (CD_CODI_SIST, CD_CODI_MENU, CD_CODI_OPCI_SIST, CD_EMPRESA, 
NM_SEQU_ORDE_SIST, NM_SEQU_ORDE_MENU, NM_SEQU_ORDE_OPCI, USUA_CREA, Estado)
VALUES
(
'GLOBAL', 'Tablas', 'RelDocSit', '0001', 1, 1, 3, 'Admin', 1
)
/* Relacionar opciones de sistema con perfil de usuario */

INSERT INTO Global.GLB_OPCI_SIST_PERFIL(CD_CODIGO_PERFIL, CD_EMPRESA, CD_CODI_SIST, CD_CODI_MENU,
CD_CODI_OPCI_SIST, USUA_CREA, Estado)
VALUES
(
'Administrador', '0001', 'GLOBAL', 'Tablas', 'RelDocSit', 'Admin', 1
)