/*    ==Scripting Parameters==

    Source Server Version : SQL Server 2017 (14.0.1000)
    Source Database Engine Edition : Microsoft SQL Server Express Edition
    Source Database Engine Type : Standalone SQL Server

    Target Server Version : SQL Server 2017
    Target Database Engine Edition : Microsoft SQL Server Standard Edition
    Target Database Engine Type : Standalone SQL Server
*/
USE [master]
GO
/****** Object:  Database [Planilla]    Script Date: 4/21/2020 6:05:09 PM ******/
CREATE DATABASE [Planilla]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'Planilla', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\Planilla.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'Planilla_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\Planilla_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [Planilla] SET COMPATIBILITY_LEVEL = 140
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Planilla].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [Planilla] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [Planilla] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [Planilla] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [Planilla] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [Planilla] SET ARITHABORT OFF 
GO
ALTER DATABASE [Planilla] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [Planilla] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [Planilla] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [Planilla] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [Planilla] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [Planilla] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [Planilla] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [Planilla] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [Planilla] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [Planilla] SET  DISABLE_BROKER 
GO
ALTER DATABASE [Planilla] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [Planilla] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [Planilla] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [Planilla] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [Planilla] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [Planilla] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [Planilla] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [Planilla] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [Planilla] SET  MULTI_USER 
GO
ALTER DATABASE [Planilla] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [Planilla] SET DB_CHAINING OFF 
GO
ALTER DATABASE [Planilla] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [Planilla] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [Planilla] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [Planilla] SET QUERY_STORE = OFF
GO
USE [Planilla]
GO
/****** Object:  Table [dbo].[AsociaciondeEmpleados]    Script Date: 4/21/2020 6:05:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AsociaciondeEmpleados](
	[codempleado] [nvarchar](20) NOT NULL,
	[primernombre] [nvarchar](30) NULL,
	[apellidos] [nvarchar](30) NULL,
	[asociar] [tinyint] NULL,
	[CicloAsignar] [nvarchar](20) NULL,
	[empresa] [nvarchar](20) NULL,
	[Tipoempl] [nvarchar](20) NULL,
 CONSTRAINT [PK_AsociaciondeEmpleados] PRIMARY KEY CLUSTERED 
(
	[codempleado] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[BankAccount]    Script Date: 4/21/2020 6:05:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BankAccount](
	[No] [nvarchar](20) NOT NULL,
	[Name] [nvarchar](50) NULL,
	[SearchName] [nvarchar](50) NULL,
	[Name2] [nvarchar](50) NULL,
	[Address] [nvarchar](50) NULL,
	[Address2] [nvarchar](50) NULL,
	[City] [nvarchar](30) NULL,
	[Contact] [nvarchar](50) NULL,
	[PhoneNo] [nvarchar](30) NULL,
	[TelexNo] [nvarchar](20) NULL,
	[BankAccountNo] [nvarchar](30) NULL,
	[TransitNo] [nvarchar](20) NULL,
	[TerritoryCode] [nvarchar](10) NULL,
	[GlobalDimension1Code] [nvarchar](20) NULL,
	[GlobalDimension2Code] [nvarchar](20) NULL,
	[ChainName] [nvarchar](10) NULL,
	[MinBalance] [decimal](38, 20) NULL,
	[BankAccPostingGroup] [nvarchar](10) NULL,
	[CurrencyCode] [nvarchar](10) NULL,
	[LanguageCode] [nvarchar](10) NULL,
	[StatisticsGroup] [int] NULL,
	[OurContactCode] [nvarchar](10) NULL,
	[CountryRegionCode] [nvarchar](10) NULL,
	[Amount] [decimal](38, 20) NULL,
	[Comment] [tinyint] NULL,
	[Blocked] [tinyint] NULL,
	[LastStatementNo] [nvarchar](20) NULL,
	[LastPaymentStatementNo] [nvarchar](20) NULL,
	[LastDateModified] [datetime] NULL,
	[DateFilter] [datetime] NULL,
	[GlobalDimension1Filter] [nvarchar](20) NULL,
	[GlobalDimension2Filter] [nvarchar](20) NULL,
	[Balance] [decimal](38, 20) NULL,
	[BalanceLCY] [decimal](38, 20) NULL,
	[NetChange] [decimal](38, 20) NULL,
	[NetChangeLCY] [decimal](38, 20) NULL,
	[TotalonChecks] [decimal](38, 20) NULL,
	[FaxNo] [nvarchar](30) NULL,
	[TelexAnswerBack] [nvarchar](20) NULL,
	[Picture] [image] NULL,
	[PostCode] [nvarchar](20) NULL,
	[County] [nvarchar](30) NULL,
	[LastCheckNo] [nvarchar](20) NULL,
	[BalanceLastStatement] [decimal](38, 20) NULL,
	[BalanceatDate] [decimal](38, 20) NULL,
	[BalanceatDateLCY] [decimal](38, 20) NULL,
	[DebitAmount] [decimal](38, 20) NULL,
	[CreditAmount] [decimal](38, 20) NULL,
	[DebitAmountLCY] [decimal](38, 20) NULL,
	[CreditAmountLCY] [decimal](38, 20) NULL,
	[BankBranchNo] [nvarchar](20) NULL,
	[EMail] [nvarchar](80) NULL,
	[HomePage] [nvarchar](80) NULL,
	[NoSeries] [nvarchar](10) NULL,
	[CheckReportID] [int] NULL,
	[CheckReportName] [nvarchar](250) NULL,
	[IBAN] [nvarchar](50) NULL,
	[SWIFTCode] [nvarchar](20) NULL,
	[BankStatementImportFormat] [nvarchar](20) NULL,
	[CreditTransferMsgNos] [nvarchar](10) NULL,
	[DirectDebitMsgNos] [nvarchar](10) NULL,
	[SEPADirectDebitExpFormat] [nvarchar](20) NULL,
	[CreditorNo] [nvarchar](35) NULL,
	[PaymentExportFormat] [nvarchar](20) NULL,
	[BankClearingCode] [nvarchar](50) NULL,
	[BankClearingStandard] [nvarchar](50) NULL,
	[BankNameDataConversion] [nvarchar](50) NULL,
	[MatchToleranceType] [int] NULL,
	[MatchToleranceValue] [decimal](38, 20) NULL,
	[PositivePayExportCode] [nvarchar](20) NULL,
	[EPayExportFilePath] [nvarchar](250) NULL,
	[LastEPayExportFileName] [nvarchar](30) NULL,
	[EPayTransProgramPath] [nvarchar](250) NULL,
	[EPayTransProgramCommand] [nvarchar](80) NULL,
	[LastACHFileIDModifier] [nvarchar](1) NULL,
	[LastRemittanceAdviceNo] [nvarchar](20) NULL,
	[ExportFormat] [int] NULL,
	[LastEPayFileCreationNo] [int] NULL,
	[ClientNo] [nvarchar](10) NULL,
	[ClientName] [nvarchar](30) NULL,
	[InputQualifier] [nvarchar](30) NULL,
	[BankCommunication] [int] NULL,
	[CheckDateFormat] [int] NULL,
	[CheckDateSeparator] [int] NULL,
	[BankCode] [nvarchar](3) NULL,
	[DirectDepositLayout] [nvarchar](20) NULL,
	[OriginatorID] [nvarchar](10) NULL,
	[OriginatorShortName] [nvarchar](15) NULL,
	[OriginatorLongName] [nvarchar](30) NULL,
	[DataCenter] [int] NULL,
	[PrintMICREncoding] [tinyint] NULL,
	[Divider1] [nvarchar](1) NULL,
	[Format1] [int] NULL,
	[Divider2] [nvarchar](1) NULL,
	[Format2] [int] NULL,
	[Divider3] [nvarchar](1) NULL,
	[Format3] [int] NULL,
	[Divider4] [nvarchar](1) NULL,
	[Divider5] [nvarchar](1) NULL,
	[Divider6] [nvarchar](1) NULL,
	[Spacing1] [int] NULL,
	[Spacing2] [int] NULL,
 CONSTRAINT [PK_BankAccount] PRIMARY KEY CLUSTERED 
(
	[No] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[BaseAmount]    Script Date: 4/21/2020 6:05:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BaseAmount](
	[Code] [nvarchar](20) NOT NULL,
	[EffectiveDate] [datetime] NOT NULL,
	[Name] [nvarchar](50) NULL,
	[EditStatus] [int] NULL,
	[NumberofOccurrences] [int] NULL,
	[BaseAmountDetailsExist] [tinyint] NULL,
 CONSTRAINT [PK_BaseAmount_1] PRIMARY KEY CLUSTERED 
(
	[Code] ASC,
	[EffectiveDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[BaseAmountDetail]    Script Date: 4/21/2020 6:05:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BaseAmountDetail](
	[BaseAmountCode] [nvarchar](20) NOT NULL,
	[EffectiveDate] [datetime] NOT NULL,
	[LineNumber] [int] NOT NULL,
	[ControlCodeFilter] [nvarchar](80) NULL,
	[TypeFilter] [nvarchar](30) NULL,
	[NameFilter] [nvarchar](80) NULL,
	[RepAuthTypeFilter] [nvarchar](30) NULL,
	[WorkRepAuthCodeFilter] [nvarchar](80) NULL,
	[GLPostTypeFilter] [nvarchar](30) NULL,
	[WorkTypeFilter] [nvarchar](80) NULL,
	[CountyFilter] [nvarchar](30) NULL,
	[LocalityFilter] [nvarchar](80) NULL,
 CONSTRAINT [PK_BaseAmountDetail] PRIMARY KEY CLUSTERED 
(
	[BaseAmountCode] ASC,
	[EffectiveDate] ASC,
	[LineNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Bracket]    Script Date: 4/21/2020 6:05:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Bracket](
	[Code] [nvarchar](20) NOT NULL,
	[BracketTypeCode] [nvarchar](20) NOT NULL,
	[EffectiveDate] [datetime] NOT NULL,
	[Description] [nvarchar](80) NULL,
	[EditStatus] [int] NULL,
	[BracketDetailsExist] [tinyint] NULL,
 CONSTRAINT [PK_Bracket] PRIMARY KEY CLUSTERED 
(
	[Code] ASC,
	[BracketTypeCode] ASC,
	[EffectiveDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[BracketDetail]    Script Date: 4/21/2020 6:05:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BracketDetail](
	[BracketCode] [nvarchar](20) NOT NULL,
	[BracketTypeCode] [nvarchar](20) NOT NULL,
	[BracketEffectiveDate] [datetime] NOT NULL,
	[LineNumber] [int] NOT NULL,
	[FilingStatusCode] [nvarchar](10) NULL,
	[PayFrequency] [int] NULL,
	[AmountOver] [decimal](38, 20) NULL,
	[Limit] [decimal](38, 20) NULL,
	[TaxPercent] [decimal](38, 20) NULL,
	[PercentValue] [decimal](38, 20) NULL,
	[Quantity] [decimal](38, 20) NULL,
	[TaxAmount] [decimal](38, 20) NULL,
	[Amount] [decimal](38, 20) NULL,
	[RateMultiple] [decimal](38, 20) NULL,
	[MaxWH] [decimal](38, 20) NULL,
	[MinAmount] [decimal](38, 20) NULL,
	[MaxAmount] [decimal](38, 20) NULL,
	[PerAllowance] [tinyint] NULL,
	[FromAllowance] [int] NULL,
	[MatchPercent] [decimal](38, 20) NULL,
	[MaxPercent] [decimal](38, 20) NULL,
	[ReportingAuthorityType] [int] NULL,
	[ReportingAuthorityCode] [nvarchar](10) NULL,
	[ActivationCode] [nvarchar](10) NOT NULL,
 CONSTRAINT [PK_BracketDetail] PRIMARY KEY CLUSTERED 
(
	[BracketCode] ASC,
	[BracketTypeCode] ASC,
	[BracketEffectiveDate] ASC,
	[LineNumber] ASC,
	[ActivationCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[BracketType]    Script Date: 4/21/2020 6:05:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BracketType](
	[Code] [nvarchar](20) NOT NULL,
	[Description] [nvarchar](50) NULL,
	[BracketPageID] [int] NULL,
	[UsesMultipleBracketLines] [tinyint] NULL,
	[UsesFilingStatus] [tinyint] NULL,
	[UsesPayFrequency] [tinyint] NULL,
	[UsesAmountOver] [tinyint] NULL,
	[UsesLimit] [tinyint] NULL,
	[UsesTaxPercent] [tinyint] NULL,
	[UsesPercent] [tinyint] NULL,
	[UsesQuantity] [tinyint] NULL,
	[UsesTaxAmount] [tinyint] NULL,
	[UsesAmount] [tinyint] NULL,
	[UsesRateMultiple] [tinyint] NULL,
	[UsesMaxWH] [tinyint] NULL,
	[UsesMinAmount] [tinyint] NULL,
	[UsesMaxAmount] [tinyint] NULL,
	[UsesPerAllowance] [tinyint] NULL,
	[UsesFromAllowance] [tinyint] NULL,
	[UsesMatchPercent] [tinyint] NULL,
	[UsesMaxPercent] [tinyint] NULL,
	[UsesActivationCode] [tinyint] NULL,
 CONSTRAINT [PK_BracketType] PRIMARY KEY CLUSTERED 
(
	[Code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CalculationOrder]    Script Date: 4/21/2020 6:05:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CalculationOrder](
	[Code] [nvarchar](10) NOT NULL,
	[Description] [nvarchar](50) NULL,
	[MasterOrder] [tinyint] NULL,
	[PayrollControls] [int] NULL,
	[SynchRequired] [tinyint] NULL,
 CONSTRAINT [PK_CalculationOrder] PRIMARY KEY CLUSTERED 
(
	[Code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CalculationOrderControl]    Script Date: 4/21/2020 6:05:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CalculationOrderControl](
	[CalculationOrderCode] [nvarchar](10) NOT NULL,
	[OrderNo] [int] NOT NULL,
	[PayrollControlCode] [nvarchar](20) NOT NULL,
	[AnotherOrderUses] [tinyint] NULL,
	[MasterOrder] [tinyint] NULL,
	[CalculationOrders] [int] NULL,
 CONSTRAINT [PK_CalculationOrderControl] PRIMARY KEY CLUSTERED 
(
	[CalculationOrderCode] ASC,
	[OrderNo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Customer]    Script Date: 4/21/2020 6:05:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Customer](
	[No] [nvarchar](20) NOT NULL,
	[Name] [nvarchar](50) NULL,
	[SearchName] [nvarchar](50) NULL,
	[Name2] [nvarchar](50) NULL,
	[Address] [nvarchar](50) NULL,
	[Address2] [nvarchar](50) NULL,
	[City] [nvarchar](30) NULL,
	[Contact] [nvarchar](50) NULL,
	[PhoneNo] [nvarchar](30) NULL,
	[TelexNo] [nvarchar](20) NULL,
	[DocumentSendingProfile] [nvarchar](20) NULL,
	[OurAccountNo] [nvarchar](20) NULL,
	[TerritoryCode] [nvarchar](10) NULL,
	[GlobalDimension1Code] [nvarchar](20) NULL,
	[GlobalDimension2Code] [nvarchar](20) NULL,
	[ChainName] [nvarchar](10) NULL,
	[BudgetedAmount] [decimal](38, 20) NULL,
	[CreditLimitLCY] [decimal](38, 20) NULL,
	[CustomerPostingGroup] [nvarchar](10) NULL,
	[CurrencyCode] [nvarchar](10) NULL,
	[CustomerPriceGroup] [nvarchar](10) NULL,
	[LanguageCode] [nvarchar](10) NULL,
	[StatisticsGroup] [int] NULL,
	[PaymentTermsCode] [nvarchar](10) NULL,
	[FinChargeTermsCode] [nvarchar](10) NULL,
	[SalespersonCode] [nvarchar](10) NULL,
	[ShipmentMethodCode] [nvarchar](10) NULL,
	[ShippingAgentCode] [nvarchar](10) NULL,
	[PlaceofExport] [nvarchar](20) NULL,
	[InvoiceDiscCode] [nvarchar](20) NULL,
	[CustomerDiscGroup] [nvarchar](20) NULL,
	[CountryRegionCode] [nvarchar](10) NULL,
	[CollectionMethod] [nvarchar](20) NULL,
	[Amount] [decimal](38, 20) NULL,
	[Comment] [tinyint] NULL,
	[Blocked] [int] NULL,
	[InvoiceCopies] [int] NULL,
	[LastStatementNo] [int] NULL,
	[PrintStatements] [tinyint] NULL,
	[BilltoCustomerNo] [nvarchar](20) NULL,
	[Priority] [int] NULL,
	[PaymentMethodCode] [nvarchar](10) NULL,
	[LastDateModified] [datetime] NULL,
	[DateFilter] [datetime] NULL,
	[GlobalDimension1Filter] [nvarchar](20) NULL,
	[GlobalDimension2Filter] [nvarchar](20) NULL,
	[Balance] [decimal](38, 20) NULL,
	[BalanceLCY] [decimal](38, 20) NULL,
	[NetChange] [decimal](38, 20) NULL,
	[NetChangeLCY] [decimal](38, 20) NULL,
	[SalesLCY] [decimal](38, 20) NULL,
	[ProfitLCY] [decimal](38, 20) NULL,
	[InvDiscountsLCY] [decimal](38, 20) NULL,
	[PmtDiscountsLCY] [decimal](38, 20) NULL,
	[BalanceDue] [decimal](38, 20) NULL,
	[BalanceDueLCY] [decimal](38, 20) NULL,
	[Payments] [decimal](38, 20) NULL,
	[InvoiceAmounts] [decimal](38, 20) NULL,
	[CrMemoAmounts] [decimal](38, 20) NULL,
	[FinanceChargeMemoAmounts] [decimal](38, 20) NULL,
	[PaymentsLCY] [decimal](38, 20) NULL,
	[InvAmountsLCY] [decimal](38, 20) NULL,
	[CrMemoAmountsLCY] [decimal](38, 20) NULL,
	[FinChargeMemoAmountsLCY] [decimal](38, 20) NULL,
	[OutstandingOrders] [decimal](38, 20) NULL,
	[ShippedNotInvoiced] [decimal](38, 20) NULL,
	[ApplicationMethod] [int] NULL,
	[PricesIncludingVAT] [tinyint] NULL,
	[LocationCode] [nvarchar](10) NULL,
	[FaxNo] [nvarchar](30) NULL,
	[TelexAnswerBack] [nvarchar](20) NULL,
	[VATRegistrationNo] [nvarchar](20) NULL,
	[CombineShipments] [tinyint] NULL,
	[GenBusPostingGroup] [nvarchar](10) NULL,
	[Picture] [image] NULL,
	[GLN] [nvarchar](13) NULL,
	[PostCode] [nvarchar](20) NULL,
	[County] [nvarchar](30) NULL,
	[DebitAmount] [decimal](38, 20) NULL,
	[CreditAmount] [decimal](38, 20) NULL,
	[DebitAmountLCY] [decimal](38, 20) NULL,
	[CreditAmountLCY] [decimal](38, 20) NULL,
	[EMail] [nvarchar](80) NULL,
	[HomePage] [nvarchar](80) NULL,
	[ReminderTermsCode] [nvarchar](10) NULL,
	[ReminderAmounts] [decimal](38, 20) NULL,
	[ReminderAmountsLCY] [decimal](38, 20) NULL,
	[NoSeries] [nvarchar](10) NULL,
	[TaxAreaCode] [nvarchar](20) NULL,
	[TaxLiable] [tinyint] NULL,
	[VATBusPostingGroup] [nvarchar](10) NULL,
	[CurrencyFilter] [nvarchar](10) NULL,
	[OutstandingOrdersLCY] [decimal](38, 20) NULL,
	[ShippedNotInvoicedLCY] [decimal](38, 20) NULL,
	[Reserve] [int] NULL,
	[BlockPaymentTolerance] [tinyint] NULL,
	[PmtDiscToleranceLCY] [decimal](38, 20) NULL,
	[PmtToleranceLCY] [decimal](38, 20) NULL,
	[ICPartnerCode] [nvarchar](20) NULL,
	[Refunds] [decimal](38, 20) NULL,
	[RefundsLCY] [decimal](38, 20) NULL,
	[OtherAmounts] [decimal](38, 20) NULL,
	[OtherAmountsLCY] [decimal](38, 20) NULL,
	[PrepaymentPercentValue] [decimal](38, 20) NULL,
	[OutstandingInvoicesLCY] [decimal](38, 20) NULL,
	[OutstandingInvoices] [decimal](38, 20) NULL,
	[BilltoNoOfArchivedDoc] [int] NULL,
	[SelltoNoOfArchivedDoc] [int] NULL,
	[PartnerType] [int] NULL,
	[PreferredBankAccount] [nvarchar](10) NULL,
	[CashFlowPaymentTermsCode] [nvarchar](10) NULL,
	[PrimaryContactNo] [nvarchar](20) NULL,
	[ResponsibilityCenter] [nvarchar](10) NULL,
	[ShippingAdvice] [int] NULL,
	[ShippingTime] [varchar](32) NULL,
	[ShippingAgentServiceCode] [nvarchar](10) NULL,
	[ServiceZoneCode] [nvarchar](10) NULL,
	[ContractGainLossAmount] [decimal](38, 20) NULL,
	[ShiptoFilter] [nvarchar](10) NULL,
	[OutstandingServOrdersLCY] [decimal](38, 20) NULL,
	[ServShippedNotInvoicedLCY] [decimal](38, 20) NULL,
	[OutstandingServInvoicesLCY] [decimal](38, 20) NULL,
	[AllowLineDisc] [tinyint] NULL,
	[NoofQuotes] [int] NULL,
	[NoofBlanketOrders] [int] NULL,
	[NoofOrders] [int] NULL,
	[NoofInvoices] [int] NULL,
	[NoofReturnOrders] [int] NULL,
	[NoofCreditMemos] [int] NULL,
	[NoofPstdShipments] [int] NULL,
	[NoofPstdInvoices] [int] NULL,
	[NoofPstdReturnReceipts] [int] NULL,
	[NoofPstdCreditMemos] [int] NULL,
	[NoofShiptoAddresses] [int] NULL,
	[BillToNoofQuotes] [int] NULL,
	[BillToNoofBlanketOrders] [int] NULL,
	[BillToNoofOrders] [int] NULL,
	[BillToNoofInvoices] [int] NULL,
	[BillToNoofReturnOrders] [int] NULL,
	[BillToNoofCreditMemos] [int] NULL,
	[BillToNoofPstdShipments] [int] NULL,
	[BillToNoofPstdInvoices] [int] NULL,
	[BillToNoofPstdReturnR] [int] NULL,
	[BillToNoofPstdCrMemos] [int] NULL,
	[BaseCalendarCode] [nvarchar](10) NULL,
	[CopySelltoAddrtoQteFrom] [int] NULL,
	[UPSZone] [nvarchar](2) NULL,
	[TaxExemptionNo] [nvarchar](30) NULL,
	[BankCommunication] [int] NULL,
	[CheckDateFormat] [int] NULL,
	[CheckDateSeparator] [int] NULL,
	[BalanceonDate] [decimal](38, 20) NULL,
	[BalanceonDateLCY] [decimal](38, 20) NULL,
	[RFCNo] [nvarchar](13) NULL,
	[CURPNo] [nvarchar](18) NULL,
	[StateInscription] [nvarchar](30) NULL,
	[TaxIdentificationType] [int] NULL,
	[new_nit] [nvarchar](17) NULL,
	[Store] [nvarchar](10) NULL,
	[Cooperativa] [tinyint] NULL,
	[MainAccount] [nvarchar](20) NULL,
	[TemporadaPACASA] [nvarchar](10) NULL,
	[ABN] [nvarchar](11) NULL,
	[Registered] [tinyint] NULL,
	[ABNDivisionPartNo] [nvarchar](3) NULL,
	[WHTBusinessPostingGroup] [nvarchar](10) NULL,
	[WHTPayableAmountLCY] [decimal](38, 20) NULL,
	[TaxDocumentType] [int] NULL,
	[PostDatedChecksLCY] [decimal](38, 20) NULL,
	[ClientePOS] [tinyint] NULL,
	[Neteo] [tinyint] NULL,
	[ClienteMayoreoTienda] [tinyint] NULL,
	[Rubro] [nvarchar](50) NULL,
	[ReferenciaExoneracion] [nvarchar](50) NULL,
	[NoOrdenCompExenta] [nvarchar](20) NULL,
	[NoConstanciaRegExonerado] [nvarchar](20) NULL,
	[NoRegistroSecretaria] [nvarchar](20) NULL,
	[TerminoPag] [varchar](32) NULL,
	[CategoriaContado] [nvarchar](10) NULL,
	[CategoriaCredito] [nvarchar](10) NULL,
	[SaldoTemporadaNormal] [decimal](38, 20) NULL,
	[Longitude] [nvarchar](20) NULL,
	[Latitude] [nvarchar](20) NULL,
	[CuotasEmpl] [int] NULL,
	[DateCreated] [datetime] NULL,
	[CreatedbyUser] [nvarchar](50) NULL,
	[CustomerID] [nvarchar](10) NULL,
	[ReasonCode] [nvarchar](10) NULL,
	[RestrictionFunctionality] [int] NULL,
	[PrintDocumentInvoice] [tinyint] NULL,
	[TransactionLimit] [decimal](38, 20) NULL,
	[DaytimePhoneNo] [nvarchar](30) NULL,
	[MobilePhoneNo] [nvarchar](30) NULL,
	[HouseNo] [nvarchar](30) NULL,
	[RetailCustomerGroup] [nvarchar](10) NULL,
	[DefaultWeight] [decimal](38, 20) NULL,
	[OtherTenderinFinalizing] [tinyint] NULL,
	[AmtChargedOnPOS] [decimal](38, 20) NULL,
	[PostasShipment] [tinyint] NULL,
	[AmtChargedPosted] [decimal](38, 20) NULL,
 CONSTRAINT [PK_Customer] PRIMARY KEY CLUSTERED 
(
	[No] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DetalledePrestamos]    Script Date: 4/21/2020 6:05:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DetalledePrestamos](
	[Correlativo] [int] NOT NULL,
	[NoEmpleado] [nvarchar](20) NULL,
	[Referencia] [nvarchar](20) NULL,
	[EntidadFinanciera] [nvarchar](10) NULL,
	[FecAplicacion] [datetime] NULL,
	[Monto] [decimal](38, 20) NULL,
	[RefDescuento] [nvarchar](50) NULL,
	[PayCycleCode] [nvarchar](10) NULL,
	[PayCyclePeriod] [int] NULL,
	[PayCycleTerm] [nvarchar](10) NULL,
	[DimensionSetID] [int] NULL,
	[DocumentNo] [nvarchar](20) NULL,
 CONSTRAINT [PK_DetalledePrestamos] PRIMARY KEY CLUSTERED 
(
	[Correlativo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DirectDepositBuffer]    Script Date: 4/21/2020 6:05:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DirectDepositBuffer](
	[FilePath] [nvarchar](250) NOT NULL,
	[FileName] [nvarchar](30) NULL,
	[FileDate] [datetime] NULL,
	[FileTime] [datetime] NULL,
	[TotalLineCount] [int] NULL,
	[TotalHeaderCount] [int] NULL,
	[TotalDetailCount] [int] NULL,
	[TotalFooterCount] [int] NULL,
	[TotalCreditCount] [int] NULL,
	[TotalDebitCount] [int] NULL,
	[TotalBatchCount] [int] NULL,
	[BatchDetailCount] [int] NULL,
	[TotalCreditAmt] [decimal](38, 20) NULL,
	[TotalDebitAmt] [decimal](38, 20) NULL,
	[HashAmount1] [decimal](38, 20) NULL,
	[HashAmount2] [decimal](38, 20) NULL,
	[BatchCreditAmt] [decimal](38, 20) NULL,
	[BatchDebitAmt] [decimal](38, 20) NULL,
	[BatchHashAmount1] [decimal](38, 20) NULL,
	[BatchHashAmount2] [decimal](38, 20) NULL,
	[PaymentAmount] [decimal](38, 20) NULL,
	[LanguageCode] [nvarchar](30) NULL,
	[TransactionCode] [nvarchar](30) NULL,
	[RecordLength] [int] NULL,
	[BlockingFactor] [decimal](38, 20) NULL,
	[BlockCount] [decimal](38, 20) NULL,
	[NavisionWorkDate] [datetime] NULL,
	[PaymentsForThisEmployee] [int] NULL,
 CONSTRAINT [PK_DirectDepositBuffer] PRIMARY KEY CLUSTERED 
(
	[FilePath] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DirectDepositLayout]    Script Date: 4/21/2020 6:05:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DirectDepositLayout](
	[Code] [nvarchar](20) NOT NULL,
	[Description] [nvarchar](250) NULL,
	[LayoutType] [int] NULL,
	[RecordLength] [int] NULL,
	[BlockingFactor] [int] NULL,
	[BlockFiller] [nvarchar](1) NULL,
 CONSTRAINT [PK_DirectDepositLayout] PRIMARY KEY CLUSTERED 
(
	[Code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DirectDepositLines]    Script Date: 4/21/2020 6:05:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DirectDepositLines](
	[DirectDepositLayoutCode] [nvarchar](20) NOT NULL,
	[SectionType] [int] NOT NULL,
	[SectionLineNumber] [int] NOT NULL,
	[LineNumber] [int] NOT NULL,
	[Description] [nvarchar](50) NULL,
	[StartPosition] [int] NULL,
	[LengthtoExport] [int] NULL,
	[TypetoExport] [int] NULL,
	[LineType] [int] NULL,
	[LiteralValue] [nvarchar](30) NULL,
	[TableNo] [int] NULL,
	[TableName] [nvarchar](30) NULL,
	[FieldNo] [int] NULL,
	[FieldName] [nvarchar](30) NULL,
	[FieldTypeandLength] [nvarchar](30) NULL,
	[Justification] [int] NULL,
	[FillCharacter] [nvarchar](1) NULL,
	[ConverttoUpperCase] [tinyint] NULL,
	[RemoveSpaces] [tinyint] NULL,
	[RemoveSpecialCharacters] [tinyint] NULL,
	[IncludeDecimal] [tinyint] NULL,
	[DateFormat] [int] NULL,
	[TimeFormat] [int] NULL,
	[FieldStartPosition] [int] NULL,
	[FieldLength] [int] NULL,
	[RequiredField] [tinyint] NULL,
	[CheckDigit] [tinyint] NULL,
	[UpdateTotalFileDebitAmt] [tinyint] NULL,
	[UpdateTotalFileCreditAmt] [tinyint] NULL,
	[UpdateHashAmount1] [tinyint] NULL,
	[UpdateHashAmount2] [tinyint] NULL,
	[UpdateBatchDebitAmt] [tinyint] NULL,
	[UpdateBatchCreditAmt] [tinyint] NULL,
	[UpdateBatchHashAmt1] [tinyint] NULL,
	[UpdateBatchHashAmt2] [tinyint] NULL,
 CONSTRAINT [PK_DirectDepositLines] PRIMARY KEY CLUSTERED 
(
	[DirectDepositLayoutCode] ASC,
	[SectionType] ASC,
	[SectionLineNumber] ASC,
	[LineNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DirectDepositSections]    Script Date: 4/21/2020 6:05:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DirectDepositSections](
	[DirectDepositLayoutCode] [nvarchar](20) NOT NULL,
	[SectionType] [int] NOT NULL,
	[LineNumber] [int] NOT NULL,
	[SequenceNumber] [int] NULL,
	[Description] [nvarchar](50) NULL,
	[IncTotalLineCount] [tinyint] NULL,
	[IncTotalHeaderCount] [tinyint] NULL,
	[IncTotalDetailCount] [tinyint] NULL,
	[IncTotalFooterCount] [tinyint] NULL,
	[IncTotalCreditCount] [tinyint] NULL,
	[IncTotalDebitCount] [tinyint] NULL,
	[IncTotalBatchCount] [tinyint] NULL,
	[IncBatchDetailCount] [tinyint] NULL,
	[ResetBatchTotals] [tinyint] NULL,
 CONSTRAINT [PK_DirectDepositSections] PRIMARY KEY CLUSTERED 
(
	[DirectDepositLayoutCode] ASC,
	[SectionType] ASC,
	[LineNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Employee]    Script Date: 4/21/2020 6:05:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Employee](
	[No] [nvarchar](20) NOT NULL,
	[FirstName] [nvarchar](30) NULL,
	[MiddleName] [nvarchar](30) NULL,
	[LastName] [nvarchar](30) NULL,
	[Initials] [nvarchar](30) NULL,
	[JobTitle] [nvarchar](40) NULL,
	[SearchName] [nvarchar](30) NULL,
	[Address] [nvarchar](50) NULL,
	[Address2] [nvarchar](50) NULL,
	[City] [nvarchar](30) NULL,
	[PostCode] [nvarchar](20) NULL,
	[County] [nvarchar](30) NULL,
	[PhoneNo] [nvarchar](30) NULL,
	[MobilePhoneNo] [nvarchar](30) NULL,
	[EMail] [nvarchar](80) NULL,
	[AltAddressCode] [nvarchar](10) NULL,
	[AltAddressStartDate] [datetime] NULL,
	[AltAddressEndDate] [datetime] NULL,
	[Picture] [image] NULL,
	[BirthDate] [datetime] NULL,
	[SocialSecurityNo] [nvarchar](30) NULL,
	[UnionCode] [nvarchar](10) NULL,
	[UnionMembershipNo] [nvarchar](30) NULL,
	[Gender] [int] NULL,
	[CountryRegionCode] [nvarchar](10) NULL,
	[ManagerNo] [nvarchar](20) NULL,
	[EmplymtContractCode] [nvarchar](10) NULL,
	[StatisticsGroupCode] [nvarchar](10) NULL,
	[EmploymentDate] [datetime] NULL,
	[Status] [int] NULL,
	[InactiveDate] [datetime] NULL,
	[CauseofInactivityCode] [nvarchar](10) NULL,
	[TerminationDate] [datetime] NULL,
	[GroundsforTermCode] [nvarchar](10) NULL,
	[GlobalDimension1Code] [nvarchar](20) NULL,
	[GlobalDimension2Code] [nvarchar](20) NULL,
	[ResourceNo] [nvarchar](20) NULL,
	[Comment] [tinyint] NULL,
	[LastDateModified] [datetime] NULL,
	[DateFilter] [datetime] NULL,
	[GlobalDimension1Filter] [nvarchar](20) NULL,
	[GlobalDimension2Filter] [nvarchar](20) NULL,
	[CauseofAbsenceFilter] [nvarchar](10) NULL,
	[TotalAbsenceBase] [decimal](38, 20) NULL,
	[Extension] [nvarchar](30) NULL,
	[EmployeeNoFilter] [nvarchar](20) NULL,
	[Pager] [nvarchar](30) NULL,
	[FaxNo] [nvarchar](30) NULL,
	[CompanyEMail] [nvarchar](80) NULL,
	[Title] [nvarchar](30) NULL,
	[SalespersPurchCode] [nvarchar](10) NULL,
	[NoSeries] [nvarchar](10) NULL,
	[CostCenterCode] [nvarchar](20) NULL,
	[CostObjectCode] [nvarchar](20) NULL,
	[NoRAP] [nvarchar](15) NULL,
	[NoPasaporte] [nvarchar](15) NULL,
	[NoLicenciadeConducir] [nvarchar](15) NULL,
	[TipodeSangre] [int] NULL,
	[NoRTN] [nvarchar](20) NULL,
	[Nacionalidad] [nvarchar](50) NULL,
	[ProfesiónOficio] [nvarchar](50) NULL,
	[FecUltima13] [datetime] NULL,
	[FecUltima14] [datetime] NULL,
	[Gradoacademico] [nvarchar](70) NULL,
	[DevengahorasExtras] [int] NULL,
	[TipodeManodeobra] [int] NULL,
	[Birthmonth] [int] NULL,
	[NoIHSS] [nvarchar](30) NULL,
	[Comision] [tinyint] NULL,
	[AMOUNT2] [decimal](38, 20) NULL,
	[ClassCode] [nvarchar](10) NULL,
	[EmployerNo] [nvarchar](20) NULL,
	[MiddleInitial] [nvarchar](5) NULL,
	[Suffix] [nvarchar](5) NULL,
	[HomePhoneNo] [nvarchar](30) NULL,
	[Race] [nvarchar](10) NULL,
	[Blocked] [tinyint] NULL,
	[DefaultWorkTypeCode] [nvarchar](10) NULL,
	[Deceased] [tinyint] NULL,
	[SSNIsVerified] [tinyint] NULL,
	[PayCycleCode] [nvarchar](10) NULL,
	[StatutoryEmployee] [tinyint] NULL,
	[RetirementPlan] [tinyint] NULL,
	[LegalRepresentative] [tinyint] NULL,
	[HouseholdEmployee] [tinyint] NULL,
	[DeferredCompensation] [tinyint] NULL,
	[ThirdPartySickPay] [tinyint] NULL,
	[DefaultRepAuthCode] [nvarchar](20) NULL,
	[SocialInsuranceNo] [nvarchar](30) NULL,
	[CPPExempt] [tinyint] NULL,
	[EIExempt] [tinyint] NULL,
	[RPPorDPSP] [nvarchar](10) NULL,
	[OverrideInsurableHours] [decimal](38, 20) NULL,
	[Salaried] [tinyint] NULL,
	[CtrlFilter] [nvarchar](20) NULL,
	[CtrlTypeFilter] [int] NULL,
	[CtrlNameFilter] [nvarchar](30) NULL,
	[CountyFilter] [nvarchar](3) NULL,
	[LocalityFilter] [nvarchar](30) NULL,
	[GLPostTypeFilter] [int] NULL,
	[LedgEntryNoFilter] [int] NULL,
	[WorkTypeFilter] [nvarchar](10) NULL,
	[DateWorkedFilter] [datetime] NULL,
	[PayCycleCodeFilter] [nvarchar](10) NULL,
	[PayCycleTermFilter] [nvarchar](10) NULL,
	[PayCyclePeriodFilter] [int] NULL,
	[PPStartDateFilter] [datetime] NULL,
	[PPEndDateFilter] [datetime] NULL,
	[PayDateFilter] [datetime] NULL,
	[EmployerNoFilter] [nvarchar](20) NULL,
	[RATypeFilter] [int] NULL,
	[WorkRACodeFilter] [nvarchar](20) NULL,
	[Amount] [decimal](38, 20) NULL,
	[TaxableAmount] [decimal](38, 20) NULL,
	[AmountonPayCheck] [decimal](38, 20) NULL,
	[PayDistributions] [tinyint] NULL,
	[Splits] [tinyint] NULL,
	[SPPSpousalContribSIN] [nvarchar](30) NULL,
	[IsApplicant] [tinyint] NULL,
	[LanguageCode] [nvarchar](10) NULL,
	[HRRepNo] [nvarchar](10) NULL,
	[HRRepContact] [nvarchar](10) NULL,
	[SalutationCode] [nvarchar](10) NULL,
	[PositionCode] [nvarchar](20) NULL,
	[FTEquivalent] [decimal](38, 20) NULL,
	[Score] [decimal](38, 20) NULL,
	[EmployeeType] [nvarchar](10) NULL,
	[MaritalStatus] [int] NULL,
	[EEOJobClass] [int] NULL,
	[PPIPExempt] [tinyint] NULL,
	[OfficeLocation] [nvarchar](10) NULL,
	[NoofPositions] [int] NULL,
	[ActualFTE] [decimal](38, 20) NULL,
	[ActualAnnualSalary] [decimal](38, 20) NULL,
	[VisioShapeType] [int] NULL,
	[DefaultSeasonalCode] [nvarchar](10) NULL,
	[TwitterUsername] [nvarchar](20) NULL,
	[DUI] [nvarchar](25) NULL,
	[NomDUI] [nvarchar](60) NULL,
	[TILetras] [nvarchar](60) NULL,
	[LugExpTI] [nvarchar](60) NULL,
	[ProfDUI] [nvarchar](60) NULL,
	[FExpTILetras] [nvarchar](50) NULL,
	[Edad] [int] NULL,
	[SelecPlanilla] [tinyint] NULL,
	[Area] [nvarchar](10) NULL,
	[Dpto] [nvarchar](10) NULL,
	[JefeArea] [nvarchar](10) NULL,
	[JefeDpto] [nvarchar](10) NULL,
	[DepGasViat] [nvarchar](10) NULL,
	[AFps] [nvarchar](10) NULL,
	[DistGast] [int] NULL,
	[FechContAjust] [datetime] NULL,
	[SecondLastName] [nvarchar](30) NULL,
	[MarriedLastname] [nvarchar](30) NULL,
	[NUP] [nvarchar](15) NULL,
	[SocialSecurityname] [nvarchar](60) NULL,
	[Workingdays] [nvarchar](20) NULL,
	[NIT] [nvarchar](17) NULL,
	[NITName] [nvarchar](60) NULL,
	[NUPName] [nvarchar](60) NULL,
	[IncapAcum] [int] NULL,
	[HrsExtras] [tinyint] NULL,
 CONSTRAINT [PK_Employee] PRIMARY KEY CLUSTERED 
(
	[No] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[EmployeeClass]    Script Date: 4/21/2020 6:05:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EmployeeClass](
	[Code] [nvarchar](10) NOT NULL,
	[Description] [nvarchar](50) NULL,
	[Employees] [int] NULL,
 CONSTRAINT [PK_EmployeeClass] PRIMARY KEY CLUSTERED 
(
	[Code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[EmployeePayDistribution]    Script Date: 4/21/2020 6:05:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EmployeePayDistribution](
	[EmployeeNo] [nvarchar](20) NOT NULL,
	[LineNumber] [int] NOT NULL,
	[Name] [nvarchar](30) NULL,
	[Name2] [nvarchar](30) NULL,
	[Address] [nvarchar](30) NULL,
	[Address2] [nvarchar](30) NULL,
	[City] [nvarchar](30) NULL,
	[PostCode] [nvarchar](20) NULL,
	[Contact] [nvarchar](30) NULL,
	[PhoneNo] [nvarchar](30) NULL,
	[TelexNo] [nvarchar](20) NULL,
	[BankBranchNo] [nvarchar](20) NULL,
	[BankAccountNo] [nvarchar](30) NULL,
	[TransitNo] [nvarchar](20) NULL,
	[CurrencyCode] [nvarchar](10) NULL,
	[CountryRegionCode] [nvarchar](10) NULL,
	[County] [nvarchar](30) NULL,
	[FaxNo] [nvarchar](30) NULL,
	[TelexAnswerBack] [nvarchar](20) NULL,
	[LanguageCode] [nvarchar](10) NULL,
	[EMail] [nvarchar](80) NULL,
	[HomePage] [nvarchar](80) NULL,
	[Type] [int] NULL,
	[FixedAmount] [decimal](38, 20) NULL,
	[PercentofNetPay] [decimal](38, 20) NULL,
	[Remainder] [tinyint] NULL,
	[Prenote] [tinyint] NULL,
 CONSTRAINT [PK_EmployeePayDistribution] PRIMARY KEY CLUSTERED 
(
	[EmployeeNo] ASC,
	[LineNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[EmployeePayrollControl]    Script Date: 4/21/2020 6:05:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EmployeePayrollControl](
	[EmployeeNo] [nvarchar](20) NOT NULL,
	[PayControlCode] [nvarchar](20) NOT NULL,
	[MonthlySchedule] [int] NULL,
	[Active] [tinyint] NULL,
	[PayrollPostingGroup] [nvarchar](10) NULL,
	[OrderNo] [int] NULL,
	[Amount] [decimal](38, 20) NULL,
	[TaxableAmount] [decimal](38, 20) NULL,
	[DateFilter] [datetime] NULL,
	[GlobalDimension1Filter] [nvarchar](20) NULL,
	[GlobalDimension2Filter] [nvarchar](20) NULL,
	[CountyFilter] [nvarchar](3) NULL,
	[LocalityFilter] [nvarchar](30) NULL,
	[GLPostTypeFilter] [int] NULL,
	[PayrollLedgEntryNoFilter] [int] NULL,
	[WorkTypeFilter] [nvarchar](10) NULL,
	[RepAuthTypeFilter] [int] NULL,
	[RepAuthCodeFilter] [nvarchar](10) NULL,
	[PayControlType] [int] NULL,
	[Calculate] [tinyint] NULL,
	[Description] [nvarchar](50) NULL,
 CONSTRAINT [PK_EmployeePayrollControl] PRIMARY KEY CLUSTERED 
(
	[EmployeeNo] ASC,
	[PayControlCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[EmployeeRate]    Script Date: 4/21/2020 6:05:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EmployeeRate](
	[EmployeeNo] [nvarchar](20) NOT NULL,
	[PayrollRateCode] [nvarchar](10) NOT NULL,
	[EffectiveDate] [datetime] NOT NULL,
	[Amount] [decimal](38, 20) NULL,
	[UnitofMeasureCode] [nvarchar](10) NULL,
	[ActivationCode] [nvarchar](10) NOT NULL,
	[Employer] [nvarchar](20) NOT NULL,
	[BorrarPosteo] [tinyint] NULL,
	[Referencia] [nvarchar](20) NULL,
	[PayControlRel] [nvarchar](10) NULL,
 CONSTRAINT [PK_EmployeeRate] PRIMARY KEY CLUSTERED 
(
	[EmployeeNo] ASC,
	[PayrollRateCode] ASC,
	[EffectiveDate] ASC,
	[ActivationCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[EmployeeType]    Script Date: 4/21/2020 6:05:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EmployeeType](
	[Code] [nvarchar](10) NOT NULL,
	[Description] [nvarchar](50) NULL,
	[TestSystemRequiredEntries] [tinyint] NULL,
	[TestUserRequiredEntries] [tinyint] NULL,
	[SynchRequired] [tinyint] NULL,
 CONSTRAINT [PK_EmployeeType] PRIMARY KEY CLUSTERED 
(
	[Code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Employer]    Script Date: 4/21/2020 6:05:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Employer](
	[No] [nvarchar](20) NOT NULL,
	[Name] [nvarchar](50) NULL,
	[Address] [nvarchar](30) NULL,
	[Address2] [nvarchar](30) NULL,
	[City] [nvarchar](30) NULL,
	[County] [nvarchar](30) NULL,
	[CountryRegionCode] [nvarchar](10) NULL,
	[PostCode] [nvarchar](20) NULL,
	[PhoneNo] [nvarchar](30) NULL,
	[FaxNo] [nvarchar](30) NULL,
	[NetPayPostingGroup] [nvarchar](10) NOT NULL,
	[HomePage] [nvarchar](80) NULL,
	[Blocked] [tinyint] NULL,
	[W2ContactName] [nvarchar](30) NULL,
	[W2ContactPhoneNo] [nvarchar](30) NULL,
	[W2Email] [nvarchar](30) NULL,
	[W2FaxNo] [nvarchar](30) NULL,
	[T4ContactName] [nvarchar](30) NULL,
	[T4ContactPhoneNo] [nvarchar](30) NULL,
	[T4ContactEmail] [nvarchar](30) NULL,
	[ROEContactName] [nvarchar](30) NULL,
	[ROEContactPhoneNo] [nvarchar](30) NULL,
	[ROESerialNos] [nvarchar](10) NULL,
	[T4EmploymentCode] [nvarchar](10) NULL,
	[T4Proprietor1SIN#] [nvarchar](30) NULL,
	[T4Proprietor2SIN#] [nvarchar](30) NULL,
	[Employees] [int] NULL,
	[Amount] [decimal](38, 20) NULL,
	[TaxableAmount] [decimal](38, 20) NULL,
	[DateFilter] [datetime] NULL,
	[GblDim1Filter] [nvarchar](20) NULL,
	[GblDim2Filter] [nvarchar](20) NULL,
	[EmpFilter] [nvarchar](20) NULL,
	[CtrlFilter] [nvarchar](20) NULL,
	[CtrlTypeFilter] [int] NULL,
	[CtrlNameFilter] [nvarchar](30) NULL,
	[RATypeFilter] [int] NULL,
	[RACodeFilter] [nvarchar](10) NULL,
	[GLPostTypeFilter] [int] NULL,
	[WorkRACodeFilter] [nvarchar](10) NULL,
	[WorkTypeFilter] [nvarchar](10) NULL,
	[LdgrEntFilter] [int] NULL,
	[PPStartDateFilter] [datetime] NULL,
	[PPEndDateFilter] [datetime] NULL,
	[CycleCodeFilter] [nvarchar](10) NULL,
	[CycleTermFilter] [nvarchar](10) NULL,
	[CyclePeriodFilter] [int] NULL,
	[PayDateFilter] [datetime] NULL,
	[CountyFilter] [nvarchar](3) NULL,
	[LocalityFilter] [nvarchar](30) NULL,
	[DateWorkedFilter] [datetime] NULL,
	[RL1NEQ] [nvarchar](10) NULL,
	[LastDateModified] [datetime] NULL,
	[Comment] [tinyint] NULL,
	[HQOfficeLocation] [nvarchar](10) NULL,
	[EEOCompanyNo] [nvarchar](7) NULL,
	[EEOQuestionC2] [tinyint] NULL,
	[EEOQuestionC3] [tinyint] NULL,
	[EEODunBradstreetIDNo] [nvarchar](9) NULL,
	[StatusFilter] [int] NULL,
	[W2KindofEmployer] [int] NULL,
	[3rdPartyDesignee] [tinyint] NULL,
	[DesigneePin] [int] NULL,
	[DesigneeContactName] [nvarchar](30) NULL,
	[DesigneeContactPhoneNo] [nvarchar](30) NULL,
	[ACAContactName] [nvarchar](30) NULL,
	[ACAContactPhoneNo] [nvarchar](30) NULL,
	[ACADesignatedGovEntity] [nvarchar](20) NULL,
	[ACAContactTitle] [nvarchar](30) NULL,
	[ACAContactEmail] [nvarchar](30) NULL,
	[ACAContactFaxNo] [nvarchar](30) NULL,
	[ROEFolder] [nvarchar](30) NULL,
	[CodIncapacidad] [nvarchar](10) NULL,
	[DiaIncap] [int] NULL,
	[SocialSecurityNo] [nvarchar](9) NULL,
	[CorrelIHSSDoc] [int] NULL,
	[CodRapEmpresa] [nvarchar](10) NULL,
	[Codigoempresa] [nvarchar](30) NULL,
	[EPH] [nvarchar](30) NULL,
	[EPHCT] [nvarchar](30) NULL,
	[RTN] [nvarchar](30) NULL,
	[Emailcompany] [nvarchar](30) NULL,
	[NombredeContactoMPAL] [nvarchar](30) NULL,
	[LogoMunicipalidad] [image] NULL,
	[NombreMunicipalidad] [nvarchar](30) NULL,
	[NoPatronalRAP] [nvarchar](30) NULL,
	[TwitterUsername] [nvarchar](20) NULL,
	[CodigoVacacion] [nvarchar](10) NULL,
 CONSTRAINT [PK_Employer] PRIMARY KEY CLUSTERED 
(
	[No] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[EmployerRate]    Script Date: 4/21/2020 6:05:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EmployerRate](
	[EmployerNo] [nvarchar](20) NOT NULL,
	[PayrollRateCode] [nvarchar](10) NOT NULL,
	[EffectiveDate] [datetime] NOT NULL,
	[Amount] [decimal](38, 20) NULL,
	[UnitofMeasureCode] [nvarchar](10) NULL,
	[ActivationCode] [nvarchar](10) NOT NULL,
 CONSTRAINT [PK_EmployerRate] PRIMARY KEY CLUSTERED 
(
	[EmployerNo] ASC,
	[PayrollRateCode] ASC,
	[EffectiveDate] ASC,
	[ActivationCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[EmployerReportingAuthority]    Script Date: 4/21/2020 6:05:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EmployerReportingAuthority](
	[EmployerNo] [nvarchar](20) NOT NULL,
	[ReportingAuthorityCode] [nvarchar](10) NOT NULL,
	[IdentificationNo] [nvarchar](20) NULL,
	[VendorNo] [nvarchar](20) NOT NULL,
	[Amount] [decimal](38, 20) NULL,
	[TaxableAmount] [decimal](38, 20) NULL,
	[DateFilter] [datetime] NULL,
	[GlobalDimension1Filter] [nvarchar](20) NULL,
	[GlobalDimension2Filter] [nvarchar](20) NULL,
	[EmployeeNoFilter] [nvarchar](20) NULL,
	[PayrollControlTypeFilter] [int] NULL,
	[GLPostTypeFilter] [int] NULL,
	[WorkTypeFilter] [nvarchar](10) NULL,
	[WorkRepAuthCodeFilter] [nvarchar](10) NULL,
	[ReportingAuthorityType] [int] NULL,
 CONSTRAINT [PK_EmployerReportingAuthority] PRIMARY KEY CLUSTERED 
(
	[EmployerNo] ASC,
	[ReportingAuthorityCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[FileAttachment]    Script Date: 4/21/2020 6:05:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FileAttachment](
	[TableID] [int] NOT NULL,
	[PrimaryKeyValue1] [nvarchar](20) NOT NULL,
	[PrimaryKeyValue2] [nvarchar](20) NOT NULL,
	[PrimaryKeyValue3] [nvarchar](20) NOT NULL,
	[No] [nvarchar](20) NOT NULL,
	[Description] [nvarchar](250) NULL,
	[Attachment] [image] NULL,
	[NoSeries] [nvarchar](10) NULL,
	[FileName] [nvarchar](100) NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[DateCreated] [datetime] NULL,
	[LastModified] [datetime] NULL,
	[LastModifiedBy] [nvarchar](50) NULL,
	[TableName] [nvarchar](30) NULL,
 CONSTRAINT [PK_FileAttachment] PRIMARY KEY CLUSTERED 
(
	[TableID] ASC,
	[PrimaryKeyValue1] ASC,
	[PrimaryKeyValue2] ASC,
	[PrimaryKeyValue3] ASC,
	[No] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[FileAttachmentSetup]    Script Date: 4/21/2020 6:05:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FileAttachmentSetup](
	[PrimaryKey] [nvarchar](10) NOT NULL,
	[AttachmentNos] [nvarchar](10) NULL,
 CONSTRAINT [PK_FileAttachmentSetup] PRIMARY KEY CLUSTERED 
(
	[PrimaryKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[GlAccount]    Script Date: 4/21/2020 6:05:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[GlAccount](
	[No] [nvarchar](20) NOT NULL,
	[Name] [nvarchar](50) NULL,
	[SearchName] [nvarchar](50) NULL,
	[AccountType] [int] NULL,
	[GlobalDimension1Code] [nvarchar](20) NULL,
	[GlobalDimension2Code] [nvarchar](20) NULL,
	[IncomeBalance] [int] NULL,
	[DebitCredit] [int] NULL,
	[No2] [nvarchar](20) NULL,
	[Comment] [tinyint] NULL,
	[Blocked] [tinyint] NULL,
	[DirectPosting] [tinyint] NULL,
	[ReconciliationAccount] [tinyint] NULL,
	[NewPage] [tinyint] NULL,
	[NoofBlankLines] [int] NULL,
	[Indentation] [int] NULL,
	[LastDateModified] [datetime] NULL,
	[DateFilter] [datetime] NULL,
	[GlobalDimension1Filter] [nvarchar](20) NULL,
	[GlobalDimension2Filter] [nvarchar](20) NULL,
	[BalanceatDate] [decimal](38, 20) NULL,
	[NetChange] [decimal](38, 20) NULL,
	[BudgetedAmount] [decimal](38, 20) NULL,
	[Totaling] [nvarchar](250) NULL,
	[BudgetFilter] [nvarchar](10) NULL,
	[Balance] [decimal](38, 20) NULL,
	[BudgetatDate] [decimal](38, 20) NULL,
	[ConsolTranslationMethod] [int] NULL,
	[ConsolDebitAcc] [nvarchar](20) NULL,
	[ConsolCreditAcc] [nvarchar](20) NULL,
	[BusinessUnitFilter] [nvarchar](10) NULL,
	[GenPostingType] [int] NULL,
	[GenBusPostingGroup] [nvarchar](10) NULL,
	[GenProdPostingGroup] [nvarchar](10) NULL,
	[Picture] [image] NULL,
	[DebitAmount] [decimal](38, 20) NULL,
	[CreditAmount] [decimal](38, 20) NULL,
	[AutomaticExtTexts] [tinyint] NULL,
	[BudgetedDebitAmount] [decimal](38, 20) NULL,
	[BudgetedCreditAmount] [decimal](38, 20) NULL,
	[TaxAreaCode] [nvarchar](20) NULL,
	[TaxLiable] [tinyint] NULL,
	[TaxGroupCode] [nvarchar](10) NULL,
	[VATBusPostingGroup] [nvarchar](10) NULL,
	[VATProdPostingGroup] [nvarchar](10) NULL,
	[AdditionalCurrencyNetChange] [decimal](38, 20) NULL,
	[AddCurrencyBalanceatDate] [decimal](38, 20) NULL,
	[AdditionalCurrencyBalance] [decimal](38, 20) NULL,
	[ExchangeRateAdjustment] [int] NULL,
	[AddCurrencyDebitAmount] [decimal](38, 20) NULL,
	[AddCurrencyCreditAmount] [decimal](38, 20) NULL,
	[DefaultICPartnerGLAccNo] [nvarchar](20) NULL,
	[OmitDefaultDescrinJnl] [tinyint] NULL,
	[CostTypeNo] [nvarchar](20) NULL,
	[DefaultDeferralTemplateCode] [nvarchar](10) NULL,
	[WHTBusinessPostingGroup] [nvarchar](10) NULL,
	[WHTProductPostingGroup] [nvarchar](10) NULL,
	[GLEntryTypeFilter] [int] NULL,
	[ReasonCodeFilter] [nvarchar](10) NULL,
 CONSTRAINT [PK_GlAccount] PRIMARY KEY CLUSTERED 
(
	[No] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[HcmCommentLine]    Script Date: 4/21/2020 6:05:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HcmCommentLine](
	[TableName] [int] NOT NULL,
	[No] [nvarchar](20) NOT NULL,
	[TableLineNumber] [int] NOT NULL,
	[AlternativeAddressCode] [nvarchar](20) NOT NULL,
	[LineNumber] [int] NOT NULL,
	[Date] [datetime] NULL,
	[Code] [nvarchar](10) NULL,
	[Comment] [nvarchar](80) NULL,
 CONSTRAINT [PK_HcmCommentLine] PRIMARY KEY CLUSTERED 
(
	[TableName] ASC,
	[No] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[JornadaLaboral]    Script Date: 4/21/2020 6:05:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[JornadaLaboral](
	[codJornada] [nvarchar](10) NOT NULL,
	[luin] [datetime] NULL,
	[luout] [datetime] NULL,
	[main] [datetime] NULL,
	[maout] [datetime] NULL,
	[miin] [datetime] NULL,
	[miout] [datetime] NULL,
	[juin] [datetime] NULL,
	[juout] [datetime] NULL,
	[viin] [datetime] NULL,
	[viout] [datetime] NULL,
	[sain] [datetime] NULL,
	[saout] [datetime] NULL,
	[doin] [datetime] NULL,
	[douot] [datetime] NULL,
	[alimin] [datetime] NULL,
	[aliout] [datetime] NULL,
	[luflag] [tinyint] NULL,
	[maflag] [tinyint] NULL,
	[miflag] [tinyint] NULL,
	[juflag] [tinyint] NULL,
	[viflag] [tinyint] NULL,
	[saflag] [tinyint] NULL,
	[doflag] [tinyint] NULL,
	[entrada] [datetime] NULL,
	[salida] [datetime] NULL,
	[hrslu] [decimal](38, 20) NULL,
	[hrsma] [decimal](38, 20) NULL,
	[hrsmi] [decimal](38, 20) NULL,
	[hrsju] [decimal](38, 20) NULL,
	[hrsvi] [decimal](38, 20) NULL,
	[hrssa] [decimal](38, 20) NULL,
	[hrsdo] [decimal](38, 20) NULL,
	[jorbase] [decimal](38, 20) NULL,
	[jorsemana] [decimal](38, 20) NULL,
	[descsema] [decimal](38, 20) NULL,
 CONSTRAINT [PK_JornadaLaboral] PRIMARY KEY CLUSTERED 
(
	[codJornada] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MethodStep]    Script Date: 4/21/2020 6:05:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MethodStep](
	[Code] [nvarchar](20) NOT NULL,
	[Description] [nvarchar](80) NULL,
	[FunctionNo] [int] NULL,
	[UsesBaseAmount] [tinyint] NULL,
	[BracketTypeCode] [nvarchar](20) NOT NULL,
	[UsesPayrollRate] [tinyint] NULL,
	[RequiresEmpAuthInfo] [tinyint] NULL,
	[BracketStatusSelectionMode] [int] NULL,
	[UsesActivationCode] [tinyint] NULL,
	[RateUsedType] [int] NULL,
	[UsesPayStructure] [tinyint] NULL,
	[StepClassCode] [nvarchar](20) NOT NULL,
	[Comment] [tinyint] NULL,
 CONSTRAINT [PK_MethodStep_1] PRIMARY KEY CLUSTERED 
(
	[Code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PayControlCategory]    Script Date: 4/21/2020 6:05:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PayControlCategory](
	[Code] [nvarchar](20) NOT NULL,
	[Description] [nvarchar](80) NULL,
 CONSTRAINT [PK_PayControlCategory] PRIMARY KEY CLUSTERED 
(
	[Code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PayCycle]    Script Date: 4/21/2020 6:05:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PayCycle](
	[Code] [nvarchar](10) NOT NULL,
	[Description] [nvarchar](50) NULL,
	[PayFrequency] [int] NULL,
	[PaymentDelay] [int] NULL,
	[AnnualizingFactor] [int] NULL,
	[MonthlyFactor] [decimal](38, 20) NULL,
	[Format] [nvarchar](10) NULL,
	[caso1] [tinyint] NULL,
	[caso2] [tinyint] NULL,
	[caso3] [tinyint] NULL,
	[caso4] [tinyint] NULL,
 CONSTRAINT [PK_PayCycle] PRIMARY KEY CLUSTERED 
(
	[Code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PayCyclePeriod]    Script Date: 4/21/2020 6:05:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PayCyclePeriod](
	[PayCycleCode] [nvarchar](10) NOT NULL,
	[PayCycleTerm] [nvarchar](10) NOT NULL,
	[Period] [int] NOT NULL,
	[StartDate] [datetime] NULL,
	[EndDate] [datetime] NULL,
	[PayDate] [datetime] NULL,
	[Posted] [tinyint] NULL,
 CONSTRAINT [PK_PayCyclePeriod_1] PRIMARY KEY CLUSTERED 
(
	[PayCycleCode] ASC,
	[PayCycleTerm] ASC,
	[Period] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PayCycleTerm]    Script Date: 4/21/2020 6:05:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PayCycleTerm](
	[PayCycleCode] [nvarchar](10) NOT NULL,
	[Term] [nvarchar](10) NOT NULL,
	[DefaultPeriods] [int] NULL,
	[PeriodsGenerated] [int] NULL,
 CONSTRAINT [PK_PayCycleTerm] PRIMARY KEY CLUSTERED 
(
	[PayCycleCode] ASC,
	[Term] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PayrollCalcMethod]    Script Date: 4/21/2020 6:05:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PayrollCalcMethod](
	[PayrollControlCode] [nvarchar](20) NOT NULL,
	[Description] [nvarchar](50) NULL,
	[EffectiveDate] [datetime] NOT NULL,
 CONSTRAINT [PK_PayrollCalcMethod] PRIMARY KEY CLUSTERED 
(
	[PayrollControlCode] ASC,
	[EffectiveDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PayrollCalcMethodLine]    Script Date: 4/21/2020 6:05:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PayrollCalcMethodLine](
	[MethodStepCode] [nvarchar](20) NOT NULL,
	[PayrollControlCode] [nvarchar](20) NOT NULL,
	[LineNumber] [int] NOT NULL,
	[EffectiveDate] [datetime] NOT NULL,
	[BaseAmountCode] [nvarchar](20) NOT NULL,
	[PayrollRateCode] [nvarchar](10) NOT NULL,
	[BracketCode] [nvarchar](20) NOT NULL,
	[BracketTypeCode] [nvarchar](20) NOT NULL,
	[UnitofMeasureCode] [nvarchar](10) NULL,
	[ActivationCode] [nvarchar](10) NULL,
	[PayStructureCode] [nvarchar](20) NULL,
	[Comment] [nvarchar](80) NULL,
 CONSTRAINT [PK_PayrollCalcMethodLine] PRIMARY KEY CLUSTERED 
(
	[PayrollControlCode] ASC,
	[LineNumber] ASC,
	[EffectiveDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PayrollControl]    Script Date: 4/21/2020 6:05:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PayrollControl](
	[Code] [nvarchar](20) NOT NULL,
	[Type] [int] NULL,
	[Name] [nvarchar](30) NULL,
	[County] [nvarchar](3) NULL,
	[LastDateModified] [datetime] NULL,
	[GLPostType] [int] NULL,
	[PayrollPostingGroup] [nvarchar](10) NULL,
	[Calculate] [tinyint] NULL,
	[NormalSign] [int] NULL,
	[DateFilter] [datetime] NULL,
	[GlobalDimension1Filter] [nvarchar](20) NULL,
	[GlobalDimension2Filter] [nvarchar](20) NULL,
	[WorkTypeFilter] [nvarchar](10) NULL,
	[Amount] [decimal](38, 20) NULL,
	[TaxableAmount] [decimal](38, 20) NULL,
	[CalcMethods] [tinyint] NULL,
	[ReportingAuthorityType] [int] NULL,
	[ReportingAuthorityCode] [nvarchar](10) NULL,
	[EditStatus] [int] NULL,
	[SourcePayControl] [nvarchar](20) NULL,
	[DefinedInMasterOrder] [tinyint] NULL,
	[CalculationOrders] [int] NULL,
	[AutoInsert] [tinyint] NULL,
	[MonthlySchedule] [int] NULL,
	[EmployerNoFilter] [nvarchar](20) NULL,
	[SourceBaseAmountCode] [nvarchar](20) NULL,
	[DistrByDimension] [tinyint] NULL,
	[DistrByEarnedPayCycle] [tinyint] NULL,
	[CategoryCode] [nvarchar](20) NULL,
	[Comment] [tinyint] NULL,
	[Individual] [tinyint] NULL,
	[LoanControl] [tinyint] NULL,
	[CreateCashReceiptJournal] [tinyint] NULL,
	[PayrollControlPagoHoras] [nvarchar](250) NULL,
	[CodTarifa] [nvarchar](10) NULL,
	[PorcentajeTarifa] [nvarchar](10) NULL,
 CONSTRAINT [PK_PayrollControl] PRIMARY KEY CLUSTERED 
(
	[Code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PayrollControlGroup]    Script Date: 4/21/2020 6:05:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PayrollControlGroup](
	[Code] [nvarchar](10) NOT NULL,
	[Name] [nvarchar](30) NULL,
	[PayControls] [int] NULL,
	[IsGroupUsed] [tinyint] NULL,
	[PayControlFilter] [nvarchar](20) NULL,
 CONSTRAINT [PK_PayrollControlGroup] PRIMARY KEY CLUSTERED 
(
	[Code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PayrollControlGroupControl]    Script Date: 4/21/2020 6:05:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PayrollControlGroupControl](
	[PayrollControlGroupCode] [nvarchar](10) NOT NULL,
	[PayControlCode] [nvarchar](20) NOT NULL,
	[PayControlName] [nvarchar](30) NULL,
	[PayrollControlGroupName] [nvarchar](30) NULL,
	[MonthlySchedule] [int] NULL,
	[PayrollPostingGroup] [nvarchar](10) NULL,
 CONSTRAINT [PK_PayrollControlGroupControl] PRIMARY KEY CLUSTERED 
(
	[PayrollControlGroupCode] ASC,
	[PayControlCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PayrollJournalLine]    Script Date: 4/21/2020 6:05:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PayrollJournalLine](
	[JournalTemplateName] [nvarchar](10) NOT NULL,
	[LineNumber] [int] NOT NULL,
	[EmployeeNo] [nvarchar](20) NOT NULL,
	[PostingDate] [datetime] NOT NULL,
	[DocumentType] [int] NULL,
	[DocumentNo] [nvarchar](20) NULL,
	[Description] [nvarchar](50) NULL,
	[PayPeriodEndDate] [datetime] NOT NULL,
	[PayCycleCode] [nvarchar](10) NOT NULL,
	[PostedtoJobsResources] [tinyint] NULL,
	[Amount] [decimal](38, 20) NULL,
	[PayrollAmount] [decimal](38, 20) NULL,
	[TaxableAmount] [decimal](38, 20) NULL,
	[CashAmount] [decimal](38, 20) NULL,
	[HoursMinutes] [decimal](38, 20) NULL,
	[WeeksDays] [decimal](38, 20) NULL,
	[EmployeeName] [nvarchar](50) NULL,
	[PayrollPostingGroup] [nvarchar](10) NOT NULL,
	[ShortcutDimension1Code] [nvarchar](20) NULL,
	[ShortcutDimension2Code] [nvarchar](20) NULL,
	[SourceCode] [nvarchar](10) NULL,
	[PayrollControlCode] [nvarchar](20) NOT NULL,
	[Calculated] [tinyint] NULL,
	[PayrollControlType] [int] NULL,
	[GLPostType] [int] NULL,
	[PayrollControlName] [nvarchar](30) NULL,
	[PrintonCheck] [int] NULL,
	[County] [nvarchar](3) NULL,
	[Locality] [nvarchar](30) NULL,
	[NormalSign] [int] NULL,
	[RegularPay] [tinyint] NULL,
	[JobNo] [nvarchar](20) NULL,
	[JobTaskNo] [nvarchar](20) NULL,
	[ResourceNo] [nvarchar](20) NULL,
	[WorkTypeCode] [nvarchar](10) NULL,
	[Chargeable] [tinyint] NULL,
	[UnitPrice] [decimal](38, 20) NULL,
	[TotalPrice] [decimal](38, 20) NULL,
	[BusinessUnitCode] [nvarchar](10) NULL,
	[JournalBatchName] [nvarchar](10) NOT NULL,
	[ReasonCode] [nvarchar](10) NULL,
	[RecurringMethod] [int] NULL,
	[ExpirationDate] [datetime] NULL,
	[RecurringFrequency] [nvarchar](10) NULL,
	[PayrollRunNoSeries] [nvarchar](10) NOT NULL,
	[CheckPrinted] [tinyint] NULL,
	[BankPaymentType] [int] NULL,
	[NoPrinted] [int] NULL,
	[OriginalAmount] [decimal](38, 20) NULL,
	[AdjustedAmount] [decimal](38, 20) NULL,
	[CheckExported] [tinyint] NULL,
	[ExportFileName] [nvarchar](30) NULL,
	[CheckTransmitted] [tinyint] NULL,
	[EntryDate] [datetime] NULL,
	[DateWorked] [datetime] NULL,
	[PayCycleTerm] [nvarchar](10) NOT NULL,
	[PayCyclePeriod] [int] NOT NULL,
	[EarnedPayCycleCode] [nvarchar](10) NULL,
	[EarnedPayCycleTerm] [nvarchar](10) NULL,
	[EarnedPayCyclePeriod] [int] NULL,
	[ReportingAuthorityType] [int] NULL,
	[ReportingAuthorityCode] [nvarchar](10) NULL,
	[WorkReportingAuthorityCode] [nvarchar](10) NULL,
	[TempAmount] [decimal](38, 20) NULL,
	[InsurableHours] [decimal](38, 20) NULL,
	[TaxTypeCode] [nvarchar](10) NULL,
	[EmployerNo] [nvarchar](20) NULL,
	[SeasonalCode] [nvarchar](10) NULL,
	[SeasonalWages] [tinyint] NULL,
	[DimensionSetID] [int] NULL,
	[Año] [nvarchar](4) NULL,
	[mes] [nvarchar](2) NULL,
	[dia] [nvarchar](2) NULL,
 CONSTRAINT [PK_PayrollJournalLine] PRIMARY KEY CLUSTERED 
(
	[JournalTemplateName] ASC,
	[LineNumber] ASC,
	[JournalBatchName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PayrollJournalTemplate]    Script Date: 4/21/2020 6:05:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PayrollJournalTemplate](
	[Name] [nvarchar](10) NOT NULL,
	[Description] [nvarchar](80) NULL,
	[TestReportID] [int] NULL,
	[PageID] [int] NULL,
	[PostingReportID] [int] NULL,
	[ForcePostingReport] [tinyint] NULL,
	[CheckPrintReportID] [int] NULL,
	[SourceCode] [nvarchar](10) NULL,
	[ReasonCode] [nvarchar](10) NULL,
	[Recurring] [tinyint] NULL,
	[GLPostOption] [int] NULL,
	[LastPayrollRunNo] [nvarchar](20) NULL,
	[TestReportName] [nvarchar](250) NULL,
	[PageName] [nvarchar](250) NULL,
	[PostingReportName] [nvarchar](250) NULL,
	[CheckPrintReportName] [nvarchar](250) NULL,
	[PayrollRunNoSeries] [nvarchar](10) NOT NULL,
 CONSTRAINT [PK_PayrollJournalTemplate] PRIMARY KEY CLUSTERED 
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PayrollLedgerEntry]    Script Date: 4/21/2020 6:05:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PayrollLedgerEntry](
	[EntryNo] [int] NOT NULL,
	[EmployeeNo] [nvarchar](20) NULL,
	[PostingDate] [datetime] NULL,
	[DocumentType] [int] NULL,
	[DocumentNo] [nvarchar](20) NULL,
	[Description] [nvarchar](50) NULL,
	[RegularPay] [tinyint] NULL,
	[Amount] [decimal](38, 20) NULL,
	[TaxableAmount] [decimal](38, 20) NULL,
	[AmountonPayCheck] [decimal](38, 20) NULL,
	[PayrollPostingGroup] [nvarchar](10) NULL,
	[GlobalDimension1Code] [nvarchar](20) NULL,
	[GlobalDimension2Code] [nvarchar](20) NULL,
	[ResourceNo] [nvarchar](20) NULL,
	[UserID] [nvarchar](50) NULL,
	[SourceCode] [nvarchar](10) NULL,
	[PayrollControlCode] [nvarchar](20) NULL,
	[PayrollControlType] [int] NULL,
	[PayrollControlName] [nvarchar](30) NULL,
	[County] [nvarchar](3) NULL,
	[Locality] [nvarchar](30) NULL,
	[PayPeriodEndDate] [datetime] NULL,
	[GLPostType] [int] NULL,
	[VendorLedgerEntryNo] [int] NULL,
	[VendorLedgerPosting] [int] NULL,
	[JobNo] [nvarchar](20) NULL,
	[JobTaskNo] [nvarchar](20) NULL,
	[BurdenAllocated] [tinyint] NULL,
	[Chargeable] [tinyint] NULL,
	[WorkTypeCode] [nvarchar](10) NULL,
	[JournalBatchName] [nvarchar](10) NULL,
	[ReasonCode] [nvarchar](10) NULL,
	[CheckNo] [nvarchar](20) NULL,
	[PayDate] [datetime] NULL,
	[ReportingAuthorityType] [int] NULL,
	[ReportingAuthorityCode] [nvarchar](10) NULL,
	[EntryDate] [datetime] NULL,
	[DateWorked] [datetime] NULL,
	[PayCycleCode] [nvarchar](10) NULL,
	[PayCycleTerm] [nvarchar](10) NULL,
	[PayCyclePeriod] [int] NULL,
	[PayPeriodStartDate] [datetime] NULL,
	[EarnedPayCycleCode] [nvarchar](10) NULL,
	[EarnedPayCycleTerm] [nvarchar](10) NULL,
	[EarnedPayCyclePeriod] [int] NULL,
	[EarnedPeriodStartDate] [datetime] NULL,
	[EarnedPeriodEndDate] [datetime] NULL,
	[WorkReportingAuthorityCode] [nvarchar](10) NULL,
	[LedgerPostingDate] [datetime] NULL,
	[InsurableHours] [decimal](38, 20) NULL,
	[TaxTypeCode] [nvarchar](10) NULL,
	[EmployerNo] [nvarchar](20) NULL,
	[SeasonalCode] [nvarchar](10) NULL,
	[SeasonalWages] [tinyint] NULL,
	[DimensionSetID] [int] NULL,
	[calcReport] [tinyint] NULL,
 CONSTRAINT [PK_PayrollLedgerEntry] PRIMARY KEY CLUSTERED 
(
	[EntryNo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PayrollPostingGroup]    Script Date: 4/21/2020 6:05:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PayrollPostingGroup](
	[Code] [nvarchar](10) NOT NULL,
	[EarningsAccount] [nvarchar](20) NULL,
	[BankAccountNo] [nvarchar](20) NULL,
	[LiabilityAccount] [nvarchar](20) NULL,
	[ExpenseAccount] [nvarchar](20) NULL,
	[DateFilter] [datetime] NULL,
	[DetailExists] [tinyint] NULL,
	[CurrentEffectiveDate] [datetime] NULL,
 CONSTRAINT [PK_PayrollPostingGroup] PRIMARY KEY CLUSTERED 
(
	[Code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PayrollPostingGroupDetail]    Script Date: 4/21/2020 6:05:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PayrollPostingGroupDetail](
	[Code] [nvarchar](10) NOT NULL,
	[EffectiveDate] [datetime] NOT NULL,
	[EarningsAccount] [nvarchar](20) NULL,
	[BankAccountNo] [nvarchar](20) NULL,
	[LiabilityAccount] [nvarchar](20) NULL,
	[ExpenseAccount] [nvarchar](20) NULL,
 CONSTRAINT [PK_PayrollPostingGroupDetail] PRIMARY KEY CLUSTERED 
(
	[Code] ASC,
	[EffectiveDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PayrollRate]    Script Date: 4/21/2020 6:05:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PayrollRate](
	[Code] [nvarchar](10) NOT NULL,
	[Description] [nvarchar](30) NULL,
	[EditStatus] [int] NULL,
	[UnitofMeasureCode] [nvarchar](10) NULL,
	[BorrarPosteo] [tinyint] NULL,
 CONSTRAINT [PK_PayrollRate] PRIMARY KEY CLUSTERED 
(
	[Code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PayrollReportingAuthority]    Script Date: 4/21/2020 6:05:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PayrollReportingAuthority](
	[Code] [nvarchar](10) NOT NULL,
	[Type] [int] NULL,
	[Name] [nvarchar](30) NULL,
	[County] [nvarchar](3) NULL,
	[Locality] [nvarchar](30) NULL,
	[DateFilter] [datetime] NULL,
	[GlobalDimension1Filter] [nvarchar](20) NULL,
	[GlobalDimension2Filter] [nvarchar](20) NULL,
	[PayrollControlTypeFilter] [int] NULL,
	[GLPostTypeFilter] [int] NULL,
	[WorkTypeFilter] [nvarchar](10) NULL,
	[Amount] [decimal](38, 20) NULL,
	[TaxableAmount] [decimal](38, 20) NULL,
	[UnemploymentInsurance] [tinyint] NULL,
	[StateNoCode] [nvarchar](2) NULL,
	[TaxingEntityCode] [nvarchar](5) NULL,
	[ParentRepAuthCode] [nvarchar](10) NULL,
	[EditStatus] [int] NULL,
	[EmployerNoFilter] [nvarchar](20) NULL,
 CONSTRAINT [PK_PayrollReportingAuthority] PRIMARY KEY CLUSTERED 
(
	[Code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ReferenciadePrestamo]    Script Date: 4/21/2020 6:05:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ReferenciadePrestamo](
	[NoEmpleado] [nvarchar](20) NOT NULL,
	[Referencia] [nvarchar](20) NOT NULL,
	[EntidadFinanciera] [nvarchar](10) NULL,
	[FecInicio] [datetime] NULL,
	[FecFinal] [datetime] NULL,
	[FecRealFin] [datetime] NULL,
	[Monto] [decimal](38, 20) NULL,
	[Saldo] [decimal](38, 20) NULL,
	[MontoCuota] [decimal](38, 20) NULL,
	[NumCuotas] [int] NULL,
	[CuotasPendientes] [int] NULL,
	[MontoUltCuota] [decimal](38, 20) NULL,
	[PayControlRel] [nvarchar](10) NULL,
	[Estado] [int] NULL,
 CONSTRAINT [PK_ReferenciadePrestamo] PRIMARY KEY CLUSTERED 
(
	[NoEmpleado] ASC,
	[Referencia] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Vendor]    Script Date: 4/21/2020 6:05:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Vendor](
	[No] [nvarchar](20) NOT NULL,
	[Name] [nvarchar](50) NULL,
	[SearchName] [nvarchar](50) NULL,
	[Name2] [nvarchar](50) NULL,
	[Address] [nvarchar](50) NULL,
	[Address2] [nvarchar](50) NULL,
	[City] [nvarchar](30) NULL,
	[Contact] [nvarchar](50) NULL,
	[PhoneNo] [nvarchar](30) NULL,
	[TelexNo] [nvarchar](20) NULL,
	[OurAccountNo] [nvarchar](20) NULL,
	[TerritoryCode] [nvarchar](10) NULL,
	[GlobalDimension1Code] [nvarchar](20) NULL,
	[GlobalDimension2Code] [nvarchar](20) NULL,
	[BudgetedAmount] [decimal](38, 20) NULL,
	[VendorPostingGroup] [nvarchar](10) NULL,
	[CurrencyCode] [nvarchar](10) NULL,
	[LanguageCode] [nvarchar](10) NULL,
	[StatisticsGroup] [int] NULL,
	[PaymentTermsCode] [nvarchar](10) NULL,
	[FinChargeTermsCode] [nvarchar](10) NULL,
	[PurchaserCode] [nvarchar](10) NULL,
	[ShipmentMethodCode] [nvarchar](10) NULL,
	[ShippingAgentCode] [nvarchar](10) NULL,
	[InvoiceDiscCode] [nvarchar](20) NULL,
	[CountryRegionCode] [nvarchar](10) NULL,
	[Comment] [tinyint] NULL,
	[Blocked] [int] NULL,
	[PaytoVendorNo] [nvarchar](20) NULL,
	[Priority] [int] NULL,
	[PaymentMethodCode] [nvarchar](10) NULL,
	[LastDateModified] [datetime] NULL,
	[DateFilter] [datetime] NULL,
	[GlobalDimension1Filter] [nvarchar](20) NULL,
	[GlobalDimension2Filter] [nvarchar](20) NULL,
	[Balance] [decimal](38, 20) NULL,
	[BalanceLCY] [decimal](38, 20) NULL,
	[NetChange] [decimal](38, 20) NULL,
	[NetChangeLCY] [decimal](38, 20) NULL,
	[PurchasesLCY] [decimal](38, 20) NULL,
	[InvDiscountsLCY] [decimal](38, 20) NULL,
	[PmtDiscountsLCY] [decimal](38, 20) NULL,
	[BalanceDue] [decimal](38, 20) NULL,
	[BalanceDueLCY] [decimal](38, 20) NULL,
	[Payments] [decimal](38, 20) NULL,
	[InvoiceAmounts] [decimal](38, 20) NULL,
	[CrMemoAmounts] [decimal](38, 20) NULL,
	[FinanceChargeMemoAmounts] [decimal](38, 20) NULL,
	[PaymentsLCY] [decimal](38, 20) NULL,
	[InvAmountsLCY] [decimal](38, 20) NULL,
	[CrMemoAmountsLCY] [decimal](38, 20) NULL,
	[FinChargeMemoAmountsLCY] [decimal](38, 20) NULL,
	[OutstandingOrders] [decimal](38, 20) NULL,
	[AmtRcdNotInvoiced] [decimal](38, 20) NULL,
	[ApplicationMethod] [int] NULL,
	[PricesIncludingVAT] [tinyint] NULL,
	[FaxNo] [nvarchar](30) NULL,
	[TelexAnswerBack] [nvarchar](20) NULL,
	[VATRegistrationNo] [nvarchar](20) NULL,
	[GenBusPostingGroup] [nvarchar](10) NULL,
	[Picture] [image] NULL,
	[GLN] [nvarchar](13) NULL,
	[PostCode] [nvarchar](20) NULL,
	[County] [nvarchar](30) NULL,
	[DebitAmount] [decimal](38, 20) NULL,
	[CreditAmount] [decimal](38, 20) NULL,
	[DebitAmountLCY] [decimal](38, 20) NULL,
	[CreditAmountLCY] [decimal](38, 20) NULL,
	[EMail] [nvarchar](80) NULL,
	[HomePage] [nvarchar](80) NULL,
	[ReminderAmounts] [decimal](38, 20) NULL,
	[ReminderAmountsLCY] [decimal](38, 20) NULL,
	[NoSeries] [nvarchar](10) NULL,
	[TaxAreaCode] [nvarchar](20) NULL,
	[TaxLiable] [tinyint] NULL,
	[VATBusPostingGroup] [nvarchar](10) NULL,
	[CurrencyFilter] [nvarchar](10) NULL,
	[OutstandingOrdersLCY] [decimal](38, 20) NULL,
	[AmtRcdNotInvoicedLCY] [decimal](38, 20) NULL,
	[BlockPaymentTolerance] [tinyint] NULL,
	[PmtDiscToleranceLCY] [decimal](38, 20) NULL,
	[PmtToleranceLCY] [decimal](38, 20) NULL,
	[ICPartnerCode] [nvarchar](20) NULL,
	[Refunds] [decimal](38, 20) NULL,
	[RefundsLCY] [decimal](38, 20) NULL,
	[OtherAmounts] [decimal](38, 20) NULL,
	[OtherAmountsLCY] [decimal](38, 20) NULL,
	[PrepaymentPercent] [decimal](38, 20) NULL,
	[OutstandingInvoices] [decimal](38, 20) NULL,
	[OutstandingInvoicesLCY] [decimal](38, 20) NULL,
	[PaytoNoOfArchivedDoc] [int] NULL,
	[BuyfromNoOfArchivedDoc] [int] NULL,
	[PartnerType] [int] NULL,
	[CreditorNo] [nvarchar](20) NULL,
	[PreferredBankAccount] [nvarchar](10) NULL,
	[CashFlowPaymentTermsCode] [nvarchar](10) NULL,
	[PrimaryContactNo] [nvarchar](20) NULL,
	[ResponsibilityCenter] [nvarchar](10) NULL,
	[LocationCode] [nvarchar](10) NULL,
	[LeadTimeCalculation] [varchar](32) NULL,
	[NoofPstdReceipts] [int] NULL,
	[NoofPstdInvoices] [int] NULL,
	[NoofPstdReturnShipments] [int] NULL,
	[NoofPstdCreditMemos] [int] NULL,
	[PaytoNoofOrders] [int] NULL,
	[PaytoNoofInvoices] [int] NULL,
	[PaytoNoofReturnOrders] [int] NULL,
	[PaytoNoofCreditMemos] [int] NULL,
	[PaytoNoofPstdReceipts] [int] NULL,
	[PaytoNoofPstdInvoices] [int] NULL,
	[PaytoNoofPstdReturnS] [int] NULL,
	[PaytoNoofPstdCrMemos] [int] NULL,
	[NoofQuotes] [int] NULL,
	[NoofBlanketOrders] [int] NULL,
	[NoofOrders] [int] NULL,
	[NoofInvoices] [int] NULL,
	[NoofReturnOrders] [int] NULL,
	[NoofCreditMemos] [int] NULL,
	[NoofOrderAddresses] [int] NULL,
	[PaytoNoofQuotes] [int] NULL,
	[PaytoNoofBlanketOrders] [int] NULL,
	[BaseCalendarCode] [nvarchar](10) NULL,
	[UPSZone] [nvarchar](2) NULL,
	[FederalIDNo] [nvarchar](30) NULL,
	[BankCommunication] [int] NULL,
	[CheckDateFormat] [int] NULL,
	[CheckDateSeparator] [int] NULL,
	[IRS1099Code] [nvarchar](10) NULL,
	[BalanceonDate] [decimal](38, 20) NULL,
	[BalanceonDateLCY] [decimal](38, 20) NULL,
	[RFCNo] [nvarchar](13) NULL,
	[CURPNo] [nvarchar](18) NULL,
	[StateInscription] [nvarchar](30) NULL,
	[FATCAfilingrequirement] [tinyint] NULL,
	[TaxIdentificationType] [int] NULL,
	[CAI] [nvarchar](50) NULL,
	[FechavencimientoCAI] [datetime] NULL,
	[ABN] [nvarchar](11) NULL,
	[Registered] [tinyint] NULL,
	[ABNDivisionPartNo] [nvarchar](3) NULL,
	[ForeignVend] [tinyint] NULL,
	[WHTBusinessPostingGroup] [nvarchar](10) NULL,
	[WHTPayableAmountLCY] [decimal](38, 20) NULL,
	[WHTRegistrationID] [nvarchar](20) NULL,
	[IDNo] [nvarchar](20) NULL,
	[PostDatedChecksLCY] [decimal](38, 20) NULL,
	[RTCFilterField] [int] NULL,
	[BuyerGroupCode] [nvarchar](10) NULL,
	[BuyerID] [nvarchar](50) NULL,
 CONSTRAINT [PK_Vendor] PRIMARY KEY CLUSTERED 
(
	[No] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[BaseAmountDetail]  WITH CHECK ADD  CONSTRAINT [FK_BaseAmountDetail_BaseAmount] FOREIGN KEY([BaseAmountCode], [EffectiveDate])
REFERENCES [dbo].[BaseAmount] ([Code], [EffectiveDate])
GO
ALTER TABLE [dbo].[BaseAmountDetail] CHECK CONSTRAINT [FK_BaseAmountDetail_BaseAmount]
GO
ALTER TABLE [dbo].[Bracket]  WITH CHECK ADD  CONSTRAINT [FK_Bracket_BracketType] FOREIGN KEY([BracketTypeCode])
REFERENCES [dbo].[BracketType] ([Code])
GO
ALTER TABLE [dbo].[Bracket] CHECK CONSTRAINT [FK_Bracket_BracketType]
GO
ALTER TABLE [dbo].[BracketDetail]  WITH CHECK ADD  CONSTRAINT [FK_BracketDetail_Bracket] FOREIGN KEY([BracketCode], [BracketTypeCode], [BracketEffectiveDate])
REFERENCES [dbo].[Bracket] ([Code], [BracketTypeCode], [EffectiveDate])
GO
ALTER TABLE [dbo].[BracketDetail] CHECK CONSTRAINT [FK_BracketDetail_Bracket]
GO
ALTER TABLE [dbo].[CalculationOrderControl]  WITH CHECK ADD  CONSTRAINT [FK_CalculationOrderControl_CalculationOrder] FOREIGN KEY([CalculationOrderCode])
REFERENCES [dbo].[CalculationOrder] ([Code])
GO
ALTER TABLE [dbo].[CalculationOrderControl] CHECK CONSTRAINT [FK_CalculationOrderControl_CalculationOrder]
GO
ALTER TABLE [dbo].[CalculationOrderControl]  WITH CHECK ADD  CONSTRAINT [FK_CalculationOrderControl_PayrollControl] FOREIGN KEY([PayrollControlCode])
REFERENCES [dbo].[PayrollControl] ([Code])
GO
ALTER TABLE [dbo].[CalculationOrderControl] CHECK CONSTRAINT [FK_CalculationOrderControl_PayrollControl]
GO
ALTER TABLE [dbo].[DetalledePrestamos]  WITH CHECK ADD  CONSTRAINT [FK_DetalledePrestamos_ReferenciadePrestamo] FOREIGN KEY([NoEmpleado], [Referencia])
REFERENCES [dbo].[ReferenciadePrestamo] ([NoEmpleado], [Referencia])
GO
ALTER TABLE [dbo].[DetalledePrestamos] CHECK CONSTRAINT [FK_DetalledePrestamos_ReferenciadePrestamo]
GO
ALTER TABLE [dbo].[DirectDepositLines]  WITH CHECK ADD  CONSTRAINT [FK_DirectDepositLines_DirectDepositSections] FOREIGN KEY([DirectDepositLayoutCode], [SectionType], [SectionLineNumber])
REFERENCES [dbo].[DirectDepositSections] ([DirectDepositLayoutCode], [SectionType], [LineNumber])
GO
ALTER TABLE [dbo].[DirectDepositLines] CHECK CONSTRAINT [FK_DirectDepositLines_DirectDepositSections]
GO
ALTER TABLE [dbo].[DirectDepositSections]  WITH CHECK ADD  CONSTRAINT [FK_DirectDepositSections_DirectDepositLayout] FOREIGN KEY([DirectDepositLayoutCode])
REFERENCES [dbo].[DirectDepositLayout] ([Code])
GO
ALTER TABLE [dbo].[DirectDepositSections] CHECK CONSTRAINT [FK_DirectDepositSections_DirectDepositLayout]
GO
ALTER TABLE [dbo].[EmployeePayDistribution]  WITH CHECK ADD  CONSTRAINT [FK_EmployeePayDistribution_Employee] FOREIGN KEY([EmployeeNo])
REFERENCES [dbo].[Employee] ([No])
GO
ALTER TABLE [dbo].[EmployeePayDistribution] CHECK CONSTRAINT [FK_EmployeePayDistribution_Employee]
GO
ALTER TABLE [dbo].[EmployeePayrollControl]  WITH CHECK ADD  CONSTRAINT [FK_EmployeePayrollControl_Employee] FOREIGN KEY([EmployeeNo])
REFERENCES [dbo].[Employee] ([No])
GO
ALTER TABLE [dbo].[EmployeePayrollControl] CHECK CONSTRAINT [FK_EmployeePayrollControl_Employee]
GO
ALTER TABLE [dbo].[EmployeePayrollControl]  WITH CHECK ADD  CONSTRAINT [FK_EmployeePayrollControl_PayrollControl] FOREIGN KEY([PayControlCode])
REFERENCES [dbo].[PayrollControl] ([Code])
GO
ALTER TABLE [dbo].[EmployeePayrollControl] CHECK CONSTRAINT [FK_EmployeePayrollControl_PayrollControl]
GO
ALTER TABLE [dbo].[EmployeeRate]  WITH CHECK ADD  CONSTRAINT [FK_EmployeeRate_Employee] FOREIGN KEY([EmployeeNo])
REFERENCES [dbo].[Employee] ([No])
GO
ALTER TABLE [dbo].[EmployeeRate] CHECK CONSTRAINT [FK_EmployeeRate_Employee]
GO
ALTER TABLE [dbo].[EmployeeRate]  WITH CHECK ADD  CONSTRAINT [FK_EmployeeRate_Employer] FOREIGN KEY([Employer])
REFERENCES [dbo].[Employer] ([No])
GO
ALTER TABLE [dbo].[EmployeeRate] CHECK CONSTRAINT [FK_EmployeeRate_Employer]
GO
ALTER TABLE [dbo].[EmployeeRate]  WITH CHECK ADD  CONSTRAINT [FK_EmployeeRate_PayrollRate] FOREIGN KEY([PayrollRateCode])
REFERENCES [dbo].[PayrollRate] ([Code])
GO
ALTER TABLE [dbo].[EmployeeRate] CHECK CONSTRAINT [FK_EmployeeRate_PayrollRate]
GO
ALTER TABLE [dbo].[Employer]  WITH CHECK ADD  CONSTRAINT [FK_Employer_PayrollPostingGroup] FOREIGN KEY([NetPayPostingGroup])
REFERENCES [dbo].[PayrollPostingGroup] ([Code])
GO
ALTER TABLE [dbo].[Employer] CHECK CONSTRAINT [FK_Employer_PayrollPostingGroup]
GO
ALTER TABLE [dbo].[EmployerRate]  WITH CHECK ADD  CONSTRAINT [FK_EmployerRate_Employer] FOREIGN KEY([EmployerNo])
REFERENCES [dbo].[Employer] ([No])
GO
ALTER TABLE [dbo].[EmployerRate] CHECK CONSTRAINT [FK_EmployerRate_Employer]
GO
ALTER TABLE [dbo].[EmployerRate]  WITH CHECK ADD  CONSTRAINT [FK_EmployerRate_PayrollRate] FOREIGN KEY([PayrollRateCode])
REFERENCES [dbo].[PayrollRate] ([Code])
GO
ALTER TABLE [dbo].[EmployerRate] CHECK CONSTRAINT [FK_EmployerRate_PayrollRate]
GO
ALTER TABLE [dbo].[EmployerReportingAuthority]  WITH CHECK ADD  CONSTRAINT [FK_EmployerReportingAuthority_Employer] FOREIGN KEY([EmployerNo])
REFERENCES [dbo].[Employer] ([No])
GO
ALTER TABLE [dbo].[EmployerReportingAuthority] CHECK CONSTRAINT [FK_EmployerReportingAuthority_Employer]
GO
ALTER TABLE [dbo].[EmployerReportingAuthority]  WITH CHECK ADD  CONSTRAINT [FK_EmployerReportingAuthority_PayrollReportingAuthority] FOREIGN KEY([ReportingAuthorityCode])
REFERENCES [dbo].[PayrollReportingAuthority] ([Code])
GO
ALTER TABLE [dbo].[EmployerReportingAuthority] CHECK CONSTRAINT [FK_EmployerReportingAuthority_PayrollReportingAuthority]
GO
ALTER TABLE [dbo].[EmployerReportingAuthority]  WITH CHECK ADD  CONSTRAINT [FK_EmployerReportingAuthority_Vendor] FOREIGN KEY([VendorNo])
REFERENCES [dbo].[Vendor] ([No])
GO
ALTER TABLE [dbo].[EmployerReportingAuthority] CHECK CONSTRAINT [FK_EmployerReportingAuthority_Vendor]
GO
ALTER TABLE [dbo].[MethodStep]  WITH CHECK ADD  CONSTRAINT [FK_MethodStep_BracketType] FOREIGN KEY([BracketTypeCode])
REFERENCES [dbo].[BracketType] ([Code])
GO
ALTER TABLE [dbo].[MethodStep] CHECK CONSTRAINT [FK_MethodStep_BracketType]
GO
ALTER TABLE [dbo].[PayCyclePeriod]  WITH CHECK ADD  CONSTRAINT [FK_PayCyclePeriod_PayCycleTerm] FOREIGN KEY([PayCycleCode], [PayCycleTerm])
REFERENCES [dbo].[PayCycleTerm] ([PayCycleCode], [Term])
GO
ALTER TABLE [dbo].[PayCyclePeriod] CHECK CONSTRAINT [FK_PayCyclePeriod_PayCycleTerm]
GO
ALTER TABLE [dbo].[PayCycleTerm]  WITH CHECK ADD  CONSTRAINT [FK_PayCycleTerm_PayCycle] FOREIGN KEY([PayCycleCode])
REFERENCES [dbo].[PayCycle] ([Code])
GO
ALTER TABLE [dbo].[PayCycleTerm] CHECK CONSTRAINT [FK_PayCycleTerm_PayCycle]
GO
ALTER TABLE [dbo].[PayrollCalcMethod]  WITH CHECK ADD  CONSTRAINT [FK_PayrollCalcMethod_PayrollControl] FOREIGN KEY([PayrollControlCode])
REFERENCES [dbo].[PayrollControl] ([Code])
GO
ALTER TABLE [dbo].[PayrollCalcMethod] CHECK CONSTRAINT [FK_PayrollCalcMethod_PayrollControl]
GO
ALTER TABLE [dbo].[PayrollCalcMethodLine]  WITH CHECK ADD  CONSTRAINT [FK_PayrollCalcMethodLine_BracketType] FOREIGN KEY([BracketTypeCode])
REFERENCES [dbo].[BracketType] ([Code])
GO
ALTER TABLE [dbo].[PayrollCalcMethodLine] CHECK CONSTRAINT [FK_PayrollCalcMethodLine_BracketType]
GO
ALTER TABLE [dbo].[PayrollCalcMethodLine]  WITH CHECK ADD  CONSTRAINT [FK_PayrollCalcMethodLine_MethodStep] FOREIGN KEY([MethodStepCode])
REFERENCES [dbo].[MethodStep] ([Code])
GO
ALTER TABLE [dbo].[PayrollCalcMethodLine] CHECK CONSTRAINT [FK_PayrollCalcMethodLine_MethodStep]
GO
ALTER TABLE [dbo].[PayrollCalcMethodLine]  WITH CHECK ADD  CONSTRAINT [FK_PayrollCalcMethodLine_PayrollCalcMethod] FOREIGN KEY([PayrollControlCode], [EffectiveDate])
REFERENCES [dbo].[PayrollCalcMethod] ([PayrollControlCode], [EffectiveDate])
GO
ALTER TABLE [dbo].[PayrollCalcMethodLine] CHECK CONSTRAINT [FK_PayrollCalcMethodLine_PayrollCalcMethod]
GO
ALTER TABLE [dbo].[PayrollCalcMethodLine]  WITH CHECK ADD  CONSTRAINT [FK_PayrollCalcMethodLine_PayrollCalcMethod1] FOREIGN KEY([PayrollControlCode], [EffectiveDate])
REFERENCES [dbo].[PayrollCalcMethod] ([PayrollControlCode], [EffectiveDate])
GO
ALTER TABLE [dbo].[PayrollCalcMethodLine] CHECK CONSTRAINT [FK_PayrollCalcMethodLine_PayrollCalcMethod1]
GO
ALTER TABLE [dbo].[PayrollCalcMethodLine]  WITH CHECK ADD  CONSTRAINT [FK_PayrollCalcMethodLine_PayrollRate] FOREIGN KEY([PayrollRateCode])
REFERENCES [dbo].[PayrollRate] ([Code])
GO
ALTER TABLE [dbo].[PayrollCalcMethodLine] CHECK CONSTRAINT [FK_PayrollCalcMethodLine_PayrollRate]
GO
ALTER TABLE [dbo].[PayrollControl]  WITH CHECK ADD  CONSTRAINT [FK_PayrollControl_PayControlCategory] FOREIGN KEY([CategoryCode])
REFERENCES [dbo].[PayControlCategory] ([Code])
GO
ALTER TABLE [dbo].[PayrollControl] CHECK CONSTRAINT [FK_PayrollControl_PayControlCategory]
GO
ALTER TABLE [dbo].[PayrollControl]  WITH CHECK ADD  CONSTRAINT [FK_PayrollControl_PayrollPostingGroup] FOREIGN KEY([PayrollPostingGroup])
REFERENCES [dbo].[PayrollPostingGroup] ([Code])
GO
ALTER TABLE [dbo].[PayrollControl] CHECK CONSTRAINT [FK_PayrollControl_PayrollPostingGroup]
GO
ALTER TABLE [dbo].[PayrollControlGroupControl]  WITH CHECK ADD  CONSTRAINT [FK_PayrollControlGroupControl_PayrollControlGroup] FOREIGN KEY([PayrollControlGroupCode])
REFERENCES [dbo].[PayrollControlGroup] ([Code])
GO
ALTER TABLE [dbo].[PayrollControlGroupControl] CHECK CONSTRAINT [FK_PayrollControlGroupControl_PayrollControlGroup]
GO
ALTER TABLE [dbo].[PayrollJournalLine]  WITH CHECK ADD  CONSTRAINT [FK_PayrollJournalLine_EmployeePayrollControl] FOREIGN KEY([EmployeeNo], [PayrollControlCode])
REFERENCES [dbo].[EmployeePayrollControl] ([EmployeeNo], [PayControlCode])
GO
ALTER TABLE [dbo].[PayrollJournalLine] CHECK CONSTRAINT [FK_PayrollJournalLine_EmployeePayrollControl]
GO
ALTER TABLE [dbo].[PayrollJournalLine]  WITH CHECK ADD  CONSTRAINT [FK_PayrollJournalLine_PayCyclePeriod] FOREIGN KEY([PayCycleCode], [PayCycleTerm], [PayCyclePeriod])
REFERENCES [dbo].[PayCyclePeriod] ([PayCycleCode], [PayCycleTerm], [Period])
GO
ALTER TABLE [dbo].[PayrollJournalLine] CHECK CONSTRAINT [FK_PayrollJournalLine_PayCyclePeriod]
GO
ALTER TABLE [dbo].[PayrollJournalLine]  WITH CHECK ADD  CONSTRAINT [FK_PayrollJournalLine_PayrollJournalTemplate] FOREIGN KEY([JournalTemplateName])
REFERENCES [dbo].[PayrollJournalTemplate] ([Name])
GO
ALTER TABLE [dbo].[PayrollJournalLine] CHECK CONSTRAINT [FK_PayrollJournalLine_PayrollJournalTemplate]
GO
ALTER TABLE [dbo].[PayrollPostingGroupDetail]  WITH CHECK ADD  CONSTRAINT [FK_PayrollPostingGroupDetail_BankAccount] FOREIGN KEY([BankAccountNo])
REFERENCES [dbo].[BankAccount] ([No])
GO
ALTER TABLE [dbo].[PayrollPostingGroupDetail] CHECK CONSTRAINT [FK_PayrollPostingGroupDetail_BankAccount]
GO
ALTER TABLE [dbo].[PayrollPostingGroupDetail]  WITH CHECK ADD  CONSTRAINT [FK_PayrollPostingGroupDetail_PayrollPostingGroup] FOREIGN KEY([Code])
REFERENCES [dbo].[PayrollPostingGroup] ([Code])
GO
ALTER TABLE [dbo].[PayrollPostingGroupDetail] CHECK CONSTRAINT [FK_PayrollPostingGroupDetail_PayrollPostingGroup]
GO
ALTER TABLE [dbo].[ReferenciadePrestamo]  WITH CHECK ADD  CONSTRAINT [FK_ReferenciadePrestamo_Employee] FOREIGN KEY([NoEmpleado])
REFERENCES [dbo].[Employee] ([No])
GO
ALTER TABLE [dbo].[ReferenciadePrestamo] CHECK CONSTRAINT [FK_ReferenciadePrestamo_Employee]
GO
EXEC sys.sp_addextendedproperty @name=N'Descripcion', @value=N'Se utilizan para describir e identificar todos los ingresos o retenciones que se deben aplicar a cada colaborador y cuya suma algebraica establece el líquido a recibir por cada persona. A parte de ello, permite establecer valores “misceláneos” que se utilizan sólo para efectos matemáticos de algún cálculo' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PayrollControl'
GO
USE [master]
GO
ALTER DATABASE [Planilla] SET  READ_WRITE 
GO
