USE [Planilla]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FormatoPlanillaDetalleEgresos](
	[ID] INT IDENTITY NOT NULL,
	[ID_CABECERA] VARCHAR(20) NOT NULL,
	[ID_FORMATO] VARCHAR(20) NOT NULL,
	[PayControlCode] [nvarchar](20) NOT NULL,
	[USUA_CREA] [varchar](50) NOT NULL,
	[FECH_CREA] [datetime] NOT NULL DEFAULT (SYSDATETIME()),
	[USUA_ACTUA] [varchar](50) NULL,
	[FECH_ACTUA] [datetime] NULL
 CONSTRAINT [PK_FormatoPlanillaDetalleEgresos] PRIMARY KEY CLUSTERED 
(
	[ID]
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[FormatoPlanillaDetalleEgresos] WITH CHECK ADD CONSTRAINT 
[FK_FormatoPlanillaDetalleEgresos_FormatoPlanilla] FOREIGN KEY([ID_FORMATO])
REFERENCES [dbo].[FormatoPlanilla] ([ID_FORMATO])

ALTER TABLE [dbo].[FormatoPlanillaDetalleEgresos]  CHECK CONSTRAINT [FK_FormatoPlanillaDetalleEgresos_FormatoPlanilla]
GO

ALTER TABLE [dbo].[FormatoPlanillaDetalleEgresos] WITH CHECK ADD CONSTRAINT 
[FK_FormatoPlanillaDetalleEgresos_FormatoPlanillaEncabezadoEgresos] FOREIGN KEY([ID_CABECERA], [ID_FORMATO])
REFERENCES [dbo].[FormatoPlanillaEncabezadoEgresos] ([ID_CABECERA], [ID_FORMATO])

ALTER TABLE [dbo].[FormatoPlanillaDetalleEgresos]  CHECK CONSTRAINT [FK_FormatoPlanillaDetalleEgresos_FormatoPlanillaEncabezadoEgresos]
GO

ALTER TABLE [dbo].[FormatoPlanillaDetalleEgresos] WITH CHECK ADD CONSTRAINT 
[FK_FormatoPlanillaDetalleEgresos_PayrollControl] FOREIGN KEY([PayControlCode])
REFERENCES [dbo].[PayrollControl]([Code])

ALTER TABLE [dbo].[FormatoPlanillaDetalleEgresos]  CHECK CONSTRAINT [FK_FormatoPlanillaDetalleEgresos_PayrollControl]
GO
