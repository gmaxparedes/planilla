USE Planilla
GO


ALTER TABLE [dbo].[BaseAmountDetail] ALTER COLUMN [TypeFilter] INT
ALTER TABLE [dbo].[BaseAmountDetail] ALTER COLUMN [RepAuthTypeFilter] INT
ALTER TABLE [dbo].[BaseAmountDetail] ALTER COLUMN [GLPostTypeFilter] INT
ALTER TABLE [dbo].[BaseAmountDetail] ALTER COLUMN [ControlCodeFilter] NVARCHAR(20)

ALTER TABLE [dbo].[BaseAmountDetail] WITH CHECK ADD CONSTRAINT 
[FK_BaseAmountDetail_PayrollControl] FOREIGN KEY([ControlCodeFilter])
REFERENCES [dbo].[PayrollControl] ([Code])

ALTER TABLE [dbo].[BaseAmountDetail] CHECK CONSTRAINT [FK_BaseAmountDetail_PayrollControl]
GO


ALTER TABLE [dbo].[BaseAmountDetail] WITH CHECK ADD CONSTRAINT 
[FK_BaseAmountDetail_PayrollControlType] FOREIGN KEY([TypeFilter])
REFERENCES [dbo].[PayrollControlType] ([ID_TYPE])

ALTER TABLE [dbo].[BaseAmountDetail] CHECK CONSTRAINT [FK_BaseAmountDetail_PayrollControlType]
GO


ALTER TABLE [dbo].[BaseAmountDetail] WITH CHECK ADD CONSTRAINT 
[FK_BaseAmountDetail_TypeReportingAuthority] FOREIGN KEY([RepAuthTypeFilter])
REFERENCES [dbo].[PayrollTypeReportingAuthority]([ID_TYPE])

ALTER TABLE [dbo].[BaseAmountDetail]  CHECK CONSTRAINT [FK_BaseAmountDetail_TypeReportingAuthority]
GO


ALTER TABLE [dbo].[BaseAmountDetail] WITH CHECK ADD CONSTRAINT 
[FK_BaseAmountDetail_PayrollPostType] FOREIGN KEY([GLPostTypeFilter])
REFERENCES [dbo].[PayrollPostType] ([ID_POST])

ALTER TABLE [dbo].[BaseAmountDetail] CHECK CONSTRAINT [FK_BaseAmountDetail_PayrollPostType]
GO
