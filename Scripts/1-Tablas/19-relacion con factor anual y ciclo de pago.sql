


--Relacionar con paycycle

ALTER TABLE [dbo].[PayCycle] WITH CHECK ADD CONSTRAINT [FK_Cyle_Factor] FOREIGN KEY([AnnualizingFactor])
REFERENCES [dbo].[PayFactorAnual] ([ID_FAC_ANUAL])

ALTER TABLE [dbo].[PayCycle] CHECK CONSTRAINT [FK_Cyle_Factor]
GO