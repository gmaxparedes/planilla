Use Planilla
Go

Alter table [dbo].[PayCyclePeriod] alter column [StartDate] Datetime not null

Alter table [dbo].[PayCyclePeriod] alter column [EndDate] Datetime not null

Alter table [dbo].[PayCyclePeriod] alter column [PayDate] Datetime not null