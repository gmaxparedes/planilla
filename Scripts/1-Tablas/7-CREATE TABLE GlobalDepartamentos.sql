CREATE TABLE [Global].[DEPARTAMENTOS]
(
ID_DEPTO INT IDENTITY(1,1),
CD_PAIS VARCHAR (10),
NOMBRE_DEPTO VARCHAR(50) NOT NULL,
CODIGO_DEPTO VARCHAR(10) NULL,
USUA_CREA VARCHAR(30) NOT NULL,
FECH_CREA DATETIME NOT NULL DEFAULT SYSDATETIME(),
USUA_ACTU VARCHAR(30) NULL,
FECH_ACTU DATETIME NULL
 CONSTRAINT [PK_GlobalDepartamentos] PRIMARY KEY CLUSTERED 
(
	[ID_DEPTO] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [Global].[DEPARTAMENTOS] WITH CHECK ADD CONSTRAINT [FK_GLB_DEPTO_PAIS] FOREIGN KEY([CD_PAIS])
REFERENCES [Global].[PAISES] ([CD_PAIS])

ALTER TABLE [Global].[DEPARTAMENTOS] CHECK CONSTRAINT [FK_GLB_DEPTO_PAIS]
GO