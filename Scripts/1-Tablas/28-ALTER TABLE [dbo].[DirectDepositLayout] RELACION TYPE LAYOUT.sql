Use Planilla
GO



ALTER TABLE [dbo].[DirectDepositLayout] WITH CHECK ADD CONSTRAINT [FK_DirectDepositLayout_LayoutType] FOREIGN KEY([LayoutType])
REFERENCES [dbo].[DirectDepositTypeLayout] ([ID_LAYOUT])

ALTER TABLE [dbo].[DirectDepositLayout] CHECK CONSTRAINT [FK_DirectDepositLayout_LayoutType]
GO


