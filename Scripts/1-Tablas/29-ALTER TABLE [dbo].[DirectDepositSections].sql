USE Planilla
go



ALTER TABLE [dbo].[DirectDepositSections] ALTER COLUMN [SequenceNumber] [int] NOT NULL
ALTER TABLE [dbo].[DirectDepositSections] ALTER COLUMN [Description] [nvarchar](50) NOT NULL
ALTER TABLE [dbo].[DirectDepositSections] ALTER COLUMN [IncTotalLineCount] [bit] NOT NULL
ALTER TABLE [dbo].[DirectDepositSections] ALTER COLUMN [IncTotalHeaderCount] [bit] NOT NULL
ALTER TABLE [dbo].[DirectDepositSections] ALTER COLUMN [IncTotalDetailCount] [bit] NOT NULL
ALTER TABLE [dbo].[DirectDepositSections] ALTER COLUMN [IncTotalFooterCount] [bit] NOT NULL
ALTER TABLE [dbo].[DirectDepositSections] ALTER COLUMN [IncTotalCreditCount] [bit] NOT NULL
ALTER TABLE [dbo].[DirectDepositSections] ALTER COLUMN [IncTotalDebitCount] [bit] NOT NULL
ALTER TABLE [dbo].[DirectDepositSections] ALTER COLUMN [IncTotalBatchCount] [bit] NOT NULL
ALTER TABLE [dbo].[DirectDepositSections] ALTER COLUMN [IncBatchDetailCount] [bit] NOT NULL
ALTER TABLE [dbo].[DirectDepositSections] ALTER COLUMN [ResetBatchTotals] [bit] NOT NULL