/****** Script for SelectTopNRows command from SSMS  ******/

USE Planilla
GO

ALTER TABLE [dbo].[DirectDepositLines] ALTER COLUMN [ConverttoUpperCase] BIT NOT NULL;
ALTER TABLE [dbo].[DirectDepositLines] ALTER COLUMN [RemoveSpaces] BIT NOT NULL;
ALTER TABLE [dbo].[DirectDepositLines] ALTER COLUMN [RemoveSpecialCharacters] BIT NOT NULL;
ALTER TABLE [dbo].[DirectDepositLines] ALTER COLUMN [IncludeDecimal] BIT NOT NULL;
ALTER TABLE [dbo].[DirectDepositLines] ALTER COLUMN [RequiredField] BIT NOT NULL;
ALTER TABLE [dbo].[DirectDepositLines] ALTER COLUMN [CheckDigit] BIT NOT NULL;
ALTER TABLE [dbo].[DirectDepositLines] ALTER COLUMN [UpdateTotalFileDebitAmt] BIT NOT NULL;
ALTER TABLE [dbo].[DirectDepositLines] ALTER COLUMN [UpdateTotalFileCreditAmt] BIT NOT NULL;
ALTER TABLE [dbo].[DirectDepositLines] ALTER COLUMN [UpdateHashAmount1] BIT NOT NULL;
ALTER TABLE [dbo].[DirectDepositLines] ALTER COLUMN [UpdateHashAmount2] BIT NOT NULL;
ALTER TABLE [dbo].[DirectDepositLines] ALTER COLUMN [UpdateBatchDebitAmt] BIT NOT NULL;
ALTER TABLE [dbo].[DirectDepositLines] ALTER COLUMN [UpdateBatchCreditAmt] BIT NOT NULL;
ALTER TABLE [dbo].[DirectDepositLines] ALTER COLUMN [UpdateBatchHashAmt1] BIT NOT NULL;
ALTER TABLE [dbo].[DirectDepositLines] ALTER COLUMN [UpdateBatchHashAmt2] BIT NOT NULL;