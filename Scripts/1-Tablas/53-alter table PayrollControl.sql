use Planilla
go

Alter table PayrollControl alter column NormalSign bit not null
Alter table PayrollControl alter column AutoInsert bit not null
Alter table PayrollControl alter column Calculate bit not null
Alter table PayrollControl alter column CalcMethods bit not null
Alter table PayrollControl alter column Individual bit not null
Alter table PayrollControl alter column LoanControl bit not null
Alter table PayrollControl alter column Comment varchar(150) null
Alter table PayrollControl alter column CreateCashReceiptJournal bit not null
Alter table PayrollControl add PostAbsence bit not null
Alter table PayrollControl add Absencecode int null
Alter table PayrollControl alter column DistrByDimension bit not null
Alter table PayrollControl alter column DistrByEarnedPayCycle bit not null
Alter table PayrollControl alter column DefinedInMasterOrder bit not null
Alter table PayrollControl add NetPayTest int null

Alter table PayrollControl add TaxTypeCode varchar(10) null
Alter table PayrollControl add ArrearsCode int null
Alter table PayrollControl add PrintOnCheck int null


ALTER TABLE [dbo].[PayrollControl] WITH CHECK ADD CONSTRAINT 
[FK_PayrollControl_TypeReportingAuthority] FOREIGN KEY([ReportingAuthorityType])
REFERENCES [dbo].[PayrollTypeReportingAuthority]([ID_TYPE])

ALTER TABLE [dbo].[PayrollControl]  CHECK CONSTRAINT [FK_PayrollControl_TypeReportingAuthority]
GO

ALTER TABLE [dbo].[PayrollControl] WITH CHECK ADD CONSTRAINT 
[FK_PayrollControl_ReportingAuthorityCode] FOREIGN KEY([ReportingAuthorityCode])
REFERENCES [dbo].[PayrollReportingAuthority] ([Code])

ALTER TABLE [dbo].[PayrollControl] CHECK CONSTRAINT [FK_PayrollControl_ReportingAuthorityCode]
GO


ALTER TABLE [dbo].[PayrollControl] WITH CHECK ADD CONSTRAINT 
[FK_PayrollControl_PayRateEditStatus] FOREIGN KEY([EditStatus])
REFERENCES [dbo].[PayRateEditStatus] ([ID_EDIT])

ALTER TABLE [dbo].[PayrollControl] CHECK CONSTRAINT [FK_PayrollControl_PayRateEditStatus]
GO

ALTER TABLE [dbo].[PayrollControl] WITH CHECK ADD CONSTRAINT 
[FK_PayrollControl_PayrollPostType] FOREIGN KEY([GLPostType])
REFERENCES [dbo].[PayrollPostType] ([ID_POST])

ALTER TABLE [dbo].[PayrollControl] CHECK CONSTRAINT [FK_PayrollControl_PayrollPostType]
GO

ALTER TABLE [dbo].[PayrollControl] WITH CHECK ADD CONSTRAINT 
[FK_PayrollControl_PayrollControlMonthlySchedule] FOREIGN KEY([MonthlySchedule])
REFERENCES [dbo].[PayrollControlMonthlySchedule] ([ID_CALENDARIO])

ALTER TABLE [dbo].[PayrollControl] CHECK CONSTRAINT [FK_PayrollControl_PayrollControlMonthlySchedule]
GO

ALTER TABLE [dbo].[PayrollControl] WITH CHECK ADD CONSTRAINT 
[FK_PayrollControl_PayrollControlType] FOREIGN KEY([Type])
REFERENCES [dbo].[PayrollControlType] ([ID_TYPE])

ALTER TABLE [dbo].[PayrollControl] CHECK CONSTRAINT [FK_PayrollControl_PayrollControlType]
GO

ALTER TABLE [dbo].[PayrollControl] WITH CHECK ADD CONSTRAINT 
[FK_PayrollControl_PayrollControlAbsenceCode] FOREIGN KEY([Absencecode])
REFERENCES [dbo].[PayrollControlAbsenceCode] ([ID_ABSENCE])

ALTER TABLE [dbo].[PayrollControl] CHECK CONSTRAINT [FK_PayrollControl_PayrollControlAbsenceCode]
GO

ALTER TABLE [dbo].[PayrollControl] WITH CHECK ADD CONSTRAINT 
[FK_PayrollControl_PayrollControlNetPayTest] FOREIGN KEY([NetPayTest])
REFERENCES [dbo].[PayrollControlNetPayTest] ([ID_NETPAYTEST])

ALTER TABLE [dbo].[PayrollControl] CHECK CONSTRAINT [FK_PayrollControl_PayrollControlNetPayTest]
GO

ALTER TABLE [dbo].[PayrollControl] WITH CHECK ADD CONSTRAINT 
[FK_PayrollControl_PayrollControlArrearsCode] FOREIGN KEY([ArrearsCode])
REFERENCES [dbo].[PayrollControlArrearsCode] ([ID_ARREARS])

ALTER TABLE [dbo].[PayrollControl] CHECK CONSTRAINT [FK_PayrollControl_PayrollControlArrearsCode]
GO

ALTER TABLE [dbo].[PayrollControl] WITH CHECK ADD CONSTRAINT 
[FK_PayrollControl_GLB_TIPOS_DE_IMPUESTOS] FOREIGN KEY([TaxTypeCode])
REFERENCES [Global].[GLB_TIPOS_DE_IMPUESTOS] ([CD_CODIGO_IMPUESTO])

ALTER TABLE [dbo].[PayrollControl] CHECK CONSTRAINT [FK_PayrollControl_GLB_TIPOS_DE_IMPUESTOS]
GO

ALTER TABLE [dbo].[PayrollControl] WITH CHECK ADD CONSTRAINT 
[FK_PayrollControl_PayrollControlPrintOnCheck] FOREIGN KEY([PrintOnCheck])
REFERENCES [dbo].[PayrollControlPrintOnCheck] ([ID_PRINT])

ALTER TABLE [dbo].[PayrollControl] CHECK CONSTRAINT [FK_PayrollControl_PayrollControlPrintOnCheck]
GO