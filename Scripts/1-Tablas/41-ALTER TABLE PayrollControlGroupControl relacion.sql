Use Planilla
Go

ALTER TABLE [dbo].[PayrollControlGroupControl] WITH CHECK ADD CONSTRAINT 
[FK_PayrollControlGroupControl_PayrollPostingGroup] FOREIGN KEY([PayrollPostingGroup])
REFERENCES [dbo].[PayrollPostingGroup]([Code])

ALTER TABLE [dbo].[PayrollControlGroupControl]  CHECK CONSTRAINT [FK_PayrollControlGroupControl_PayrollPostingGroup]
GO

ALTER TABLE [dbo].[PayrollControlGroup] alter column [IsGroupUsed] bit not null

ALTER TABLE [dbo].[PayrollControlGroupControl] WITH CHECK ADD CONSTRAINT 
[FK_PayrollControlGroupControl_PayrollControlMonthlySchedule] FOREIGN KEY([MonthlySchedule])
REFERENCES [dbo].[PayrollControlMonthlySchedule] ([ID_CALENDARIO])

ALTER TABLE [dbo].[PayrollControlGroupControl] CHECK CONSTRAINT [FK_PayrollControlGroupControl_PayrollControlMonthlySchedule]
GO
