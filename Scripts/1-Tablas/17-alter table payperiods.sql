USE [Planilla]
GO
ALTER TABLE [dbo].[PayCyclePeriod] DROP CONSTRAINT [FK_PayCyclePeriod_PayCycleTerm]
GO
/****** Object:  Table [dbo].[PayCyclePeriod]    Script Date: 18/05/2020 18:09:00 ******/
DROP TABLE [dbo].[PayCyclePeriod]
GO
/****** Object:  Table [dbo].[PayCyclePeriod]    Script Date: 18/05/2020 18:09:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PayCyclePeriod](
	[PayCycleCode] [nvarchar](10) NOT NULL,
	[PayCycleTerm] [nvarchar](10) NOT NULL,
	[Period] [nvarchar](10) NOT NULL,
	[StartDate] [datetime] NULL,
	[EndDate] [datetime] NULL,
	[PayDate] [datetime] NULL,
 CONSTRAINT [PK_PayCyclePeriod_1] PRIMARY KEY CLUSTERED 
(
	[PayCycleCode] ASC,
	[PayCycleTerm] ASC,
	[Period] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[PayCyclePeriod]  WITH CHECK ADD  CONSTRAINT [FK_PayCyclePeriod_PayCycleTerm] FOREIGN KEY([PayCycleCode], [PayCycleTerm])
REFERENCES [dbo].[PayCycleTerm] ([PayCycleCode], [Term])
GO
ALTER TABLE [dbo].[PayCyclePeriod] CHECK CONSTRAINT [FK_PayCyclePeriod_PayCycleTerm]
GO
