ALTER TABLE [dbo].[Employee] ALTER COLUMN City INT
GO
ALTER TABLE [dbo].[Employee] ADD Departamento INT
GO
ALTER TABLE [dbo].[Employee] ALTER COLUMN CountryRegionCode VARCHAR(10)
GO
ALTER TABLE [dbo].[Employee] ALTER COLUMN Area int
GO
ALTER TABLE [dbo].[Employee] ALTER COLUMN Dpto int
GO
ALTER TABLE [dbo].[Employee] ADD JornadaLaboral nvarchar(10)
GO
ALTER TABLE [dbo].[Employee] ALTER COLUMN DefaultRepAuthCode nvarchar(10)
GO
--Relacion con departamentos
ALTER TABLE [dbo].[Employee] WITH CHECK ADD CONSTRAINT [FK_EMPLOYEE_GLB_DEPTO] FOREIGN KEY([Departamento])
REFERENCES [Global].[DEPARTAMENTOS] ([ID_DEPTO])
GO
ALTER TABLE [dbo].[Employee] CHECK CONSTRAINT [FK_EMPLOYEE_GLB_DEPTO]
GO


--Relacion con global.ciudades
ALTER TABLE [dbo].[Employee] WITH CHECK ADD CONSTRAINT [FK_EMPLOYEE_GLB_CIUDAD] FOREIGN KEY([City])
REFERENCES [Global].[CIUDADES] ([ID_CIUDAD])
GO
ALTER TABLE [dbo].[Employee] CHECK CONSTRAINT [FK_EMPLOYEE_GLB_CIUDAD]
GO

--Relacionar con estado civil
ALTER TABLE [dbo].[Employee] WITH CHECK ADD CONSTRAINT [FK_EMPLOYEE_GLB_ESTADO_CIVIL] FOREIGN KEY([MaritalStatus])
REFERENCES [Global].[ESTADOS_CIVILES] ([ID_ESTADO_CIVIL])
GO
ALTER TABLE [dbo].[Employee] CHECK CONSTRAINT [FK_EMPLOYEE_GLB_CIUDAD]
GO

--Relacionar con pais
ALTER TABLE [dbo].[Employee] WITH CHECK ADD CONSTRAINT [FK_EMPLOYEE_GLB_PAISES] FOREIGN KEY([CountryRegionCode])
REFERENCES [Global].[PAISES] ([CD_PAIS])
GO
ALTER TABLE [dbo].[Employee] CHECK CONSTRAINT [FK_EMPLOYEE_GLB_PAISES]
GO

--Relacionar con ciclo de pago
ALTER TABLE [dbo].[Employee] WITH CHECK ADD CONSTRAINT [FK_EMPLOYEE_PAY_CYCLE] FOREIGN KEY([PayCycleCode])
REFERENCES [dbo].[PayCycle] ([CODE])
GO
ALTER TABLE [dbo].[Employee] CHECK CONSTRAINT [FK_EMPLOYEE_PAY_CYCLE]
GO
--Relacionar clase de empleado
ALTER TABLE [dbo].[Employee] WITH CHECK ADD CONSTRAINT [FK_EMPLOYEE_EMPLOYEE_CLASS] FOREIGN KEY([ClassCode])
REFERENCES [dbo].[EmployeeClass] ([Code])
GO
ALTER TABLE [dbo].[Employee] CHECK CONSTRAINT [FK_EMPLOYEE_EMPLOYEE_CLASS]
GO
--Relacionar empleador
ALTER TABLE [dbo].[Employee] WITH CHECK ADD CONSTRAINT [FK_EMPLOYEE_EMPLOYER] FOREIGN KEY([EmployerNo])
REFERENCES [dbo].[Employer] ([No])
GO
ALTER TABLE [dbo].[Employee] CHECK CONSTRAINT [FK_EMPLOYEE_EMPLOYER]
GO
--Relacionar tipo de empleado
ALTER TABLE [dbo].[Employee] WITH CHECK ADD CONSTRAINT [FK_EMPLOYEE_TYPE] FOREIGN KEY([EmployeeType])
REFERENCES [dbo].[EmployeeType] ([Code])
GO
ALTER TABLE [dbo].[Employee] CHECK CONSTRAINT [FK_EMPLOYEE_TYPE]
GO

--Relacionar area
ALTER TABLE [dbo].[Employee] WITH CHECK ADD CONSTRAINT [FK_EMPLOYEE_AREA] FOREIGN KEY([Area])
REFERENCES [Global].[AREAS_EMPRESA] ([ID_AREA])
GO
ALTER TABLE [dbo].[Employee] CHECK CONSTRAINT [FK_EMPLOYEE_AREA]
GO
--Relacionar departamento
ALTER TABLE [dbo].[Employee] WITH CHECK ADD CONSTRAINT [FK_EMPLOYEE_DEPTO] FOREIGN KEY([Dpto])
REFERENCES [Global].[DEPTO_EMPRESA] ([ID_DEPTO])
GO
ALTER TABLE [dbo].[Employee] CHECK CONSTRAINT [FK_EMPLOYEE_DEPTO]
GO
--Relacionar causas de inactividad
ALTER TABLE [dbo].[Employee] WITH CHECK ADD CONSTRAINT [FK_EMPLOYEE_INAC_CAUSAS] FOREIGN KEY([CauseofInactivityCode])
REFERENCES [Global].[CAUSAS_INACTIVIDAD_EMPRESA] ([CauseofInactivityCode])
GO
ALTER TABLE [dbo].[Employee] CHECK CONSTRAINT [FK_EMPLOYEE_INAC_CAUSAS]
GO
--Relacionar tipo de mano de obra
ALTER TABLE [dbo].[Employee] WITH CHECK ADD CONSTRAINT [FK_EMPLOYEE_MANO_OBRA] FOREIGN KEY([TipodeManodeobra])
REFERENCES [Global].[TIPO_MANO_OBRA_EMPRESA] ([ID_MANO_OBRA])
GO
ALTER TABLE [dbo].[Employee] CHECK CONSTRAINT [FK_EMPLOYEE_INAC_CAUSAS]
GO
--Relacionar jornada laboral
ALTER TABLE [dbo].[Employee] WITH CHECK ADD CONSTRAINT [FK_EMPLOYEE_JORNADA] FOREIGN KEY([JornadaLaboral])
REFERENCES [dbo].[JornadaLaboral] ([codJornada])
GO
ALTER TABLE [dbo].[Employee] CHECK CONSTRAINT [FK_EMPLOYEE_JORNADA]
GO
--Relacionar DefaultRepAuthCode
ALTER TABLE [dbo].[Employee] WITH CHECK ADD CONSTRAINT [FK_EMPLOYEE_ReportingAuthorityCode] FOREIGN KEY([DefaultRepAuthCode])
REFERENCES [dbo].[EmployerReportingAuthority] ([ReportingAuthorityCode])
GO
ALTER TABLE [dbo].[Employee] CHECK CONSTRAINT [FK_EMPLOYEE_ReportingAuthorityCode]
GO

----No pude crear la relacion DefaultRepAuthCode
----Relacionar DefaultRepAuthCode
--ALTER TABLE [dbo].[Employee] WITH CHECK ADD CONSTRAINT [FK_EMPLOYEE_ReportingAuthorityCode] FOREIGN KEY([DefaultRepAuthCode])
--REFERENCES [dbo].[EmployerReportingAuthority] ([ReportingAuthorityCode])
--GO
--ALTER TABLE [dbo].[Employee] CHECK CONSTRAINT [FK_EMPLOYEE_ReportingAuthorityCode]
--GO
----No encontre tabla para DefaultWorkTypeCode
----DefaultWorkTypeCode
