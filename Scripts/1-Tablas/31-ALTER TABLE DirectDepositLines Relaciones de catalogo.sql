USE Planilla
GO


ALTER TABLE [dbo].[DirectDepositLines] WITH CHECK ADD CONSTRAINT [FK_DirectDepositLayout_LineType] FOREIGN KEY([LineType])
REFERENCES [dbo].[DirectDepositLineType] ([ID_LINE_TYPE])

ALTER TABLE [dbo].[DirectDepositLines] CHECK CONSTRAINT [FK_DirectDepositLayout_LineType]
GO

ALTER TABLE [dbo].[DirectDepositLines] WITH CHECK ADD CONSTRAINT [FK_DirectDepositLayout_TypeToExport] FOREIGN KEY([TypetoExport])
REFERENCES [dbo].[DirectDepositTypeToExport] ([ID_TYPE_EXPORT])

ALTER TABLE [dbo].[DirectDepositLines] CHECK CONSTRAINT [FK_DirectDepositLayout_TypeToExport]
GO

ALTER TABLE [dbo].[DirectDepositLines] WITH CHECK ADD CONSTRAINT [FK_DirectDepositLayout_TypeJustification] FOREIGN KEY([Justification])
REFERENCES [dbo].[DirectDepositTypeJustification] ([ID_JUSTIFICATION])

ALTER TABLE [dbo].[DirectDepositLines] CHECK CONSTRAINT [FK_DirectDepositLayout_TypeJustification]
GO

ALTER TABLE [dbo].[DirectDepositLines] WITH CHECK ADD CONSTRAINT [FK_DirectDepositLayout_DateFormat] FOREIGN KEY([DateFormat])
REFERENCES [dbo].[DirectDepositDateFormat] ([ID_DATE_FORMAT])

ALTER TABLE [dbo].[DirectDepositLines] CHECK CONSTRAINT [FK_DirectDepositLayout_DateFormat]
GO

ALTER TABLE [dbo].[DirectDepositLines] WITH CHECK ADD CONSTRAINT [FK_DirectDepositLayout_TimeFormat] FOREIGN KEY([TimeFormat])
REFERENCES [dbo].[DirectDepositTimeFormat] ([ID_TIME_FORMAT])

ALTER TABLE [dbo].[DirectDepositLines] CHECK CONSTRAINT [FK_DirectDepositLayout_TimeFormat]
GO
