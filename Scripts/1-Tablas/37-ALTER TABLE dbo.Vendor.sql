USE Planilla
GO


ALTER TABLE dbo.Vendor alter column PricesIncludingVAT bit not null
ALTER TABLE dbo.Vendor alter column CheckDateFormat bit not null
ALTER TABLE dbo.Vendor alter column CheckDateSeparator bit not null
ALTER TABLE dbo.Vendor alter column Registered bit not null
ALTER TABLE dbo.Vendor alter column ForeignVend bit not null
ALTER TABLE dbo.Vendor alter column Blocked bit not null
ALTER TABLE dbo.Vendor alter column TaxLiable bit not null
ALTER TABLE dbo.Vendor alter column BlockPaymentTolerance bit not null
ALTER TABLE dbo.Vendor alter column FATCAfilingrequirement bit not null
ALTER TABLE dbo.Vendor alter column Comment varchar(150)


ALTER TABLE [dbo].[Vendor] WITH CHECK ADD CONSTRAINT [FK_Vendor_Statistics] FOREIGN KEY([StatisticsGroup])
REFERENCES [dbo].[VendorStatisticsGroup] ([ID_STATISCS])
ALTER TABLE [dbo].[Vendor] CHECK CONSTRAINT [FK_Vendor_Statistics]
GO

ALTER TABLE [dbo].[Vendor] WITH CHECK ADD CONSTRAINT [FK_Vendor_PaymentTermsCode] FOREIGN KEY([PaymentTermsCode])
REFERENCES [dbo].[VendorPaymentTermsCode] ([ID_PAYMENTTERMSCODE])
ALTER TABLE [dbo].[Vendor] CHECK CONSTRAINT [FK_Vendor_PaymentTermsCode]
GO

ALTER TABLE [dbo].[Vendor] WITH CHECK ADD CONSTRAINT [FK_Vendor_ApplicationMethod] FOREIGN KEY([ApplicationMethod])
REFERENCES [dbo].[VendorApplicationMethod] ([ID_APPLICATIONMETHOD])
ALTER TABLE [dbo].[Vendor] CHECK CONSTRAINT [FK_Vendor_ApplicationMethod]
GO

ALTER TABLE [dbo].[Vendor] WITH CHECK ADD CONSTRAINT [FK_Vendor_TaxIdentificationType] FOREIGN KEY([TaxIdentificationType])
REFERENCES [dbo].[VendorTaxIdentificationType] ([ID_TAXTYPE])
ALTER TABLE [dbo].[Vendor] CHECK CONSTRAINT [FK_Vendor_TaxIdentificationType]
GO

ALTER TABLE [dbo].[Vendor] WITH CHECK ADD CONSTRAINT [FK_Vendor_PartnerType] FOREIGN KEY([PartnerType])
REFERENCES [dbo].[VendorPartnerType] ([ID_PARTNERTYPE])
ALTER TABLE [dbo].[Vendor] CHECK CONSTRAINT [FK_Vendor_PartnerType]
GO


/*
StatisticsGroup
PaymentTermsCode
ApplicationMethod
TaxIdentificationType
PartnerType
VendorShipmentMethod
*/