CREATE TABLE [Global].[ESTADOS_CIVILES]
(
ID_ESTADO_CIVIL INT IDENTITY(1,1),
DESCRIPCION VARCHAR(20) NOT NULL,
USUA_CREA VARCHAR(30) NOT NULL,
FECH_CREA DATETIME NOT NULL DEFAULT SYSDATETIME(),
USUA_ACTU VARCHAR(30) NULL,
FECH_ACTU DATETIME NULL
 CONSTRAINT [PK_GlobalEstadoCivil] PRIMARY KEY CLUSTERED 
(
	[ID_ESTADO_CIVIL] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO