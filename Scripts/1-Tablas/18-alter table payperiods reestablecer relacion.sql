USE [Planilla]
GO

ALTER TABLE [dbo].[PayrollJournalLine]  WITH CHECK ADD  CONSTRAINT [FK_PayrollJournalLine_PayCyclePeriod] FOREIGN KEY([PayCycleCode], [PayCycleTerm], [PayCyclePeriod])
REFERENCES [dbo].[PayCyclePeriod] ([PayCycleCode], [PayCycleTerm], [Period])
GO

ALTER TABLE [dbo].[PayrollJournalLine] CHECK CONSTRAINT [FK_PayrollJournalLine_PayCyclePeriod]
GO


