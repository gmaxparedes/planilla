use Planilla
go


--alter table [dbo].[PayrollReportingAuthority] alter column Code nvarchar(20)
alter table [dbo].[PayrollReportingAuthority] alter column [UnemploymentInsurance] bit not null
alter table [dbo].[PayrollReportingAuthority] alter column [County] varchar(10) not null
alter table [dbo].[PayrollReportingAuthority] alter column [DateFilter] Date not null

ALTER TABLE [dbo].[PayrollReportingAuthority] WITH CHECK ADD CONSTRAINT 
[FK_ReportingAuthority_Type] FOREIGN KEY([Type])
REFERENCES [dbo].[PayrollTypeReportingAuthority] ([ID_TYPE])
ALTER TABLE [dbo].[PayrollReportingAuthority] CHECK CONSTRAINT [FK_ReportingAuthority_Type]
GO

ALTER TABLE [dbo].[PayrollReportingAuthority] WITH CHECK ADD CONSTRAINT 
[FK_ReportingAuthority_PayRateEditStatus] FOREIGN KEY([EditStatus])
REFERENCES [dbo].[PayRateEditStatus] ([ID_EDIT])
ALTER TABLE [dbo].[PayrollReportingAuthority] CHECK CONSTRAINT [FK_ReportingAuthority_PayRateEditStatus]
GO

ALTER TABLE [dbo].[PayrollReportingAuthority] WITH CHECK ADD CONSTRAINT 
[FK_ReportingAuthority_PayrollPostType] FOREIGN KEY([GLPostTypeFilter])
REFERENCES [dbo].[PayrollPostType] ([ID_POST])
ALTER TABLE [dbo].[PayrollReportingAuthority] CHECK CONSTRAINT [FK_ReportingAuthority_PayrollPostType]
GO

ALTER TABLE [dbo].[PayrollReportingAuthority] WITH CHECK ADD CONSTRAINT 
[FK_ReportingAuthority_PayrollControlType] FOREIGN KEY([PayrollControlTypeFilter])
REFERENCES [dbo].[PayrollControlType] ([ID_TYPE])
ALTER TABLE [dbo].[PayrollReportingAuthority] CHECK CONSTRAINT [FK_ReportingAuthority_PayrollControlType]
GO
