﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ControlPlanilla.Models
{
    public partial class TIPO_MANO_OBRA_EMPRESAView
    {
        public int ID_MANO_OBRA { get; set; }
        public string CD_EMPRESA { get; set; }
        public string DESCRIPCION { get; set; }
        public string USUA_CREA { get; set; }
        public System.DateTime FECH_CREA { get; set; }
        public string USUA_ACTU { get; set; }
        public Nullable<System.DateTime> FECH_ACTU { get; set; }

        public virtual EMPRESAS EMPRESAS { get; set; }
    }
}