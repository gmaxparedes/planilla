﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ControlPlanilla.Models
{
    public class SubMenusViewModels
    {
        //b.DS_NOMBRE_FORMA, c.DS_DESC_MENU, b.DS_LINK_URL, b.CD_CODI_OPCI_SIST, b.CD_INDI_REFRE_CAMP

        public string DS_NOMBRE_FORMA {get;set;}
        public string DS_DESC_MENU { get;set;}
        public string DS_LINK_URL { get;set;}
        public string CD_CODI_OPCI_SIST { get;set;}
        public string CD_INDI_REFRE_CAMP { get;set;}

        public string CD_CODI_SIST { get; set; }
        public string DS_CODI_SIST { get; set; }
    }
}