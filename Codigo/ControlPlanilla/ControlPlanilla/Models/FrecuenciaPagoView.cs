﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ControlPlanilla.Models
{
    public class FrecuenciaPagoView
    {
        public int PayFrequency { get; set; }
        [StringLength(15)]
        [Display(Name = "Frecuencia", Description = "Frecuencia de pago")]
        public string Description { get; set; }

    }
}