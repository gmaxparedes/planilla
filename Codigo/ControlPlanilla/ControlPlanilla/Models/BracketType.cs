//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ControlPlanilla.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class BracketType
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public BracketType()
        {
            this.Bracket = new HashSet<Bracket>();
            this.MethodStep = new HashSet<MethodStep>();
            this.PayrollCalcMethodLine = new HashSet<PayrollCalcMethodLine>();
        }
    
        public string Code { get; set; }
        public string Description { get; set; }
        public Nullable<int> BracketPageID { get; set; }
        public bool UsesMultipleBracketLines { get; set; }
        public bool UsesFilingStatus { get; set; }
        public bool UsesPayFrequency { get; set; }
        public bool UsesAmountOver { get; set; }
        public bool UsesLimit { get; set; }
        public bool UsesTaxPercent { get; set; }
        public bool UsesPercent { get; set; }
        public bool UsesQuantity { get; set; }
        public bool UsesTaxAmount { get; set; }
        public bool UsesAmount { get; set; }
        public bool UsesRateMultiple { get; set; }
        public bool UsesMaxWH { get; set; }
        public bool UsesMinAmount { get; set; }
        public bool UsesMaxAmount { get; set; }
        public bool UsesPerAllowance { get; set; }
        public bool UsesFromAllowance { get; set; }
        public bool UsesMatchPercent { get; set; }
        public bool UsesMaxPercent { get; set; }
        public bool UsesActivationCode { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Bracket> Bracket { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MethodStep> MethodStep { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PayrollCalcMethodLine> PayrollCalcMethodLine { get; set; }
    }
}
