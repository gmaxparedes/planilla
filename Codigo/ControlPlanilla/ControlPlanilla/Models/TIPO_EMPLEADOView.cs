﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ControlPlanilla.Models
{
    public partial class TIPO_EMPLEADOview
    {
        public int ID_TIPO_EMPLEADO { get; set; }
        public string NOMBRE_TIPO { get; set; }
        public string USUA_CREA { get; set; }
        public System.DateTime FECH_CREA { get; set; }
        public string USUA_ACTU { get; set; }
        public Nullable<System.DateTime> FECH_ACTU { get; set; }
    }
}