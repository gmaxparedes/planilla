﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ControlPlanilla.Models
{
    public class PayCycleView
    {
        [StringLength(10)]
        [Display(Name = "Codigo", Description = "Codigo de ciclo de pago")]
        public string Code { get; set; }

        [StringLength(50)]
        [Display(Name = "Descripcion", Description = "Descripcion")]
        public string Description { get; set; }


        [Display(Name = "Frecuencia", Description = "Frecuencia de pago")]
        public int PayFrequency { get; set; }
        public IEnumerable<FrecuenciaPagoView> FrecuenciaPagoViews { get; set; }

        [Display(Name = "Retraso", Description = "Retraso en el pago")]
        public int PaymentDelay { get; set; }

        [Display(Name = "Anualización", Description = "Factor de anualización")]
        public int AnnualizingFactor { get; set; }

        [Display(Name = "Mensualidad", Description = "Factor mensual")]
        public decimal MonthlyFactor { get; set; }

        [Display(Name = "Formato", Description = "Formato")]
        public string Format { get; set; }

        [Display(Name = "Grupo Cntls. Tipo Empleado", Description = "Grupo Cntls segun tipo de empleado")]
        public bool Caso1 { get; set; }
        [Display(Name = "Grupo Cntls. Clase Empleado", Description = "Grupo Cntls segun clase de empleado")]
        public bool Caso2 { get; set; }

        [Display(Name = "Grupo Cntls. AFPs Empleado", Description = "Grupo Cntls segun AFP de empleado")]
        public bool Caso3 { get; set; }
    }
}