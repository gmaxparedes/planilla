﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ControlPlanilla.Models
{
    public class ListaMenusSubMenus
    {
        public IList<MenusViewModels> Menus { get; set; }
        public IList<SubMenusViewModels> SubMenus { get; set; }
    }
}