﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ControlPlanilla.Models
{
    public partial class PayrollPostingGroupView
    {
        public string Code { get; set; }
        public string EarningsAccount { get; set; }
        public string BankAccountNo { get; set; }
        public string LiabilityAccount { get; set; }
        public string ExpenseAccount { get; set; }
        public Nullable<System.DateTime> DateFilter { get; set; }
        public Nullable<byte> DetailExists { get; set; }
        public Nullable<System.DateTime> CurrentEffectiveDate { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PayrollControl> PayrollControl { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PayrollControlGroupControl> PayrollControlGroupControl { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PayrollPostingGroupDetail> PayrollPostingGroupDetail { get; set; }
    }
}