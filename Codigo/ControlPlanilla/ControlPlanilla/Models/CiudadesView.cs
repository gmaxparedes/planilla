﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ControlPlanilla.Models
{
    public class CiudadesView
    {
        public int ID_CIUDAD { get; set; }
        public string NOMBRE_CIUDAD { get; set; }
    }
}