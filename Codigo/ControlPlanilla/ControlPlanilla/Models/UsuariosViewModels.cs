﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations.Schema;

namespace ControlPlanilla.Models
{
    public class UsuariosViewModels
    {

        [Display(Name = "Código de Usuario", Description = "CD_CODIGO_USUARIO")]
        public string CD_CODIGO_USUARIO { get; set; }
        [Required]
        [Display(Name = "Nombre de Usuario", Description = "DS_NOMBRE_USUARIO")]
        public string DS_NOMBRE_USUARIO { get; set; }
        [Required]
        [Display(Name = "Clave de Usuario", Description = "CD_CLAVE_USUARIO")]
        public string CD_CLAVE_USUARIO { get; set; }

        [Required]
        [RegularExpression("^[a-zA-Z0-9_\\.-]+@([a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$", ErrorMessage = "E-mail no es válido")]
        //[DataType(DataType.EmailAddress, ErrorMessage = "E-mail no es válido")]
        [Display(Name = "Correo Electrónico", Description = "CD_DIRE_EMAIL")]

        public string CD_DIRE_EMAIL { get; set; }
        [Required]
        [Display(Name = "Estado", Description = "CD_ESTADO")]
        public string CD_ESTADO { get; set; }
        [Required]
        [Display(Name = "Empresa ", Description = "CD_EMPRESA_DEFECTO")]
        public string CD_EMPRESA_DEFECTO { get; set; }
    }
}