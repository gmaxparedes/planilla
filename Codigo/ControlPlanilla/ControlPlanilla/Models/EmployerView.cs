﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ControlPlanilla.Models
{
    public partial class EmployerView
    {
        public string No { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string County { get; set; }
        public string CountryRegionCode { get; set; }
        public string PostCode { get; set; }
        public string PhoneNo { get; set; }
        public string FaxNo { get; set; }
        public string NetPayPostingGroup { get; set; }
        public string HomePage { get; set; }
        public Nullable<byte> Blocked { get; set; }
        public string W2ContactName { get; set; }
        public string W2ContactPhoneNo { get; set; }
        public string W2Email { get; set; }
        public string W2FaxNo { get; set; }
        public string T4ContactName { get; set; }
        public string T4ContactPhoneNo { get; set; }
        public string T4ContactEmail { get; set; }
        public string ROEContactName { get; set; }
        public string ROEContactPhoneNo { get; set; }
        public string ROESerialNos { get; set; }
        public string T4EmploymentCode { get; set; }
        public string T4Proprietor1SIN_ { get; set; }
        public string T4Proprietor2SIN_ { get; set; }
        public Nullable<int> Employees { get; set; }
        public Nullable<decimal> Amount { get; set; }
        public Nullable<decimal> TaxableAmount { get; set; }
        public Nullable<System.DateTime> DateFilter { get; set; }
        public string GblDim1Filter { get; set; }
        public string GblDim2Filter { get; set; }
        public string EmpFilter { get; set; }
        public string CtrlFilter { get; set; }
        public Nullable<int> CtrlTypeFilter { get; set; }
        public string CtrlNameFilter { get; set; }
        public Nullable<int> RATypeFilter { get; set; }
        public string RACodeFilter { get; set; }
        public Nullable<int> GLPostTypeFilter { get; set; }
        public string WorkRACodeFilter { get; set; }
        public string WorkTypeFilter { get; set; }
        public Nullable<int> LdgrEntFilter { get; set; }
        public Nullable<System.DateTime> PPStartDateFilter { get; set; }
        public Nullable<System.DateTime> PPEndDateFilter { get; set; }
        public string CycleCodeFilter { get; set; }
        public string CycleTermFilter { get; set; }
        public Nullable<int> CyclePeriodFilter { get; set; }
        public Nullable<System.DateTime> PayDateFilter { get; set; }
        public string CountyFilter { get; set; }
        public string LocalityFilter { get; set; }
        public Nullable<System.DateTime> DateWorkedFilter { get; set; }
        public string RL1NEQ { get; set; }
        public Nullable<System.DateTime> LastDateModified { get; set; }
        public Nullable<byte> Comment { get; set; }
        public string HQOfficeLocation { get; set; }
        public string EEOCompanyNo { get; set; }
        public Nullable<byte> EEOQuestionC2 { get; set; }
        public Nullable<byte> EEOQuestionC3 { get; set; }
        public string EEODunBradstreetIDNo { get; set; }
        public Nullable<int> StatusFilter { get; set; }
        public Nullable<int> W2KindofEmployer { get; set; }
        public Nullable<byte> C3rdPartyDesignee { get; set; }
        public Nullable<int> DesigneePin { get; set; }
        public string DesigneeContactName { get; set; }
        public string DesigneeContactPhoneNo { get; set; }
        public string ACAContactName { get; set; }
        public string ACAContactPhoneNo { get; set; }
        public string ACADesignatedGovEntity { get; set; }
        public string ACAContactTitle { get; set; }
        public string ACAContactEmail { get; set; }
        public string ACAContactFaxNo { get; set; }
        public string ROEFolder { get; set; }
        public string CodIncapacidad { get; set; }
        public Nullable<int> DiaIncap { get; set; }
        public string SocialSecurityNo { get; set; }
        public Nullable<int> CorrelIHSSDoc { get; set; }
        public string CodRapEmpresa { get; set; }
        public string Codigoempresa { get; set; }
        public string EPH { get; set; }
        public string EPHCT { get; set; }
        public string RTN { get; set; }
        public string Emailcompany { get; set; }
        public string NombredeContactoMPAL { get; set; }
        public byte[] LogoMunicipalidad { get; set; }
        public string NombreMunicipalidad { get; set; }
        public string NoPatronalRAP { get; set; }
        public string TwitterUsername { get; set; }
        public string CodigoVacacion { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Employee> Employee { get; set; }
    }
}