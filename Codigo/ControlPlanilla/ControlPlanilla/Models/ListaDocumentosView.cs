﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ControlPlanilla.Models
{
    public class ListaDocumentosView
    {
        public int Id { get; set; }
        public string OpcionSistema { get; set; }
        public string DsOpcionSistema { get; set; }
        public string CdForaneo { get; set; }
        public string DsForaneo { get; set; }
        public string CdTipoDocumento { get; set; }
        public string DsTipoDocumento { get; set; }
        public string DsDocumento { get; set;}
        public byte[] Archivo { get; set; }
        public DateTime Desde { get; set; }
        public DateTime? Hasta { get; set; }

    }
}