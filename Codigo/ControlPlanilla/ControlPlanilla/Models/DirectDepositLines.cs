//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ControlPlanilla.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class DirectDepositLines
    {
        public string DirectDepositLayoutCode { get; set; }
        public int SectionType { get; set; }
        public int SectionLineNumber { get; set; }
        public int LineNumber { get; set; }
        public string Description { get; set; }
        public Nullable<int> StartPosition { get; set; }
        public Nullable<int> LengthtoExport { get; set; }
        public Nullable<int> TypetoExport { get; set; }
        public Nullable<int> LineType { get; set; }
        public string LiteralValue { get; set; }
        public Nullable<int> TableNo { get; set; }
        public string TableName { get; set; }
        public Nullable<int> FieldNo { get; set; }
        public string FieldName { get; set; }
        public string FieldTypeandLength { get; set; }
        public Nullable<int> Justification { get; set; }
        public string FillCharacter { get; set; }
        public bool ConverttoUpperCase { get; set; }
        public bool RemoveSpaces { get; set; }
        public bool RemoveSpecialCharacters { get; set; }
        public bool IncludeDecimal { get; set; }
        public Nullable<int> DateFormat { get; set; }
        public Nullable<int> TimeFormat { get; set; }
        public Nullable<int> FieldStartPosition { get; set; }
        public Nullable<int> FieldLength { get; set; }
        public bool RequiredField { get; set; }
        public bool CheckDigit { get; set; }
        public bool UpdateTotalFileDebitAmt { get; set; }
        public bool UpdateTotalFileCreditAmt { get; set; }
        public bool UpdateHashAmount1 { get; set; }
        public bool UpdateHashAmount2 { get; set; }
        public bool UpdateBatchDebitAmt { get; set; }
        public bool UpdateBatchCreditAmt { get; set; }
        public bool UpdateBatchHashAmt1 { get; set; }
        public bool UpdateBatchHashAmt2 { get; set; }
    
        public virtual DirectDepositDateFormat DirectDepositDateFormat { get; set; }
        public virtual DirectDepositLineType DirectDepositLineType { get; set; }
        public virtual DirectDepositTimeFormat DirectDepositTimeFormat { get; set; }
        public virtual DirectDepositTypeJustification DirectDepositTypeJustification { get; set; }
        public virtual DirectDepositTypeToExport DirectDepositTypeToExport { get; set; }
        public virtual DirectDepositSections DirectDepositSections { get; set; }
    }
}
