//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ControlPlanilla.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class GLB_MENUS_SISTEMAS
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public GLB_MENUS_SISTEMAS()
        {
            this.GLB_CONF_OPCI_SIST = new HashSet<GLB_CONF_OPCI_SIST>();
        }
    
        public string CD_CODI_MENU { get; set; }
        public string CD_EMPRESA { get; set; }
        public string DS_DESC_MENU { get; set; }
        public bool CD_ESTADO_MENU { get; set; }
        public string USUA_CREA { get; set; }
        public System.DateTime FECH_CREA { get; set; }
        public string USUA_ACTU { get; set; }
        public Nullable<System.DateTime> FECH_ACTU { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<GLB_CONF_OPCI_SIST> GLB_CONF_OPCI_SIST { get; set; }
    }
}
