﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ControlPlanilla.Models
{
    public class PaisesViewModels
    {
        public string CD_PAIS { get; set; }
        public string DS_PAIS { get; set; }
    }
}