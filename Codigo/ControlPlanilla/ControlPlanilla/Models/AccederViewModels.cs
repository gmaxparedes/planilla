﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ControlPlanilla.Models
{
    public class AccederViewModels
    {
        [Required]
        [Display(Name ="Usuario", Description ="Codigo de usuario")]
        public string CD_CODIGO_USUARIO { get; set; }

        [Required]
        [Display(Name = "Clave", Description = "Clave de usuario")]
        public string CD_CLAVE_USUARIO { get; set; }
        
        public IEnumerable<PaisesViewModels> PAISES { get; set; }
        [Required]
        public string SelectedPais { get; set; }

        public IEnumerable<EmpresasView> Empresas { get; set; }
        [Required]
        public string SelectedEmpresa { get; set; }
    }
}