﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ControlPlanilla.Models
{
    public partial class DEP_EMPRESASView
    {
        public int ID_DEPTO { get; set; }
        public int ID_AREA { get; set; }
        public string CD_EMPRESA { get; set; }
        public string DESCRIPCION { get; set; }
        public string USUA_CREA { get; set; }
        public System.DateTime FECH_CREA { get; set; }
        public string USUA_ACTU { get; set; }
        public Nullable<System.DateTime> FECH_ACTU { get; set; }

        public virtual AREAS_EMPRESA AREAS_EMPRESA { get; set; }
        public virtual EMPRESAS EMPRESAS { get; set; }
    }
}