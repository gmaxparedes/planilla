﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ControlPlanilla.Models
{
    public partial class EmployeeView
    {
        public string No { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Initials { get; set; }
        public string JobTitle { get; set; }
        public string SearchName { get; set; }
        public string Address { get; set; }
        public string Address2 { get; set; }
        public Nullable<int> City { get; set; }
        public string PostCode { get; set; }
        public string PhoneNo { get; set; }
        public string MobilePhoneNo { get; set; }
        public string EMail { get; set; }
        public Nullable<System.DateTime> BirthDate { get; set; }
        public string SocialSecurityNo { get; set; }
        public Nullable<int> Gender { get; set; }
        public string CountryRegionCode { get; set; }
        public string ManagerNo { get; set; }
        public string EmplymtContractCode { get; set; }
        public Nullable<System.DateTime> EmploymentDate { get; set; }
        public Nullable<int> Status { get; set; }
        public string CauseofInactivityCode { get; set; }
        public Nullable<System.DateTime> TerminationDate { get; set; }
        public Nullable<System.DateTime> LastDateModified { get; set; }
        public string Extension { get; set; }
        public string CompanyEMail { get; set; }
        public string ProfesiónOficio { get; set; }
        public Nullable<int> DevengahorasExtras { get; set; }
        public Nullable<int> TipodeManodeobra { get; set; }
        public Nullable<byte> Comision { get; set; }
        public string ClassCode { get; set; }
        public string EmployerNo { get; set; }
        public string MiddleInitial { get; set; }
        public string Suffix { get; set; }
        public string HomePhoneNo { get; set; }
        public string Race { get; set; }
        public Nullable<byte> Blocked { get; set; }
        public string DefaultWorkTypeCode { get; set; }
        public string PayCycleCode { get; set; }
        public string DefaultRepAuthCode { get; set; }
        public string HRRepNo { get; set; }
        public string EmployeeType { get; set; }
        public Nullable<int> MaritalStatus { get; set; }
        public string OfficeLocation { get; set; }
        public string DUI { get; set; }
        public string NomDUI { get; set; }
        public string TILetras { get; set; }
        public string LugExpTI { get; set; }
        public string ProfDUI { get; set; }
        public string FExpTILetras { get; set; }
        public Nullable<int> Edad { get; set; }
        public Nullable<int> Area { get; set; }
        public Nullable<int> Dpto { get; set; }
        public string DepGasViat { get; set; }
        public string AFps { get; set; }
        public Nullable<int> DistGast { get; set; }
        public Nullable<System.DateTime> FechContAjust { get; set; }
        public string SecondLastName { get; set; }
        public string MarriedLastname { get; set; }
        public string NUP { get; set; }
        public string SocialSecurityname { get; set; }
        public string NIT { get; set; }
        public string NITName { get; set; }
        public string NUPName { get; set; }
        public Nullable<int> IncapAcum { get; set; }
        public Nullable<byte> HrsExtras { get; set; }
        public Nullable<int> Departamento { get; set; }
        public string JornadaLaboral { get; set; }
        public Nullable<int> ID_CARGO { get; set; }
        public Nullable<int> ID_TIPO_EMPLEADO { get; set; }
        public Nullable<int> ID_DVC { get; set; }
        public Nullable<int> ID_DISTRIBUCION { get; set; }
        public Nullable<int> ID_MOV_TERMIN { get; set; }
        public Nullable<int> ID_CONTRATO { get; set; }
        public Nullable<int> ID_HORASEXTRA { get; set; }
        public Nullable<int> ID_AFP { get; set; }

        public virtual EmployeeClass EmployeeClass { get; set; }
        public virtual Employer Employer { get; set; }
        public virtual EmployeeType EmployeeType1 { get; set; }
       
    }
}