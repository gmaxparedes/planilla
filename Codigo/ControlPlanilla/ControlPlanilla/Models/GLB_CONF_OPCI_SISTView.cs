﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ControlPlanilla.Models
{
    public partial class GLB_CONF_OPCI_SISTView
    {
        public string CD_CODI_SIST { get; set; }
        public string CD_CODI_MENU { get; set; }
        public string CD_CODI_OPCI_SIST { get; set; }
        public string CD_EMPRESA { get; set; }
        public int NM_SEQU_ORDE_SIST { get; set; }
        public int NM_SEQU_ORDE_MENU { get; set; }
        public int NM_SEQU_ORDE_OPCI { get; set; }
        public string USUA_CREA { get; set; }
        public System.DateTime FECH_CREA { get; set; }
        public string USUA_ACTU { get; set; }
        public Nullable<System.DateTime> FECH_ACTU { get; set; }
        public Nullable<bool> Estado { get; set; }

    }
}