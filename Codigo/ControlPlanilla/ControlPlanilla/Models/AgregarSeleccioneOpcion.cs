﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ControlPlanilla.Models
{
    public class AgregarSeleccioneOpcion
    {
        public string Text { get; set; }
        public string Value { get; set; }

        public AgregarSeleccioneOpcion()
        {
            this.Text = "Seleccione opción";
            this.Value = null;
        }
        
    }
}