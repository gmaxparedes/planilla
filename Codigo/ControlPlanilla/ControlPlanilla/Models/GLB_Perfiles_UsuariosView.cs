﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ControlPlanilla.Models
{
    public partial class GLB_Perfiles_UsuariosView
    {
        public string CD_CODIGO_PERFIL { get; set; }
        public string CD_EMPRESA { get; set; }
        public string DS_NOMBRE_PERFIL { get; set; }
        public bool CD_ESTADO_PERFIL { get; set; }
        public string USUA_CREA { get; set; }
        public System.DateTime FECH_CREA { get; set; }
        public string USUA_ACTU { get; set; }
        public Nullable<System.DateTime> FECH_ACTU { get; set; }
        public string CD_INDI_FILT_BODEGA { get; set; }
        public string CD_BODEGA_RECEP { get; set; }
    }
}