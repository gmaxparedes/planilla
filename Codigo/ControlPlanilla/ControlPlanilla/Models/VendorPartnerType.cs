//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ControlPlanilla.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class VendorPartnerType
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public VendorPartnerType()
        {
            this.Vendor = new HashSet<Vendor>();
        }
    
        public int ID_PARTNERTYPE { get; set; }
        public string DESCRIPCION { get; set; }
        public string USUA_CREA { get; set; }
        public System.DateTime FECH_CREA { get; set; }
        public string USUA_ACTUA { get; set; }
        public Nullable<System.DateTime> FECH_ACTUA { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Vendor> Vendor { get; set; }
    }
}
