//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ControlPlanilla.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class EmployeePayrollControl
    {
        public string EmployeeNo { get; set; }
        public string PayControlCode { get; set; }
        public Nullable<int> MonthlySchedule { get; set; }
        public Nullable<byte> Active { get; set; }
        public string PayrollPostingGroup { get; set; }
        public Nullable<int> OrderNo { get; set; }
        public Nullable<decimal> Amount { get; set; }
        public Nullable<decimal> TaxableAmount { get; set; }
        public Nullable<System.DateTime> DateFilter { get; set; }
        public string GlobalDimension1Filter { get; set; }
        public string GlobalDimension2Filter { get; set; }
        public string CountyFilter { get; set; }
        public string LocalityFilter { get; set; }
        public Nullable<int> GLPostTypeFilter { get; set; }
        public Nullable<int> PayrollLedgEntryNoFilter { get; set; }
        public string WorkTypeFilter { get; set; }
        public Nullable<int> RepAuthTypeFilter { get; set; }
        public string RepAuthCodeFilter { get; set; }
        public Nullable<int> PayControlType { get; set; }
        public Nullable<byte> Calculate { get; set; }
        public string Description { get; set; }
    
        public virtual Employee Employee { get; set; }
    }
}
