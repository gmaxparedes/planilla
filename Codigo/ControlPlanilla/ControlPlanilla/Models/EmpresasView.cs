﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ControlPlanilla.Models
{
    public class EmpresasView
    {
        public string CD_EMPRESA { get; set; }
        public string DS_EMPRESA { get; set; }
    }
}