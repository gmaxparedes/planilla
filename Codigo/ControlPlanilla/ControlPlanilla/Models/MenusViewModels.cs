﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ControlPlanilla.Models
{
    public class MenusViewModels
    {
        public string DS_DESC_MENU { get;set;}
        public string CD_CODI_SIST { get; set; }
        public string DS_CODI_SIST { get; set; }
    }
}