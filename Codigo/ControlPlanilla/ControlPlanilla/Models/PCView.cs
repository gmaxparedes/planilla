﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ControlPlanilla.Models
{
    public partial class PCView
    {
        public int Id_Codigo { get; set; }
        public string Codigo_Postal { get; set; }
        public int ID_CIUDAD { get; set; }
        public string USUA_CREA { get; set; }
        public System.DateTime FECH_CREA { get; set; }
        public string USUA_ACTU { get; set; }
        public Nullable<System.DateTime> FECH_ACTU { get; set; }

        public virtual CIUDADES CIUDADES { get; set; }
    }
}