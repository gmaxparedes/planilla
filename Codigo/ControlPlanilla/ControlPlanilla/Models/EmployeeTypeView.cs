﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ControlPlanilla.Models
{
    public partial class EmployeeTypeView
    {
        public string Code { get; set; }
        public string Description { get; set; }
        public Nullable<byte> TestSystemRequiredEntries { get; set; }
        public Nullable<byte> TestUserRequiredEntries { get; set; }
        public Nullable<byte> SynchRequired { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Employee> Employee { get; set; }
    }
}