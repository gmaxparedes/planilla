﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ControlPlanilla.Models
{
    public class DepartamentosView
    {
        public int ID_DEPTO { get; set; }
        public string NOMBRE_DEPTO { get; set; }
    }
}