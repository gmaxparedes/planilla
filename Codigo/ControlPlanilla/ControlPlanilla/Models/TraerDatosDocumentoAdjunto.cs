﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ControlPlanilla.Models
{
    public class TraerDatosDocumentoAdjunto
    {
        public int Id { get; set; }
        public string CD_OPCI_SIST { get; set; }
        public string DS_OPCI_SIST { get; set; }
        public string CD_FORANEO { get; set; }
        public string DS_FORANEO { get; set; }
        public string CD_TIPO_DOCUMENTO { get; set; }
        public string TIPO_CONTENIDO { get; set; }
        public string DS_DOCUMENTO { get; set; }
        public DateTime VIGENCIA_DESDE { get; set; }
        public DateTime? VIGENCIA_HASTA { get; set; }
    }
}