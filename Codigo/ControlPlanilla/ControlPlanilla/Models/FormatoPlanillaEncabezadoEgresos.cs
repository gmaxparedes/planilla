//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ControlPlanilla.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class FormatoPlanillaEncabezadoEgresos
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public FormatoPlanillaEncabezadoEgresos()
        {
            this.FormatoPlanillaDetalleEgresos = new HashSet<FormatoPlanillaDetalleEgresos>();
        }
    
        public string ID_CABECERA { get; set; }
        public string ID_FORMATO { get; set; }
        public int CHECK { get; set; }
        public string USUA_CREA { get; set; }
        public System.DateTime FECH_CREA { get; set; }
        public string USUA_ACTUA { get; set; }
        public Nullable<System.DateTime> FECH_ACTUA { get; set; }
    
        public virtual FormatoPlanilla FormatoPlanilla { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FormatoPlanillaDetalleEgresos> FormatoPlanillaDetalleEgresos { get; set; }
    }
}
