﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ControlPlanilla.Models
{
    public partial class GLB_OPCIONES_SISTEMAView
    {
        public string CD_CODI_OPCI_SIST { get; set; }
        public string CD_EMPRESA { get; set; }
        public string DS_CODI_OPCI_SIST { get; set; }
        public string DS_NOMBRE_FORMA { get; set; }
        public string CD_INDI_REFRE_CAMP { get; set; }
        public string DS_LINK_URL { get; set; }
        public string CD_INDI_NECE_EMPR { get; set; }
        public string CD_INDI_NECE_PERI_CONT { get; set; }
        public string CD_INDI_VALI_PERI_CONT { get; set; }
        public bool CD_ESTADO_OPCION_SISTEMA { get; set; }
        public string USUA_CREA { get; set; }
        public System.DateTime FECH_CREA { get; set; }
        public string USUA_ACTU { get; set; }
        public Nullable<System.DateTime> FECH_ACTU { get; set; }
        public string CD_INDI_VALI_PERI_LOGI { get; set; }

    }
}