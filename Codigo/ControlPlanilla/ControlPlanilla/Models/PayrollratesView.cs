﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ControlPlanilla.Models
{ 
    public partial class PayrollratesView
    {
        public string Code { get; set; }
        public string Description { get; set; }
        public Nullable<int> EditStatus { get; set; }
        public string UnitofMeasureCode { get; set; }
        public Nullable<byte> BorrarPosteo { get; set; }

        public virtual PayRateEditStatus PayRateEditStatus { get; set; }

    }
}