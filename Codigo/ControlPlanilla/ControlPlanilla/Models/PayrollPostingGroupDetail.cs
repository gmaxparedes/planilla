//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ControlPlanilla.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class PayrollPostingGroupDetail
    {
        public string Code { get; set; }
        public System.DateTime EffectiveDate { get; set; }
        public string EarningsAccount { get; set; }
        public string BankAccountNo { get; set; }
        public string LiabilityAccount { get; set; }
        public string ExpenseAccount { get; set; }
    
        public virtual PayrollPostingGroup PayrollPostingGroup { get; set; }
    }
}
