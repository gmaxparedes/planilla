﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ControlPlanilla.Models
{
    public class VerificarCodigo
    {
        public string CD_CODIGO_USUARIO { get; set; }
        [Required]
        [Display(Description ="Ingrese código de validción", Name ="Código de validación")]
        public string CodigoGeneraddo { get; set; }
        
    }
}