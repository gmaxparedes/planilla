//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ControlPlanilla.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class GLB_TIPOS_DE_IMPUESTOS
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public GLB_TIPOS_DE_IMPUESTOS()
        {
            this.PayrollControl = new HashSet<PayrollControl>();
        }
    
        public string CD_CODIGO_IMPUESTO { get; set; }
        public string CD_PAIS { get; set; }
        public string CD_EMPRESA { get; set; }
        public string DS_DESCRIPCION_IMPUESTO { get; set; }
        public decimal VL_PORCENTAJE_1 { get; set; }
        public Nullable<decimal> VL_PORCENTAJE_2 { get; set; }
        public string USUA_CREA { get; set; }
        public System.DateTime FECH_CREA { get; set; }
        public string USUA_ACTU { get; set; }
        public Nullable<System.DateTime> FECH_ACTU { get; set; }
        public string CD_CUENTA_CONTABLE { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PayrollControl> PayrollControl { get; set; }
        public virtual EMPRESAS EMPRESAS { get; set; }
        public virtual PAISES PAISES { get; set; }
    }
}
