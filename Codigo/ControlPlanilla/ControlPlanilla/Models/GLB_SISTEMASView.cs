﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ControlPlanilla.Models
{
    public partial class GLB_SISTEMASView
    {
        public string CD_CODI_SIST { get; set; }
        public string CD_EMPRESA { get; set; }
        public string DS_NOMB_SIST { get; set; }
        public Nullable<bool> CD_ESTADO_SISTEMA { get; set; }
        public string USUA_CREA { get; set; }
        public System.DateTime FECH_CREA { get; set; }
        public string USUA_ACTU { get; set; }
        public Nullable<System.DateTime> FECH_ACTU { get; set; }

    }
}