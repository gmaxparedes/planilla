﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ControlPlanilla.Models;

namespace ControlPlanilla.Controllers
{
    public class PayrollTypeReportingAuthoritiesController : AuditoriaController
    {
        // GET: PayrollTypeReportingAuthorities
        public ActionResult Index()
        {
            return View(dbPayControl.PayrollTypeReportingAuthority.ToList());
        }

        // GET: PayrollTypeReportingAuthorities/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PayrollTypeReportingAuthority payrollTypeReportingAuthority = dbPayControl.PayrollTypeReportingAuthority.Find(id);
            if (payrollTypeReportingAuthority == null)
            {
                return HttpNotFound();
            }
            return View(payrollTypeReportingAuthority);
        }

        // GET: PayrollTypeReportingAuthorities/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: PayrollTypeReportingAuthorities/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID_TYPE,DESCRIPCION")] PayrollTypeReportingAuthority payrollTypeReportingAuthority)
        {
            if (ModelState.IsValid)
            {
                PayrollTypeReportingAuthority guardar = new PayrollTypeReportingAuthority();
                guardar.DESCRIPCION = payrollTypeReportingAuthority.DESCRIPCION;
                guardar.USUA_CREA = Usuario;
                guardar.FECH_CREA = Ahora;
                
                dbPayControl.PayrollTypeReportingAuthority.Add(guardar);
                dbPayControl.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(payrollTypeReportingAuthority);
        }

        // GET: PayrollTypeReportingAuthorities/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PayrollTypeReportingAuthority payrollTypeReportingAuthority = dbPayControl.PayrollTypeReportingAuthority.Find(id);
            if (payrollTypeReportingAuthority == null)
            {
                return HttpNotFound();
            }
            return View(payrollTypeReportingAuthority);
        }

        // POST: PayrollTypeReportingAuthorities/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID_TYPE,DESCRIPCION")] PayrollTypeReportingAuthority payrollTypeReportingAuthority)
        {
            if (ModelState.IsValid)
            {
                PayrollTypeReportingAuthority guardar = dbPayControl.PayrollTypeReportingAuthority.Find(payrollTypeReportingAuthority.ID_TYPE);
                guardar.DESCRIPCION = payrollTypeReportingAuthority.DESCRIPCION;
                guardar.USUA_ACTUA = Usuario;
                guardar.FECH_ACTUA = Ahora;
                dbPayControl.Entry(guardar).State = EntityState.Modified;
                dbPayControl.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(payrollTypeReportingAuthority);
        }

        // GET: PayrollTypeReportingAuthorities/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PayrollTypeReportingAuthority payrollTypeReportingAuthority = dbPayControl.PayrollTypeReportingAuthority.Find(id);
            if (payrollTypeReportingAuthority == null)
            {
                return HttpNotFound();
            }
            return View(payrollTypeReportingAuthority);
        }

        // POST: PayrollTypeReportingAuthorities/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            PayrollTypeReportingAuthority payrollTypeReportingAuthority = dbPayControl.PayrollTypeReportingAuthority.Find(id);
            dbPayControl.PayrollTypeReportingAuthority.Remove(payrollTypeReportingAuthority);
            dbPayControl.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                dbPayControl.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
