﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ControlPlanilla.Models;
using PagedList;


namespace ControlPlanilla.Controllers
{
    public class GlobalFunctions : AuditoriaController
    {


        private PlanillaPayControls db = new PlanillaPayControls();

        public decimal MethodStep1(string JournalTemplateName, int LineNumber) {
            decimal Adjusted_Amount = 0;
            if (ModelState.IsValid)
            {
                PayrollJournalLine Proll = db.PayrollJournalLine.Find (JournalTemplateName, LineNumber);
                Adjusted_Amount = (decimal)Proll.TaxableAmount;
            }
            return Adjusted_Amount;
        }
        public decimal MethodStep2(string JournalTemplateName, int LineNumber)
        {
            decimal Adjusted_Amount = 0;
            if (ModelState.IsValid)
            {
                PayrollJournalLine Proll = db.PayrollJournalLine.Find(JournalTemplateName, LineNumber);
                Adjusted_Amount = (decimal)Proll.AdjustedAmount;
            }
            return Adjusted_Amount;
        }
        public decimal MethodStep4(string JournalTemplateName, int LineNumber)
        {
            decimal Taxable_Amount = 0;
            if (ModelState.IsValid)
            {
                PayrollJournalLine Proll = db.PayrollJournalLine.Find(JournalTemplateName, LineNumber);
                Taxable_Amount = (decimal)Proll.PayrollAmount;
            }
            return Taxable_Amount;
        }
        public decimal MethodStep5(string JournalTemplateName, int LineNumber)
        {
            decimal Taxable_Amount = 0;
            if (ModelState.IsValid)
            {
                PayrollJournalLine Proll = db.PayrollJournalLine.Find(JournalTemplateName, LineNumber);
                Taxable_Amount = (decimal)Proll.AdjustedAmount;
            }
            return Taxable_Amount;
        }
        public decimal MethodStep6(string JournalTemplateName, int LineNumber)
        {
            decimal Taxable_Amount = 0;
            if (ModelState.IsValid)
            {
                PayrollJournalLine Proll = db.PayrollJournalLine.Find(JournalTemplateName, LineNumber);
                Taxable_Amount = (decimal)Proll.TaxableAmount;
            }
            return Taxable_Amount;
        }
        public decimal MethodStep7(string JournalTemplateName, int LineNumber)
        {
            decimal Taxable_Amount = 0;
            if (ModelState.IsValid)
            {
                PayrollJournalLine Proll = db.PayrollJournalLine.Find(JournalTemplateName, LineNumber);
                Taxable_Amount = (decimal)Proll.AdjustedAmount;
            }
            return Taxable_Amount;
        }
        public decimal MethodStep11(string JournalTemplateName, int LineNumber)
        {
            decimal Taxable_Amount = 0;
            if (ModelState.IsValid)
            {
                PayrollJournalLine Proll = db.PayrollJournalLine.Find(JournalTemplateName, LineNumber);
                Taxable_Amount = (decimal)Proll.TaxableAmount;
            }
            return Taxable_Amount;
        }
        public decimal MethodStep14(string JournalTemplateName, int LineNumber)
        {
            decimal Taxable_Amount = 0;
            if (ModelState.IsValid)
            {
                PayrollJournalLine Proll = db.PayrollJournalLine.Find(JournalTemplateName, LineNumber);
                Taxable_Amount = (decimal)Proll.TaxableAmount;
            }
            return Taxable_Amount;
        }
        public decimal MethodStep15(string JournalTemplateName, int LineNumber)
        {
            decimal Taxable_Amount = 0;
            if (ModelState.IsValid)
            {
                PayrollJournalLine Proll = db.PayrollJournalLine.Find(JournalTemplateName, LineNumber);
                Taxable_Amount = (-1) * (decimal)Proll.PayrollAmount;
            }
            return Taxable_Amount;
        }
        public Boolean MethodStep23(string JournalTemplateName, int LineNumber)
        {
            decimal Taxable_Amount = 0;
            Boolean resp = false;
            if (ModelState.IsValid)
            {
                decimal BaseAmount = Convert.ToDecimal(db.Database.SqlQuery<Decimal>("select dbo.[GetEmployeeRate] ('" + JournalTemplateName + "'," + LineNumber.ToString() + ") "));
                PayrollJournalLine Proll = db.PayrollJournalLine.Find(JournalTemplateName, LineNumber);
                Taxable_Amount = (decimal)Proll.TaxableAmount+BaseAmount;
                if (Taxable_Amount == 0)
                    resp = true;
            }
            return resp;
        }
        public decimal MethodStep22(string No, string codefilter, string idtemplate)
        {
            decimal Taxable_Amount = 0;
            Boolean resp = false;
            if (ModelState.IsValid)
            {
                decimal BaseAmount = db.Database.SqlQuery<Decimal>("select dbo.[GetEmployeeRate] ('" + idtemplate + "','" + No + "','" + codefilter + "' ) ").Single();
                Taxable_Amount =  BaseAmount;

                return  Taxable_Amount;
            }
            return Taxable_Amount;
        }

        public decimal MethodStep26(string JournalTemplateName, int LineNumber)
        {
            decimal Taxable_Amount = 0;
            if (ModelState.IsValid)
            {
                PayrollJournalLine Proll = db.PayrollJournalLine.Find(JournalTemplateName, LineNumber);
                Taxable_Amount = (decimal)Proll.PayrollAmount + (decimal)Proll.TaxableAmount;
            }
            return Taxable_Amount;
        }
        public decimal MethodStep27(string JournalTemplateName, int LineNumber)
        {
            decimal Taxable_Amount = 0;
            if (ModelState.IsValid)
            {
                PayrollJournalLine Proll = db.PayrollJournalLine.Find(JournalTemplateName, LineNumber);
                Taxable_Amount = (decimal)Proll.PayrollAmount + (decimal)Proll.AdjustedAmount;
            }
            return Taxable_Amount;
        }
        public decimal MethodStep29(string JournalTemplateName, int LineNumber)
        {
            decimal Taxable_Amount = 0;
            if (ModelState.IsValid)
            {
                PayrollJournalLine Proll = db.PayrollJournalLine.Find(JournalTemplateName, LineNumber);
                Taxable_Amount = (decimal)Proll.TaxableAmount + (decimal)Proll.AdjustedAmount;
            }
            return Taxable_Amount;
        }
        public decimal MethodStep30(string JournalTemplateName, int LineNumber)
        {
            decimal Taxable_Amount = 0;
            if (ModelState.IsValid)
            {
                PayrollJournalLine Proll = db.PayrollJournalLine.Find(JournalTemplateName, LineNumber);
                Taxable_Amount = (decimal)Proll.PayrollAmount + (decimal)Proll.AdjustedAmount;
            }
            return Taxable_Amount;
        }
        public decimal MethodStep32(string JournalTemplateName, int LineNumber)
        {
            decimal Taxable_Amount = 0;
            return Taxable_Amount;
        }
        public decimal MethodStep33(string JournalTemplateName, int LineNumber)
        {
            decimal Taxable_Amount = 0;
            return Taxable_Amount;
        }
        public decimal MethodStep34(string JournalTemplateName, int LineNumber)
        {
            decimal Taxable_Amount = 0;
            return Taxable_Amount;
        }
        public Boolean MethodStep38(string JournalTemplateName, int LineNumber)
        {
            decimal Taxable_Amount = 0;
            Boolean resp = false;
            if (ModelState.IsValid)
            {
                PayrollJournalLine Proll = db.PayrollJournalLine.Find(JournalTemplateName, LineNumber);
                Taxable_Amount = (decimal)Proll.TaxableAmount;
                if (Taxable_Amount == 0)
                    resp = true;
            }
            return resp;
        }

        public decimal MethodStep220(string No, string codefilter, string idtemplate)
        {
            decimal Taxable_Amount = 0;
            
            if (ModelState.IsValid)
            {
                decimal BaseAmount=0;

                BaseAmount = db.Database.SqlQuery<Decimal>("select dbo.[FBaseAmount] ('" + idtemplate + "','" + No + "','" + codefilter + "' ) ").Single();
                Taxable_Amount =  BaseAmount;
                return Taxable_Amount;
            }
            return Taxable_Amount;
        }

        public Boolean MethodStep247(string JournalTemplateName, int LineNumber)
        {
            decimal Taxable_Amount = 0;
            Boolean resp = false;
            if (ModelState.IsValid)
            {
                decimal BaseAmount = Convert.ToDecimal(db.Database.SqlQuery<Decimal>("select dbo.[BaseBalance] ('" + JournalTemplateName + "'," + LineNumber.ToString() + ") "));
                PayrollJournalLine Proll = db.PayrollJournalLine.Find(JournalTemplateName, LineNumber);
                Taxable_Amount = (decimal)Proll.TaxableAmount + BaseAmount;
                if (Taxable_Amount == 0)
                    resp = true;
            }
            return resp;
        }

        public decimal MethodStep251(string JournalTemplateName, int LineNumber)
        {
            decimal Taxable_Amount = 0;
            
            if (ModelState.IsValid)
            {
                decimal BaseAmount = Convert.ToDecimal(db.Database.SqlQuery<Decimal>("select dbo.[TAX_BRACKETS] ('" + JournalTemplateName + "'," + LineNumber.ToString() + ") "));

                Taxable_Amount = BaseAmount;
            
            }
            return Taxable_Amount;
        }
        public decimal MethodStep255(string No, string codefilter, string idtemplate)
        {
            decimal Taxable_Amount = 0;
            
            if (ModelState.IsValid)
            {
                decimal BaseAmount = db.Database.SqlQuery<Decimal>("select dbo.[GetEmployeeRate] ('" + idtemplate + "','" + No + "','" + codefilter + "' ) ").Single();
                decimal quantity = db.Database.SqlQuery<Decimal>("select dbo.[BracketQuantity] ('" + idtemplate + "','" + No + "','" + codefilter + "' ) ").Single();
                
                Taxable_Amount = (decimal)(quantity * BaseAmount);
                
            }
            return Taxable_Amount;
        }
        
        public Boolean MethodStep269(string JournalTemplateName, int LineNumber)
        {
            decimal Taxable_Amount = 0;
            Boolean resp = false;
            if (ModelState.IsValid)
            {
                decimal BaseAmount = Convert.ToDecimal(db.Database.SqlQuery<Decimal>("select dbo.[GetEmployeeRate] ('" + JournalTemplateName + "',"+ LineNumber.ToString() +") "));
                PayrollJournalLine Proll = db.PayrollJournalLine.Find(JournalTemplateName, LineNumber);
                Taxable_Amount = (decimal)Proll.TaxableAmount + BaseAmount;
                if (Taxable_Amount == 0)
                    resp = true;
            }
            return resp;
        }
        public decimal MethodStep285(string JournalTemplateName, int LineNumber)
        {
            decimal Taxable_Amount = 0;
            Boolean resp = false;
            if (ModelState.IsValid)
            {
                decimal BaseAmount = Convert.ToDecimal(db.Database.SqlQuery<Decimal>("select dbo.[GetEmployeeRate] ('" + JournalTemplateName + "'," + LineNumber.ToString() + ") "));
                PayrollJournalLine Proll = db.PayrollJournalLine.Find(JournalTemplateName, LineNumber);
                Taxable_Amount =  BaseAmount;
                if (Taxable_Amount == 0)
                    resp = true;
            }
            return Taxable_Amount;
        }

        public decimal MethodStep287()
        {
            decimal Taxable_Amount = 0;
            return Taxable_Amount;
        }
        public Boolean MethodStep288(string JournalTemplateName, int LineNumber)
        {
            decimal Taxable_Amount = 0;
            Boolean resp = false;
            if (ModelState.IsValid)
            {
                PayrollJournalLine Proll = db.PayrollJournalLine.Find(JournalTemplateName, LineNumber);
                Taxable_Amount = (decimal)Proll.TaxableAmount;
                if (Taxable_Amount == 0)
                    resp = true;
            }
            return resp;
        }
        public decimal MethodStep289(string JournalTemplateName, int LineNumber)
        {
            decimal Taxable_Amount = 0;
            if (ModelState.IsValid)
            {
                PayrollJournalLine Proll = db.PayrollJournalLine.Find(JournalTemplateName, LineNumber);
                Taxable_Amount = (decimal)Proll.PayrollAmount;
            }
            return Taxable_Amount;
        }

        public decimal MethodStep290(string JournalTemplateName, int LineNumber)
        {
            decimal Taxable_Amount = 0;
            if (ModelState.IsValid)
            {
                PayrollJournalLine Proll = db.PayrollJournalLine.Find(JournalTemplateName, LineNumber);
                Taxable_Amount = (decimal)Proll.TempAmount;
            }
            return Taxable_Amount;
        }
        public decimal MethodStep291(string JournalTemplateName, int LineNumber)
        {
            decimal Taxable_Amount = 0;
            if (ModelState.IsValid)
            {
                PayrollJournalLine Proll = db.PayrollJournalLine.Find(JournalTemplateName, LineNumber);
                Taxable_Amount = (decimal)Proll.TempAmount;
            }
            return Taxable_Amount;
        }
        public decimal MethodStep292(string JournalTemplateName, int LineNumber)
        {
            decimal Taxable_Amount = 0;
            if (ModelState.IsValid)
            {
                PayrollJournalLine Proll = db.PayrollJournalLine.Find(JournalTemplateName, LineNumber);
                Taxable_Amount = (decimal)Proll.TempAmount;
            }
            return Taxable_Amount;
        }
        public decimal MethodStep293(string JournalTemplateName, int LineNumber)
        {
            decimal Taxable_Amount = 0;
            if (ModelState.IsValid)
            {
                PayrollJournalLine Proll = db.PayrollJournalLine.Find(JournalTemplateName, LineNumber);
                Taxable_Amount = (decimal)Proll.TempAmount;
            }
            return Taxable_Amount;
        }
        public decimal MethodStep294(string JournalTemplateName, int LineNumber)
        {
            decimal Taxable_Amount = 0;
            if (ModelState.IsValid)
            {
                PayrollJournalLine Proll = db.PayrollJournalLine.Find(JournalTemplateName, LineNumber);
                Taxable_Amount = (decimal)Proll.PayrollAmount+(decimal)Proll.TempAmount;
            }
            return Taxable_Amount;
        }
        public decimal MethodStep297(string JournalTemplateName, int LineNumber)
        {
            decimal Taxable_Amount = 0;
            if (ModelState.IsValid)
            {
                PayrollJournalLine Proll = db.PayrollJournalLine.Find(JournalTemplateName, LineNumber);
                Taxable_Amount = (decimal)Proll.PayrollAmount + (decimal)Proll.TempAmount;
            }
            return Taxable_Amount;
        }
        public decimal MethodStep304(string JournalTemplateName, int LineNumber)
        {
            decimal Taxable_Amount = 0;
            if (ModelState.IsValid)
            {
                decimal BaseAmount = Convert.ToDecimal(db.Database.SqlQuery<Decimal>("select dbo.[GetEmployeeRate] ('" + JournalTemplateName + "'," + LineNumber.ToString() + ") "));
                Taxable_Amount = BaseAmount;
            }
            return Taxable_Amount;
        }
        public decimal MethodStep308(string JournalTemplateName, int LineNumber)
        {
            decimal Taxable_Amount = 0;
            if (ModelState.IsValid)
            {
                decimal BaseAmount = Convert.ToDecimal(db.Database.SqlQuery<Decimal>("select dbo.[GetEmployeeRate] ('" + JournalTemplateName + "'," + LineNumber.ToString() + ") "));
                Taxable_Amount = BaseAmount;
            }
            return Taxable_Amount;
        }

        public decimal MethodStep317(string JournalTemplateName, int LineNumber)
        {
            decimal Taxable_Amount = 0;
            if (ModelState.IsValid)
            {
                decimal BaseAmount = Convert.ToDecimal(db.Database.SqlQuery<Decimal>("select dbo.[BaseAmountMTD] ('" + JournalTemplateName + "'," + LineNumber.ToString() + ") "));
                PayrollJournalLine Proll = db.PayrollJournalLine.Find(JournalTemplateName, LineNumber);
                Taxable_Amount = (decimal)Proll.PayrollAmount +BaseAmount;
            }
            return Taxable_Amount;
        }
        public decimal MethodStep339(string JournalTemplateName, int LineNumber)
        {
            decimal Taxable_Amount = 0;
            if (ModelState.IsValid)
            {
                PayrollJournalLine Proll = db.PayrollJournalLine.Find(JournalTemplateName, LineNumber);
                Taxable_Amount = (decimal)Proll.PayrollAmount + (decimal)Proll.TempAmount;
            }
            return Taxable_Amount;
        }
        public Boolean MethodStep368(string JournalTemplateName, int LineNumber)
        {
            Boolean resp = false;
            return resp;
        }
        public Boolean MethodStep372(string JournalTemplateName, int LineNumber)
        {
            decimal Taxable_Amount = 0;
            Boolean resp = false;
            if (ModelState.IsValid)
            {
                PayrollJournalLine Proll = db.PayrollJournalLine.Find(JournalTemplateName, LineNumber);
                Taxable_Amount = (decimal)Proll.TaxableAmount;
                    if (Taxable_Amount > 0)
                    resp = true;
            }
            return resp;
        }
        public Boolean MethodStep374(string JournalTemplateName, int LineNumber)
        {
            decimal Taxable_Amount = 0;
            Boolean resp = false;
            if (ModelState.IsValid)
            {
                PayrollJournalLine Proll = db.PayrollJournalLine.Find(JournalTemplateName, LineNumber);
                Taxable_Amount = (decimal)Proll.PayrollAmount;
                if (Taxable_Amount < 0)
                    resp = true;
            }
            return resp;
        }
        public Boolean MethodStep375(string JournalTemplateName, int LineNumber)
        {
            decimal Taxable_Amount = 0;
            Boolean resp = false;
            if (ModelState.IsValid)
            {
                PayrollJournalLine Proll = db.PayrollJournalLine.Find(JournalTemplateName, LineNumber);
                Taxable_Amount = (decimal)Proll.PayrollAmount;
                if (Taxable_Amount > 0)
                    resp = true;
            }
            return resp;
        }

        public Boolean MethodStep379(string JournalTemplateName, int LineNumber)
        {
            decimal Taxable_Amount = 0;
            Boolean resp = false;
            if (ModelState.IsValid)
            {
                PayrollJournalLine Proll = db.PayrollJournalLine.Find(JournalTemplateName, LineNumber);
                Taxable_Amount = (decimal)Proll.PayrollAmount;
                if (Taxable_Amount > 0)
                    resp = true;
            }
            return resp;
        }
        public decimal MethodStep382(string No, string codefilter, string idtemplate)
        {
            decimal Taxable_Amount = 0;
            decimal payrollamount = 0;
            decimal taxable = 0;
            Boolean resp = false;
            if (ModelState.IsValid)
            {
                payrollamount = MethodStep255(No, codefilter, idtemplate);
                taxable =  MethodStep220(No, codefilter, idtemplate);
                
                Taxable_Amount = (decimal)payrollamount * (decimal)taxable;
                
            }
            return Taxable_Amount;
        }
        public decimal MethodStep391(string JournalTemplateName, int LineNumber)
        {
            decimal Taxable_Amount = 0;

            if (ModelState.IsValid)
            {
                int BaseAmount = Convert.ToInt32(db.Database.SqlQuery<Decimal>("select dbo.[BracketQuantity] ('" + JournalTemplateName + "'," + LineNumber.ToString() + ") "));
                PayrollJournalLine Proll = db.PayrollJournalLine.Find(JournalTemplateName, LineNumber);
                Taxable_Amount = (decimal)(Proll.PayrollAmount / BaseAmount);
                Taxable_Amount = BaseAmount;

            }
            return Taxable_Amount;
        }

        public decimal MethodStep1060(string JournalTemplateName, int LineNumber)
        {
            decimal Taxable_Amount = 0;

            if (ModelState.IsValid)
            {
                int BaseAmount = Convert.ToInt32(db.Database.SqlQuery<Decimal>("select dbo.[PROPOR_SUELDO] ('" + JournalTemplateName + "'," + LineNumber.ToString() + ") "));
                  Taxable_Amount = BaseAmount;
            }
            return Taxable_Amount;
        }

        public decimal MethodStep1100(string JournalTemplateName, int LineNumber)
        {
            decimal Taxable_Amount = 0;

            if (ModelState.IsValid)
            {
                int BaseAmount = Convert.ToInt32(db.Database.SqlQuery<Decimal>("select dbo.[PROPOR_SUELDO] ('" + JournalTemplateName + "'," + LineNumber.ToString() + ") "));
                Taxable_Amount = BaseAmount+1;
            }
            return Taxable_Amount;
        }

        public decimal MethodStep1130(string JournalTemplateName, int LineNumber)
        {
            decimal Taxable_Amount = 0;

            if (ModelState.IsValid)
            {
                int BaseAmount = Convert.ToInt32(db.Database.SqlQuery<Decimal>("select dbo.[DIAS360] ('" + JournalTemplateName + "'," + LineNumber.ToString() + ") "));
                Taxable_Amount = BaseAmount ;
            }
            return Taxable_Amount;
        }

        public decimal MethodStep1140(string JournalTemplateName, int LineNumber)
        {
            decimal Taxable_Amount = 0;

            if (ModelState.IsValid)
            {
                int BaseAmount = Convert.ToInt32(db.Database.SqlQuery<Decimal>("select dbo.[Liquidacion1] ('" + JournalTemplateName + "'," + LineNumber.ToString() + ") "));
                Taxable_Amount = BaseAmount;
            }
            return Taxable_Amount;
        }

        public decimal MethodStep1150(string JournalTemplateName, int LineNumber)
        {
            decimal Taxable_Amount = 0;

            if (ModelState.IsValid)
            {
                int BaseAmount = Convert.ToInt32(db.Database.SqlQuery<Decimal>("select dbo.[Liquidacion2] ('" + JournalTemplateName + "'," + LineNumber.ToString() + ") "));
                Taxable_Amount = BaseAmount;
            }
            return Taxable_Amount;
        }
        public Decimal MethodStep1082(string JournalTemplateName, int LineNumber)
        {
            decimal Taxable_Amount = 0;
            
            if (ModelState.IsValid)
            {
                PayrollJournalLine Proll = db.PayrollJournalLine.Find(JournalTemplateName, LineNumber);
                Taxable_Amount = (decimal)Proll.PayrollAmount / (decimal)Proll.TaxableAmount;
            
            }
            return Taxable_Amount;
        }
        public decimal MethodStep1160(string JournalTemplateName, int LineNumber)
        {
            decimal Taxable_Amount = 0;

            if (ModelState.IsValid)
            {
                int BaseAmount = Convert.ToInt32(db.Database.SqlQuery<Decimal>("select dbo.[Factor] ('" + JournalTemplateName + "'," + LineNumber.ToString() + ") "));
                PayrollJournalLine Proll = db.PayrollJournalLine.Find(JournalTemplateName, LineNumber);
                Taxable_Amount = (decimal)Proll.TaxableAmount * BaseAmount;
                
            }
            return Taxable_Amount;
        }



        public decimal MethodStep1170(string JournalTemplateName, int LineNumber)
        {
            decimal Taxable_Amount = 0;

            if (ModelState.IsValid)
            {
                int BaseAmount = Convert.ToInt32(db.Database.SqlQuery<Decimal>("select dbo.[Factor] ('" + JournalTemplateName + "'," + LineNumber.ToString() + ") "))+1;
                PayrollJournalLine Proll = db.PayrollJournalLine.Find(JournalTemplateName, LineNumber);
                Taxable_Amount = (decimal)Proll.PayrollAmount / BaseAmount;
            }
            return Taxable_Amount;
        }

        public decimal actualizarpayrolljournalline(string No, string codefilter, string idtemplate,string value)
        {
            decimal Taxable_Amount = 0;

            if (ModelState.IsValid)
            {
                int BaseAmount = db.Database.SqlQuery<int>("EXEC [dbo].[PRAL_ACTU_JOURNAL_LINE] '" + No + "','" + codefilter + "','" + idtemplate + "',"+ value ).Single();
                
                Taxable_Amount = (decimal)(BaseAmount);

            }
            return Taxable_Amount;
        }

    }


}