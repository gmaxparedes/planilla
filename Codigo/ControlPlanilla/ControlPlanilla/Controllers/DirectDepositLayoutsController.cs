﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ControlPlanilla.Models;
using PagedList;

namespace ControlPlanilla.Controllers
{
    public class DirectDepositLayoutsController : AuditoriaController
    {


        // GET: DirectDepositLayouts
        public ActionResult Index(string SearchString, int? page)
        {
            var directDepositLayout = dbDDL.DirectDepositLayout.Include(d => d.DirectDepositTypeLayout).ToList();
            if (!string.IsNullOrEmpty(SearchString))
            {
                directDepositLayout = directDepositLayout.Where(c => c.Description.ToUpper().Contains(SearchString.ToUpper())).ToList();
            }

            int pageNumber = (page ?? 1);

            var result = directDepositLayout.ToPagedList(pageNumber, pageSize);

            return View(result);
        }

        // GET: DirectDepositLayouts/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DirectDepositLayout directDepositLayout = dbDDL.DirectDepositLayout.Find(id);
            if (directDepositLayout == null)
            {
                return HttpNotFound();
            }
            return View(directDepositLayout);
        }

        // GET: DirectDepositLayouts/Create
        public ActionResult Create()
        {
            ViewBag.LayoutType = new SelectList(dbDDL.DirectDepositTypeLayout, "ID_LAYOUT", "DESCRIPCION");
            return View();
        }

        // POST: DirectDepositLayouts/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Code,Description,LayoutType,RecordLength,BlockingFactor,BlockFiller")] DirectDepositLayout directDepositLayout)
        {
            if (ModelState.IsValid && !String.IsNullOrEmpty(directDepositLayout.Code) && !String.IsNullOrEmpty(directDepositLayout.Description))
            {
                dbDDL.DirectDepositLayout.Add(directDepositLayout);
                dbDDL.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.LayoutType = new SelectList(dbDDL.DirectDepositTypeLayout, "ID_LAYOUT", "DESCRIPCION", directDepositLayout.LayoutType);
            return View(directDepositLayout);
        }

        // GET: DirectDepositLayouts/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DirectDepositLayout directDepositLayout = dbDDL.DirectDepositLayout.Find(id);
            if (directDepositLayout == null)
            {
                return HttpNotFound();
            }
            ViewBag.LayoutType = new SelectList(dbDDL.DirectDepositTypeLayout, "ID_LAYOUT", "DESCRIPCION", directDepositLayout.LayoutType);
            return View(directDepositLayout);
        }

        // POST: DirectDepositLayouts/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Code,Description,LayoutType,RecordLength,BlockingFactor,BlockFiller")] DirectDepositLayout directDepositLayout)
        {
            if (ModelState.IsValid && !String.IsNullOrEmpty(directDepositLayout.Code) && !String.IsNullOrEmpty(directDepositLayout.Description))
            {
                dbDDL.Entry(directDepositLayout).State = EntityState.Modified;
                dbDDL.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.LayoutType = new SelectList(dbDDL.DirectDepositTypeLayout, "ID_LAYOUT", "DESCRIPCION", directDepositLayout.LayoutType);
            return View(directDepositLayout);
        }

        // GET: DirectDepositLayouts/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DirectDepositLayout directDepositLayout = dbDDL.DirectDepositLayout.Find(id);
            if (directDepositLayout == null)
            {
                return HttpNotFound();
            }
            return View(directDepositLayout);
        }

        // POST: DirectDepositLayouts/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            DirectDepositLayout directDepositLayout = dbDDL.DirectDepositLayout.Find(id);
            try
            {
                dbDDL.DirectDepositLayout.Remove(directDepositLayout);
                dbDDL.SaveChanges();
                return RedirectToAction("Index");
            }
            catch (SystemException ex)
            {
                logRepository.CapturarTodoError(ex);
                return View(directDepositLayout);
            }

        }

        #region Direct Deposit Sections
        [HttpPost]
        [AllowAnonymous]
        public PartialViewResult DirectDepositSections(string Codigo, int? page)
        {
            try
            {
                var directDepositSections = dbDDL.DirectDepositSections.Where(c => c.DirectDepositLayoutCode == Codigo)
                .Include(d => d.DirectDepositLayout).Include(d => d.DirectDepositTypeContent).ToList();

                ViewBag.DirectDepositLayoutCode = new SelectList(dbDDL.DirectDepositLayout, "Code", "Description").Where(c => c.Value == Codigo);
                ViewBag.SectionType = new SelectList(dbDDL.DirectDepositTypeContent, "ID_CONTENT", "DESCRIPCION");
                ViewBag.Codigo = Codigo;

                int pageNumber = (page ?? 1);

                var result = directDepositSections.ToPagedList(pageNumber, pageSize);

                return PartialView(result);
            }
            catch (SystemException ex)
            {
                logRepository.CapturarTodoError(ex);
                return PartialView();
            }
        }
        [HttpPost]
        public bool CrearSections([Bind(Include = "DirectDepositLayoutCode,SectionType,SequenceNumber,Description,IncTotalLineCount,IncTotalHeaderCount,IncTotalDetailCount,IncTotalFooterCount,IncTotalCreditCount,IncTotalDebitCount,IncTotalBatchCount,IncBatchDetailCount,ResetBatchTotals")] DirectDepositSections directDepositSections)
        {
            bool result = false;
            try
            {
                if (ModelState.IsValid)
                {
                    var DDS = dbDDL.DirectDepositSections.Where(c => c.DirectDepositLayoutCode == directDepositSections.DirectDepositLayoutCode)
                        .OrderByDescending(c => c.LineNumber).FirstOrDefault();
                    if (DDS != null)
                    {
                        directDepositSections.LineNumber = DDS.LineNumber + 1;
                    }
                    else
                    {
                        directDepositSections.LineNumber = 1;
                    }

                    DirectDepositSections guardar = new DirectDepositSections();

                    guardar = directDepositSections;

                    dbDDL.DirectDepositSections.Add(guardar);
                    dbDDL.SaveChanges();
                    result = true;
                }
                ViewBag.DirectDepositLayoutCode = new SelectList(dbDDL.DirectDepositLayout, "Code", "Description", directDepositSections.DirectDepositLayoutCode);
                ViewBag.SectionType = new SelectList(dbDDL.DirectDepositTypeContent, "ID_CONTENT", "DESCRIPCION", directDepositSections.SectionType);
            }
            catch (SystemException ex)
            {
                logRepository.CapturarTodoError(ex);
            }

            return result;
        }

        [HttpPost]
        public bool EditarSections([Bind(Include = "DirectDepositLayoutCode, LineNumber, SectionType, SequenceNumber,Description,IncTotalLineCount,IncTotalHeaderCount,IncTotalDetailCount,IncTotalFooterCount,IncTotalCreditCount,IncTotalDebitCount,IncTotalBatchCount,IncBatchDetailCount,ResetBatchTotals")] DirectDepositSections directDepositSections)
        {
            bool result = false;
            try
            {
                if (ModelState.IsValid)
                {
                    DirectDepositSections guardar = dbDDL.DirectDepositSections.Where
                        (c => c.DirectDepositLayoutCode == directDepositSections.DirectDepositLayoutCode &&
                        c.LineNumber == directDepositSections.LineNumber).FirstOrDefault();

                    if (guardar != null)
                    {
                        guardar.SequenceNumber = directDepositSections.SequenceNumber;
                        guardar.Description = directDepositSections.Description;
                        guardar.IncTotalLineCount = directDepositSections.IncTotalLineCount;
                        guardar.IncTotalHeaderCount = directDepositSections.IncTotalHeaderCount;
                        guardar.IncTotalDetailCount = directDepositSections.IncTotalDetailCount;
                        guardar.IncTotalFooterCount = directDepositSections.IncTotalFooterCount;
                        guardar.IncTotalCreditCount = directDepositSections.IncTotalCreditCount;
                        guardar.IncTotalDebitCount = directDepositSections.IncTotalDebitCount;
                        guardar.IncTotalBatchCount = directDepositSections.IncTotalBatchCount;
                        guardar.IncBatchDetailCount = directDepositSections.IncBatchDetailCount;
                        guardar.ResetBatchTotals = directDepositSections.ResetBatchTotals;


                        dbDDL.Entry(guardar).State = EntityState.Modified;
                        dbDDL.SaveChanges();
                        result = true;
                    }

                }
                ViewBag.DirectDepositLayoutCode = new SelectList(dbDDL.DirectDepositLayout, "Code", "Description", directDepositSections.DirectDepositLayoutCode);
                ViewBag.SectionType = new SelectList(dbDDL.DirectDepositTypeContent, "ID_CONTENT", "DESCRIPCION", directDepositSections.SectionType);
            }
            catch (SystemException ex)
            {
                logRepository.CapturarTodoError(ex);
            }

            return result;
        }

        [HttpPost]
        public bool EliminarSections([Bind(Include = "DirectDepositLayoutCode, LineNumber, SectionType")] DirectDepositSections directDepositSections)
        {
            bool result = false;
            try
            {
                if (ModelState.IsValid)
                {
                    DirectDepositSections remov = dbDDL.DirectDepositSections.Where
                        (c => c.DirectDepositLayoutCode == directDepositSections.DirectDepositLayoutCode &&
                        c.SectionType == directDepositSections.SectionType
                        && c.LineNumber == directDepositSections.LineNumber).FirstOrDefault();
                    dbDDL.DirectDepositSections.Remove(remov);
                    dbDDL.SaveChanges();
                    result = true;
                }

                //ViewBag.DirectDepositLayoutCode = new SelectList(dbDDL.DirectDepositLayout, "Code", "Description", directDepositSections.DirectDepositLayoutCode);
                //ViewBag.SectionType = new SelectList(dbDDL.DirectDepositTypeContent, "ID_CONTENT", "DESCRIPCION", directDepositSections.SectionType);
            }
            catch (SystemException ex)
            {
                logRepository.CapturarTodoError(ex);
            }

            return result;
        }

        #endregion

        #region Direct Deposit Lines
        [HttpPost]
        [AllowAnonymous]
        public PartialViewResult DirectDepositLines(string CodidoLayout, int SectionType, int LineNumber, int? page)
        {
            try
            {
                var directDepositLines = dbDDL.DirectDepositLines.Where(d => d.DirectDepositLayoutCode == CodidoLayout
                        && d.SectionType == SectionType && d.SectionLineNumber == LineNumber)
                        .Include(d => d.DirectDepositDateFormat)
                        .Include(d => d.DirectDepositLineType)
                        .Include(d => d.DirectDepositTimeFormat)
                        .Include(d => d.DirectDepositTypeJustification)
                        .Include(d => d.DirectDepositTypeToExport)
                        .Include(d => d.DirectDepositSections)
                        .OrderByDescending(d => d.LineNumber)
                        .ToList();

                int pageNumber = (page ?? 1);

                ViewBag.CodigoLayout = CodidoLayout;
                ViewBag.SectionType = SectionType;
                ViewBag.LineNumber = LineNumber;
                ViewBag.LineType = new SelectList(dbDDL.DirectDepositLineType, "ID_LINE_TYPE", "DESCRIPCION");
                ViewBag.TypetoExport = new SelectList(dbDDL.DirectDepositTypeToExport, "ID_TYPE_EXPORT", "DESCRIPCION");
                ViewBag.TypeJustification = new SelectList(dbDDL.DirectDepositTypeJustification, "ID_JUSTIFICATION", "DESCRIPCION");
                ViewBag.DateFormat = new SelectList(dbDDL.DirectDepositDateFormat, "ID_DATE_FORMAT", "DESCRIPCION");
                ViewBag.TimeFormat = new SelectList(dbDDL.DirectDepositTimeFormat, "ID_TIME_FORMAT", "DESCRIPCION");

                var result = directDepositLines.ToPagedList(pageNumber, pageSize);

                return PartialView(result);

            }
            catch (SystemException ex)
            {
                logRepository.CapturarTodoError(ex);
                return PartialView();
            }

        }

        [HttpPost]
        public bool CrearLine([Bind(Include = "DirectDepositLayoutCode,SectionType,SectionLineNumber, Description,StartPosition,LengthtoExport,TypetoExport,LineType,LiteralValue,TableNo,TableName,FieldNo,FieldName,FieldTypeandLength,Justification,FillCharacter,ConverttoUpperCase,RemoveSpaces,RemoveSpecialCharacters,IncludeDecimal,DateFormat,TimeFormat,FieldStartPosition,FieldLength,RequiredField,CheckDigit,UpdateTotalFileDebitAmt,UpdateTotalFileCreditAmt,UpdateHashAmount1,UpdateHashAmount2,UpdateBatchDebitAmt,UpdateBatchCreditAmt,UpdateBatchHashAmt1,UpdateBatchHashAmt2")] DirectDepositLines directDepositLines)
        {
            bool resultado = false;
            try
            {
                if (ModelState.IsValid)
                {
                    var DDL = dbDDL.DirectDepositLines.Where(c => c.DirectDepositLayoutCode == directDepositLines.DirectDepositLayoutCode
                    && c.SectionType == directDepositLines.SectionType &&
                    c.SectionLineNumber == directDepositLines.SectionLineNumber).OrderByDescending(c => c.LineNumber).FirstOrDefault();


                    if (DDL != null)
                    {
                        directDepositLines.LineNumber = DDL.LineNumber + 1;
                    }
                    else
                    {
                        directDepositLines.LineNumber = 1;
                    }

                    DirectDepositLines guardar = new DirectDepositLines();

                    guardar = directDepositLines;

                    dbDDL.DirectDepositLines.Add(guardar);
                    dbDDL.SaveChanges();
                    resultado = true;
                }

                ViewBag.LineType = new SelectList(dbDDL.DirectDepositLineType, "ID_LINE_TYPE", "DESCRIPCION");
                ViewBag.TypetoExport = new SelectList(dbDDL.DirectDepositTypeToExport, "ID_TYPE_EXPORT", "DESCRIPCION");
                ViewBag.TypeJustification = new SelectList(dbDDL.DirectDepositTypeJustification, "ID_JUSTIFICATION", "DESCRIPCION");
                ViewBag.DateFormat = new SelectList(dbDDL.DirectDepositDateFormat, "ID_DATE_FORMAT", "DESCRIPCION");
                ViewBag.TimeFormat = new SelectList(dbDDL.DirectDepositTimeFormat, "ID_TIME_FORMAT", "DESCRIPCION");
            }
            catch (SystemException ex)
            {
                logRepository.CapturarTodoError(ex);
            }
            return resultado;
        }
        [HttpPost]
        public bool EditarLine([Bind(Include = "DirectDepositLayoutCode,SectionType,SectionLineNumber, LineNumber, Description,StartPosition,LengthtoExport,TypetoExport,LineType,LiteralValue,TableNo,TableName,FieldNo,FieldName,FieldTypeandLength,Justification,FillCharacter,ConverttoUpperCase,RemoveSpaces,RemoveSpecialCharacters,IncludeDecimal,DateFormat,TimeFormat,FieldStartPosition,FieldLength,RequiredField,CheckDigit,UpdateTotalFileDebitAmt,UpdateTotalFileCreditAmt,UpdateHashAmount1,UpdateHashAmount2,UpdateBatchDebitAmt,UpdateBatchCreditAmt,UpdateBatchHashAmt1,UpdateBatchHashAmt2")] DirectDepositLines directDepositLines)
        {
            bool resultado = false;
            try
            {
                if (ModelState.IsValid)
                {
                    var guardar = dbDDL.DirectDepositLines.Where(c => c.DirectDepositLayoutCode == directDepositLines.DirectDepositLayoutCode
                    && c.SectionType == directDepositLines.SectionType &&
                    c.SectionLineNumber == directDepositLines.SectionLineNumber
                    && c.LineNumber== directDepositLines.LineNumber).FirstOrDefault();


                    if (guardar != null)
                    {
                        guardar.Description= directDepositLines.Description;
                        guardar.StartPosition =directDepositLines.StartPosition;
                        guardar.LengthtoExport = directDepositLines.LengthtoExport;
                        guardar.TypetoExport = directDepositLines.TypetoExport;
                        guardar.LineType = directDepositLines.LineType;
                        guardar.LiteralValue = directDepositLines.LiteralValue;
                        guardar.TableNo = directDepositLines.TableNo;
                        guardar.TableName = directDepositLines.TableName;
                        guardar.FieldNo = directDepositLines.FieldNo;
                        guardar.FieldName = directDepositLines.FieldName;
                        guardar.FieldTypeandLength = directDepositLines.FieldTypeandLength;
                        guardar.Justification = directDepositLines.Justification;
                        guardar.FillCharacter = directDepositLines.FillCharacter;
                        guardar.ConverttoUpperCase = directDepositLines.ConverttoUpperCase;
                        guardar.RemoveSpaces = directDepositLines.RemoveSpaces;
                        guardar.RemoveSpecialCharacters = directDepositLines.RemoveSpecialCharacters;
                        guardar.IncludeDecimal = directDepositLines.IncludeDecimal;
                        guardar.DateFormat = directDepositLines.DateFormat;
                        guardar.TimeFormat = directDepositLines.TimeFormat;
                        guardar.FieldStartPosition = directDepositLines.FieldStartPosition;
                        guardar.FieldLength = directDepositLines.FieldLength;
                        guardar.RequiredField = directDepositLines.RequiredField;
                        guardar.CheckDigit = directDepositLines.CheckDigit;
                        guardar.UpdateTotalFileDebitAmt = directDepositLines.UpdateTotalFileDebitAmt;
                        guardar.UpdateTotalFileCreditAmt = directDepositLines.UpdateTotalFileCreditAmt;
                        guardar.UpdateHashAmount1 = directDepositLines.UpdateHashAmount1;
                        guardar.UpdateHashAmount2 = directDepositLines.UpdateHashAmount2;
                        guardar.UpdateBatchDebitAmt = directDepositLines.UpdateBatchDebitAmt;
                        guardar.UpdateBatchCreditAmt = directDepositLines.UpdateBatchCreditAmt;
                        guardar.UpdateBatchHashAmt1 = directDepositLines.UpdateBatchHashAmt1;
                        guardar.UpdateBatchHashAmt2 = directDepositLines.UpdateBatchHashAmt2;

                        dbDDL.Entry(guardar).State = EntityState.Modified;
                        dbDDL.SaveChanges();
                        resultado = true;
                    }
                }

                ViewBag.LineType = new SelectList(dbDDL.DirectDepositLineType, "ID_LINE_TYPE", "DESCRIPCION");
                ViewBag.TypetoExport = new SelectList(dbDDL.DirectDepositTypeToExport, "ID_TYPE_EXPORT", "DESCRIPCION");
                ViewBag.TypeJustification = new SelectList(dbDDL.DirectDepositTypeJustification, "ID_JUSTIFICATION", "DESCRIPCION");
                ViewBag.DateFormat = new SelectList(dbDDL.DirectDepositDateFormat, "ID_DATE_FORMAT", "DESCRIPCION");
                ViewBag.TimeFormat = new SelectList(dbDDL.DirectDepositTimeFormat, "ID_TIME_FORMAT", "DESCRIPCION");
            }
            catch (SystemException ex)
            {
                logRepository.CapturarTodoError(ex);
            }
            return resultado;
        }
        [HttpPost]
        public bool EliminarLine([Bind(Include = "DirectDepositLayoutCode,SectionType,SectionLineNumber, LineNumber")] DirectDepositLines directDepositLines)
        {
            bool resultado = false;
            try
            {
                if (ModelState.IsValid)
                {
                    var remover = dbDDL.DirectDepositLines.Where(c => c.DirectDepositLayoutCode == directDepositLines.DirectDepositLayoutCode
                    && c.SectionType == directDepositLines.SectionType &&
                    c.SectionLineNumber == directDepositLines.SectionLineNumber
                    && c.LineNumber == directDepositLines.LineNumber).FirstOrDefault();


                    if (remover != null)
                    {
                        dbDDL.DirectDepositLines.Remove(remover);
                        dbDDL.SaveChanges();
                        resultado = true;
                    }
                }

                ViewBag.LineType = new SelectList(dbDDL.DirectDepositLineType, "ID_LINE_TYPE", "DESCRIPCION");
                ViewBag.TypetoExport = new SelectList(dbDDL.DirectDepositTypeToExport, "ID_TYPE_EXPORT", "DESCRIPCION");
                ViewBag.TypeJustification = new SelectList(dbDDL.DirectDepositTypeJustification, "ID_JUSTIFICATION", "DESCRIPCION");
                ViewBag.DateFormat = new SelectList(dbDDL.DirectDepositDateFormat, "ID_DATE_FORMAT", "DESCRIPCION");
                ViewBag.TimeFormat = new SelectList(dbDDL.DirectDepositTimeFormat, "ID_TIME_FORMAT", "DESCRIPCION");
            }
            catch (SystemException ex)
            {
                logRepository.CapturarTodoError(ex);
            }
            return resultado;
        }

        #endregion



        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                dbDDL.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
