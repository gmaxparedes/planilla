﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ControlPlanilla.Models;

namespace ControlPlanilla.Controllers
{
    public class PayrollJournalTemplatesController : AuditoriaController
    {
        private PlanillaCicloPago db = new PlanillaCicloPago();

        // GET: PayrollJournalTemplates
        public ActionResult Index()
        {
            return View(db.PayrollJournalTemplate.ToList());
        }

        // GET: PayrollJournalTemplates/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PayrollJournalTemplate payrollJournalTemplate = db.PayrollJournalTemplate.Find(id);
            if (payrollJournalTemplate == null)
            {
                return HttpNotFound();
            }
            return View(payrollJournalTemplate);
        }

        // GET: PayrollJournalTemplates/Create
        public ActionResult Create()
        {
            ViewBag.GLPostOption = new SelectList(ObtenerPayrollPostType(), "ID_POST", "DESCRIPCION", 0);
            return View();
        }
        public List<PayrollPostTypeView> ObtenerPayrollPostType()
        {
            List<PayrollPostTypeView> listaINI = new List<PayrollPostTypeView> {
                new PayrollPostTypeView {  ID_POST  = 0,   DESCRIPCION= "Seleccione una opción" }
            };
            var listaPrincipal = dbPayControl.PayrollPostType.Select(r => new PayrollPostTypeView
            {
                ID_POST = r.ID_POST,
                DESCRIPCION = r.DESCRIPCION
            }).ToList();
            var listFinal = listaINI.Union(listaPrincipal);
            return listFinal.ToList();
        }
        // POST: PayrollJournalTemplates/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Name,Description,TestReportID,PageID,PostingReportID,ForcePostingReport,CheckPrintReportID,SourceCode,ReasonCode,Recurring,GLPostOption,LastPayrollRunNo,TestReportName,PageName,PostingReportName,CheckPrintReportName,PayrollRunNoSeries")] PayrollJournalTemplate payrollJournalTemplate)
        {
            if (ModelState.IsValid)
            {
                db.PayrollJournalTemplate.Add(payrollJournalTemplate);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(payrollJournalTemplate);
        }

        // GET: PayrollJournalTemplates/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PayrollJournalTemplate payrollJournalTemplate = db.PayrollJournalTemplate.Find(id);
            if (payrollJournalTemplate == null)
            {
                return HttpNotFound();
            }
            ViewBag.GLPostOption = new SelectList(ObtenerPayrollPostType(), "ID_POST", "DESCRIPCION", payrollJournalTemplate.GLPostOption);
            
            return View(payrollJournalTemplate);
        }

        // POST: PayrollJournalTemplates/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Name,Description,TestReportID,PageID,PostingReportID,ForcePostingReport,CheckPrintReportID,SourceCode,ReasonCode,Recurring,GLPostOption,LastPayrollRunNo,TestReportName,PageName,PostingReportName,CheckPrintReportName,PayrollRunNoSeries")] PayrollJournalTemplate payrollJournalTemplate)
        {
            if (ModelState.IsValid)
            {
                db.Entry(payrollJournalTemplate).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(payrollJournalTemplate);
        }

        // GET: PayrollJournalTemplates/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PayrollJournalTemplate payrollJournalTemplate = db.PayrollJournalTemplate.Find(id);
            if (payrollJournalTemplate == null)
            {
                return HttpNotFound();
            }
            return View(payrollJournalTemplate);
        }

        // POST: PayrollJournalTemplates/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            PayrollJournalTemplate payrollJournalTemplate = db.PayrollJournalTemplate.Find(id);
            db.PayrollJournalTemplate.Remove(payrollJournalTemplate);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
