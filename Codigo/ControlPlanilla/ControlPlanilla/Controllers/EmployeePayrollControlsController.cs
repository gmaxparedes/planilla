﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ControlPlanilla.Models;

namespace ControlPlanilla.Controllers
{
    public class EmployeePayrollControlsController : AuditoriaController
    {
        private EmployeeEntities db = new EmployeeEntities();

        // GET: EmployeePayrollControls
        public ActionResult Index()
        {
            var employeePayrollControl = db.EmployeePayrollControl.Include(e => e.Employee);
            return View(employeePayrollControl.ToList());
        }

        // GET: EmployeePayrollControls/Details/5
        public ActionResult Details(string id, string id2)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EmployeePayrollControl employeePayrollControl = db.EmployeePayrollControl.Find(id,id2);
            if (employeePayrollControl == null)
            {
                return HttpNotFound();
            }
            return View(employeePayrollControl);
        }

        // GET: EmployeePayrollControls/Create
        public ActionResult Create()
        {
            List<SelectListItem> items = new SelectList(
                             db.Employee, "No", "FirstName"
                             ).ToList();

            items.Insert(0, (new SelectListItem
            {
                Text = "Seleccione una opción",
                Value = "0",
                Selected = true
            }));
            
            ViewBag.EmployeeNo =  new SelectList(ObtenerEmployee(), "No", "FirstName", 0);
            ViewBag.paycontrols = dbPayControl.PayrollControl.Select(a => new PayrollControlView   { Code= a.Code, Name = a.Name }).ToList();
            ViewBag.PayrollControlMonthlySchedule = dbPayControl.PayrollControlMonthlySchedule.Select(a => new PayrollControlMonthlyScheduleView { ID_CALENDARIO = a.ID_CALENDARIO, DESCRIPCION = a.DESCRIPCION }).ToList();
            ViewBag.PayrollPostingGroup = dbPayControl.PayrollPostingGroup.Select(a => new PayrollPostingGroupView { Code = a.Code }).ToList();
            return View();
        }

        // POST: EmployeePayrollControls/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "EmployeeNo,PayControlCode,MonthlySchedule,Active,PayrollPostingGroup,Description")] EmployeePayrollControl employeePayrollControl)
        {
            if (employeePayrollControl.MonthlySchedule == null)
                employeePayrollControl.MonthlySchedule = 0;

           // if (ModelState.IsValid)
           // {
                db.EmployeePayrollControl.Add(employeePayrollControl);
                db.SaveChanges();
                return RedirectToAction("Index");
            //}

            ViewBag.EmployeeNo = new SelectList(ObtenerEmployee(), "No", "FirstName", 0);
            return View(employeePayrollControl);
        }
        public List<EmployeeView> ObtenerEmployee()
        {
            List<EmployeeView> listaINI = new List<EmployeeView> {
                new EmployeeView { No  = "0",   FirstName= "Seleccione una opción" }
            };
            var listaPrincipal = db.Employee.Select(r => new EmployeeView
            {
                No = r.No,
                FirstName = r.FirstName + " " + r.MiddleName + " " + r.LastName + " " + r.SecondLastName
            }).ToList();
            var listFinal = listaINI.Union(listaPrincipal);
            return listFinal.ToList();
        }
        // GET: EmployeePayrollControls/Edit/5
        public ActionResult Edit(string id, string id2)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            EmployeePayrollControl employeePayrollControl = db.EmployeePayrollControl.Find(id,id2);
            if (employeePayrollControl == null)
            {
                return HttpNotFound();
            }


            ViewBag.EmployeeNo = new SelectList(db.Employee, "No", "FirstName", employeePayrollControl.EmployeeNo);
            ViewBag.MonthlySchedule = new SelectList( dbPayControl.PayrollControlMonthlySchedule.Select(a => new PayrollControlMonthlyScheduleView { ID_CALENDARIO = a.ID_CALENDARIO, DESCRIPCION = 
a.DESCRIPCION }), "ID_CALENDARIO", "DESCRIPCION", employeePayrollControl.MonthlySchedule);
            Boolean isactive;
            isactive = Convert.ToBoolean(  employeePayrollControl.Active);
            ViewBag.PayrollPostingGroup = new SelectList(dbPayControl.PayrollPostingGroup.Select(a => new PayrollPostingGroupView { Code = a.Code }), "Code", "Code", employeePayrollControl.PayrollPostingGroup);
            return View(employeePayrollControl);
        }

        // POST: EmployeePayrollControls/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "EmployeeNo,PayControlCode,MonthlySchedule,Active,PayrollPostingGroup,Description")] EmployeePayrollControl employeePayrollControl)
        {
            if (ModelState.IsValid)
            {
                db.Entry(employeePayrollControl).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.EmployeeNo = new SelectList(db.Employee, "No", "FirstName", employeePayrollControl.EmployeeNo);
            return View(employeePayrollControl);
        }

        // GET: EmployeePayrollControls/Delete/5
        public ActionResult Delete(string id, string id2)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EmployeePayrollControl employeePayrollControl = db.EmployeePayrollControl.Find(id,id2);
            if (employeePayrollControl == null)
            {
                return HttpNotFound();
            }
            return View(employeePayrollControl);
        }

        // POST: EmployeePayrollControls/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id, string id2)
        {
            EmployeePayrollControl employeePayrollControl = db.EmployeePayrollControl.Find(id,id2);
            db.EmployeePayrollControl.Remove(employeePayrollControl);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
