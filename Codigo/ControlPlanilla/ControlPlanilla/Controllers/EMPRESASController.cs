﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ControlPlanilla.Models;

namespace ControlPlanilla.Controllers
{
    public class EMPRESASController : AuditoriaController
    {
        private PlanillaAccesoMenus db = new PlanillaAccesoMenus();

        // GET: EMPRESAS
        public ActionResult Index()
        {
            return View(db.EMPRESAS.ToList());
        }

        // GET: EMPRESAS/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EMPRESAS eMPRESAS = db.EMPRESAS.Find(id);
            if (eMPRESAS == null)
            {
                return HttpNotFound();
            }
            return View(eMPRESAS);
        }

        // GET: EMPRESAS/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: EMPRESAS/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "CD_EMPRESA,DS_EMPRESA,ESTADO")] EMPRESAS eMPRESAS)
        {
            if (ModelState.IsValid)
            {
                var guardar = new EMPRESAS();
                guardar.CD_EMPRESA = eMPRESAS.CD_EMPRESA;
                guardar.DS_EMPRESA = eMPRESAS.DS_EMPRESA;
                guardar.ESTADO = eMPRESAS.ESTADO;
                guardar.USUA_CREA = Usuario;
                guardar.FECH_CREA = Ahora;

                db.EMPRESAS.Add(guardar);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(eMPRESAS);
        }

        // GET: EMPRESAS/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EMPRESAS eMPRESAS = db.EMPRESAS.Find(id);
            if (eMPRESAS == null)
            {
                return HttpNotFound();
            }
            return View(eMPRESAS);
        }

        // POST: EMPRESAS/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "CD_EMPRESA,DS_EMPRESA,ESTADO")] EMPRESAS eMPRESAS)
        {
            if (ModelState.IsValid)
            {
                var guardar = db.EMPRESAS.Find(eMPRESAS.CD_EMPRESA);
                if (guardar != null)
                {
                    guardar.DS_EMPRESA = eMPRESAS.DS_EMPRESA;
                    guardar.ESTADO = eMPRESAS.ESTADO;
                    guardar.USUA_ACTU = Usuario;
                    guardar.FECH_ACTU = Ahora;
                    db.Entry(guardar).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                
            }
            return View(eMPRESAS);
        }

        // GET: EMPRESAS/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EMPRESAS eMPRESAS = db.EMPRESAS.Find(id);
            if (eMPRESAS == null)
            {
                return HttpNotFound();
            }
            return View(eMPRESAS);
        }

        // POST: EMPRESAS/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            EMPRESAS eMPRESAS = db.EMPRESAS.Find(id);
            RELACION_EMPRESA_PAIS remove = db.RELACION_EMPRESA_PAIS.Where(c => c.CD_EMPRESA == id).FirstOrDefault();
            db.RELACION_EMPRESA_PAIS.Remove(remove);
            db.EMPRESAS.Remove(eMPRESAS);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        #region relacion empreesa pais
        [HttpPost]
        public PartialViewResult RelacionPais(string CdEmpresa)
        {
            
            try
            {
                RELACION_EMPRESA_PAIS resultado = db.RELACION_EMPRESA_PAIS.Where(c => c.CD_EMPRESA == CdEmpresa).FirstOrDefault();
                ViewBag.CD_PAIS = new SelectList(db.PAISES, "CD_PAIS", "DS_NOMBREPAIS");

                return PartialView(resultado);
            }
            catch (SystemException ex)
            {
                logRepository.CapturarTodoError(ex);
            }

            return PartialView();
        }

        [HttpPost]
        public bool CrearRelacion(string CdEmpresa, string CD_PAIS)
        {
            bool result = false;
            try
            {
                RELACION_EMPRESA_PAIS guardar = new RELACION_EMPRESA_PAIS();
                guardar.CD_EMPRESA = CdEmpresa;
                guardar.CD_PAIS = CD_PAIS;
                guardar.USUA_CREA = Usuario;
                guardar.FECH_CREA = Ahora;

                db.RELACION_EMPRESA_PAIS.Add(guardar);
                db.SaveChanges();
                result = true;
            }
            catch (SystemException ex)
            {
                logRepository.CapturarTodoError(ex);
            }
            return result;
        }
        [HttpPost]
        public bool EditarRelacion(int ID_RELACION, string CdEmpresa, string CD_PAIS)
        {
            bool result = false;
            try
            {
                RELACION_EMPRESA_PAIS guardar = db.RELACION_EMPRESA_PAIS.Find(ID_RELACION);
                if (guardar != null)
                {
                    guardar.CD_EMPRESA = CdEmpresa;
                    guardar.CD_PAIS = CD_PAIS;
                    guardar.USUA_ACTU = Usuario;
                    guardar.FECH_ACTU = Ahora;

                    db.Entry(guardar).State = EntityState.Modified;
                    db.SaveChanges();
                    result = true;
                }
                
            }
            catch (SystemException ex)
            {
                logRepository.CapturarTodoError(ex);
            }
            return result;
        }
        #endregion
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
