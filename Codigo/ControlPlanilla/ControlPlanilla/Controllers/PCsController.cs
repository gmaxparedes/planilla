﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ControlPlanilla.Models;

namespace ControlPlanilla.Controllers
{
    public class PCsController : AuditoriaController
    {
        private GlobalEntities db = new GlobalEntities();

        // GET: PCs
        public ActionResult Index()
        {
            var pC = db.PC.Include(p => p.CIUDADES);
            return View(pC.ToList());
        }

        // GET: PCs/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PC pC = db.PC.Find(id);
            if (pC == null)
            {
                return HttpNotFound();
            }
            return View(pC);
        }

        // GET: PCs/Create
        public ActionResult Create()
        {
            ViewBag.ID_CIUDAD = new SelectList(db.CIUDADES, "ID_CIUDAD", "NOMBRE_CIUDAD" );
            var listaPaises = dbMenus.PAISES.Select(a => new PaisesViewModels { CD_PAIS = a.CD_PAIS, DS_PAIS = a.DS_NOMBREPAIS }).ToList();
            ViewBag.cmbPaises = listaPaises;
            var listaDepartamentos = db.DEPARTAMENTOS.Select(a => new DepartamentosView { ID_DEPTO = a.ID_DEPTO, NOMBRE_DEPTO = a.NOMBRE_DEPTO }).ToList();
            ViewBag.cmbDepartamentos = listaDepartamentos;
            return View();
        }

        // POST: PCs/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Codigo_Postal,ID_CIUDAD,")] PC pC)
        {
            if (ModelState.IsValid)
            {
                pC.FECH_CREA = Ahora;
                pC.USUA_CREA = Usuario;
                db.PC.Add(pC);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ID_CIUDAD = new SelectList(db.CIUDADES, "ID_CIUDAD", "NOMBRE_CIUDAD", pC.ID_CIUDAD);
            return View(pC);
        }

        // GET: PCs/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PC pC = db.PC.Find(id);
            if (pC == null)
            {
                return HttpNotFound();
            }
            else { 
                    ViewBag.ID_CIUDAD = new SelectList(db.CIUDADES, "ID_CIUDAD", "NOMBRE_CIUDAD", pC.ID_CIUDAD);
                    var listaPaises = dbMenus.PAISES.Select(a => new PaisesViewModels { CD_PAIS = a.CD_PAIS, DS_PAIS = a.DS_NOMBREPAIS }).ToList();
                    ViewBag.cmbPaises = listaPaises;
                    var listaDepartamentos = db.DEPARTAMENTOS.Select(a => new DepartamentosView { ID_DEPTO = a.ID_DEPTO, NOMBRE_DEPTO = a.NOMBRE_DEPTO }).ToList();
                    ViewBag.cmbDepartamentos = listaDepartamentos;

                    var qdepartamento = from pc1 in db.PC
                                        join cit in db.CIUDADES on pc1.ID_CIUDAD equals cit.ID_CIUDAD
                                        join dp in db.DEPARTAMENTOS on cit.ID_DEPTO equals dp.ID_DEPTO
                                        where pc1.Id_Codigo == id
                                        select new { ID_DEPTO = cit.ID_DEPTO, CD_PAIS = dp.CD_PAIS };
                    var infon = qdepartamento.First();
                    ViewBag.CD_PAIS = infon.CD_PAIS;
                    ViewBag.ID_DEPTO = infon.ID_DEPTO;
            }
            return View(pC);
        }

        // POST: PCs/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id_Codigo,Codigo_Postal,ID_CIUDAD")] PC pC)
        {
            if (ModelState.IsValid)
            {
                PC grabar = db.PC.Find(pC.Id_Codigo);
                grabar.ID_CIUDAD = pC.ID_CIUDAD;
                
                grabar.Codigo_Postal = pC.Codigo_Postal;
                grabar.FECH_ACTU = Ahora;
                grabar.USUA_ACTU = Usuario;

                db.Entry(grabar).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ID_CIUDAD = new SelectList(db.CIUDADES, "ID_CIUDAD", "NOMBRE_CIUDAD", pC.ID_CIUDAD);
            return View(pC);
        }

        // GET: PCs/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PC pC = db.PC.Find(id);
            if (pC == null)
            {
                return HttpNotFound();
            }
            return View(pC);
        }

        // POST: PCs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            PC pC = db.PC.Find(id);
            db.PC.Remove(pC);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
