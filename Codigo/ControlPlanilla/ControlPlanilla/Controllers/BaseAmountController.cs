﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ControlPlanilla.Models;
using System.Data.Entity;
using System.Data.Sql;
using System.Data.SqlClient;
using PagedList;

namespace ControlPlanilla.Controllers
{
    public class BaseAmountController : AuditoriaController
    {
        // GET: BaseAmount
        private PlanillaPayControls db = new PlanillaPayControls();
        public ActionResult Index()
        {
            ViewBag.OpcionSistema = OpcionSistema;
            ViewBag.ArchivoAdjunto = ComprobarRequiereArchivos(OpcionSistema);
            ViewBag.EditStatus = new SelectList(db.PayRateEditStatus, "ID_EDIT", "DESCRIPCION");

            return View(dbPayControl.BaseAmount.ToList());
        }

        public ActionResult CrearBaseAmount([Bind(Include = "Code, EffectiveDate, Name, EditStatus, BaseAmountDetailsExist")] BaseAmount baseAmount)
        {
            bool result = false;
            try
            {
                if (ModelState.IsValid)
                {
                    dbPayControl.BaseAmount.Add(baseAmount);
                    dbPayControl.SaveChanges();
                    bool requiereArchivos = ComprobarRequiereArchivos(OpcionSistema);

                    if (requiereArchivos)
                    {
                        TempData["OpcionSistema"] = OpcionSistema;
                        TempData["CdForaneo"] = baseAmount.Code;
                        TempData["DsForaneo"] = baseAmount.Name;

                        bool cumplio = ComprobarArchivosAdjuntos();

                        if (!cumplio)
                        {
                            //string opcion, string DsForaneo, string CdForaneo)
                            return RedirectToAction("SubirArchivos", "Auditoria", new { opcion = OpcionSistema, DsForaneo = baseAmount.Name, CdForaneo = baseAmount.Code });
                        }
                    }
                    result = true;
                    TempData["MensajeOK"] = "Registro creado";
                }
                else
                {
                    TempData["MensajeError"] = "Error al crear registro";
                    if (ModelState.Values.Select(c => c.Errors).ToList().Count > 0)
                    {
                        var mensajes = ModelState.Values.Where(s => s.Errors.Count > 0).Select(s => s.Errors).ToList();
                        if (mensajes.Count > 0)
                        {
                            TempData["MensajeError"] = "";
                        }
                        foreach (var item in mensajes)
                        {
                            TempData["MensajeError"] += item.Select(i => i.ErrorMessage.ToString()).FirstOrDefault() + " ";
                        }
                    }
                }
            }
            catch (SystemException ex)
            {
                logRepository.CapturarTodoError(ex);
                TempData["MensajeError"] = ex.InnerException.Message.ToString();
                if (TempData["MensajeError"] == null)
                    TempData["MensajeError"] = "Ocurrio un error.";
            }
            return Json(result);
        }
        public ActionResult EditarBaseAmount([Bind(Include = "Code, EffectiveDate, Name, EditStatus, BaseAmountDetailsExist")] BaseAmount baseAmount)
        {
            bool result = false;
            try
            {
                if (ModelState.IsValid)
                {
                    BaseAmount guardar = dbPayControl.BaseAmount.Where(c => c.Code == baseAmount.Code && c.EffectiveDate == baseAmount.EffectiveDate).FirstOrDefault();
                    if (guardar != null)
                    {
                        guardar.Name = baseAmount.Name;
                        guardar.EditStatus = baseAmount.EditStatus;
                        guardar.BaseAmountDetailsExist = baseAmount.BaseAmountDetailsExist;
                        dbPayControl.Entry(guardar).State = EntityState.Modified;
                        dbPayControl.SaveChanges();
                        bool requiereArchivos = ComprobarRequiereArchivos(OpcionSistema);

                        if (requiereArchivos)
                        {
                            TempData["OpcionSistema"] = OpcionSistema;
                            TempData["CdForaneo"] = baseAmount.Code;
                            TempData["DsForaneo"] = baseAmount.Name;

                            bool cumplio = ComprobarArchivosAdjuntos();

                            if (!cumplio)
                            {
                                //string opcion, string DsForaneo, string CdForaneo)
                                return RedirectToAction("SubirArchivos", "Auditoria", new { opcion = OpcionSistema, DsForaneo = baseAmount.Name, CdForaneo = baseAmount.Code });
                            }
                        }
                        result = true;
                        result = true;
                        TempData["MensajeOK"] = "Registro eliminadó";
                    }
                    else
                    {
                        TempData["MensajeError"] = "Error al actualizar registro";
                    }

                }
                else
                {
                    TempData["MensajeError"] = "Error al actualizar registro";
                    if (ModelState.Values.Select(c => c.Errors).ToList().Count > 0)
                    {
                        var mensajes = ModelState.Values.Where(s => s.Errors.Count > 0).Select(s => s.Errors).ToList();
                        if (mensajes.Count > 0)
                        {
                            TempData["MensajeError"] = "";
                        }
                        foreach (var item in mensajes)
                        {
                            TempData["MensajeError"] += item.Select(i => i.ErrorMessage.ToString()).FirstOrDefault() + " ";
                        }
                    }
                }
            }
            catch (SystemException ex)
            {
                logRepository.CapturarTodoError(ex);
                TempData["MensajeError"] = ex.Message.ToString();
                if (TempData["MensajeError"] == null)
                    TempData["MensajeError"] = "Ocurrio un error.";
            }
            return Json(result);
        }

        public ActionResult EliminarBaseAmount([Bind(Include = "Code, EffectiveDate")] BaseAmount baseAmount)
        {
            bool result = false;
            try
            {
                if (ModelState.IsValid)
                {
                    BaseAmount guardar = dbPayControl.BaseAmount.Where(c => c.Code == baseAmount.Code && c.EffectiveDate == baseAmount.EffectiveDate).FirstOrDefault();
                    if (guardar != null)
                    {
                        dbPayControl.BaseAmount.Remove(guardar);
                        dbPayControl.SaveChanges();
                        result = true;
                        TempData["MensajeOK"] = "Registro eliminado";
                    }
                    else
                    {
                        TempData["MensajeError"] = "Error al eliminar registro";
                    }

                }
                else
                {
                    TempData["MensajeError"] = "Error al actualizar registro";
                    if (ModelState.Values.Select(c => c.Errors).ToList().Count > 0)
                    {
                        var mensajes = ModelState.Values.Where(s => s.Errors.Count > 0).Select(s => s.Errors).ToList();
                        if (mensajes.Count > 0)
                        {
                            TempData["MensajeError"] = "";
                        }
                        foreach (var item in mensajes)
                        {
                            TempData["MensajeError"] += item.Select(i => i.ErrorMessage.ToString()).FirstOrDefault() + " ";
                        }
                    }
                }
            }
            catch (SystemException ex)
            {
                logRepository.CapturarTodoError(ex);
                TempData["MensajeError"] = ex.Message.ToString();
                if (TempData["MensajeError"] == null)
                    TempData["MensajeError"] = "Ocurrio un error.";
            }
            return Json(result);
        }

        public ActionResult ConsultarBaseAmount(string Code, DateTime date, int? page)
        {
            try
            {
                var baseDetalle = dbPayControl.BaseAmountDetail
                    .Where(c => c.BaseAmountCode == Code && c.EffectiveDate == date).ToList();

                if (baseDetalle.Count == 0)
                {
                    var tipoControl = new SelectList(dbPayControl.PayrollControlType, "ID_TYPE", "DESCRIPCION");
                    List<SelectListItem> listTipoC = new SelectList(" ", "").ToList();
                    foreach (var item in tipoControl)
                    {
                        listTipoC.Add(item);
                    }

                    var GlPos = new SelectList(dbPayControl.PayrollPostType, "ID_POST", "DESCRIPCION");
                    List<SelectListItem> listGLPos = new SelectList(" ", "").ToList();
                    foreach (var item in GlPos)
                    {
                        listGLPos.Add(item);
                    }

                    var TipoAgente = new SelectList(dbPayControl.PayrollTypeReportingAuthority, "ID_TYPE", "DESCRIPCION");
                    List<SelectListItem> listTipoAgente = new SelectList(" ", "").ToList();
                    foreach (var item in TipoAgente)
                    {
                        listTipoAgente.Add(item);
                    }

                    ViewBag.PayrollControlTypeFilter = listTipoC;
                    ViewBag.GLPostTypeFilter = listGLPos;
                    ViewBag.Type = listTipoAgente;
                    return PartialView("FiltrosBaseAmount");
                }
                else
                {
                    ViewBag.Code = Code;
                    ViewBag.Fecha = date;
                    int pageNumber = (page ?? 1);
                    var res = baseDetalle.ToPagedList(pageNumber, pageSize);

                    return PartialView(res);
                }


            }
            catch (SystemException ex)
            {
                logRepository.CapturarTodoError(ex);
            }
            return PartialView(dbPayControl.BaseAmountDetail.Where(c => c.BaseAmountCode == Code && c.EffectiveDate == date).ToList());
        }

        public bool CrearDetalles(string Code, DateTime Date, int? ControlType, int? PosType, int? TipoAgente)
        {
            bool result = false;

            try
            {
                //var result = new List<DetalleEstadistico>();
                //result = _db.Database.SqlQuery<DetalleEstadistico>(
                //    "EXEC ReporteTipificacion_SP @descripcion, @idSupervisor,@idEjecutivo, @FechaInicio, @FechaFin, @idCampana",
                //    new SqlParameter("descripcion", Descripcion),
                //    new SqlParameter("idSupervisor", idSupervisor),
                //    new SqlParameter("idEjecutivo", idEjecutivo),
                //    new SqlParameter("FechaInicio", FechaInicio),
                //    new SqlParameter("FechaFin", FechaFin),
                //    new SqlParameter("idCampana", idCampana)).ToList();
                dbPayControl.Database.ExecuteSqlCommand(
                    "EXEC PRAL_INSE_BASEAMOUNT_DETAILS @CODE, @DATE, @CONTROL_TYPE, @POS_TYPE, @TYPE_AGEN",
                    new SqlParameter("CODE", Code),
                    new SqlParameter("DATE", Date),
                    new SqlParameter("CONTROL_TYPE", ControlType ?? 0),
                    new SqlParameter("POS_TYPE", PosType ?? 0),
                    new SqlParameter("TYPE_AGEN", TipoAgente ?? 0)
                    );
                //Comprobamos si hay registro
                var r = dbPayControl.BaseAmountDetail.Where(c => c.BaseAmountCode == Code && c.EffectiveDate == Date).ToList();
                if (r.Count > 0)
                {
                    result = true;
                }
                
                return result;
            }
            catch (SystemException ex)
            {
                logRepository.CapturarTodoError(ex);
            }


            return result;
        }


        public bool EditarDetalle([Bind(Include = "BaseAmountCode, EffectiveDate, LineNumber, NameFilter")]BaseAmountDetail baseAmountDetail)
        {
            bool result = false;
            try
            {
                if (ModelState.IsValid)
                {
                    var guardar = dbPayControl.BaseAmountDetail
                        .Where(c => c.BaseAmountCode == baseAmountDetail.BaseAmountCode &&
                        c.EffectiveDate == baseAmountDetail.EffectiveDate &&
                        c.LineNumber == baseAmountDetail.LineNumber).FirstOrDefault();
                    if (guardar!=null)
                    {
                        guardar.NameFilter = baseAmountDetail.NameFilter;
                        dbPayControl.Entry(guardar).State = EntityState.Modified;
                        dbPayControl.SaveChanges();
                        
                        result = true;
                    }
                }
            }
            catch (SystemException ex)
            {               
                logRepository.CapturarTodoError(ex);
            }
            return result;
        }

        public bool EliminarDetalle([Bind(Include = "BaseAmountCode, EffectiveDate, LineNumber")]BaseAmountDetail baseAmountDetail)
        {
            bool result = false;
            try
            {
                if (ModelState.IsValid)
                {
                    var guardar = dbPayControl.BaseAmountDetail
                        .Where(c => c.BaseAmountCode == baseAmountDetail.BaseAmountCode &&
                        c.EffectiveDate == baseAmountDetail.EffectiveDate &&
                        c.LineNumber == baseAmountDetail.LineNumber).FirstOrDefault();
                    if (guardar != null)
                    {
                        dbPayControl.BaseAmountDetail.Remove(guardar);
                        dbPayControl.SaveChanges();
                        result = true;
                        var up = dbPayControl.BaseAmount.Where(c => c.Code == baseAmountDetail.BaseAmountCode && c.EffectiveDate == baseAmountDetail.EffectiveDate).FirstOrDefault();
                        var det = dbPayControl.BaseAmountDetail
                            .Where(c => c.BaseAmountCode == baseAmountDetail.BaseAmountCode &&
                            c.EffectiveDate == baseAmountDetail.EffectiveDate).ToList();

                        if (up!=null)
                        {
                            up.NumberofOccurrences = det.Count();
                            dbPayControl.Entry(up).State = EntityState.Modified;
                            dbPayControl.SaveChanges();
                        }
                    }
                }
            }
            catch (SystemException ex)
            {
                logRepository.CapturarTodoError(ex);
            }
            return result;
        }
    }
}