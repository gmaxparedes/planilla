﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ControlPlanilla.Models;

namespace ControlPlanilla.Controllers
{
    public class GLB_SISTEMASController : Controller
    {
        private PlanillaAccesoMenus db = new PlanillaAccesoMenus();

        // GET: GLB_SISTEMAS
        public ActionResult Index()
        {
            return View(db.GLB_SISTEMAS.ToList());
        }

        // GET: GLB_SISTEMAS/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            GLB_SISTEMAS gLB_SISTEMAS = db.GLB_SISTEMAS.Find(id);
            if (gLB_SISTEMAS == null)
            {
                return HttpNotFound();
            }
            return View(gLB_SISTEMAS);
        }

        // GET: GLB_SISTEMAS/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: GLB_SISTEMAS/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "CD_CODI_SIST,CD_EMPRESA,DS_NOMB_SIST,CD_ESTADO_SISTEMA,USUA_CREA,FECH_CREA,USUA_ACTU,FECH_ACTU")] GLB_SISTEMAS gLB_SISTEMAS)
        {
            if (ModelState.IsValid)
            {
                db.GLB_SISTEMAS.Add(gLB_SISTEMAS);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(gLB_SISTEMAS);
        }

        // GET: GLB_SISTEMAS/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            GLB_SISTEMAS gLB_SISTEMAS = db.GLB_SISTEMAS.Find(id);
            if (gLB_SISTEMAS == null)
            {
                return HttpNotFound();
            }
            return View(gLB_SISTEMAS);
        }

        // POST: GLB_SISTEMAS/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "CD_CODI_SIST,CD_EMPRESA,DS_NOMB_SIST,CD_ESTADO_SISTEMA,USUA_CREA,FECH_CREA,USUA_ACTU,FECH_ACTU")] GLB_SISTEMAS gLB_SISTEMAS)
        {
            if (ModelState.IsValid)
            {
                db.Entry(gLB_SISTEMAS).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(gLB_SISTEMAS);
        }

        // GET: GLB_SISTEMAS/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            GLB_SISTEMAS gLB_SISTEMAS = db.GLB_SISTEMAS.Find(id);
            if (gLB_SISTEMAS == null)
            {
                return HttpNotFound();
            }
            return View(gLB_SISTEMAS);
        }

        // POST: GLB_SISTEMAS/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            GLB_SISTEMAS gLB_SISTEMAS = db.GLB_SISTEMAS.Find(id);
            db.GLB_SISTEMAS.Remove(gLB_SISTEMAS);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
