﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ControlPlanilla.Models;

namespace ControlPlanilla.Controllers
{
    public class VendorPaymentTermsCodesController : AuditoriaController
    {

        // GET: VendorPaymentTermsCodes
        public ActionResult Index()
        {
            return View(dbVendor.VendorPaymentTermsCode.ToList());
        }

        // GET: VendorPaymentTermsCodes/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VendorPaymentTermsCode vendorPaymentTermsCode = dbVendor.VendorPaymentTermsCode.Find(id);
            if (vendorPaymentTermsCode == null)
            {
                return HttpNotFound();
            }
            return View(vendorPaymentTermsCode);
        }

        // GET: VendorPaymentTermsCodes/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: VendorPaymentTermsCodes/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID_PAYMENTTERMSCODE,DESCRIPCION")] VendorPaymentTermsCode vendorPaymentTermsCode)
        {
            if (ModelState.IsValid)
            {
                VendorPaymentTermsCode guardar = new VendorPaymentTermsCode();
                guardar.ID_PAYMENTTERMSCODE = vendorPaymentTermsCode.ID_PAYMENTTERMSCODE;
                guardar.DESCRIPCION = vendorPaymentTermsCode.DESCRIPCION;
                guardar.USUA_CREA = Usuario;
                guardar.FECH_CREA = Ahora;

                dbVendor.VendorPaymentTermsCode.Add(guardar);
                dbVendor.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(vendorPaymentTermsCode);
        }

        // GET: VendorPaymentTermsCodes/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VendorPaymentTermsCode vendorPaymentTermsCode = dbVendor.VendorPaymentTermsCode.Find(id);
            if (vendorPaymentTermsCode == null)
            {
                return HttpNotFound();
            }
            return View(vendorPaymentTermsCode);
        }

        // POST: VendorPaymentTermsCodes/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID_PAYMENTTERMSCODE,DESCRIPCION")] VendorPaymentTermsCode vendorPaymentTermsCode)
        {
            if (ModelState.IsValid)
            {
                VendorPaymentTermsCode guardar = dbVendor.VendorPaymentTermsCode.Where(c => c.ID_PAYMENTTERMSCODE == vendorPaymentTermsCode.ID_PAYMENTTERMSCODE).FirstOrDefault();

                if (guardar != null)
                {
                    guardar.DESCRIPCION = vendorPaymentTermsCode.DESCRIPCION;
                    guardar.USUA_ACTUA = Usuario;
                    guardar.FECH_ACTUA = Ahora;
                    dbVendor.Entry(guardar).State = EntityState.Modified;
                    dbVendor.SaveChanges();
                    return RedirectToAction("Index");
                }
                
            }
            return View(vendorPaymentTermsCode);
        }

        // GET: VendorPaymentTermsCodes/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VendorPaymentTermsCode vendorPaymentTermsCode = dbVendor.VendorPaymentTermsCode.Find(id);
            if (vendorPaymentTermsCode == null)
            {
                return HttpNotFound();
            }
            return View(vendorPaymentTermsCode);
        }

        // POST: VendorPaymentTermsCodes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            VendorPaymentTermsCode vendorPaymentTermsCode = dbVendor.VendorPaymentTermsCode.Find(id);
            dbVendor.VendorPaymentTermsCode.Remove(vendorPaymentTermsCode);
            dbVendor.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                dbVendor.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
