﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ControlPlanilla.Models;
using PagedList;
using PagedList.Mvc;

namespace ControlPlanilla.Controllers
{
    public class PayrollControlsController : AuditoriaController
    {


        // GET: PayrollControls
        public ActionResult Index()
        {
            ViewBag.OpcionSistema = OpcionSistema;
            ViewBag.ArchivoAdjunto = ComprobarRequiereArchivos(OpcionSistema);
            var payrollControl = dbPayControl.PayrollControl.Include(p => p.PayControlCategory).Include(p => p.PayRateEditStatus).Include(p => p.PayrollControlAbsenceCode).Include(p => p.PayrollControlArrearsCode).Include(p => p.PayrollControlMonthlySchedule).Include(p => p.PayrollControlNetPayTest).Include(p => p.PayrollControlPrintOnCheck).Include(p => p.PayrollControlType).Include(p => p.PayrollPostingGroup1).Include(p => p.PayrollPostType).Include(p => p.PayrollTypeReportingAuthority).Include(p => p.PayrollControlNetPayTest).Include(p => p.GLB_TIPOS_DE_IMPUESTOS).Include(p => p.PayrollReportingAuthority);
            return View(payrollControl.ToList());
        }

        // GET: PayrollControls/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PayrollControl payrollControl = dbPayControl.PayrollControl.Find(id);
            if (payrollControl == null)
            {
                return HttpNotFound();
            }
            return View(payrollControl);
        }

        // GET: PayrollControls/Create
        public ActionResult Create()
        {
            ViewBag.CategoryCode = new SelectList(dbPayControl.PayControlCategory, "Code", "Description");
            ViewBag.EditStatus = new SelectList(dbPayControl.PayRateEditStatus, "ID_EDIT", "DESCRIPCION");
            ViewBag.Absencecode = new SelectList(dbPayControl.PayrollControlAbsenceCode, "ID_ABSENCE", "DESCRIPCION");
            ViewBag.ArrearsCode = new SelectList(dbPayControl.PayrollControlArrearsCode, "ID_ARREARS", "DESCRIPCION");
            ViewBag.MonthlySchedule = new SelectList(dbPayControl.PayrollControlMonthlySchedule, "ID_CALENDARIO", "DESCRIPCION");
            //ViewBag.Absencecode = new SelectList(dbPayControl.PayrollControlNetPayTest, "ID_NETPAYTEST", "DESCRIPCION");
            ViewBag.PrintOnCheck = new SelectList(dbPayControl.PayrollControlPrintOnCheck, "ID_PRINT", "DESCRIPCION");
            ViewBag.Type = new SelectList(dbPayControl.PayrollControlType, "ID_TYPE", "DESCRIPCION");
            ViewBag.PayrollPostingGroup = new SelectList(dbPayControl.PayrollPostingGroup, "Code", "Code");
            ViewBag.GLPostType = new SelectList(dbPayControl.PayrollPostType, "ID_POST", "DESCRIPCION");
            ViewBag.ReportingAuthorityType = new SelectList(dbPayControl.PayrollTypeReportingAuthority, "ID_TYPE", "DESCRIPCION");
            ViewBag.NetPayTest = new SelectList(dbPayControl.PayrollControlNetPayTest, "ID_NETPAYTEST", "DESCRIPCION");
            ViewBag.TaxTypeCode = new SelectList(dbPayControl.GLB_TIPOS_DE_IMPUESTOS, "CD_CODIGO_IMPUESTO", "DS_DESCRIPCION_IMPUESTO");
            ViewBag.ReportingAuthorityCode = new SelectList(dbPayControl.PayrollReportingAuthority, "Code", "Name");
            return View();
        }

        // POST: PayrollControls/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Code,Type,Name,County,GLPostType,PayrollPostingGroup,Calculate,NormalSign,DateFilter,GlobalDimension1Filter,GlobalDimension2Filter,WorkTypeFilter,Amount,TaxableAmount,CalcMethods,ReportingAuthorityType,ReportingAuthorityCode,EditStatus,SourcePayControl,DefinedInMasterOrder,CalculationOrders,AutoInsert,MonthlySchedule,EmployerNoFilter,SourceBaseAmountCode,DistrByDimension,DistrByEarnedPayCycle,CategoryCode,Comment,Individual,LoanControl,CreateCashReceiptJournal,PayrollControlPagoHoras,CodTarifa,PorcentajeTarifa,Absencecode,PostAbsence,NetPayTest,TaxTypeCode,ArrearsCode,PrintOnCheck")] PayrollControl payrollControl)
        {
            if (ModelState.IsValid)
            {
                dbPayControl.PayrollControl.Add(payrollControl);
                dbPayControl.SaveChanges();
                bool requiereArchivos = ComprobarRequiereArchivos(OpcionSistema);

                if (requiereArchivos)
                {
                    TempData["OpcionSistema"] = OpcionSistema;
                    TempData["CdForaneo"] = payrollControl.Code;
                    TempData["DsForaneo"] = payrollControl.Name;

                    bool cumplio = ComprobarArchivosAdjuntos();

                    if (!cumplio)
                    {
                        //string opcion, string DsForaneo, string CdForaneo)
                        return RedirectToAction("SubirArchivos", "Auditoria", new { opcion = OpcionSistema, DsForaneo = payrollControl.Name, CdForaneo = payrollControl.Code });
                    }
                }
                return RedirectToAction("Index");
            }

            ViewBag.CategoryCode = new SelectList(dbPayControl.PayControlCategory, "Code", "Description", payrollControl.CategoryCode);
            ViewBag.EditStatus = new SelectList(dbPayControl.PayRateEditStatus, "ID_EDIT", "DESCRIPCION", payrollControl.EditStatus);
            ViewBag.Absencecode = new SelectList(dbPayControl.PayrollControlAbsenceCode, "ID_ABSENCE", "DESCRIPCION", payrollControl.Absencecode);
            ViewBag.ArrearsCode = new SelectList(dbPayControl.PayrollControlArrearsCode, "ID_ARREARS", "DESCRIPCION", payrollControl.ArrearsCode);
            ViewBag.MonthlySchedule = new SelectList(dbPayControl.PayrollControlMonthlySchedule, "ID_CALENDARIO", "DESCRIPCION", payrollControl.MonthlySchedule);
            //ViewBag.Absencecode = new SelectList(dbPayControl.PayrollControlNetPayTest, "ID_NETPAYTEST", "DESCRIPCION", payrollControl.Absencecode);
            ViewBag.PrintOnCheck = new SelectList(dbPayControl.PayrollControlPrintOnCheck, "ID_PRINT", "DESCRIPCION", payrollControl.PrintOnCheck);
            ViewBag.Type = new SelectList(dbPayControl.PayrollControlType, "ID_TYPE", "DESCRIPCION", payrollControl.Type);
            ViewBag.PayrollPostingGroup = new SelectList(dbPayControl.PayrollPostingGroup, "Code", "Code", payrollControl.PayrollPostingGroup);
            ViewBag.GLPostType = new SelectList(dbPayControl.PayrollPostType, "ID_POST", "DESCRIPCION", payrollControl.GLPostType);
            ViewBag.ReportingAuthorityType = new SelectList(dbPayControl.PayrollTypeReportingAuthority, "ID_TYPE", "DESCRIPCION", payrollControl.ReportingAuthorityType);
            ViewBag.NetPayTest = new SelectList(dbPayControl.PayrollControlNetPayTest, "ID_NETPAYTEST", "DESCRIPCION", payrollControl.NetPayTest);
            ViewBag.TaxTypeCode = new SelectList(dbPayControl.GLB_TIPOS_DE_IMPUESTOS, "CD_CODIGO_IMPUESTO", "DS_DESCRIPCION_IMPUESTO", payrollControl.TaxTypeCode);
            ViewBag.ReportingAuthorityCode = new SelectList(dbPayControl.PayrollReportingAuthority, "Code", "Name", payrollControl.ReportingAuthorityCode);
            return View(payrollControl);
        }

        // GET: PayrollControls/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PayrollControl payrollControl = dbPayControl.PayrollControl.Find(id);
            if (payrollControl == null)
            {
                return HttpNotFound();
            }
            ViewBag.CategoryCode = new SelectList(dbPayControl.PayControlCategory, "Code", "Description", payrollControl.CategoryCode);
            ViewBag.EditStatus = new SelectList(dbPayControl.PayRateEditStatus, "ID_EDIT", "DESCRIPCION", payrollControl.EditStatus);
            ViewBag.Absencecode = new SelectList(dbPayControl.PayrollControlAbsenceCode, "ID_ABSENCE", "DESCRIPCION", payrollControl.Absencecode);
            ViewBag.ArrearsCode = new SelectList(dbPayControl.PayrollControlArrearsCode, "ID_ARREARS", "DESCRIPCION", payrollControl.ArrearsCode);
            ViewBag.MonthlySchedule = new SelectList(dbPayControl.PayrollControlMonthlySchedule, "ID_CALENDARIO", "DESCRIPCION", payrollControl.MonthlySchedule);
            //ViewBag.Absencecode = new SelectList(dbPayControl.PayrollControlNetPayTest, "ID_NETPAYTEST", "DESCRIPCION", payrollControl.Absencecode);
            ViewBag.PrintOnCheck = new SelectList(dbPayControl.PayrollControlPrintOnCheck, "ID_PRINT", "DESCRIPCION", payrollControl.PrintOnCheck);
            ViewBag.Type = new SelectList(dbPayControl.PayrollControlType, "ID_TYPE", "DESCRIPCION", payrollControl.Type);
            ViewBag.PayrollPostingGroup = new SelectList(dbPayControl.PayrollPostingGroup, "Code", "Code", payrollControl.PayrollPostingGroup);
            ViewBag.GLPostType = new SelectList(dbPayControl.PayrollPostType, "ID_POST", "DESCRIPCION", payrollControl.GLPostType);
            ViewBag.ReportingAuthorityType = new SelectList(dbPayControl.PayrollTypeReportingAuthority, "ID_TYPE", "DESCRIPCION", payrollControl.ReportingAuthorityType);
            ViewBag.NetPayTest = new SelectList(dbPayControl.PayrollControlNetPayTest, "ID_NETPAYTEST", "DESCRIPCION", payrollControl.NetPayTest);
            ViewBag.TaxTypeCode = new SelectList(dbPayControl.GLB_TIPOS_DE_IMPUESTOS, "CD_CODIGO_IMPUESTO", "DS_DESCRIPCION_IMPUESTO", payrollControl.TaxTypeCode);
            ViewBag.ReportingAuthorityCode = new SelectList(dbPayControl.PayrollReportingAuthority, "Code", "Name", payrollControl.ReportingAuthorityCode);
            return View(payrollControl);
        }

        // POST: PayrollControls/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Code,Type,Name,County,GLPostType,PayrollPostingGroup,Calculate,NormalSign,DateFilter,GlobalDimension1Filter,")] PayrollControl payrollControl)
        {
            if (ModelState.IsValid)
            {
                PayrollControl guardar = dbPayControl.PayrollControl.Find(payrollControl.Code);
                guardar.Code = payrollControl.Code;
                guardar.Type = payrollControl.Type;
                guardar.Name = payrollControl.Name;
                guardar.LastDateModified = Ahora;
                guardar.County = payrollControl.County;
                guardar.GLPostType = payrollControl.GLPostType;
                guardar.PayrollPostingGroup = payrollControl.PayrollPostingGroup;
                guardar.Calculate = payrollControl.Calculate;
                guardar.NormalSign = payrollControl.NormalSign;
                guardar.DateFilter = payrollControl.DateFilter;
                guardar.GlobalDimension1Filter = payrollControl.GlobalDimension1Filter;
                guardar.GlobalDimension2Filter = payrollControl.GlobalDimension2Filter;
                guardar.WorkTypeFilter = payrollControl.WorkTypeFilter;
                guardar.Amount = payrollControl.Amount;
                guardar.TaxableAmount = payrollControl.TaxableAmount;
                guardar.CalcMethods = payrollControl.CalcMethods;
                guardar.ReportingAuthorityType = payrollControl.ReportingAuthorityType;
                guardar.ReportingAuthorityCode = payrollControl.ReportingAuthorityCode;
                guardar.EditStatus = payrollControl.EditStatus;
                guardar.SourcePayControl = payrollControl.SourcePayControl;
                guardar.DefinedInMasterOrder = payrollControl.DefinedInMasterOrder;
                guardar.CalculationOrders = payrollControl.CalculationOrders;
                guardar.AutoInsert = payrollControl.AutoInsert;
                guardar.MonthlySchedule = payrollControl.MonthlySchedule;
                guardar.EmployerNoFilter = payrollControl.EmployerNoFilter;
                guardar.SourceBaseAmountCode = payrollControl.SourceBaseAmountCode;
                guardar.DistrByDimension = payrollControl.DistrByDimension;
                guardar.DistrByEarnedPayCycle = payrollControl.DistrByEarnedPayCycle;
                guardar.CategoryCode = payrollControl.CategoryCode;
                guardar.Comment = payrollControl.Comment;
                guardar.Individual = payrollControl.Individual;
                guardar.LoanControl = payrollControl.LoanControl;
                guardar.CreateCashReceiptJournal = payrollControl.CreateCashReceiptJournal;
                guardar.PayrollControlPagoHoras = payrollControl.PayrollControlPagoHoras;
                guardar.CodTarifa = payrollControl.CodTarifa;
                guardar.PorcentajeTarifa = payrollControl.PorcentajeTarifa;
                guardar.Absencecode = payrollControl.Absencecode;
                guardar.PostAbsence = payrollControl.PostAbsence;
                guardar.NetPayTest = payrollControl.NetPayTest;
                guardar.TaxTypeCode = payrollControl.TaxTypeCode;
                guardar.ArrearsCode = payrollControl.ArrearsCode;
                guardar.PrintOnCheck = payrollControl.PrintOnCheck;


                dbPayControl.Entry(guardar).State = EntityState.Modified;
                dbPayControl.SaveChanges();
                bool requiereArchivos = ComprobarRequiereArchivos(OpcionSistema);

                if (requiereArchivos)
                {
                    TempData["OpcionSistema"] = OpcionSistema;
                    TempData["CdForaneo"] = payrollControl.Code;
                    TempData["DsForaneo"] = payrollControl.Name;

                    bool cumplio = ComprobarArchivosAdjuntos();

                    if (!cumplio)
                    {
                        //string opcion, string DsForaneo, string CdForaneo)
                        return RedirectToAction("SubirArchivos", "Auditoria", new { opcion = OpcionSistema, DsForaneo = payrollControl.Name, CdForaneo = payrollControl.Code });
                    }
                }
                return RedirectToAction("Index");
            }
            ViewBag.CategoryCode = new SelectList(dbPayControl.PayControlCategory, "Code", "Description", payrollControl.CategoryCode);
            ViewBag.EditStatus = new SelectList(dbPayControl.PayRateEditStatus, "ID_EDIT", "DESCRIPCION", payrollControl.EditStatus);
            ViewBag.Absencecode = new SelectList(dbPayControl.PayrollControlAbsenceCode, "ID_ABSENCE", "DESCRIPCION", payrollControl.Absencecode);
            ViewBag.ArrearsCode = new SelectList(dbPayControl.PayrollControlArrearsCode, "ID_ARREARS", "DESCRIPCION", payrollControl.ArrearsCode);
            ViewBag.MonthlySchedule = new SelectList(dbPayControl.PayrollControlMonthlySchedule, "ID_CALENDARIO", "DESCRIPCION", payrollControl.MonthlySchedule);
            //ViewBag.Absencecode = new SelectList(dbPayControl.PayrollControlNetPayTest, "ID_NETPAYTEST", "DESCRIPCION", payrollControl.Absencecode);
            ViewBag.PrintOnCheck = new SelectList(dbPayControl.PayrollControlPrintOnCheck, "ID_PRINT", "DESCRIPCION", payrollControl.PrintOnCheck);
            ViewBag.Type = new SelectList(dbPayControl.PayrollControlType, "ID_TYPE", "DESCRIPCION", payrollControl.Type);
            ViewBag.PayrollPostingGroup = new SelectList(dbPayControl.PayrollPostingGroup, "Code", "Code", payrollControl.PayrollPostingGroup);
            ViewBag.GLPostType = new SelectList(dbPayControl.PayrollPostType, "ID_POST", "DESCRIPCION", payrollControl.GLPostType);
            ViewBag.ReportingAuthorityType = new SelectList(dbPayControl.PayrollTypeReportingAuthority, "ID_TYPE", "DESCRIPCION", payrollControl.ReportingAuthorityType);
            ViewBag.NetPayTest = new SelectList(dbPayControl.PayrollControlNetPayTest, "ID_NETPAYTEST", "DESCRIPCION", payrollControl.NetPayTest);
            ViewBag.TaxTypeCode = new SelectList(dbPayControl.GLB_TIPOS_DE_IMPUESTOS, "CD_CODIGO_IMPUESTO", "DS_DESCRIPCION_IMPUESTO", payrollControl.TaxTypeCode);
            ViewBag.ReportingAuthorityCode = new SelectList(dbPayControl.PayrollReportingAuthority, "Code", "Name", payrollControl.ReportingAuthorityCode);
            return View(payrollControl);
        }

        // GET: PayrollControls/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PayrollControl payrollControl = dbPayControl.PayrollControl.Find(id);
            if (payrollControl == null)
            {
                return HttpNotFound();
            }
            return View(payrollControl);
        }

        // POST: PayrollControls/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            PayrollControl payrollControl = dbPayControl.PayrollControl.Find(id);
            dbPayControl.PayrollControl.Remove(payrollControl);
            dbPayControl.SaveChanges();
            return RedirectToAction("Index");
        }

        /*CRUD PARA COMENTARIOS EN PAY CONTROL*/
        [HttpPost]
        public PartialViewResult CommentsPayControl(string PayControl, int? page)
        {
            try
            {
                var comentarios = dbPayControl.PayrollControlComment.Where(c => c.CodePayControl == PayControl).ToList();
                int pageNumber = (page ?? 1);
                var result = comentarios.ToPagedList(pageNumber, pageSize);
                ViewBag.PayControl = PayControl;
                return PartialView(result);
            }
            catch (Exception)
            {

                throw;
            }
        }
        [HttpPost]
        public bool CrearComment([Bind(Include = "CodePayControl, COMMENT")] PayrollControlComment payrollControlComment)
        {
            bool result = false;
            try
            {
                if (ModelState.IsValid)
                {
                    PayrollControlComment guardar = new PayrollControlComment();
                    guardar.CodePayControl = payrollControlComment.CodePayControl;
                    guardar.COMMENT = payrollControlComment.COMMENT;
                    guardar.USUA_CREA = Usuario;
                    guardar.FECH_CREA = Ahora;
                    dbPayControl.PayrollControlComment.Add(guardar);
                    dbPayControl.SaveChanges();
                    result = true;
                }


            }
            catch (SystemException ex)
            {
                logRepository.CapturarTodoError(ex);

            }
            return result;
        }
        [HttpPost]
        public bool EditarComment([Bind(Include = "ID_COMMENT,CodePayControl, COMMENT")] PayrollControlComment payrollControlComment)
        {
            bool result = false;
            try
            {
                if (ModelState.IsValid)
                {
                    PayrollControlComment guardar = dbPayControl.PayrollControlComment.Find(payrollControlComment.ID_COMMENT);
                    guardar.COMMENT = payrollControlComment.COMMENT;
                    guardar.USUA_ACTUA = Usuario;
                    guardar.FECH_ACTUA = Ahora;
                    dbPayControl.Entry(guardar).State = EntityState.Modified;
                    dbPayControl.SaveChanges();
                    result = true;
                }


            }
            catch (SystemException ex)
            {
                logRepository.CapturarTodoError(ex);

            }
            return result;
        }
        [HttpPost]
        public bool EliminarComment(int Id)
        {
            bool result = false;
            try
            {

                PayrollControlComment remv = dbPayControl.PayrollControlComment.Find(Id);
                if (remv != null)
                {
                    dbPayControl.PayrollControlComment.Remove(remv);
                    dbPayControl.SaveChanges();
                    result = true;
                }

            }
            catch (SystemException ex)
            {
                logRepository.CapturarTodoError(ex);

            }
            return result;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                dbPayControl.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
