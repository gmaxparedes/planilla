﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ControlPlanilla.Models;

namespace ControlPlanilla.Controllers
{
    public class GLB_TIPOS_DE_IMPUESTOSController : AuditoriaController
    {
        // GET: GLB_TIPOS_DE_IMPUESTOS
        public ActionResult Index()
        {
            /**Si da error que Impuestos no tiene empresa y/paises agregar las siguientes lineas en la 
             clase GLB_TIPOS_DE_IMPUESTOS 
            public virtual EMPRESAS EMPRESAS {get; set;}
            public virtual PAISES PAISES {get; set;}
             */
            var gLB_TIPOS_DE_IMPUESTOS = dbMenus.GLB_TIPOS_DE_IMPUESTOS.Include(g => g.EMPRESAS).Include(g => g.PAISES);
            return View(gLB_TIPOS_DE_IMPUESTOS.ToList());
        }

        // GET: GLB_TIPOS_DE_IMPUESTOS/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            GLB_TIPOS_DE_IMPUESTOS gLB_TIPOS_DE_IMPUESTOS = dbMenus.GLB_TIPOS_DE_IMPUESTOS.Find(id);
            if (gLB_TIPOS_DE_IMPUESTOS == null)
            {
                return HttpNotFound();
            }
            return View(gLB_TIPOS_DE_IMPUESTOS);
        }

        // GET: GLB_TIPOS_DE_IMPUESTOS/Create
        public ActionResult Create()
        {
            ViewBag.CD_EMPRESA = new SelectList(dbMenus.EMPRESAS, "CD_EMPRESA", "DS_EMPRESA");
            ViewBag.CD_PAIS = new SelectList(dbMenus.PAISES, "CD_PAIS", "DS_NOMBREPAIS");
            return View();
        }

        // POST: GLB_TIPOS_DE_IMPUESTOS/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "CD_CODIGO_IMPUESTO,CD_PAIS,CD_EMPRESA,DS_DESCRIPCION_IMPUESTO,VL_PORCENTAJE_1,VL_PORCENTAJE_2, CD_CUENTA_CONTABLE")] GLB_TIPOS_DE_IMPUESTOS gLB_TIPOS_DE_IMPUESTOS)
        {
            if (ModelState.IsValid)
            {
                GLB_TIPOS_DE_IMPUESTOS guardar = new GLB_TIPOS_DE_IMPUESTOS();
                guardar = gLB_TIPOS_DE_IMPUESTOS;
                guardar.USUA_CREA = Usuario;
                guardar.FECH_CREA = Ahora;
                dbMenus.GLB_TIPOS_DE_IMPUESTOS.Add(guardar);
                dbMenus.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.CD_EMPRESA = new SelectList(dbMenus.EMPRESAS, "CD_EMPRESA", "DS_EMPRESA", gLB_TIPOS_DE_IMPUESTOS.CD_EMPRESA);
            ViewBag.CD_PAIS = new SelectList(dbMenus.PAISES, "CD_PAIS", "DS_NOMBREPAIS", gLB_TIPOS_DE_IMPUESTOS.CD_PAIS);
            return View(gLB_TIPOS_DE_IMPUESTOS);
        }

        // GET: GLB_TIPOS_DE_IMPUESTOS/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            GLB_TIPOS_DE_IMPUESTOS gLB_TIPOS_DE_IMPUESTOS = dbMenus.GLB_TIPOS_DE_IMPUESTOS.Find(id);
            if (gLB_TIPOS_DE_IMPUESTOS == null)
            {
                return HttpNotFound();
            }
            ViewBag.CD_EMPRESA = new SelectList(dbMenus.EMPRESAS, "CD_EMPRESA", "DS_EMPRESA", gLB_TIPOS_DE_IMPUESTOS.CD_EMPRESA);
            ViewBag.CD_PAIS = new SelectList(dbMenus.PAISES, "CD_PAIS", "DS_NOMBREPAIS", gLB_TIPOS_DE_IMPUESTOS.CD_PAIS);
            return View(gLB_TIPOS_DE_IMPUESTOS);
        }

        // POST: GLB_TIPOS_DE_IMPUESTOS/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "CD_CODIGO_IMPUESTO,CD_PAIS,CD_EMPRESA,DS_DESCRIPCION_IMPUESTO,VL_PORCENTAJE_1,VL_PORCENTAJE_2,CD_CUENTA_CONTABLE")] GLB_TIPOS_DE_IMPUESTOS gLB_TIPOS_DE_IMPUESTOS)
        {
            if (ModelState.IsValid)
            {
                GLB_TIPOS_DE_IMPUESTOS guardar = dbMenus.GLB_TIPOS_DE_IMPUESTOS.Find(gLB_TIPOS_DE_IMPUESTOS.CD_CODIGO_IMPUESTO);
                guardar.DS_DESCRIPCION_IMPUESTO = gLB_TIPOS_DE_IMPUESTOS.DS_DESCRIPCION_IMPUESTO;
                guardar.CD_PAIS = gLB_TIPOS_DE_IMPUESTOS.CD_PAIS;
                guardar.CD_EMPRESA = gLB_TIPOS_DE_IMPUESTOS.CD_EMPRESA;
                guardar.VL_PORCENTAJE_1 = gLB_TIPOS_DE_IMPUESTOS.VL_PORCENTAJE_1;
                guardar.VL_PORCENTAJE_2 = gLB_TIPOS_DE_IMPUESTOS.VL_PORCENTAJE_2;
                guardar.USUA_ACTU = Usuario;
                guardar.FECH_ACTU = Ahora;

                dbMenus.Entry(guardar).State = EntityState.Modified;
                dbMenus.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CD_EMPRESA = new SelectList(dbMenus.EMPRESAS, "CD_EMPRESA", "DS_EMPRESA", gLB_TIPOS_DE_IMPUESTOS.CD_EMPRESA);
            ViewBag.CD_PAIS = new SelectList(dbMenus.PAISES, "CD_PAIS", "DS_NOMBREPAIS", gLB_TIPOS_DE_IMPUESTOS.CD_PAIS);
            return View(gLB_TIPOS_DE_IMPUESTOS);
        }

        // GET: GLB_TIPOS_DE_IMPUESTOS/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            GLB_TIPOS_DE_IMPUESTOS gLB_TIPOS_DE_IMPUESTOS = dbMenus.GLB_TIPOS_DE_IMPUESTOS.Find(id);
            if (gLB_TIPOS_DE_IMPUESTOS == null)
            {
                return HttpNotFound();
            }
            return View(gLB_TIPOS_DE_IMPUESTOS);
        }

        // POST: GLB_TIPOS_DE_IMPUESTOS/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            GLB_TIPOS_DE_IMPUESTOS gLB_TIPOS_DE_IMPUESTOS = dbMenus.GLB_TIPOS_DE_IMPUESTOS.Find(id);
            dbMenus.GLB_TIPOS_DE_IMPUESTOS.Remove(gLB_TIPOS_DE_IMPUESTOS);
            dbMenus.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                dbMenus.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
