﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ControlPlanilla.Models;

namespace ControlPlanilla.Controllers
{
    public class VendorApplicationMethodsController : AuditoriaController
    {

        // GET: VendorApplicationMethods
        public ActionResult Index()
        {
            return View(dbVendor.VendorApplicationMethod.ToList());
        }

        // GET: VendorApplicationMethods/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VendorApplicationMethod vendorApplicationMethod = dbVendor.VendorApplicationMethod.Find(id);
            if (vendorApplicationMethod == null)
            {
                return HttpNotFound();
            }
            return View(vendorApplicationMethod);
        }

        // GET: VendorApplicationMethods/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: VendorApplicationMethods/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "DESCRIPCION")] VendorApplicationMethod vendorApplicationMethod)
        {
            if (ModelState.IsValid)
            {
                VendorApplicationMethod guardar = new VendorApplicationMethod();

                guardar.DESCRIPCION = vendorApplicationMethod.DESCRIPCION;
                guardar.USUA_CREA = Usuario;
                guardar.FECH_CREA = Ahora;
                dbVendor.VendorApplicationMethod.Add(guardar);
                dbVendor.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(vendorApplicationMethod);
        }

        // GET: VendorApplicationMethods/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VendorApplicationMethod vendorApplicationMethod = dbVendor.VendorApplicationMethod.Find(id);
            if (vendorApplicationMethod == null)
            {
                return HttpNotFound();
            }
            return View(vendorApplicationMethod);
        }

        // POST: VendorApplicationMethods/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID_APPLICATIONMETHOD,DESCRIPCION")] VendorApplicationMethod vendorApplicationMethod)
        {
            if (ModelState.IsValid)
            {
                VendorApplicationMethod guardar = dbVendor.VendorApplicationMethod.Find(vendorApplicationMethod.ID_APPLICATIONMETHOD);

                guardar.DESCRIPCION = vendorApplicationMethod.DESCRIPCION;
                guardar.USUA_ACTUA = Usuario;
                guardar.FECH_ACTUA = Ahora;
                dbVendor.Entry(guardar).State = EntityState.Modified;
                dbVendor.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(vendorApplicationMethod);
        }

        // GET: VendorApplicationMethods/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VendorApplicationMethod vendorApplicationMethod = dbVendor.VendorApplicationMethod.Find(id);
            if (vendorApplicationMethod == null)
            {
                return HttpNotFound();
            }
            return View(vendorApplicationMethod);
        }

        // POST: VendorApplicationMethods/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            VendorApplicationMethod vendorApplicationMethod = dbVendor.VendorApplicationMethod.Find(id);
            dbVendor.VendorApplicationMethod.Remove(vendorApplicationMethod);
            dbVendor.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                dbVendor.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
