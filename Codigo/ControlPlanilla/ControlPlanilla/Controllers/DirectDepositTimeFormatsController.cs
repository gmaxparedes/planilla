﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ControlPlanilla.Models;

namespace ControlPlanilla.Controllers
{
    public class DirectDepositTimeFormatsController : AuditoriaController
    {

        // GET: DirectDepositTimeFormats
        public ActionResult Index()
        {
            return View(dbDDL.DirectDepositTimeFormat.ToList());
        }

        // GET: DirectDepositTimeFormats/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DirectDepositTimeFormat directDepositTimeFormat = dbDDL.DirectDepositTimeFormat.Find(id);
            if (directDepositTimeFormat == null)
            {
                return HttpNotFound();
            }
            return View(directDepositTimeFormat);
        }

        // GET: DirectDepositTimeFormats/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: DirectDepositTimeFormats/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID_TIME_FORMAT,DESCRIPCION")] DirectDepositTimeFormat directDepositTimeFormat)
        {
            if (ModelState.IsValid)
            {
                directDepositTimeFormat.USUA_CREA = Usuario;
                directDepositTimeFormat.FECH_CREA = Ahora;
                dbDDL.DirectDepositTimeFormat.Add(directDepositTimeFormat);
                dbDDL.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(directDepositTimeFormat);
        }

        // GET: DirectDepositTimeFormats/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DirectDepositTimeFormat directDepositTimeFormat = dbDDL.DirectDepositTimeFormat.Find(id);
            if (directDepositTimeFormat == null)
            {
                return HttpNotFound();
            }
            return View(directDepositTimeFormat);
        }

        // POST: DirectDepositTimeFormats/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID_TIME_FORMAT,DESCRIPCION")] DirectDepositTimeFormat directDepositTimeFormat)
        {
            if (ModelState.IsValid)
            {
                var guardar = dbDDL.DirectDepositTimeFormat.Find(directDepositTimeFormat.ID_TIME_FORMAT);
                guardar.DESCRIPCION = directDepositTimeFormat.DESCRIPCION;
                guardar.USUA_ACTUA = Usuario;
                guardar.FECH_ACTUA = Ahora;
                dbDDL.Entry(guardar).State = EntityState.Modified;
                dbDDL.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(directDepositTimeFormat);
        }

        // GET: DirectDepositTimeFormats/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DirectDepositTimeFormat directDepositTimeFormat = dbDDL.DirectDepositTimeFormat.Find(id);
            if (directDepositTimeFormat == null)
            {
                return HttpNotFound();
            }
            return View(directDepositTimeFormat);
        }

        // POST: DirectDepositTimeFormats/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            DirectDepositTimeFormat directDepositTimeFormat = dbDDL.DirectDepositTimeFormat.Find(id);
            dbDDL.DirectDepositTimeFormat.Remove(directDepositTimeFormat);
            dbDDL.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                dbDDL.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
