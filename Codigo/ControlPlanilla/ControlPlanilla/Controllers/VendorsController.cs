﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ControlPlanilla.Models;
using PagedList;

namespace ControlPlanilla.Controllers
{
    public class VendorsController : AuditoriaController
    {
        

        // GET: Vendors
        public ActionResult Index(string SearchString, int? page)
        {
            var vendor = dbVendor.Vendor.Include(v => v.VendorApplicationMethod).Include(v => v.VendorPartnerType).Include(v => v.VendorPaymentTermsCode).Include(v => v.VendorStatisticsGroup).Include(v => v.VendorTaxIdentificationType).ToList();
            if (!string.IsNullOrEmpty(SearchString))
            {
                vendor = vendor.Where(c => c.SearchName == SearchString).ToList();
            }
            int pageNumber = (page ?? 1);

            var result = vendor.ToPagedList(pageNumber, pageSize);
            return View(result);
        }

        // GET: Vendors/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Vendor vendor = dbVendor.Vendor.Find(id);
            if (vendor == null)
            {
                return HttpNotFound();
            }
            return View(vendor);
        }

        // GET: Vendors/Create
        public ActionResult Create()
        {
            ViewBag.ApplicationMethod = new SelectList(dbVendor.VendorApplicationMethod, "ID_APPLICATIONMETHOD", "DESCRIPCION");
            ViewBag.PartnerType = new SelectList(dbVendor.VendorPartnerType, "ID_PARTNERTYPE", "DESCRIPCION");
            ViewBag.PaymentTermsCode = new SelectList(dbVendor.VendorPaymentTermsCode, "ID_PAYMENTTERMSCODE", "DESCRIPCION");
            ViewBag.StatisticsGroup = new SelectList(dbVendor.VendorStatisticsGroup, "ID_STATISCS", "DESCRIPCION");
            ViewBag.TaxIdentificationType = new SelectList(dbVendor.VendorTaxIdentificationType, "ID_TAXTYPE", "DESCRIPCION");
            return View();
        }

        // POST: Vendors/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "No,Name,SearchName,Name2,Address,Address2,City,Contact,PhoneNo,TelexNo,OurAccountNo,TerritoryCode,GlobalDimension1Code,GlobalDimension2Code,BudgetedAmount,VendorPostingGroup,CurrencyCode,LanguageCode,StatisticsGroup,PaymentTermsCode,FinChargeTermsCode,PurchaserCode,ShipmentMethodCode,ShippingAgentCode,InvoiceDiscCode,CountryRegionCode,Comment,Blocked,PaytoVendorNo,Priority,PaymentMethodCode,DateFilter,GlobalDimension1Filter,GlobalDimension2Filter,Balance,BalanceLCY,NetChange,NetChangeLCY,PurchasesLCY,InvDiscountsLCY,PmtDiscountsLCY,BalanceDue,BalanceDueLCY,Payments,InvoiceAmounts,CrMemoAmounts,FinanceChargeMemoAmounts,PaymentsLCY,InvAmountsLCY,CrMemoAmountsLCY,FinChargeMemoAmountsLCY,OutstandingOrders,AmtRcdNotInvoiced,ApplicationMethod,PricesIncludingVAT,FaxNo,TelexAnswerBack,VATRegistrationNo,GenBusPostingGroup,Picture,GLN,PostCode,County,DebitAmount,CreditAmount,DebitAmountLCY,CreditAmountLCY,EMail,HomePage,ReminderAmounts,ReminderAmountsLCY,NoSeries,TaxAreaCode,TaxLiable,VATBusPostingGroup,CurrencyFilter,OutstandingOrdersLCY,AmtRcdNotInvoicedLCY,BlockPaymentTolerance,PmtDiscToleranceLCY,PmtToleranceLCY,ICPartnerCode,Refunds,RefundsLCY,OtherAmounts,OtherAmountsLCY,PrepaymentPercent,OutstandingInvoices,OutstandingInvoicesLCY,PaytoNoOfArchivedDoc,BuyfromNoOfArchivedDoc,PartnerType,CreditorNo,PreferredBankAccount,CashFlowPaymentTermsCode,PrimaryContactNo,ResponsibilityCenter,LocationCode,LeadTimeCalculation,NoofPstdReceipts,NoofPstdInvoices,NoofPstdReturnShipments,NoofPstdCreditMemos,PaytoNoofOrders,PaytoNoofInvoices,PaytoNoofReturnOrders,PaytoNoofCreditMemos,PaytoNoofPstdReceipts,PaytoNoofPstdInvoices,PaytoNoofPstdReturnS,PaytoNoofPstdCrMemos,NoofQuotes,NoofBlanketOrders,NoofOrders,NoofInvoices,NoofReturnOrders,NoofCreditMemos,NoofOrderAddresses,PaytoNoofQuotes,PaytoNoofBlanketOrders,BaseCalendarCode,UPSZone,FederalIDNo,BankCommunication,CheckDateFormat,CheckDateSeparator,IRS1099Code,BalanceonDate,BalanceonDateLCY,RFCNo,CURPNo,StateInscription,FATCAfilingrequirement,TaxIdentificationType,CAI,FechavencimientoCAI,ABN,Registered,ABNDivisionPartNo,ForeignVend,WHTBusinessPostingGroup,WHTPayableAmountLCY,WHTRegistrationID,IDNo,PostDatedChecksLCY,RTCFilterField,BuyerGroupCode,BuyerID")] Vendor vendor)
        {
            if (ModelState.IsValid)
            {
                dbVendor.Vendor.Add(vendor);
                dbVendor.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ApplicationMethod = new SelectList(dbVendor.VendorApplicationMethod, "ID_APPLICATIONMETHOD", "DESCRIPCION", vendor.ApplicationMethod);
            ViewBag.PartnerType = new SelectList(dbVendor.VendorPartnerType, "ID_PARTNERTYPE", "DESCRIPCION", vendor.PartnerType);
            ViewBag.PaymentTermsCode = new SelectList(dbVendor.VendorPaymentTermsCode, "ID_PAYMENTTERMSCODE", "DESCRIPCION", vendor.PaymentTermsCode);
            ViewBag.StatisticsGroup = new SelectList(dbVendor.VendorStatisticsGroup, "ID_STATISCS", "DESCRIPCION", vendor.StatisticsGroup);
            ViewBag.TaxIdentificationType = new SelectList(dbVendor.VendorTaxIdentificationType, "ID_TAXTYPE", "DESCRIPCION", vendor.TaxIdentificationType);
            return View(vendor);
        }

        // GET: Vendors/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Vendor vendor = dbVendor.Vendor.Find(id);
            if (vendor == null)
            {
                return HttpNotFound();
            }

            ViewBag.ApplicationMethod = new SelectList(dbVendor.VendorApplicationMethod, "ID_APPLICATIONMETHOD", "DESCRIPCION", vendor.ApplicationMethod);
            ViewBag.PartnerType = new SelectList(dbVendor.VendorPartnerType, "ID_PARTNERTYPE", "DESCRIPCION", vendor.PartnerType);
            ViewBag.PaymentTermsCode = new SelectList(dbVendor.VendorPaymentTermsCode, "ID_PAYMENTTERMSCODE", "DESCRIPCION", vendor.PaymentTermsCode);
            ViewBag.StatisticsGroup = new SelectList(dbVendor.VendorStatisticsGroup, "ID_STATISCS", "DESCRIPCION", vendor.StatisticsGroup);
            ViewBag.TaxIdentificationType = new SelectList(dbVendor.VendorTaxIdentificationType, "ID_TAXTYPE", "DESCRIPCION", vendor.TaxIdentificationType);
            return View(vendor);
        }

        // POST: Vendors/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "No,Name,SearchName,Name2,Address,Address2,City,Contact,PhoneNo,TelexNo,OurAccountNo,TerritoryCode,GlobalDimension1Code,GlobalDimension2Code,BudgetedAmount,VendorPostingGroup,CurrencyCode,LanguageCode,StatisticsGroup,PaymentTermsCode,FinChargeTermsCode,PurchaserCode,ShipmentMethodCode,ShippingAgentCode,InvoiceDiscCode,CountryRegionCode,Comment,Blocked,PaytoVendorNo,Priority,PaymentMethodCode,DateFilter,GlobalDimension1Filter,GlobalDimension2Filter,Balance,BalanceLCY,NetChange,NetChangeLCY,PurchasesLCY,InvDiscountsLCY,PmtDiscountsLCY,BalanceDue,BalanceDueLCY,Payments,InvoiceAmounts,CrMemoAmounts,FinanceChargeMemoAmounts,PaymentsLCY,InvAmountsLCY,CrMemoAmountsLCY,FinChargeMemoAmountsLCY,OutstandingOrders,AmtRcdNotInvoiced,ApplicationMethod,PricesIncludingVAT,FaxNo,TelexAnswerBack,VATRegistrationNo,GenBusPostingGroup,Picture,GLN,PostCode,County,DebitAmount,CreditAmount,DebitAmountLCY,CreditAmountLCY,EMail,HomePage,ReminderAmounts,ReminderAmountsLCY,NoSeries,TaxAreaCode,TaxLiable,VATBusPostingGroup,CurrencyFilter,OutstandingOrdersLCY,AmtRcdNotInvoicedLCY,BlockPaymentTolerance,PmtDiscToleranceLCY,PmtToleranceLCY,ICPartnerCode,Refunds,RefundsLCY,OtherAmounts,OtherAmountsLCY,PrepaymentPercent,OutstandingInvoices,OutstandingInvoicesLCY,PaytoNoOfArchivedDoc,BuyfromNoOfArchivedDoc,PartnerType,CreditorNo,PreferredBankAccount,CashFlowPaymentTermsCode,PrimaryContactNo,ResponsibilityCenter,LocationCode,LeadTimeCalculation,NoofPstdReceipts,NoofPstdInvoices,NoofPstdReturnShipments,NoofPstdCreditMemos,PaytoNoofOrders,PaytoNoofInvoices,PaytoNoofReturnOrders,PaytoNoofCreditMemos,PaytoNoofPstdReceipts,PaytoNoofPstdInvoices,PaytoNoofPstdReturnS,PaytoNoofPstdCrMemos,NoofQuotes,NoofBlanketOrders,NoofOrders,NoofInvoices,NoofReturnOrders,NoofCreditMemos,NoofOrderAddresses,PaytoNoofQuotes,PaytoNoofBlanketOrders,BaseCalendarCode,UPSZone,FederalIDNo,BankCommunication,CheckDateFormat,CheckDateSeparator,IRS1099Code,BalanceonDate,BalanceonDateLCY,RFCNo,CURPNo,StateInscription,FATCAfilingrequirement,TaxIdentificationType,CAI,FechavencimientoCAI,ABN,Registered,ABNDivisionPartNo,ForeignVend,WHTBusinessPostingGroup,WHTPayableAmountLCY,WHTRegistrationID,IDNo,PostDatedChecksLCY,RTCFilterField,BuyerGroupCode,BuyerID")] Vendor vendor)
        {
            if (ModelState.IsValid)
            {
                vendor.LastDateModified = Ahora;
                dbVendor.Entry(vendor).State = EntityState.Modified;
                dbVendor.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ApplicationMethod = new SelectList(dbVendor.VendorApplicationMethod, "ID_APPLICATIONMETHOD", "DESCRIPCION", vendor.ApplicationMethod);
            ViewBag.PartnerType = new SelectList(dbVendor.VendorPartnerType, "ID_PARTNERTYPE", "DESCRIPCION", vendor.PartnerType);
            ViewBag.PaymentTermsCode = new SelectList(dbVendor.VendorPaymentTermsCode, "ID_PAYMENTTERMSCODE", "DESCRIPCION", vendor.PaymentTermsCode);
            ViewBag.StatisticsGroup = new SelectList(dbVendor.VendorStatisticsGroup, "ID_STATISCS", "DESCRIPCION", vendor.StatisticsGroup);
            ViewBag.TaxIdentificationType = new SelectList(dbVendor.VendorTaxIdentificationType, "ID_TAXTYPE", "DESCRIPCION", vendor.TaxIdentificationType);
            return View(vendor);
        }

        // GET: Vendors/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Vendor vendor = dbVendor.Vendor.Find(id);
            if (vendor == null)
            {
                return HttpNotFound();
            }
            return View(vendor);
        }

        // POST: Vendors/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            Vendor vendor = dbVendor.Vendor.Find(id);
            dbVendor.Vendor.Remove(vendor);
            dbVendor.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                dbVendor.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
