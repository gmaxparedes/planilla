﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ControlPlanilla.Models;

namespace ControlPlanilla.Controllers
{
    public class ArchivosAdjuntosController : AuditoriaController
    {
        

        // GET: ArchivosAdjuntos
        public ActionResult Index()
        {
            return View();
        }
        //POST
        [HttpPost]
        public ActionResult Index(string CdOpcionSistema)
        {
            CdOpcionSistema = "PayFrec";
            ViewBag.CD_TIPO_DOCUMENTO = dbMenus.GLB_RELA_TIPO_DOCUMENTOS_OPCI_SIST.Where(c => c.CD_CODI_OPCI_SIST == CdOpcionSistema)
                    .Select(e => new SelectListItem()
                    {
                        Text = e.GLB_TIPO_DOCUMENTOS.DS_TIPO_DOCUMENTO,
                        Value = e.CD_TIPO_DOCUMENTO
                    }).ToList();
            //ViewBag.CD_TIPO_DOCUMENTO = DocRequeridos;

            //ViewBag.CD_TIPO_DOCUMENTO = new SelectList(dbMenus.GLB_TIPO_DOCUMENTOS, "CD_TIPO_DOCUMENTO", "DS_TIPO_DOCUMENTO");
            return View();
        }

        // GET: ArchivosAdjuntos/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ARCHIVOS_ADJUNTOS aRCHIVOS_ADJUNTOS = dbMenus.ARCHIVOS_ADJUNTOS.Find(id);
            if (aRCHIVOS_ADJUNTOS == null)
            {
                return HttpNotFound();
            }
            return View(aRCHIVOS_ADJUNTOS);
        }

        // GET: ArchivosAdjuntos/Create
        public ActionResult Create()
        {
            
            ViewBag.CD_CODI_OPCI_SIST = new SelectList(dbMenus.GLB_OPCIONES_SISTEMA, "CD_CODI_OPCI_SIST", "DS_CODI_OPCI_SIST");
            ViewBag.CD_TIPO_DOCUMENTO = new SelectList(dbMenus.GLB_TIPO_DOCUMENTOS, "CD_TIPO_DOCUMENTO", "DS_TIPO_DOCUMENTO");
            return View();
        }

        // POST: ArchivosAdjuntos/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID_ADJUNTO,CD_EMPRESA,CD_CODI_OPCI_SIST,DS_FORANEO,CD_FORANEO,CD_TIPO_DOCUMENTO,DS_DOCUMENTO,ARCHIVO,TIPO_CONTENIDO,VIGENCIA_DESDE,VIGENCIA_HASTA,USUA_CREA,FECH_CREA,USUA_ACTUA,FECH_ACTUA")] ARCHIVOS_ADJUNTOS aRCHIVOS_ADJUNTOS)
        {
            if (ModelState.IsValid)
            {
                dbMenus.ARCHIVOS_ADJUNTOS.Add(aRCHIVOS_ADJUNTOS);
                dbMenus.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.CD_CODI_OPCI_SIST = new SelectList(dbMenus.GLB_OPCIONES_SISTEMA, "CD_CODI_OPCI_SIST", "CD_EMPRESA", aRCHIVOS_ADJUNTOS.CD_CODI_OPCI_SIST);
            ViewBag.CD_TIPO_DOCUMENTO = new SelectList(dbMenus.GLB_TIPO_DOCUMENTOS, "CD_TIPO_DOCUMENTO", "CD_EMPRESA", aRCHIVOS_ADJUNTOS.CD_TIPO_DOCUMENTO);
            return View(aRCHIVOS_ADJUNTOS);
        }

        // GET: ArchivosAdjuntos/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ARCHIVOS_ADJUNTOS aRCHIVOS_ADJUNTOS = dbMenus.ARCHIVOS_ADJUNTOS.Find(id);
            if (aRCHIVOS_ADJUNTOS == null)
            {
                return HttpNotFound();
            }
            ViewBag.CD_CODI_OPCI_SIST = new SelectList(dbMenus.GLB_OPCIONES_SISTEMA, "CD_CODI_OPCI_SIST", "CD_EMPRESA", aRCHIVOS_ADJUNTOS.CD_CODI_OPCI_SIST);
            ViewBag.CD_TIPO_DOCUMENTO = new SelectList(dbMenus.GLB_TIPO_DOCUMENTOS, "CD_TIPO_DOCUMENTO", "CD_EMPRESA", aRCHIVOS_ADJUNTOS.CD_TIPO_DOCUMENTO);
            return View(aRCHIVOS_ADJUNTOS);
        }

        // POST: ArchivosAdjuntos/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID_ADJUNTO,CD_EMPRESA,CD_CODI_OPCI_SIST,DS_FORANEO,CD_FORANEO,CD_TIPO_DOCUMENTO,DS_DOCUMENTO,ARCHIVO,TIPO_CONTENIDO,VIGENCIA_DESDE,VIGENCIA_HASTA,USUA_CREA,FECH_CREA,USUA_ACTUA,FECH_ACTUA")] ARCHIVOS_ADJUNTOS aRCHIVOS_ADJUNTOS)
        {
            if (ModelState.IsValid)
            {
                dbMenus.Entry(aRCHIVOS_ADJUNTOS).State = EntityState.Modified;
                dbMenus.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CD_CODI_OPCI_SIST = new SelectList(dbMenus.GLB_OPCIONES_SISTEMA, "CD_CODI_OPCI_SIST", "CD_EMPRESA", aRCHIVOS_ADJUNTOS.CD_CODI_OPCI_SIST);
            ViewBag.CD_TIPO_DOCUMENTO = new SelectList(dbMenus.GLB_TIPO_DOCUMENTOS, "CD_TIPO_DOCUMENTO", "CD_EMPRESA", aRCHIVOS_ADJUNTOS.CD_TIPO_DOCUMENTO);
            return View(aRCHIVOS_ADJUNTOS);
        }

        // GET: ArchivosAdjuntos/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ARCHIVOS_ADJUNTOS aRCHIVOS_ADJUNTOS = dbMenus.ARCHIVOS_ADJUNTOS.Find(id);
            if (aRCHIVOS_ADJUNTOS == null)
            {
                return HttpNotFound();
            }
            return View(aRCHIVOS_ADJUNTOS);
        }

        // POST: ArchivosAdjuntos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ARCHIVOS_ADJUNTOS aRCHIVOS_ADJUNTOS = dbMenus.ARCHIVOS_ADJUNTOS.Find(id);
            dbMenus.ARCHIVOS_ADJUNTOS.Remove(aRCHIVOS_ADJUNTOS);
            dbMenus.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                dbMenus.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
