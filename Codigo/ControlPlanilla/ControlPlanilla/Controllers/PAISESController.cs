﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ControlPlanilla.Models;

namespace ControlPlanilla.Controllers
{
    public class PAISESController : AuditoriaController
    {
        //private GlobalEntities db = new GlobalEntities();

        // GET: PAISES
        public ActionResult Index()
        {
            return View(dbMenus.PAISES.ToList());
        }

        // GET: PAISES/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PAISES pAISES = dbMenus.PAISES.Find(id);
            if (pAISES == null)
            {
                return HttpNotFound();
            }
            return View(pAISES);
        }

        // GET: PAISES/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: PAISES/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "CD_PAIS,DS_NOMBREPAIS,CD_AREA")] PAISES pAISES)
        {
            if (ModelState.IsValid)
            {
                pAISES.FECH_CREA = Ahora;
                pAISES.USUA_CREA = Usuario;

                dbMenus.PAISES.Add(pAISES);
                dbMenus.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(pAISES);
        }

        // GET: PAISES/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PAISES pAISES = dbMenus.PAISES.Find(id);
            if (pAISES == null)
            {
                return HttpNotFound();
            }
            return View(pAISES);
        }

        // POST: PAISES/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "CD_PAIS,DS_NOMBREPAIS,CD_AREA")] PAISES pAISES)
        {
            if (ModelState.IsValid)
            {
                PAISES guardar = dbMenus.PAISES.Find(pAISES.CD_PAIS);
                guardar.CD_AREA = pAISES.CD_AREA;
                guardar.DS_NOMBREPAIS = pAISES.DS_NOMBREPAIS;
                guardar.CD_PAIS = pAISES.CD_PAIS;
                guardar.FECH_ACTU = Ahora;
                guardar.USUA_ACTU = Usuario;
                dbMenus.Entry(guardar).State = EntityState.Modified;
                dbMenus.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(pAISES);
        }

        // GET: PAISES/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PAISES pAISES = dbMenus.PAISES.Find(id);
            if (pAISES == null)
            {
                return HttpNotFound();
            }
            return View(pAISES);
        }

        // POST: PAISES/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            PAISES pAISES = dbMenus.PAISES.Find(id);
            dbMenus.PAISES.Remove(pAISES);
            dbMenus.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                dbMenus.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
