﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Data;
using System.Data.Entity;
using System.Net;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;
using ControlPlanilla.Helpers;
using ControlPlanilla.Models;
using System.Web.Security;
using System.Web.Configuration;
using NLog;
using ControlPlanilla.Repository;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Configuration;
using PagedList;

namespace ControlPlanilla.Controllers
{
    public class AuditoriaController : Controller, IResultFilter
    {
        public Crypto Crypto = new Crypto();


        public PlanillaCicloPago dbCiclo = new PlanillaCicloPago();
        public PlanillaAccesoMenus dbMenus = new PlanillaAccesoMenus();
        public PlanillaAccesoMenus dbe = new PlanillaAccesoMenus();

        public PlanillaDDL dbDDL = new PlanillaDDL();

        public PlanillaVendor dbVendor = new PlanillaVendor();
        public PlanillaPayControls dbPayControl = new PlanillaPayControls();

        public int pageSize = Convert.ToInt32(ConfigurationManager.AppSettings["DefaultPageSize"]);
       
        //public PlanillaEntities dbm = new PlanillaEntities();

        public NLogRepository logRepository = new NLogRepository(new PlanillaAccesoMenus());

        public PlanillaPayControls dbTarifas = new PlanillaPayControls();

        public PlanillaPayControls dbStatusRate = new PlanillaPayControls();

        public DateTime Ahora = DateTime.Now;

        public string Usuario { get; set; }

        public string NombreCompletoUsuario { get; set; }
        public string CdPais { get; set; }
        public string CdEmpresa { get; set; }

        public string NombreEmpresa { get; set; }
        public string OpcionSistema { get; set; }




        [HttpPost]
        public async Task<ActionResult> Validar(AccederViewModels acceder)
        {
            try
            {

                if (ModelState.IsValid)
                {
                    if (Session["Suspendido"] != null)
                    {
                        var actual = DateTime.Now; //Date.Parse(Now());
                        DateTime suspendido = Convert.ToDateTime(Session["Suspendido"].ToString());
                        if (actual >= suspendido)
                        {
                            Session.RemoveAll();
                        }
                    }
                    var expiracionValor = WebConfigurationManager.AppSettings["expiracion"];
                    HttpCookie cogeCookie = Request.Cookies.Get("googlecookie");

                    if (cogeCookie != null)
                    {
                        TempData["errorMensaje"] = "Ha superado el numero de intentos,Podra intentar en " + expiracionValor + " minutos";
                        return RedirectToAction("Index", "Home");
                    }

                    if (Session["intento"] != null)
                    {
                        int intentos = Convert.ToInt32(Session["intento"].ToString());
                        if (intentos > 10)
                        {
                            TempData["errorMensaje"] = "Ha superado el numero de intentos,Podra intentar en " + expiracionValor + " minutos";
                            HttpCookie addCookie = new HttpCookie("googleCookie", DateTime.Now.ToString());
                            addCookie.Expires = DateTime.Now.AddSeconds(30);
                            Session["Suspendido"] = DateTime.Now.AddSeconds(30);
                            Response.Cookies.Add(addCookie);
                            return RedirectToAction("Index", "Home");
                        }
                    }

                    int expiracion = TraerExpiracion();

                    //Buscamos el usuario
                    USUARIOS usrs = dbMenus.USUARIOS.Find(acceder.CD_CODIGO_USUARIO);
                    if (usrs != null)
                    {
                        //Comprobamos la contrasenia
                        if (usrs.CD_CLAVE_USUARIO == Crypto.EnCryptString(acceder.CD_CLAVE_USUARIO))
                        {
                            //Comprobar el estado del usuario
                            if (usrs.CD_ESTADO_USUARIO.ToUpper() == "A")
                            {
                                //Generar y enviar codigo de validacion
                                Random generator = new Random();
                                int codeAl = generator.Next(100000, 1000000);

                                var body = "<p>Estimado usuario: {0} ({1})</p><p>Por este medio compartimos el código para confirmar su inicio sesión:</p><p>{2}</p>";
                                var message = new MailMessage();
                                message.To.Add(new MailAddress(usrs.CD_DIRE_EMAIL));
                                message.From = new MailAddress("cuenta2019sis@gmail.com");
                                message.Subject = "Codigo de verificacion";
                                message.Body = string.Format(body, usrs.DS_NOMBRE_USUARIO, usrs.CD_DIRE_EMAIL, "Su código es: " + codeAl.ToString());
                                message.IsBodyHtml = true;

                                using (var smtp = new SmtpClient())
                                {
                                    var credential = new NetworkCredential
                                    {
                                        UserName = "cuenta2019sis@gmail.com",
                                        Password = "ayssa2019"
                                    };
                                    smtp.UseDefaultCredentials = false;
                                    smtp.Credentials = credential;
                                    smtp.Host = "smtp.gmail.com";
                                    smtp.Port = 587;
                                    smtp.EnableSsl = true;
                                    //await smtp.SendMailAsync(message);

                                    VerificarCodigo verificar = new VerificarCodigo();

                                    verificar.CD_CODIGO_USUARIO = acceder.CD_CODIGO_USUARIO;
                                    Session["cd_empresa"] = acceder.SelectedEmpresa;
                                    Session["CodigoGeneraddo"] = codeAl.ToString();

                                    return RedirectToAction("ValidarCodigo", "Home", verificar);
                                }


                            }
                            else if (usrs.CD_ESTADO_USUARIO.ToUpper() == "S")
                            {
                                TempData["errorMensaje"] = "Su cuenta esta suspendida.";
                                return RedirectToAction("Index", "Home");
                            }

                        }
                        else
                        {
                            TempData["errorMensaje"] = "Usuario o Contraseña incorrecta.";

                            TempData["usuario"] = acceder.CD_CODIGO_USUARIO;
                            return RedirectToAction("Index", "Home");
                        }
                    }
                    else
                    {
                        if (Session["intento"] == null)
                        {
                            Session["intento"] = 1;
                        }
                        else
                        {
                            Session["intento"] = 1;
                        }
                        TempData["errorMensaje"] = "Usuario o Contraseña incorrecta.";
                    }
                }
                else
                {
                    TempData["errorMensaje"] = "Ingrese los datos";
                }
                TempData["usuario"] = acceder.CD_CODIGO_USUARIO;
                return RedirectToAction("Index", "Home");
            }
            catch (SystemException ex)
            {
                logRepository.CapturarTodoError(ex);
            }
            ModelState.AddModelError("Contrasena", "Usuario o Contraseña incorrecta. ");
            TempData["usuario"] = acceder.CD_CODIGO_USUARIO;
            return RedirectToAction("Index", "Home");
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult VerificarCodigo(VerificarCodigo acceder)
        {
            if (Session["CodigoGeneraddo"] != null)
            {
                if (Session["CodigoGeneraddo"].ToString() == acceder.CodigoGeneraddo)
                {
                    USUARIOS usrs = dbMenus.USUARIOS.Find(acceder.CD_CODIGO_USUARIO);
                    Session["usuario"] = usrs.CD_CODIGO_USUARIO;
                    Session["nombreUser"] = usrs.DS_NOMBRE_USUARIO;
                    Session["estado"] = usrs.CD_ESTADO_USUARIO;


                    NombreCompletoUsuario = usrs.DS_NOMBRE_USUARIO;
                    FormsAuthentication.SetAuthCookie(usrs.CD_CODIGO_USUARIO, false);

                    HttpCookie cookieConfig = new HttpCookie(Constantes.NombreCookie);

                    string galletaoculta = Crypto.EnCryptString(usrs.CD_CODIGO_USUARIO);

                    cookieConfig.Values.Add(Constantes.NombreUsuarioCookie, galletaoculta);
                    cookieConfig.Values.Add(Constantes.NombreEmpresaCookie, Session["cd_empresa"].ToString());
                    cookieConfig.Values.Add(Constantes.NombreCompletoUsuarioCookie, Crypto.EnCryptString(NombreCompletoUsuario));
                    //generar la session
                    //string hash = HacerHash(usuario, acceder.Empresa);
                    string hash = HacerHash();
                    cookieConfig.Values.Add(Constantes.NombreCodigoCookie, hash);
                    Response.Cookies.Add(cookieConfig);

                    TempData["LoginName"] = usrs.CD_CODIGO_USUARIO;

                    CdEmpresa = Session["cd_empresa"].ToString();
                    Usuario = Session["usuario"].ToString();
                    logRepository.Entrar(Usuario);

                    return RedirectToAction("Inicio", "Home");
                }
            }
            TempData["errorMensaje"] = "Error";
            return RedirectToAction("ValidarCodigo", "Home", acceder);

        }

        protected override IAsyncResult BeginExecute(RequestContext requestContext, AsyncCallback callback, object state)
        {
            dbMenus.Configuration.ProxyCreationEnabled = true;
            dbMenus.Configuration.ProxyCreationEnabled = true;
            UrlHelper _urlHelper = new UrlHelper(requestContext);
            HttpContextBase context = requestContext.HttpContext;
            HttpRequestBase request = requestContext.HttpContext.Request;


            if (request.Url.Segments.Count() > 1)
            {
                var url = request.Url.Segments[1].ToString();

                url = url.Replace("/", "");

                if (url != "Home")
                {
                    ObtenerCdOpcionSistema(string.Concat("/", url));
                }
            }
            //if (string.IsNullOrWhiteSpace(help_root))
            //{
            //    help_root = _urlHelper.Action("root", "root");
            //    help_root = help_root.Replace("root/root", "").Replace("//", "/");
            //}
            ViewBag.useragent = "";
            if (context.Request.UserAgent.Contains("MSIE") || request.UserAgent.Contains("Trident"))
                ViewBag.useragent = "F_IE";
            else if (context.Request.UserAgent.Contains("Edge/"))
                ViewBag.useragent = "EDGE";
            else if (context.Request.UserAgent.Contains("Firefox"))
                ViewBag.useragent = "FIREFOX";
            else if (context.Request.UserAgent.Contains("Chrome"))

                ViewBag.useragent = "CHROME";
            if (context.Session != null)
                context.Session["S_Ahora"] = Ahora;
            string galletaRevelada = Constantes.NombreUsuarioNoAutenticado;

            NombreCompletoUsuario = string.Empty;
            if (request != null)
            {
                HttpCookie cookieConfig = request.Cookies[Constantes.NombreCookie];

                if (cookieConfig != null && !string.IsNullOrEmpty(cookieConfig.Values[Constantes.NombreUsuarioCookie]))
                {
                    string galletaOculta = cookieConfig.Values[Constantes.NombreUsuarioCookie].ToString();
                    galletaRevelada = Crypto.DeCryptString(galletaOculta);
                    CdEmpresa = cookieConfig.Values[Constantes.NombreEmpresaCookie].ToString();
                    if (!string.IsNullOrEmpty(CdEmpresa))
                    {
                        //Obtener el nombre de la empresa
                        NombreEmpresa = ObtenerNombreEmpresaSeleccionada(CdEmpresa);
                    }
                    NombreCompletoUsuario = Crypto.DeCryptString(cookieConfig.Values[Constantes.NombreCompletoUsuarioCookie].ToString());
                }
                if (galletaRevelada != "UNAUTHORIZED_USER")
                {
                    Usuario = galletaRevelada;
                }
                else
                {
                    Usuario = string.Empty;
                }

                ViewBag.Usuario = Usuario;
                //Empresa = Convert.ToInt32(empresa);
            }
            return base.BeginExecute(requestContext, callback, state);
        }

        public string ObtenerNombreEmpresaSeleccionada(string cd_empresa)
        {
            string nombre = dbMenus.EMPRESAS.Where(e => e.CD_EMPRESA == cd_empresa).FirstOrDefault().DS_EMPRESA;

            return nombre;
        }
        public void ObtenerCdOpcionSistema(string Url)
        {
            var datos = dbMenus.GLB_OPCIONES_SISTEMA.Where(o => o.DS_LINK_URL == Url).FirstOrDefault();
            if (datos != null)
            {
                OpcionSistema = datos.CD_CODI_OPCI_SIST;
            }
        }


        #region "Control de sesiones"
        private string HacerHash()
        {
            DateTime fechahoy = DateTime.Now;

            string tokenFormato = string.Format("{0}{1}{2}{3}", fechahoy.Hour, fechahoy.Minute, fechahoy.Second, fechahoy.Millisecond);
            ValidacionCookie vc = new ValidacionCookie();

            string hash = vc.ObtenerHash(Convert.ToUInt32(tokenFormato));
            //string system = WebConfigurationManager.AppSettings["system"].ToString();

            return hash;
        }
        public int TraerExpiracion()
        {
            //Parametro para el cambio de contrasenia
            try
            {
                int expiracion = 60;

                PARAMETROS_GENERALES gp = dbMenus.PARAMETROS_GENERALES.Where(p =>
                p.CODIGO_PARAMETRO == "CMBPASS").FirstOrDefault();

                if (gp != null)
                {
                    expiracion = Convert.ToInt32(gp.VALOR_PARAMETRO.ToString());
                }

                return expiracion;
            }
            catch (Exception)
            {

                throw;
            }

        }

        public Action CerrarSesion()
        {
            FormsAuthentication.SetAuthCookie(Usuario, false);
            Session.Abandon();
            Session.Clear();

            HttpCookie addCookie = new HttpCookie("googlecookie", DateTime.Now.ToString());

            addCookie.Expires = DateTime.Now;

            FormsAuthentication.SignOut();

            Response.AppendHeader("Pragma", "no-cache");
            Response.AppendHeader("Cache-Control", "no-cache");
            Response.CacheControl = "no-cache";
            Response.Expires = -1;
            Response.ExpiresAbsolute = new DateTime(1900, 1, 1);
            //Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache);
            Response.Cache.SetAllowResponseInBrowserHistory(false);
            Response.Cache.SetNoStore();

            if (HttpContext.Response.StatusCode == 200)
            {
                //Elminar cookie que almacena usuario. empresa
                HttpCookie cookieConfig = HttpContext.Response.Cookies[Constantes.NombreCookie];
                cookieConfig.Expires = DateTime.Now.AddDays(-1d);
                HttpContext.Response.Cookies.Add(cookieConfig);
                cookieConfig = HttpContext.Response.Cookies[Constantes.NombreUsuarioCookie];
                cookieConfig.Expires = DateTime.Now.AddDays(-1d);
                HttpContext.Response.Cookies.Add(cookieConfig);
                cookieConfig = HttpContext.Response.Cookies[Constantes.NombreEmpresaCookie];
                cookieConfig.Expires = DateTime.Now.AddDays(-1d);
                HttpContext.Response.Cookies.Add(cookieConfig);
            }

            logRepository.Salir(Usuario);
            return null;
        }
        #endregion



        public JsonResult EstadoUsuario()
        {
            List<EstadosUsuarioViewModel> CmbEstadoUsuarios = new List<EstadosUsuarioViewModel>();

            EstadosUsuarioViewModel list1 = new EstadosUsuarioViewModel();
            list1.Text = "Activo";
            list1.Value = "A";
            CmbEstadoUsuarios.Add(list1);
            list1 = new EstadosUsuarioViewModel();
            list1.Text = "Desactivado";
            list1.Value = "D";
            CmbEstadoUsuarios.Add(list1);
            list1 = new EstadosUsuarioViewModel();
            list1.Text = "Suspendido";
            list1.Value = "S";
            CmbEstadoUsuarios.Add(list1);

            return Json(CmbEstadoUsuarios, JsonRequestBehavior.AllowGet);

        }

        public JsonResult RateUsedType()
        {
            List<EstadosUsuarioViewModel> CmbRateUsedType = new List<EstadosUsuarioViewModel>();

            EstadosUsuarioViewModel list1 = new EstadosUsuarioViewModel();
            list1.Text = "Seleccione una opción";
            list1.Value = "0";
            CmbRateUsedType.Add(list1);
            list1 = new EstadosUsuarioViewModel();
            list1.Text = "Employee";
            list1.Value = "1";
            CmbRateUsedType.Add(list1);
            list1 = new EstadosUsuarioViewModel();
            list1.Text = "Employer";
            list1.Value = "2";
            CmbRateUsedType.Add(list1);

            return Json(CmbRateUsedType, JsonRequestBehavior.AllowGet);

        }

        [AllowAnonymous]
        public JsonResult ObtenerNombreEmpresa(string cd_empresa)
        {
            //var listaEmpresas = db.GlobalEmpresas.Select(e => 
            //new { Text = e.DS_EMPRESA, Value = e.CD_EMPRESA})
            //    .Where(e=> e.CD_EMPRESA==cd_empresa);
            var listaEmpresas = dbMenus.EMPRESAS.Where(e => e.CD_EMPRESA == cd_empresa).
                Select(r => new EmpresasView
                {
                    CD_EMPRESA = r.CD_EMPRESA,
                    DS_EMPRESA = r.DS_EMPRESA
                }).ToList();

            return Json(listaEmpresas, JsonRequestBehavior.AllowGet);
        }

        [AllowAnonymous]
        public JsonResult ObtenerPaisSeleccionado(string cd_empresa)
        {
            //var listaEmpresas = db.GlobalEmpresas.Select(e => 
            //new { Text = e.DS_EMPRESA, Value = e.CD_EMPRESA})
            //    .Where(e=> e.CD_EMPRESA==cd_empresa);
            var cdPais = dbMenus.RELACION_EMPRESA_PAIS.Where(r => r.CD_EMPRESA == cd_empresa)
                .Select(p => p.CD_PAIS).FirstOrDefault();


            return Json(cdPais, JsonRequestBehavior.AllowGet);
        }

        #region Adjuntar Archivos
        public ActionResult SubirArchivos(string opcion, string DsForaneo, string CdForaneo)
        {
            try
            {
                //CdForaneo = "A0001";
                //opcion = "MttnUsers";
                ViewBag.CD_TIPO_DOCUMENTO = dbMenus.GLB_RELA_TIPO_DOCUMENTOS_OPCI_SIST.Where(c => c.CD_CODI_OPCI_SIST == opcion)
                    .Select(e => new SelectListItem()
                    {
                        Text = e.GLB_TIPO_DOCUMENTOS.DS_TIPO_DOCUMENTO,
                        Value = e.CD_TIPO_DOCUMENTO
                    }).ToList();
                ViewBag.CD_OPC_SIST = opcion;
                ViewBag.CdForaneo = CdForaneo;
                ViewBag.DsForaneo = DsForaneo;
                ViewBag.DS_LINK = dbMenus.GLB_OPCIONES_SISTEMA.Where(c => c.CD_CODI_OPCI_SIST == opcion).FirstOrDefault().DS_LINK_URL;

                return View();
            }
            catch (SystemException ex)
            {
                logRepository.CapturarTodoError(ex);
                return View("SubirArchivos");
            }
        }



        public bool ComprobarRequiereArchivos(string OpcionSistema)
        {
            bool comprobar = false;
            try
            {
                //Verificar si se requiere subir archivos
                var adj = dbMenus.GLB_RELA_TIPO_DOCUMENTOS_OPCI_SIST.Where(c => c.CD_CODI_OPCI_SIST == OpcionSistema).ToList();
                if (adj.Count > 0)
                {
                    comprobar = true;
                }
            }
            catch (SystemException ex)
            {
                logRepository.CapturarTodoError(ex);
                throw;
            }
            return comprobar;
        }

        public bool ComprobarArchivosAdjuntos()
        {
            bool cumplio = false;
            try
            {
                if (!string.IsNullOrEmpty(TempData["OpcionSistema"].ToString()))
                {
                    string OpcionSistema = TempData["OpcionSistema"].ToString();
                    string CdForaneo = TempData["CdForaneo"].ToString();
                    string DsForaneo = TempData["DsForaneo"].ToString();

                    var adj = dbMenus.GLB_RELA_TIPO_DOCUMENTOS_OPCI_SIST.Where(c => c.CD_CODI_OPCI_SIST == OpcionSistema)
                        .Select(c => c.CD_TIPO_DOCUMENTO).Distinct().ToList();

                    var docsAdj = dbMenus.ARCHIVOS_ADJUNTOS.Where(a => a.CD_CODI_OPCI_SIST == OpcionSistema && a.CD_FORANEO == CdForaneo)
                        .Select(s => s.CD_TIPO_DOCUMENTO).Distinct().ToList();

                    if (adj.Count == docsAdj.Count)
                    {
                        cumplio = true;
                    }
                }


            }
            catch (SystemException ex)
            {
                logRepository.CapturarTodoError(ex);
                //throw;
            }
            return cumplio;
        }
        [HttpPost]
        public ActionResult ListaDocumentos(string OpcionSistema, string CdForaneo, int? page, int? OpcionConsulta)
        {
            try
            {
                //int pageSize = 1;
                //int pageSize = Convert.ToInt32(ConfigurationManager.AppSettings["DefaultPageSize"]);
                int pageNumber = (page ?? 1);

                var docsAdj = dbMenus.ARCHIVOS_ADJUNTOS.Where(a => a.CD_CODI_OPCI_SIST == (!string.IsNullOrEmpty(OpcionSistema) ? OpcionSistema : a.CD_CODI_OPCI_SIST) &&
                              a.CD_FORANEO == (!string.IsNullOrEmpty(CdForaneo) ? CdForaneo : a.CD_FORANEO)).Select(c =>
                              new ListaDocumentosView()
                              {
                                  Id = c.ID_ADJUNTO,
                                  OpcionSistema = c.CD_CODI_OPCI_SIST,
                                  DsOpcionSistema = c.GLB_OPCIONES_SISTEMA.DS_CODI_OPCI_SIST,
                                  CdForaneo = c.CD_FORANEO,
                                  DsForaneo = c.DS_FORANEO,
                                  CdTipoDocumento = c.CD_TIPO_DOCUMENTO,
                                  DsTipoDocumento = c.GLB_TIPO_DOCUMENTOS.DS_TIPO_DOCUMENTO,
                                  DsDocumento = c.DS_DOCUMENTO,
                                  Archivo = c.ARCHIVO,
                                  Desde = c.VIGENCIA_DESDE,
                                  Hasta = c.VIGENCIA_HASTA
                              }).ToList();
                var result = docsAdj.ToPagedList(pageNumber, pageSize);
                ViewBag.CD_OPC_SIST = OpcionSistema;
                ViewBag.CdForaneo = CdForaneo;
                ViewBag.OpcionConsulta = OpcionConsulta;
                //ViewBag.CD_TIPO_DOCUMENTO = dbMenus.GLB_RELA_TIPO_DOCUMENTOS_OPCI_SIST.Where(c => c.CD_CODI_OPCI_SIST == OpcionSistema)
                //    .Select(e => new SelectListItem()
                //    {
                //        Text = e.GLB_TIPO_DOCUMENTOS.DS_TIPO_DOCUMENTO,
                //        Value = e.CD_TIPO_DOCUMENTO
                //    }).ToList();

                return PartialView(result);
            }
            catch (SystemException ex)
            {
                logRepository.CapturarTodoError(ex);
                throw;
            }

        }

        //Convertir en byte[]
        //public JsonResult ConvertirByte(HttpPostedFileBase File)
        //{
        //    try
        //    {
        //        if (File != null)
        //        {
        //            byte[] array;
        //            using (MemoryStream ms = new MemoryStream())
        //            {
        //                File.InputStream.CopyTo(ms);
        //                array = ms.GetBuffer();
        //            }
        //            return Json(array, JsonRequestBehavior.AllowGet);
        //        }
        //        return Json("", JsonRequestBehavior.AllowGet);
        //    }
        //    catch (SystemException ex)
        //    {
        //        logRepository.CapturarTodoError(ex);
        //        return Json("", JsonRequestBehavior.AllowGet); ;
        //    }
        //}



        //public bool AdjuntarArchivo([Bind(Include = "CD_CODI_OPCI_SIST, CD_FORANEO, CD_TIPO_DOCUMENTO, DS_DOCUMENTO, ARCHIVO, VIGENCIA_DESDE, VIGENCIA")] ARCHIVOS_ADJUNTOS Archivos)
        [HttpPost]
        public bool AdjuntarArchivo([Bind(Include = "CD_CODI_OPCI_SIST, CD_FORANEO, DS_FORANEO, CD_TIPO_DOCUMENTO, DS_DOCUMENTO, ARCHIVO, TIPO_CONTENIDO, VIGENCIA_DESDE, VIGENCIA_HASTA")] ARCHIVOS_ADJUNTOS Archivos)
        {
            bool result = false;
            try
            {
                if (ModelState.IsValid)
                {

                    ARCHIVOS_ADJUNTOS guardar = new ARCHIVOS_ADJUNTOS();

                    guardar.CD_EMPRESA = CdEmpresa;
                    guardar.CD_CODI_OPCI_SIST = Archivos.CD_CODI_OPCI_SIST;
                    guardar.CD_FORANEO = Archivos.CD_FORANEO;
                    guardar.DS_FORANEO = Archivos.DS_FORANEO;
                    guardar.CD_TIPO_DOCUMENTO = Archivos.CD_TIPO_DOCUMENTO;
                    guardar.CD_TIPO_DOCUMENTO = Archivos.CD_TIPO_DOCUMENTO;
                    guardar.DS_DOCUMENTO = Archivos.DS_DOCUMENTO;
                    guardar.TIPO_CONTENIDO = Archivos.TIPO_CONTENIDO;
                    guardar.ARCHIVO = Archivos.ARCHIVO;
                    guardar.VIGENCIA_DESDE = Archivos.VIGENCIA_DESDE;
                    guardar.VIGENCIA_HASTA = Archivos.VIGENCIA_HASTA;
                    guardar.USUA_CREA = Usuario;
                    guardar.FECH_CREA = Ahora;



                    dbMenus.ARCHIVOS_ADJUNTOS.Add(guardar);
                    dbMenus.SaveChanges();

                    result = true;
                }

            }
            catch (SystemException ex)
            {
                logRepository.CapturarTodoError(ex);
            }
            return result;
        }

        [HttpGet]
        public JsonResult TraerDatosDocumento(int Id)
        {
            try
            {
                //campana.FechaInicio.HasValue ? campana.FechaInicio.Value.ToString("dd-MM-yyyy") : "",
                var datosDoc = dbMenus.ARCHIVOS_ADJUNTOS.Where(a => a.ID_ADJUNTO == Id).Select
                    (s => new TraerDatosDocumentoAdjunto
                    {
                        Id = s.ID_ADJUNTO,
                        CD_OPCI_SIST = s.CD_CODI_OPCI_SIST,
                        DS_OPCI_SIST = s.GLB_OPCIONES_SISTEMA.DS_CODI_OPCI_SIST,
                        CD_FORANEO = s.CD_FORANEO,
                        DS_FORANEO = s.DS_FORANEO,
                        CD_TIPO_DOCUMENTO = s.CD_TIPO_DOCUMENTO,
                        TIPO_CONTENIDO = s.TIPO_CONTENIDO,
                        DS_DOCUMENTO = s.DS_DOCUMENTO,
                        VIGENCIA_DESDE = s.VIGENCIA_DESDE,
                        VIGENCIA_HASTA = s.VIGENCIA_HASTA
                    }).ToList();
                ViewBag.ID_ADJUNTO = Id;
                return Json(datosDoc, JsonRequestBehavior.AllowGet);
            }
            catch (SystemException ex)
            {
                logRepository.CapturarTodoError(ex);
                throw;
            }
        }
        public JsonResult TraerTipoDocumentos(string CdOpcionSistema)
        {
            var datosDoc = dbMenus.GLB_RELA_TIPO_DOCUMENTOS_OPCI_SIST.Where(c => c.CD_CODI_OPCI_SIST == CdOpcionSistema)
                         .Select(e => new SelectListItem()
                         {
                             Text = e.GLB_TIPO_DOCUMENTOS.DS_TIPO_DOCUMENTO,
                             Value = e.CD_TIPO_DOCUMENTO
                         }).ToList();

            return Json(datosDoc, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ObtenerImagen(int Id)
        {
            var datosDoc = dbMenus.ARCHIVOS_ADJUNTOS.Where(a => a.ID_ADJUNTO == Id).FirstOrDefault();

            if (datosDoc != null)
            {
                var imagen = datosDoc.ARCHIVO;
                var result = File(imagen, datosDoc.TIPO_CONTENIDO);
                return result;
            }
            return null;
        }
        public ActionResult ObtenerPDF(int Id)
        {
            var datosDoc = dbMenus.ARCHIVOS_ADJUNTOS.Where(a => a.ID_ADJUNTO == Id).FirstOrDefault();

            if (datosDoc != null)
            {
                var pdf = datosDoc.ARCHIVO;
                var result = File(pdf, datosDoc.TIPO_CONTENIDO);
                return result;
            }

            return null;
        }

        //EditarArchivo
        [HttpPost]
        public bool EditarArchivo([Bind(Include = "ID_ADJUNTO, CD_CODI_OPCI_SIST, CD_FORANEO, DS_FORANEO, CD_TIPO_DOCUMENTO, DS_DOCUMENTO, ARCHIVO, TIPO_CONTENIDO, VIGENCIA_DESDE, VIGENCIA_HASTA")] ARCHIVOS_ADJUNTOS Archivos)
        {
            bool result = false;
            try
            {
                if (ModelState.IsValid)
                {

                    ARCHIVOS_ADJUNTOS guardar = dbMenus.ARCHIVOS_ADJUNTOS.Find(Archivos.ID_ADJUNTO);

                    if (guardar != null)
                    {
                        guardar.CD_EMPRESA = CdEmpresa;
                        guardar.CD_CODI_OPCI_SIST = Archivos.CD_CODI_OPCI_SIST;
                        guardar.CD_FORANEO = Archivos.CD_FORANEO;
                        guardar.DS_FORANEO = Archivos.DS_FORANEO;
                        guardar.CD_TIPO_DOCUMENTO = Archivos.CD_TIPO_DOCUMENTO;
                        guardar.DS_FORANEO = Archivos.DS_FORANEO;
                        guardar.DS_DOCUMENTO = Archivos.DS_DOCUMENTO;
                        if (Archivos.ARCHIVO != null)
                            guardar.ARCHIVO = Archivos.ARCHIVO;
                        if (Archivos.TIPO_CONTENIDO != null)
                            guardar.TIPO_CONTENIDO = Archivos.TIPO_CONTENIDO;
                        guardar.VIGENCIA_DESDE = Archivos.VIGENCIA_DESDE;
                        guardar.VIGENCIA_HASTA = Archivos.VIGENCIA_HASTA;
                        guardar.USUA_ACTUA = Usuario;
                        guardar.FECH_ACTUA = Ahora;

                        dbMenus.Entry(guardar).State = EntityState.Modified;
                        dbMenus.SaveChanges();

                        result = true;
                    }
                }

            }
            catch (SystemException ex)
            {
                logRepository.CapturarTodoError(ex);
            }
            return result;
        }

        //EliminarArchivo
        [HttpPost]
        public bool EliminarArchivo(int Id)
        {
            bool result = false;
            try
            {
                ARCHIVOS_ADJUNTOS Archivo = dbMenus.ARCHIVOS_ADJUNTOS.Find(Id);

                if (Archivo != null)
                {
                    dbMenus.ARCHIVOS_ADJUNTOS.Remove(Archivo);
                    dbMenus.SaveChanges();
                    result = true;
                }

            }
            catch (SystemException ex)
            {
                logRepository.CapturarTodoError(ex);
            }
            return result;
        }

        #endregion



    }



}
