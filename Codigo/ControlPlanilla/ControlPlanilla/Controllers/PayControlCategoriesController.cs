﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ControlPlanilla.Models;

namespace ControlPlanilla.Controllers
{
    public class PayControlCategoriesController : AuditoriaController
    {
        

        // GET: PayControlCategories
        public ActionResult Index()
        {
            return View(dbPayControl.PayControlCategory.ToList());
        }

        // GET: PayControlCategories/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PayControlCategory payControlCategory = dbPayControl.PayControlCategory.Find(id);
            if (payControlCategory == null)
            {
                return HttpNotFound();
            }
            return View(payControlCategory);
        }

        // GET: PayControlCategories/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: PayControlCategories/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Code,Description")] PayControlCategory payControlCategory)
        {
            if (ModelState.IsValid)
            {
                dbPayControl.PayControlCategory.Add(payControlCategory);
                dbPayControl.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(payControlCategory);
        }

        // GET: PayControlCategories/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PayControlCategory payControlCategory = dbPayControl.PayControlCategory.Find(id);
            if (payControlCategory == null)
            {
                return HttpNotFound();
            }
            return View(payControlCategory);
        }

        // POST: PayControlCategories/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Code,Description")] PayControlCategory payControlCategory)
        {
            if (ModelState.IsValid)
            {
                dbPayControl.Entry(payControlCategory).State = EntityState.Modified;
                dbPayControl.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(payControlCategory);
        }

        // GET: PayControlCategories/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PayControlCategory payControlCategory = dbPayControl.PayControlCategory.Find(id);
            if (payControlCategory == null)
            {
                return HttpNotFound();
            }
            return View(payControlCategory);
        }

        // POST: PayControlCategories/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            PayControlCategory payControlCategory = dbPayControl.PayControlCategory.Find(id);
            dbPayControl.PayControlCategory.Remove(payControlCategory);
            dbPayControl.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                dbPayControl.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
