﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ControlPlanilla.Models;

namespace ControlPlanilla.Controllers
{
    public class CIUDADESController : AuditoriaController
    {
        private GlobalEntities db = new GlobalEntities();

        // GET: CIUDADES
        public ActionResult Index()
        {
            var cIUDADES = dbMenus.CIUDADES.Include(c => c.DEPARTAMENTOS);
            return View(cIUDADES.ToList());
        }

        // GET: CIUDADES/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CIUDADES cIUDADES = dbMenus.CIUDADES.Find(id);
            if (cIUDADES == null)
            {
                return HttpNotFound();
            }
            return View(cIUDADES);
        }

        // GET: CIUDADES/Create
        public ActionResult Create()
        {
            ViewBag.ID_DEPTO = new SelectList(dbMenus.DEPARTAMENTOS, "ID_DEPTO", "NOMBRE_DEPTO");

            var listaPaises = dbMenus.PAISES.Select(a => new PaisesViewModels { CD_PAIS = a.CD_PAIS, DS_PAIS = a.DS_NOMBREPAIS }).ToList();
            ViewBag.cmbPaises = listaPaises;
            return View();
        }

        // POST: CIUDADES/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID_DEPTO,NOMBRE_CIUDAD,CODIGO_CIUDAD")] CIUDADES cIUDADES)
        {
            if (ModelState.IsValid)
            {
                cIUDADES.USUA_CREA = Usuario;
                cIUDADES.FECH_CREA = Ahora;
                dbMenus.CIUDADES.Add(cIUDADES);
                dbMenus.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ID_DEPTO = new SelectList(dbMenus.DEPARTAMENTOS, "ID_DEPTO", "CD_PAIS", cIUDADES.ID_DEPTO);
            return View(cIUDADES);
        }

        // GET: CIUDADES/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CIUDADES cIUDADES = dbMenus.CIUDADES.Find(id);
            if (cIUDADES == null)
            {
                return HttpNotFound();
            }
            ViewBag.ID_DEPTO = new SelectList(dbMenus.DEPARTAMENTOS, "ID_DEPTO", "NOMBRE_DEPTO", cIUDADES.ID_DEPTO);
            var listaPaises = dbMenus.PAISES.Select(a => new PaisesViewModels { CD_PAIS = a.CD_PAIS, DS_PAIS = a.DS_NOMBREPAIS }).ToList();
            ViewBag.cmbPaises = listaPaises;
            return View(cIUDADES);
        }

        // POST: CIUDADES/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID_CIUDAD,ID_DEPTO,NOMBRE_CIUDAD,CODIGO_CIUDAD")] CIUDADES cIUDADES)
        {
            if (ModelState.IsValid)
            {
                CIUDADES grabar = dbMenus.CIUDADES.Find(cIUDADES.ID_CIUDAD);
                grabar.ID_DEPTO = cIUDADES.ID_DEPTO;
                grabar.NOMBRE_CIUDAD = cIUDADES.NOMBRE_CIUDAD;
                grabar.CODIGO_CIUDAD = cIUDADES.CODIGO_CIUDAD;
                grabar.FECH_ACTU = Ahora;
                grabar.USUA_ACTU= Usuario;
                dbMenus.Entry(grabar).State = EntityState.Modified;
                dbMenus.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ID_DEPTO = new SelectList(dbMenus.DEPARTAMENTOS, "ID_DEPTO", "CD_PAIS", cIUDADES.ID_DEPTO);
            return View(cIUDADES);
        }

        // GET: CIUDADES/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CIUDADES cIUDADES = dbMenus.CIUDADES.Find(id);
            if (cIUDADES == null)
            {
                return HttpNotFound();
            }
            return View(cIUDADES);
        }

        // POST: CIUDADES/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            CIUDADES cIUDADES = dbMenus.CIUDADES.Find(id);
            dbMenus.CIUDADES.Remove(cIUDADES);
            dbMenus.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                dbMenus.Dispose();
            }
            base.Dispose(disposing);
        }

        public JsonResult ObtenerDepartamentos(string cd_pais)
        {
             List<DepartamentosView> listaDepartamentos = new List<DepartamentosView> {
                 new DepartamentosView { ID_DEPTO = 0, NOMBRE_DEPTO = "Seleccione una opción" }
             };
             var listaDepartamentos1 = dbMenus.DEPARTAMENTOS.Where(ep => ep.CD_PAIS == cd_pais).
                Select(r => new  DepartamentosView
                {
                    ID_DEPTO = r.ID_DEPTO,
                    NOMBRE_DEPTO = r.NOMBRE_DEPTO
                }).ToList();
            var listFinal = listaDepartamentos.Union(listaDepartamentos1);
            return Json(listFinal, JsonRequestBehavior.AllowGet);
        }
        public JsonResult ObtenerMunicipios(int ID_DEPTO)
        {
            List<CiudadesView> listaMuni = new List<CiudadesView> {
                new CiudadesView { ID_CIUDAD = 0, NOMBRE_CIUDAD = "Seleccione una opción" }
            };
            var listaMuni1 = dbMenus.CIUDADES.Where(ep => ep.ID_DEPTO== ID_DEPTO).
                Select(r => new CiudadesView
                {
                    ID_CIUDAD = r.ID_CIUDAD,
                    NOMBRE_CIUDAD = r.NOMBRE_CIUDAD
                }).ToList();
            var listFinal = listaMuni.Union(listaMuni1);
            return Json(listFinal, JsonRequestBehavior.AllowGet);
        }
    }
}
