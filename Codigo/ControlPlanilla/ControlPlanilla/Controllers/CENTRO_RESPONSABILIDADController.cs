﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ControlPlanilla.Models;

namespace ControlPlanilla.Controllers
{
    public class CENTRO_RESPONSABILIDADController : AuditoriaController
    {
        private EmployeeEntities db = new EmployeeEntities();

        // GET: CENTRO_RESPONSABILIDAD
        public ActionResult Index()
        {
            return View(db.CENTRO_RESPONSABILIDAD.ToList());
        }

        // GET: CENTRO_RESPONSABILIDAD/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CENTRO_RESPONSABILIDAD cENTRO_RESPONSABILIDAD = db.CENTRO_RESPONSABILIDAD.Find(id);
            if (cENTRO_RESPONSABILIDAD == null)
            {
                return HttpNotFound();
            }
            return View(cENTRO_RESPONSABILIDAD);
        }

        // GET: CENTRO_RESPONSABILIDAD/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: CENTRO_RESPONSABILIDAD/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "NOMBRE")] CENTRO_RESPONSABILIDAD cENTRO_RESPONSABILIDAD)
        {
            if (ModelState.IsValid)
            {
                cENTRO_RESPONSABILIDAD.FECH_CREA = Ahora;
                cENTRO_RESPONSABILIDAD.USUA_CREA = Usuario;
                db.CENTRO_RESPONSABILIDAD.Add(cENTRO_RESPONSABILIDAD);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(cENTRO_RESPONSABILIDAD);
        }

        // GET: CENTRO_RESPONSABILIDAD/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CENTRO_RESPONSABILIDAD cENTRO_RESPONSABILIDAD = db.CENTRO_RESPONSABILIDAD.Find(id);
            if (cENTRO_RESPONSABILIDAD == null)
            {
                return HttpNotFound();
            }
            return View(cENTRO_RESPONSABILIDAD);
        }

        // POST: CENTRO_RESPONSABILIDAD/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID_CENTRO,NOMBRE")] CENTRO_RESPONSABILIDAD cENTRO_RESPONSABILIDAD)
        {
            if (ModelState.IsValid)
            {

                CENTRO_RESPONSABILIDAD grabar = db.CENTRO_RESPONSABILIDAD.Find(cENTRO_RESPONSABILIDAD.ID_CENTRO);
                grabar.USUA_ACTU = Usuario;
                grabar.FECH_ACTU= Ahora;
                db.Entry(grabar).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(cENTRO_RESPONSABILIDAD);
        }

        // GET: CENTRO_RESPONSABILIDAD/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CENTRO_RESPONSABILIDAD cENTRO_RESPONSABILIDAD = db.CENTRO_RESPONSABILIDAD.Find(id);
            if (cENTRO_RESPONSABILIDAD == null)
            {
                return HttpNotFound();
            }
            return View(cENTRO_RESPONSABILIDAD);
        }

        // POST: CENTRO_RESPONSABILIDAD/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            CENTRO_RESPONSABILIDAD cENTRO_RESPONSABILIDAD = db.CENTRO_RESPONSABILIDAD.Find(id);
            db.CENTRO_RESPONSABILIDAD.Remove(cENTRO_RESPONSABILIDAD);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
