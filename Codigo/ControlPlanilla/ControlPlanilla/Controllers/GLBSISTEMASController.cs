﻿using ControlPlanilla.Models;
using DevExpress.Web.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;

namespace ControlPlanilla.Controllers
{
    public class GLBSISTEMASController : AuditoriaController
    {
        static string idMenu;
        public ActionResult Index(string id)
        {
            if (id != null)
            {
                ViewBag.idseleccinado = id;
            }
            return View();
        }

        ControlPlanilla.Models.PlanillaAccesoMenus db = new ControlPlanilla.Models.PlanillaAccesoMenus();

        public JsonResult ObtenerMenus(string CD_CODI_SIST)
        {
            var listaEmployee = db.GLB_SISTEMAS.Where(ep => ep.CD_CODI_SIST == CD_CODI_SIST).
                Select(r => new GLB_SISTEMASView
                {
                    CD_CODI_SIST = r.CD_CODI_SIST,
                    CD_EMPRESA = r.CD_EMPRESA,
                    CD_ESTADO_SISTEMA = r.CD_ESTADO_SISTEMA,
                    DS_NOMB_SIST = r.DS_NOMB_SIST,
                }).ToList();
            var listFinal = listaEmployee;
            return Json(listFinal, JsonRequestBehavior.AllowGet);
        }

        [ValidateInput(false)]
        public ActionResult BuscarID(string key)
        {
            idMenu = key;
            return RedirectToAction("Index", new { id = key });
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult Create_Menu(string CD_CODI_SIST, string CD_ESTADO_SISTEMA, string DS_NOMB_SIST)
        {
            if (CD_CODI_SIST != "")
            {
                GLB_SISTEMAS grabar;
                //validar si existe
                var existincia = (from e in db.GLB_SISTEMAS
                                  where e.CD_CODI_SIST == CD_CODI_SIST
                                  select e.CD_CODI_SIST).Count();

                if (existincia > 0)
                {
                    grabar = db.GLB_SISTEMAS.Find(CD_CODI_SIST);
                }
                else
                {
                    grabar = new GLB_SISTEMAS();
                }

                grabar.CD_CODI_SIST = CD_CODI_SIST;
                grabar.CD_EMPRESA = CdEmpresa;
                grabar.CD_ESTADO_SISTEMA = Convert.ToBoolean(CD_ESTADO_SISTEMA);
                grabar.DS_NOMB_SIST = DS_NOMB_SIST;
                grabar.FECH_CREA = Ahora;
                grabar.USUA_CREA = Usuario;

                if (existincia > 0)
                {
                    db.Entry(grabar).State = EntityState.Modified;
                }
                else
                {
                    db.GLB_SISTEMAS.Add(grabar);
                }
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View();
        }

        [ValidateInput(false)]
        public ActionResult GridView3Partial()
        {
            var model = db.GLB_SISTEMAS;
            return PartialView("_GridView3Partial", model.ToList());
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult GridView3PartialAddNew([ModelBinder(typeof(DevExpressEditorsBinder))] ControlPlanilla.Models.GLB_SISTEMAS item)
        {
            var model = db.GLB_SISTEMAS;
            if (ModelState.IsValid)
            {
                try
                {
                    model.Add(item);
                    db.SaveChanges();
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
                ViewData["EditError"] = "Please, correct all errors.";
            return PartialView("_GridView3Partial", model.ToList());
        }
        [HttpPost, ValidateInput(false)]
        public ActionResult GridView3PartialUpdate([ModelBinder(typeof(DevExpressEditorsBinder))] ControlPlanilla.Models.GLB_SISTEMAS item)
        {
            var model = db.GLB_SISTEMAS;
            if (ModelState.IsValid)
            {
                try
                {
                    var modelItem = model.FirstOrDefault(it => it.CD_CODI_SIST == item.CD_CODI_SIST);
                    if (modelItem != null)
                    {
                        this.UpdateModel(modelItem);
                        db.SaveChanges();
                    }
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
                ViewData["EditError"] = "Please, correct all errors.";
            return PartialView("_GridView3Partial", model.ToList());
        }
        [HttpPost, ValidateInput(false)]
        public ActionResult GridView3PartialDelete(System.String CD_CODI_SIST)
        {
            var model = db.GLB_SISTEMAS;
            if (CD_CODI_SIST != null)
            {
                try
                {
                    var item = model.FirstOrDefault(it => it.CD_CODI_SIST == CD_CODI_SIST);
                    if (item != null)
                        model.Remove(item);
                    db.SaveChanges();
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            return PartialView("_GridView3Partial", model.ToList());
        }
    }
}