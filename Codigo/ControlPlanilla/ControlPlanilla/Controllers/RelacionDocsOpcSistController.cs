﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ControlPlanilla.Models;

namespace ControlPlanilla.Controllers
{
    public class RelacionDocsOpcSistController : AuditoriaController
    {
        

        // GET: RelacionDocsOpcSist
        public ActionResult Index()
        {
            var gLB_RELA_TIPO_DOCUMENTOS_OPCI_SIST = dbMenus.GLB_RELA_TIPO_DOCUMENTOS_OPCI_SIST.Include(g => g.GLB_OPCIONES_SISTEMA).Include(g => g.GLB_TIPO_DOCUMENTOS);
            return View(gLB_RELA_TIPO_DOCUMENTOS_OPCI_SIST.ToList());
        }

        // GET: RelacionDocsOpcSist/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            GLB_RELA_TIPO_DOCUMENTOS_OPCI_SIST gLB_RELA_TIPO_DOCUMENTOS_OPCI_SIST = dbMenus.GLB_RELA_TIPO_DOCUMENTOS_OPCI_SIST.Find(id);
            if (gLB_RELA_TIPO_DOCUMENTOS_OPCI_SIST == null)
            {
                return HttpNotFound();
            }
            return View(gLB_RELA_TIPO_DOCUMENTOS_OPCI_SIST);
        }

        // GET: RelacionDocsOpcSist/Create
        public ActionResult Create()
        {
            ViewBag.CD_CODI_OPCI_SIST = new SelectList(dbMenus.GLB_OPCIONES_SISTEMA, "CD_CODI_OPCI_SIST", "DS_NOMBRE_FORMA");
            ViewBag.CD_TIPO_DOCUMENTO = new SelectList(dbMenus.GLB_TIPO_DOCUMENTOS, "CD_TIPO_DOCUMENTO", "DS_TIPO_DOCUMENTO");
            return View();
        }

        // POST: RelacionDocsOpcSist/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,CD_EMPRESA,CD_CODI_OPCI_SIST,CD_TIPO_DOCUMENTO")] GLB_RELA_TIPO_DOCUMENTOS_OPCI_SIST gLB_RELA_TIPO_DOCUMENTOS_OPCI_SIST)
        {
            if (ModelState.IsValid)
            {
                gLB_RELA_TIPO_DOCUMENTOS_OPCI_SIST.USUA_CREA = Usuario;
                gLB_RELA_TIPO_DOCUMENTOS_OPCI_SIST.FECH_CREA = Ahora;

                dbMenus.GLB_RELA_TIPO_DOCUMENTOS_OPCI_SIST.Add(gLB_RELA_TIPO_DOCUMENTOS_OPCI_SIST);
                dbMenus.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.CD_CODI_OPCI_SIST = new SelectList(dbMenus.GLB_OPCIONES_SISTEMA, "CD_CODI_OPCI_SIST", "DS_NOMBRE_FORMA", gLB_RELA_TIPO_DOCUMENTOS_OPCI_SIST.CD_CODI_OPCI_SIST);
            ViewBag.CD_TIPO_DOCUMENTO = new SelectList(dbMenus.GLB_TIPO_DOCUMENTOS, "CD_TIPO_DOCUMENTO", "DS_TIPO_DOCUMENTO", gLB_RELA_TIPO_DOCUMENTOS_OPCI_SIST.CD_TIPO_DOCUMENTO);
            return View(gLB_RELA_TIPO_DOCUMENTOS_OPCI_SIST);
        }

        // GET: RelacionDocsOpcSist/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            GLB_RELA_TIPO_DOCUMENTOS_OPCI_SIST gLB_RELA_TIPO_DOCUMENTOS_OPCI_SIST = dbMenus.GLB_RELA_TIPO_DOCUMENTOS_OPCI_SIST.Find(id);
            if (gLB_RELA_TIPO_DOCUMENTOS_OPCI_SIST == null)
            {
                return HttpNotFound();
            }
            ViewBag.CD_CODI_OPCI_SIST = new SelectList(dbMenus.GLB_OPCIONES_SISTEMA, "CD_CODI_OPCI_SIST", "DS_NOMBRE_FORMA", gLB_RELA_TIPO_DOCUMENTOS_OPCI_SIST.CD_CODI_OPCI_SIST);
            ViewBag.CD_TIPO_DOCUMENTO = new SelectList(dbMenus.GLB_TIPO_DOCUMENTOS, "CD_TIPO_DOCUMENTO", "DS_TIPO_DOCUMENTO", gLB_RELA_TIPO_DOCUMENTOS_OPCI_SIST.CD_TIPO_DOCUMENTO);
            return View(gLB_RELA_TIPO_DOCUMENTOS_OPCI_SIST);
        }

        // POST: RelacionDocsOpcSist/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,CD_EMPRESA,CD_CODI_OPCI_SIST,CD_TIPO_DOCUMENTO")] GLB_RELA_TIPO_DOCUMENTOS_OPCI_SIST gLB_RELA_TIPO_DOCUMENTOS_OPCI_SIST)
        {
            if (ModelState.IsValid)
            {
                GLB_RELA_TIPO_DOCUMENTOS_OPCI_SIST rela = dbMenus.GLB_RELA_TIPO_DOCUMENTOS_OPCI_SIST.Find(gLB_RELA_TIPO_DOCUMENTOS_OPCI_SIST.ID);

                rela.CD_CODI_OPCI_SIST = gLB_RELA_TIPO_DOCUMENTOS_OPCI_SIST.CD_CODI_OPCI_SIST;
                rela.CD_TIPO_DOCUMENTO = gLB_RELA_TIPO_DOCUMENTOS_OPCI_SIST.CD_TIPO_DOCUMENTO;
                rela.USUA_ACTU = Usuario;
                rela.FECH_ACTU = Ahora;

                dbMenus.Entry(rela).State = EntityState.Modified;
                dbMenus.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CD_CODI_OPCI_SIST = new SelectList(dbMenus.GLB_OPCIONES_SISTEMA, "CD_CODI_OPCI_SIST", "DS_NOMBRE_FORMA", gLB_RELA_TIPO_DOCUMENTOS_OPCI_SIST.CD_CODI_OPCI_SIST);
            ViewBag.CD_TIPO_DOCUMENTO = new SelectList(dbMenus.GLB_TIPO_DOCUMENTOS, "CD_TIPO_DOCUMENTO", "DS_TIPO_DOCUMENTO", gLB_RELA_TIPO_DOCUMENTOS_OPCI_SIST.CD_TIPO_DOCUMENTO);
            return View(gLB_RELA_TIPO_DOCUMENTOS_OPCI_SIST);
        }

        // GET: RelacionDocsOpcSist/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            GLB_RELA_TIPO_DOCUMENTOS_OPCI_SIST gLB_RELA_TIPO_DOCUMENTOS_OPCI_SIST = dbMenus.GLB_RELA_TIPO_DOCUMENTOS_OPCI_SIST.Find(id);
            if (gLB_RELA_TIPO_DOCUMENTOS_OPCI_SIST == null)
            {
                return HttpNotFound();
            }
            return View(gLB_RELA_TIPO_DOCUMENTOS_OPCI_SIST);
        }

        // POST: RelacionDocsOpcSist/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            GLB_RELA_TIPO_DOCUMENTOS_OPCI_SIST gLB_RELA_TIPO_DOCUMENTOS_OPCI_SIST = dbMenus.GLB_RELA_TIPO_DOCUMENTOS_OPCI_SIST.Find(id);
            dbMenus.GLB_RELA_TIPO_DOCUMENTOS_OPCI_SIST.Remove(gLB_RELA_TIPO_DOCUMENTOS_OPCI_SIST);
            dbMenus.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                dbMenus.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
