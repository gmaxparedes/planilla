﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ControlPlanilla.Models;

namespace ControlPlanilla.Controllers
{
    public class PayFactorAnualsController : AuditoriaController
    {

        // GET: PayFactorAnuals
        public ActionResult Index()
        {
            ViewBag.OpcionSistema = OpcionSistema;
            return View(dbCiclo.PayFactorAnual.ToList());
        }

        // GET: PayFactorAnuals/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PayFactorAnual payFactorAnual = dbCiclo.PayFactorAnual.Find(id);
            if (payFactorAnual == null)
            {
                return HttpNotFound();
            }
            return View(payFactorAnual);
        }

        // GET: PayFactorAnuals/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: PayFactorAnuals/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID_FAC_ANUAL,DESCRIPCION")] PayFactorAnual payFactorAnual)
        {
            if (ModelState.IsValid)
            {
                payFactorAnual.USUA_CREA = Usuario;
                payFactorAnual.FECH_CREA = Ahora;

                dbCiclo.PayFactorAnual.Add(payFactorAnual);
                dbCiclo.SaveChanges();

                bool requiereArchivos = ComprobarRequiereArchivos(OpcionSistema);

                if (requiereArchivos)
                {
                    TempData["OpcionSistema"] = OpcionSistema;
                    TempData["CdForaneo"] = payFactorAnual.ID_FAC_ANUAL;
                    TempData["DsForaneo"] = payFactorAnual.DESCRIPCION;

                    bool cumplio = ComprobarArchivosAdjuntos();

                    if (!cumplio)
                    {
                        //string opcion, string DsForaneo, string CdForaneo)
                        return RedirectToAction("SubirArchivos", "Auditoria", new { opcion = OpcionSistema, DsForaneo = payFactorAnual.DESCRIPCION, CdForaneo = payFactorAnual.ID_FAC_ANUAL });
                    }
                }

                return RedirectToAction("Index");
            }

            return View(payFactorAnual);
        }

        // GET: PayFactorAnuals/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PayFactorAnual payFactorAnual = dbCiclo.PayFactorAnual.Find(id);
            if (payFactorAnual == null)
            {
                return HttpNotFound();
            }
            return View(payFactorAnual);
        }

        // POST: PayFactorAnuals/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID_FAC_ANUAL,DESCRIPCION")] PayFactorAnual payFactorAnual)
        {
            if (ModelState.IsValid)
            {

                PayFactorAnual payFactor = dbCiclo.PayFactorAnual.Find(payFactorAnual.ID_FAC_ANUAL);

                payFactor.DESCRIPCION = payFactorAnual.DESCRIPCION;
                payFactor.USUA_ACTU = Usuario;
                payFactor.FECH_ACTU = Ahora;

                dbCiclo.Entry(payFactor).State = EntityState.Modified;
                dbCiclo.SaveChanges();

                bool requiereArchivos = ComprobarRequiereArchivos(OpcionSistema);

                if (requiereArchivos)
                {
                    TempData["OpcionSistema"] = OpcionSistema;
                    TempData["CdForaneo"] = payFactor.ID_FAC_ANUAL;
                    TempData["DsForaneo"] = payFactor.DESCRIPCION;

                    bool cumplio = ComprobarArchivosAdjuntos();

                    if (!cumplio)
                    {
                        //string opcion, string DsForaneo, string CdForaneo)
                        return RedirectToAction("SubirArchivos", "Auditoria", new { opcion = OpcionSistema, DsForaneo = payFactor.DESCRIPCION, CdForaneo = payFactor.ID_FAC_ANUAL });
                    }
                }


                return RedirectToAction("Index");
            }
            return View(payFactorAnual);
        }

        // GET: PayFactorAnuals/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PayFactorAnual payFactorAnual = dbCiclo.PayFactorAnual.Find(id);
            if (payFactorAnual == null)
            {
                return HttpNotFound();
            }
            return View(payFactorAnual);
        }

        // POST: PayFactorAnuals/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            PayFactorAnual payFactorAnual = dbCiclo.PayFactorAnual.Find(id);
            dbCiclo.PayFactorAnual.Remove(payFactorAnual);
            dbCiclo.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                dbCiclo.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
