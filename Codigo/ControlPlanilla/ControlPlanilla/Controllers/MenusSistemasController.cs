﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ControlPlanilla.Models;

namespace ControlPlanilla.Controllers
{
    public class MenusSistemasController : Controller
    {
        private PlanillaAccesoMenus db = new PlanillaAccesoMenus();

        // GET: MenusSistemas
        public ActionResult Index()
        {
            return View(db.GLB_MENUS_SISTEMAS.ToList());
        }

        // GET: MenusSistemas/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            GLB_MENUS_SISTEMAS gLB_MENUS_SISTEMAS = db.GLB_MENUS_SISTEMAS.Find(id);
            if (gLB_MENUS_SISTEMAS == null)
            {
                return HttpNotFound();
            }
            return View(gLB_MENUS_SISTEMAS);
        }

        // GET: MenusSistemas/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: MenusSistemas/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "CD_CODI_MENU,CD_EMPRESA,DS_DESC_MENU,CD_ESTADO_MENU,USUA_CREA,FECH_CREA,USUA_ACTU,FECH_ACTU")] GLB_MENUS_SISTEMAS gLB_MENUS_SISTEMAS)
        {
            if (ModelState.IsValid)
            {
                db.GLB_MENUS_SISTEMAS.Add(gLB_MENUS_SISTEMAS);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(gLB_MENUS_SISTEMAS);
        }

        // GET: MenusSistemas/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            GLB_MENUS_SISTEMAS gLB_MENUS_SISTEMAS = db.GLB_MENUS_SISTEMAS.Find(id);
            if (gLB_MENUS_SISTEMAS == null)
            {
                return HttpNotFound();
            }
            return View(gLB_MENUS_SISTEMAS);
        }

        // POST: MenusSistemas/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "CD_CODI_MENU,CD_EMPRESA,DS_DESC_MENU,CD_ESTADO_MENU,USUA_CREA,FECH_CREA,USUA_ACTU,FECH_ACTU")] GLB_MENUS_SISTEMAS gLB_MENUS_SISTEMAS)
        {
            if (ModelState.IsValid)
            {
                db.Entry(gLB_MENUS_SISTEMAS).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(gLB_MENUS_SISTEMAS);
        }

        // GET: MenusSistemas/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            GLB_MENUS_SISTEMAS gLB_MENUS_SISTEMAS = db.GLB_MENUS_SISTEMAS.Find(id);
            if (gLB_MENUS_SISTEMAS == null)
            {
                return HttpNotFound();
            }
            return View(gLB_MENUS_SISTEMAS);
        }

        // POST: MenusSistemas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            GLB_MENUS_SISTEMAS gLB_MENUS_SISTEMAS = db.GLB_MENUS_SISTEMAS.Find(id);
            db.GLB_MENUS_SISTEMAS.Remove(gLB_MENUS_SISTEMAS);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
