﻿using DevExpress.Web.Mvc;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure.MappingViews;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ControlPlanilla.Models;
using Microsoft.Ajax.Utilities;
using NLog;
using System.Web.Script.Serialization;
using DevExpress.XtraRichEdit.Import.Doc;

namespace ControlPlanilla.Controllers
{

    public class EmployeeController : AuditoriaController
    {
        private GlobalEntities db = new GlobalEntities();
        private EmployeeEntities dbE = new EmployeeEntities();
        private PayCycle dbC = new PayCycle();
        public ActionResult Index(string id)
        {
            if (id != null)
            {
                ViewBag.idseleccinado = id;
            }


            ViewBag.CD_OPC_SIST = "Empleados";
            ViewBag.Clave = "No";
            var listaPaises = dbMenus.PAISES.Select(a => new PaisesViewModels { CD_PAIS = a.CD_PAIS, DS_PAIS = a.DS_NOMBREPAIS }).ToList();
            ViewBag.cmbPaises = listaPaises;
            var listaCargos = db.CARGOS.Select(a => new CARGOSView { ID_CARGO = a.ID_CARGO, NOMBRE_CARGO = a.NOMBRE_CARGO }).ToList();
            ViewBag.cmbCargos = listaCargos;
            var listaAreas = dbMenus.AREAS_EMPRESA.Select(a => new AREAS_EMPRESAView { ID_AREA = a.ID_AREA, DESCRIPCION = a.DESCRIPCION }).ToList();
            ViewBag.cmbAreas = listaAreas;
            var listaECivil = db.ESTADOS_CIVILES.Select(a => new ESTADO_CIVILView { ID_ESTADO_CIVIL = a.ID_ESTADO_CIVIL, DESCRIPCION = a.DESCRIPCION }).ToList();
            ViewBag.cmbECivil = listaECivil;
            var listaTEmpleado = db.TIPO_EMPLEADO.Select(a => new TIPO_EMPLEADOview { ID_TIPO_EMPLEADO = a.ID_TIPO_EMPLEADO, NOMBRE_TIPO = a.NOMBRE_TIPO }).ToList();
            ViewBag.cmbTEmpleado = listaTEmpleado;
            var listaPrestacion = db.PRESTACIONES.Select(a => new PRESTACIONESView { ID_DVC = a.ID_DVC, NOMBRE_PRESTACION = a.NOMBRE_PRESTACION }).ToList();
            ViewBag.cmbPrestacion = listaPrestacion;
            var listaDistribucion = db.DISTRIBUCION_GASTOS.Select(a => new DISTRIBUCION_GASTOSView { ID_DISTRIBUCION = a.ID_DISTRIBUCION, NOMBRE_PRESTACION = a.NOMBRE_PRESTACION }).ToList();
            ViewBag.cmbDistribucion = listaDistribucion;
            var listaMTerminacion = db.MOTIVO_TERMINACION.Select(a => new MOTIVO_TERMINACIONView { ID_MOV_TERMIN = a.ID_MOV_TERMIN, NOMBRE_PRESTACION = a.NOMBRE_PRESTACION }).ToList();
            ViewBag.cmbMTerminacion = listaMTerminacion;
            var listaContrato = db.CONTRATO.Select(a => new CONTRATOView { ID_CONTRATO = a.ID_CONTRATO, NOMBRE_PRESTACION = a.NOMBRE_PRESTACION }).ToList();
            ViewBag.cmbContrato = listaContrato;
            var listaHExtras = db.HORAS_EXTRAS.Select(a => new HORAS_EXTRASView { ID_HORASEXTRA = a.ID_HORASEXTRA, NOMBRE_PRESTACION = a.NOMBRE_PRESTACION }).ToList();
            ViewBag.cmbHExtras = listaHExtras;
            var listaAFP = db.AFP.Select(a => new AFPView { ID_AFP = a.ID_AFP, NOMBRE_PRESTACION = a.NOMBRE_PRESTACION }).ToList();
            ViewBag.cmbAFP = listaAFP;
            var listaTMObra = db.TIPO_MANO_OBRA_EMPRESA.Select(a => new TIPO_MANO_OBRA_EMPRESAView { ID_MANO_OBRA = a.ID_MANO_OBRA, DESCRIPCION = a.DESCRIPCION }).ToList();
            ViewBag.cmTMObra = listaTMObra;
            var listaEmployer = dbE.Employer.Select(a => new EmployerView { No = a.No, Name = a.Name }).ToList();
            ViewBag.cmbEmployer = listaEmployer;
            var listaEmployeeClass = dbE.EmployeeClass.Select(a => new EmployeClassView { Code = a.Code, Description = a.Description }).ToList();
            ViewBag.cmbEmployeeClass = listaEmployeeClass;
            //ViewBag.PayCycleCode = new SelectList(dbCiclo.PayCycle, "Code", "Description");
            var listaEmployeeType = dbE.EmployeeType.Select(a => new EmployeeTypeView { Code = a.Code, Description = a.Description }).ToList();
            ViewBag.cmbEmployeeType = listaEmployeeType;
            //ViewBag.PayrollReportingAuthority = new SelectList(dbPayControl.PayrollReportingAuthority, "Code", "Name");


            if (TempData["MensajeOK"] != null)
            {
                ViewBag.MensajeOk = TempData["MensajeOK"].ToString();
            }

            return View();
        }
        /*CRUD para los terminos del ciclo de pago*/
        [HttpPost]
        [AllowAnonymous]
        public ActionResult Buscar(int code)
        {
            try
            {
                var result = "something";
                ViewBag.PayCycleCode = code;
                var listaPaises = dbMenus.PAISES.Select(a => new PaisesViewModels { CD_PAIS = a.CD_PAIS, DS_PAIS = a.DS_NOMBREPAIS }).ToList();
                ViewBag.cmbPaises = listaPaises;
                return PartialView("Buscar", result);
            }
            catch (SystemException ex)
            {
                throw;
            }
        }
        public JsonResult ObtenerCP(int ID_CIUDAD)
        {
            List<PCView> listaCP = new List<PCView> {
                new PCView {  Id_Codigo = 0,  Codigo_Postal = "Seleccione una opción" }
            };
            var listaCP1 = db.PC.Where(ep => ep.ID_CIUDAD == ID_CIUDAD).
                Select(r => new PCView
                {
                    Id_Codigo = r.Id_Codigo,
                    Codigo_Postal = r.Codigo_Postal
                }).ToList();
            var listFinal = listaCP.Union(listaCP1);
            return Json(listFinal, JsonRequestBehavior.AllowGet);
        }
        public JsonResult ObtenerCargos(int ID_CARGO)
        {
            List<CARGOSView> listaCargos = new List<CARGOSView> {
                new CARGOSView {   ID_CARGO = 0,   NOMBRE_CARGO = "Seleccione una opción" }
            };
            var listaCargos1 = db.CARGOS.Where(ep => ep.ID_CARGO == ID_CARGO).
                Select(r => new CARGOSView
                {
                    ID_CARGO = r.ID_CARGO,
                    NOMBRE_CARGO = r.NOMBRE_CARGO
                }).ToList();
            var listFinal = listaCargos.Union(listaCargos1);
            return Json(listFinal, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ObtenerEmployee(string No)
        {
            var listaEmployee = dbE.Employee.Where(ep => ep.No == No).
                Select(r => new EmployeeView
                {
                    No = r.No,
                    FirstName = r.FirstName,
                    MiddleName = r.MiddleName,
                    LastName = r.LastName,
                    SecondLastName = r.SecondLastName,
                    MarriedLastname = r.MarriedLastname,
                    MiddleInitial = r.MiddleInitial,
                    Suffix = r.Suffix,
                    Initials = r.Initials,
                    SearchName = r.SearchName,
                    Address = r.Address,
                    Address2 = r.Address2,
                    CountryRegionCode = r.CountryRegionCode,
                    Departamento = r.Departamento,
                    City = r.City,
                    PostCode = r.PostCode,
                    ID_CARGO = r.ID_CARGO,
                    Blocked = r.Blocked,
                    LastDateModified = r.LastDateModified,
                    //tab 2
                    Dpto = r.Dpto,
                    Area = r.Area,
                    //seccion documentos
                    DUI = r.DUI,
                    SocialSecurityNo = r.SocialSecurityNo,
                    NomDUI = r.NomDUI,
                    SocialSecurityname = r.SocialSecurityname,
                    LugExpTI = r.LugExpTI,
                    NUP = r.NUP,
                    ProfDUI = r.ProfDUI,
                    NUPName = r.NUPName,
                    TILetras = r.TILetras,
                    NIT = r.NIT,
                    FExpTILetras = r.FExpTILetras,
                    NITName = r.NITName,
                    PhoneNo = r.PhoneNo,
                    HomePhoneNo = r.HomePhoneNo,
                    Extension = r.Extension,
                    MobilePhoneNo = r.MobilePhoneNo,
                    CompanyEMail = r.CompanyEMail,
                    EMail = r.EMail
                }).ToList();
            var listFinal = listaEmployee;
            return Json(listFinal, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult Create_Employee(string No, string FirstName, string MiddleName, string LastName, string SecondLastName, string MarriedLastname, string MiddleInitial, string Suffix, string Initials, string SearchName, string Address, string Address2, string CountryRegionCode, string Departamento, string City, string PostCode, string ID_CARGO, string Blocked, string LastDateModified)
        {
            if (No != "")
            {
                Employee grabar;
                //validar si existe
                var existincia = (from e in dbE.Employee
                                  where e.No == No
                                  select e.No).Count();

                if (existincia > 0)
                {
                    grabar = dbE.Employee.Find(No);
                }
                else
                {
                    grabar = new Employee();
                }

                grabar.No = No;
                grabar.FirstName = FirstName;
                grabar.LastName = LastName;
                grabar.SecondLastName = SecondLastName;
                grabar.MarriedLastname = MarriedLastname;
                grabar.MiddleInitial = MiddleInitial;
                grabar.Suffix = Suffix;
                grabar.Initials = Initials;
                grabar.SearchName = SearchName;
                grabar.Address = Address;
                grabar.Address2 = Address2;
                grabar.CountryRegionCode = CountryRegionCode;
                grabar.Departamento = Convert.ToInt32(Departamento);
                grabar.City = Convert.ToInt32(City);
                grabar.PostCode = PostCode;
                grabar.ID_CARGO = Convert.ToInt32(ID_CARGO);
                grabar.Blocked = Convert.ToByte(Blocked == "on" ? "1" : "0");
                grabar.LastDateModified = Convert.ToDateTime(LastDateModified);


                if (existincia > 0)
                {
                    dbE.Entry(grabar).State = EntityState.Modified;
                }
                else
                {
                    dbE.Employee.Add(grabar);
                }
                dbE.SaveChanges();
                return RedirectToAction("Index");
            }

            return View();
        }
        //,FirstName,MiddleName,LastName,Initials,JobTitle,SearchName,Address,Address2,City,PostCode,PhoneNo,MobilePhoneNo,EMail,BirthDate,SocialSecurityNo,Gender,CountryRegionCode,ManagerNo,EmplymtContractCode,EmploymentDate,Status,CauseofInactivityCode,TerminationDate,LastDateModified,Extension,CompanyEMail,ProfesiónOficio,DevengahorasExtras,TipodeManodeobra,Comision,ClassCode,EmployerNo,MiddleInitial,Suffix,HomePhoneNo,Race,Blocked,DefaultWorkTypeCode,PayCycleCode,DefaultRepAuthCode,HRRepNo,EmployeeType,MaritalStatus,OfficeLocation,DUI,NomDUI,TILetras,LugExpTI,ProfDUI,FExpTILetras,Edad,Area,Dpto,DepGasViat,AFps,DistGast,FechContAjust,SecondLastName,MarriedLastname,NUP,SocialSecurityname,NIT,NITName,NUPName,IncapAcum,HrsExtras,Departamento,JornadaLaboral,ID_CARGO
        public JsonResult ObtenerDep_Empresas(int cd_Area, string CDEMPRESA)
        {
            List<DEP_EMPRESASView> listaDeptoAreas = new List<DEP_EMPRESASView> {
                 new DEP_EMPRESASView {  ID_DEPTO = 0,  DESCRIPCION = "Seleccione una opción" }
             };
            var listaDeptoAreas1 = dbMenus.DEPTO_EMPRESA.Where(ep => ep.ID_AREA == cd_Area && ep.CD_EMPRESA == CDEMPRESA).
               Select(r => new DEP_EMPRESASView
               {
                   ID_DEPTO = r.ID_DEPTO,
                   DESCRIPCION = r.DESCRIPCION
               }).ToList();
            var listFinal = listaDeptoAreas.Union(listaDeptoAreas1);
            return Json(listFinal, JsonRequestBehavior.AllowGet);
        }



        [HttpPost]
        [AllowAnonymous]
        public ActionResult Create_EmployeeAreas(string No, int Area, int Dpto)
        {
            if (ModelState.IsValid)
            {

                //dbE.Employee.Add();
                //
                Employee grabar = dbE.Employee.Find(No);
                grabar.Area = Area;
                grabar.Dpto = Dpto;
                dbE.Entry(grabar).State = EntityState.Modified;
                dbE.SaveChanges();
                return RedirectToAction("Index");
            }

            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult Create_EmployeeDoc(string No, string DUI, string SocialSecurityNo, string NomDUI, string SocialSecurityname, string LugExpTI, string NUP, string ProfDUI, string NUPName, string TILetras, string nit, string FExpTILetras, string NITName)
        {
            if (ModelState.IsValid)
            {

                //dbE.Employee.Add();
                //
                Employee grabar = dbE.Employee.Find(No);
                grabar.DUI = DUI;
                grabar.SocialSecurityNo = SocialSecurityNo;
                grabar.NomDUI = NomDUI;
                grabar.SocialSecurityname = SocialSecurityname;
                grabar.LugExpTI = LugExpTI;
                grabar.NUP = NUP;
                grabar.ProfDUI = ProfDUI;
                grabar.NUPName = NUPName;
                grabar.TILetras = TILetras;
                grabar.NIT = nit;
                grabar.FExpTILetras = FExpTILetras;
                grabar.NITName = NITName;
                dbE.Entry(grabar).State = EntityState.Modified;
                dbE.SaveChanges();
                TempData["MensajeOK"] = "Registro guardado";
                return RedirectToAction("Index");
            }

            return View();
        }
        [HttpPost]
        [AllowAnonymous]
        public ActionResult Create_EmployeeContac(string No, string PhoneNo, string HomePhoneNo, string Extension, string MobilePhoneNo, string CompanyEMail, string EMail)
        {
            if (ModelState.IsValid)
            {
                Employee grabar = dbE.Employee.Find(No);
                grabar.PhoneNo = PhoneNo;
                grabar.HomePhoneNo = HomePhoneNo;
                grabar.Extension = Extension;
                grabar.MobilePhoneNo = MobilePhoneNo;
                grabar.CompanyEMail = CompanyEMail;
                grabar.EMail = EMail;

                dbE.Entry(grabar).State = EntityState.Modified;

                dbE.SaveChanges();
                TempData["errorMensaje"] = "Informacion de contacto ingresada satisfactoriamente";
                ViewBag.MensajeError = TempData["errorMensaje"];
                return RedirectToAction("Index");

            }

            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult Create_EmployeePersonal(string No, string BirthDate, int Edad, int MaritalStatus, int gender)
        {
            if (ModelState.IsValid)
            {
                Employee grabar = dbE.Employee.Find(No);
                grabar.BirthDate = Convert.ToDateTime(BirthDate);
                grabar.Edad = Edad;
                grabar.MaritalStatus = MaritalStatus;
                grabar.Gender = gender;


                dbE.Entry(grabar).State = EntityState.Modified;

                dbE.SaveChanges();
                TempData["errorMensaje"] = "Informacion Personal ingresada satisfactoriamente";
                ViewBag.MensajeError = TempData["errorMensaje"];
                return RedirectToAction("Index");

            }

            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult Create_EmployeeAdministracion(string No, int ID_DISTRIBUCION, int ID_MANO_OBRA, int ID_DVC, int ID_HORASEXTRA, int Status, int ID_CONTRATO, string FechContAjust, int ID_MOV_TERMIN, string EmploymentDate, string TerminationDate, int ID_TIPO_EMPLEADO, int ID_AFP)
        {
            if (ModelState.IsValid)
            {
                Employee grabar = dbE.Employee.Find(No);
                grabar.ID_DISTRIBUCION = ID_DISTRIBUCION;
                grabar.TipodeManodeobra = ID_MANO_OBRA;
                grabar.ID_DVC = ID_DVC;
                grabar.ID_HORASEXTRA = ID_HORASEXTRA;
                grabar.Status = Status;
                grabar.ID_CONTRATO = ID_CONTRATO;
                if (FechContAjust != "") { grabar.FechContAjust = Convert.ToDateTime(FechContAjust); }

                grabar.ID_MOV_TERMIN = ID_MOV_TERMIN;
                if (EmploymentDate != "") { grabar.EmploymentDate = Convert.ToDateTime(EmploymentDate); }
                if (TerminationDate != "") { grabar.EmploymentDate = Convert.ToDateTime(TerminationDate); }

                grabar.ID_TIPO_EMPLEADO = ID_TIPO_EMPLEADO;
                grabar.ID_AFP = ID_AFP;

                dbE.Entry(grabar).State = EntityState.Modified;

                dbE.SaveChanges();
                TempData["errorMensaje"] = "Informacion Personal ingresada satisfactoriamente";
                ViewBag.MensajeError = TempData["errorMensaje"];
                return RedirectToAction("Index");

            }

            return View();
        }
        [HttpPost]
        [AllowAnonymous]
        public ActionResult CrearEmployeePayRoll(string No, string EmployerNo, string ClassCode, int ID_TIPO_EMPLEADO, string Comision)
        {
            if (ModelState.IsValid)
            {
                Employee grabar = dbE.Employee.Find(No);
                grabar.EmployerNo = EmployerNo;
                grabar.ClassCode = ClassCode;
                grabar.ID_TIPO_EMPLEADO = ID_TIPO_EMPLEADO;
                grabar.Comision = Convert.ToByte(Comision == "on" ? "1" : "0");


                dbE.Entry(grabar).State = EntityState.Modified;

                dbE.SaveChanges();
                TempData["errorMensaje"] = "Informacion Personal ingresada satisfactoriamente";
                ViewBag.MensajeError = TempData["errorMensaje"];
                return RedirectToAction("Index");

            }

            return View();
        }

        ControlPlanilla.Models.EmployeeEntities db1 = new ControlPlanilla.Models.EmployeeEntities();




        [ValidateInput(false)]
        public ActionResult GridViewPartial()
        {
            var model = db1.Employee;
            return PartialView("_GridViewPartial", model.ToList());
        }

        [ValidateInput(false)]
        public ActionResult BuscarID(string key)
        {
            return RedirectToAction("Index", new { id = key });
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult GridViewPartialAddNew([ModelBinder(typeof(DevExpressEditorsBinder))] ControlPlanilla.Models.Employee item)
        {
            var model = db1.Employee;
            if (ModelState.IsValid)
            {
                try
                {
                    model.Add(item);
                    db1.SaveChanges();
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
                ViewData["EditError"] = "Please, correct all errors.";
            return PartialView("_GridViewPartial", model.ToList());
        }
        [HttpPost, ValidateInput(false)]
        public ActionResult GridViewPartialUpdate([ModelBinder(typeof(DevExpressEditorsBinder))] ControlPlanilla.Models.Employee item)
        {
            var model = db1.Employee;
            if (ModelState.IsValid)
            {
                try
                {
                    var modelItem = model.FirstOrDefault(it => it.No == item.No);
                    if (modelItem != null)
                    {
                        this.UpdateModel(modelItem);
                        db1.SaveChanges();
                    }
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
                ViewData["EditError"] = "Please, correct all errors.";
            return PartialView("_GridViewPartial", model.ToList());
        }
        [HttpPost, ValidateInput(false)]
        public ActionResult GridViewPartialDelete(System.String No)
        {
            var model = db1.Employee;
            if (No != null)
            {
                try
                {
                    var item = model.FirstOrDefault(it => it.No == No);
                    if (item != null)
                        model.Remove(item);
                    db1.SaveChanges();
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            return PartialView("_GridViewPartial", model.ToList());
        }
    }
}