﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ControlPlanilla.Models;

namespace ControlPlanilla.Controllers
{
    public class PayrollControlMonthlySchedulesController : AuditoriaController
    {

        // GET: PayrollControlMonthlySchedules
        public ActionResult Index()
        {
            return View(dbPayControl.PayrollControlMonthlySchedule.ToList());
        }

        // GET: PayrollControlMonthlySchedules/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PayrollControlMonthlySchedule payrollControlMonthlySchedule = dbPayControl.PayrollControlMonthlySchedule.Find(id);
            if (payrollControlMonthlySchedule == null)
            {
                return HttpNotFound();
            }
            return View(payrollControlMonthlySchedule);
        }

        // GET: PayrollControlMonthlySchedules/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: PayrollControlMonthlySchedules/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID_CALENDARIO,DESCRIPCION")] PayrollControlMonthlySchedule payrollControlMonthlySchedule)
        {
            if (ModelState.IsValid)
            {
                PayrollControlMonthlySchedule guardar = new PayrollControlMonthlySchedule();
                guardar.DESCRIPCION = payrollControlMonthlySchedule.DESCRIPCION;
                guardar.USUA_CREA = Usuario;
                guardar.FECH_CREA = Ahora;
                dbPayControl.PayrollControlMonthlySchedule.Add(guardar);
                dbPayControl.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(payrollControlMonthlySchedule);
        }

        // GET: PayrollControlMonthlySchedules/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PayrollControlMonthlySchedule payrollControlMonthlySchedule = dbPayControl.PayrollControlMonthlySchedule.Find(id);
            if (payrollControlMonthlySchedule == null)
            {
                return HttpNotFound();
            }
            return View(payrollControlMonthlySchedule);
        }

        // POST: PayrollControlMonthlySchedules/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID_CALENDARIO,DESCRIPCION")] PayrollControlMonthlySchedule payrollControlMonthlySchedule)
        {
            if (ModelState.IsValid)
            {
                PayrollControlMonthlySchedule guardar = dbPayControl.PayrollControlMonthlySchedule.Find(payrollControlMonthlySchedule.ID_CALENDARIO);
                guardar.DESCRIPCION = payrollControlMonthlySchedule.DESCRIPCION;
                guardar.USUA_ACTUA = Usuario;
                guardar.FECH_ACTUA = Ahora;
                dbPayControl.Entry(guardar).State = EntityState.Modified;
                dbPayControl.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(payrollControlMonthlySchedule);
        }

        // GET: PayrollControlMonthlySchedules/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PayrollControlMonthlySchedule payrollControlMonthlySchedule = dbPayControl.PayrollControlMonthlySchedule.Find(id);
            if (payrollControlMonthlySchedule == null)
            {
                return HttpNotFound();
            }
            return View(payrollControlMonthlySchedule);
        }

        // POST: PayrollControlMonthlySchedules/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            PayrollControlMonthlySchedule payrollControlMonthlySchedule = dbPayControl.PayrollControlMonthlySchedule.Find(id);
            dbPayControl.PayrollControlMonthlySchedule.Remove(payrollControlMonthlySchedule);
            dbPayControl.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                dbPayControl.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
