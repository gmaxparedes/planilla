﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity.Core.Objects;
using ControlPlanilla.Models;
using System.Data.Entity;

namespace ControlPlanilla.Controllers
{
    public class GLB_CONF_OPCI_SISTController : AuditoriaController
    {
        // GET: GLB_CONF_OPCI_SIST
        static string idMenu;

        [ValidateInput(false)]
        public ActionResult BuscarID(string key)
        {
            idMenu = key;
            if (key != "" || key == null) {
                key = "something";
            }
            return RedirectToAction("Index", new { id = key });
        }
        public JsonResult ObtenerMenus(string CD_CODI_OPCI_SISTT)
        
        {
            
            var resultspli = idMenu.Split('|');
            string CD_CODI_SIST = resultspli[0];
            string CD_CODI_MENU = resultspli[1];
            string CD_CODI_OPCI_SIST = resultspli[2];
            var listaEmployee = db.GLB_CONF_OPCI_SIST.Where(ep =>ep.CD_CODI_SIST== CD_CODI_SIST && ep.CD_CODI_MENU== CD_CODI_MENU &&   ep.CD_CODI_OPCI_SIST == CD_CODI_OPCI_SIST).
                Select(r => new GLB_CONF_OPCI_SISTView
                {
                    CD_CODI_SIST = r.CD_CODI_SIST,
                    CD_CODI_MENU = r.CD_CODI_MENU,
                    CD_CODI_OPCI_SIST = r.CD_CODI_OPCI_SIST,
                    Estado = r.Estado,
                }).ToList();
            var listFinal = listaEmployee;
            return Json(listFinal, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Index(string id)
        {
            if (id != null)
            {
                ViewBag.idseleccinado = id;
            }
            var listaSistemas =  from modulo in db.GLB_SISTEMAS
                                where modulo.CD_ESTADO_SISTEMA == true
                                 select new GLB_SISTEMASView { CD_CODI_SIST = modulo.CD_CODI_SIST, DS_NOMB_SIST = modulo.DS_NOMB_SIST };
            var listaMenus = from menus in db.GLB_MENUS_SISTEMAS
                             where menus.CD_ESTADO_MENU == true
                             select new GLB_MENUS_SISTEMASView { CD_CODI_MENU = menus.CD_CODI_MENU, DS_DESC_MENU = menus.DS_DESC_MENU };
            var listaSubMenus = from submenus in db.GLB_OPCIONES_SISTEMA
                                where submenus.CD_ESTADO_OPCION_SISTEMA == true
                                select new GLB_OPCIONES_SISTEMAView { CD_CODI_OPCI_SIST = submenus.CD_CODI_OPCI_SIST, DS_NOMBRE_FORMA = submenus.DS_NOMBRE_FORMA };
            ViewBag.Sistema = listaSistemas.ToList();
            ViewBag.Menu = listaMenus.ToList();
            ViewBag.Modulo = listaSubMenus.ToList();
            return View();
        }
        
        ControlPlanilla.Models.PlanillaAccesoMenus db = new ControlPlanilla.Models.PlanillaAccesoMenus();
        [ValidateInput(false)]
        public ActionResult GridViewPartial()
        {
            var model = from principal in db.GLB_CONF_OPCI_SIST
                        join sistema in db.GLB_SISTEMAS on principal.CD_CODI_SIST equals sistema.CD_CODI_SIST
                        join menu in db.GLB_MENUS_SISTEMAS on principal.CD_CODI_MENU equals menu.CD_CODI_MENU
                        join opcion in db.GLB_OPCIONES_SISTEMA on principal.CD_CODI_OPCI_SIST equals opcion.CD_CODI_OPCI_SIST
                        select new { CD_CODI_SIST = principal.CD_CODI_SIST, CD_CODI_MENU = principal.CD_CODI_MENU, CD_CODI_OPCI_SIST = principal.CD_CODI_OPCI_SIST, DS_NOMB_SIST = sistema.DS_NOMB_SIST, DS_DESC_MENU = menu.DS_DESC_MENU, DS_NOMBRE_FORMA = opcion.DS_NOMBRE_FORMA, Estado = principal.Estado };
            return PartialView("_GridViewPartial", model.ToList());
        }

        [ValidateInput(false)]
        public ActionResult datosGenerales(Int32 pageSize  = 5)
        {
            var model = from principal in db.GLB_CONF_OPCI_SIST
                        join sistema in db.GLB_SISTEMAS on principal.CD_CODI_SIST equals sistema.CD_CODI_SIST
                        join menu in db.GLB_MENUS_SISTEMAS on principal.CD_CODI_MENU equals menu.CD_CODI_MENU
                        join opcion in db.GLB_OPCIONES_SISTEMA on principal.CD_CODI_OPCI_SIST equals opcion.CD_CODI_OPCI_SIST
                        select new { CD_CODI_SIST = principal.CD_CODI_SIST, CD_CODI_MENU = principal.CD_CODI_MENU, CD_CODI_OPCI_SIST = principal.CD_CODI_OPCI_SIST,   DS_NOMB_SIST = sistema.DS_NOMB_SIST, DS_DESC_MENU=menu.DS_DESC_MENU, DS_NOMBRE_FORMA=opcion.DS_NOMBRE_FORMA, Estado=principal.Estado };

            return PartialView("_GridViewPartial", model.ToList());
        }
        //
        [HttpPost]
        [AllowAnonymous]
        public ActionResult Create_Opcion(string CD_CODI_SIST, string CD_CODI_MENU, string CD_CODI_OPCI_SIST,  string Estado,int NM_SEQU_ORDE_SIST=1, int NM_SEQU_ORDE_MENU=2,int NM_SEQU_ORDE_OPCI=4)
        {
            if (CD_CODI_SIST != "")
            {
                GLB_CONF_OPCI_SIST grabar;
                //validar si existe
                var existincia = (from e in db.GLB_CONF_OPCI_SIST
                                  where e.CD_CODI_SIST == CD_CODI_SIST && e.CD_CODI_MENU== CD_CODI_MENU && e.CD_CODI_OPCI_SIST== CD_CODI_OPCI_SIST
                                  select e.CD_CODI_SIST).Count();

                if (existincia > 0)
                {
                    grabar = db.GLB_CONF_OPCI_SIST.Find(CD_CODI_SIST, CD_CODI_MENU, CD_CODI_OPCI_SIST);
                }
                else
                {
                    grabar = new GLB_CONF_OPCI_SIST();
                }

                grabar.CD_CODI_SIST = CD_CODI_SIST;
                grabar.CD_EMPRESA = CdEmpresa;
                grabar.Estado = Convert.ToBoolean(Estado);
                grabar.CD_CODI_MENU = CD_CODI_MENU;
                grabar.CD_CODI_OPCI_SIST = CD_CODI_OPCI_SIST;
                grabar.CD_CODI_MENU = CD_CODI_MENU;
                grabar.NM_SEQU_ORDE_SIST = NM_SEQU_ORDE_SIST;
                grabar.NM_SEQU_ORDE_MENU = NM_SEQU_ORDE_MENU;
                grabar.NM_SEQU_ORDE_OPCI = NM_SEQU_ORDE_OPCI;
                grabar.FECH_CREA = Ahora;
                grabar.USUA_CREA = Usuario;

                if (existincia > 0)
                {
                    db.Entry(grabar).State = EntityState.Modified;
                }
                else
                {
                    db.GLB_CONF_OPCI_SIST.Add(grabar);
                }
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View();
        }

    }
}