﻿using ControlPlanilla.Models;
using DevExpress.Web.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
namespace ControlPlanilla.Controllers
{
    public class GLB_OPCIONES_SISTEMAController : AuditoriaController
    {
        // GET: GLB_OPCIONES_SISTEMA
        static string idMenu;
        public ActionResult Index(string id)
        {
            if (id != null)
            {
                ViewBag.idseleccinado = id;
            }
            return View();
        }

        ControlPlanilla.Models.PlanillaAccesoMenus db = new ControlPlanilla.Models.PlanillaAccesoMenus();

        public JsonResult ObtenerMenus(string CD_CODI_OPCI_SIST)
        {
            var listaEmployee = db.GLB_OPCIONES_SISTEMA.Where(ep => ep.CD_CODI_OPCI_SIST== CD_CODI_OPCI_SIST).
                Select(r => new  GLB_OPCIONES_SISTEMAView
                {
                    CD_CODI_OPCI_SIST = r.CD_CODI_OPCI_SIST,
                    DS_CODI_OPCI_SIST = r.DS_CODI_OPCI_SIST,
                    DS_NOMBRE_FORMA = r.DS_NOMBRE_FORMA,
                    DS_LINK_URL = r.DS_LINK_URL,
                    CD_ESTADO_OPCION_SISTEMA = r.CD_ESTADO_OPCION_SISTEMA,
                }).ToList();
            var listFinal = listaEmployee;
            return Json(listFinal, JsonRequestBehavior.AllowGet);
        }

        [ValidateInput(false)]
        public ActionResult BuscarID(string key)
        {
            idMenu = key;
            return RedirectToAction("Index", new { id = key });
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult Create_Menu(string CD_CODI_OPCI_SIST, string DS_CODI_OPCI_SIST, string DS_NOMBRE_FORMA, string DS_LINK_URL, string CD_ESTADO_OPCION_SISTEMA)
        {
            if (CD_CODI_OPCI_SIST != "")
            {
                GLB_OPCIONES_SISTEMA grabar;
                //validar si existe
                var existincia = (from e in db.GLB_OPCIONES_SISTEMA
                                  where e.CD_CODI_OPCI_SIST == CD_CODI_OPCI_SIST
                                  select e.CD_CODI_OPCI_SIST).Count();

                if (existincia > 0)
                {
                    grabar = db.GLB_OPCIONES_SISTEMA.Find(CD_CODI_OPCI_SIST);
                }
                else
                {
                    grabar = new GLB_OPCIONES_SISTEMA();
                }

                grabar.CD_CODI_OPCI_SIST = CD_CODI_OPCI_SIST;
                grabar.DS_CODI_OPCI_SIST = DS_CODI_OPCI_SIST;
                grabar.DS_NOMBRE_FORMA = DS_NOMBRE_FORMA;
                grabar.DS_LINK_URL = DS_LINK_URL;
                grabar.CD_EMPRESA = CdEmpresa;
                grabar.CD_INDI_REFRE_CAMP = "N";
                grabar.CD_INDI_NECE_EMPR= "S";
                grabar.CD_INDI_NECE_PERI_CONT= "N";
                grabar.CD_INDI_VALI_PERI_CONT= "N";
                
                grabar.CD_ESTADO_OPCION_SISTEMA = Convert.ToBoolean(CD_ESTADO_OPCION_SISTEMA);
                grabar.FECH_CREA = Ahora;
                grabar.USUA_CREA = Usuario;

                if (existincia > 0)
                {
                    db.Entry(grabar).State = EntityState.Modified;
                }
                else
                {
                    db.GLB_OPCIONES_SISTEMA.Add(grabar);
                }
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View();
        }

        

        [ValidateInput(false)]
        public ActionResult GridViewPartial()
        {
            var model = db.GLB_OPCIONES_SISTEMA;
            return PartialView("_GridViewPartial", model.ToList());
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult GridViewPartialAddNew([ModelBinder(typeof(DevExpressEditorsBinder))] ControlPlanilla.Models.GLB_OPCIONES_SISTEMA item)
        {
            var model = db.GLB_OPCIONES_SISTEMA;
            if (ModelState.IsValid)
            {
                try
                {
                    model.Add(item);
                    db.SaveChanges();
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
                ViewData["EditError"] = "Please, correct all errors.";
            return PartialView("_GridViewPartial", model.ToList());
        }
        [HttpPost, ValidateInput(false)]
        public ActionResult GridViewPartialUpdate([ModelBinder(typeof(DevExpressEditorsBinder))] ControlPlanilla.Models.GLB_OPCIONES_SISTEMA item)
        {
            var model = db.GLB_OPCIONES_SISTEMA;
            if (ModelState.IsValid)
            {
                try
                {
                    var modelItem = model.FirstOrDefault(it => it.CD_CODI_OPCI_SIST == item.CD_CODI_OPCI_SIST);
                    if (modelItem != null)
                    {
                        this.UpdateModel(modelItem);
                        db.SaveChanges();
                    }
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
                ViewData["EditError"] = "Please, correct all errors.";
            return PartialView("_GridViewPartial", model.ToList());
        }
        [HttpPost, ValidateInput(false)]
        public ActionResult GridViewPartialDelete(System.String CD_CODI_OPCI_SIST)
        {
            var model = db.GLB_OPCIONES_SISTEMA;
            if (CD_CODI_OPCI_SIST != null)
            {
                try
                {
                    var item = model.FirstOrDefault(it => it.CD_CODI_OPCI_SIST == CD_CODI_OPCI_SIST);
                    if (item != null)
                        model.Remove(item);
                    db.SaveChanges();
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            return PartialView("_GridViewPartial", model.ToList());
        }
    }
}