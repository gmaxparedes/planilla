﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ControlPlanilla.Models;

namespace ControlPlanilla.Controllers
{
    public class VendorPartnerTypesController : AuditoriaController
    {
        

        // GET: VendorPartnerTypes
        public ActionResult Index()
        {
            return View(dbVendor.VendorPartnerType.ToList());
        }

        // GET: VendorPartnerTypes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VendorPartnerType vendorPartnerType = dbVendor.VendorPartnerType.Find(id);
            if (vendorPartnerType == null)
            {
                return HttpNotFound();
            }
            return View(vendorPartnerType);
        }

        // GET: VendorPartnerTypes/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: VendorPartnerTypes/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "DESCRIPCION")] VendorPartnerType vendorPartnerType)
        {
            if (ModelState.IsValid)
            {
                VendorPartnerType guardar = new VendorPartnerType();
                guardar.DESCRIPCION = vendorPartnerType.DESCRIPCION;
                guardar.USUA_CREA = Usuario;
                guardar.FECH_CREA = Ahora;

                dbVendor.VendorPartnerType.Add(guardar);
                dbVendor.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(vendorPartnerType);
        }

        // GET: VendorPartnerTypes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VendorPartnerType vendorPartnerType = dbVendor.VendorPartnerType.Find(id);
            if (vendorPartnerType == null)
            {
                return HttpNotFound();
            }
            return View(vendorPartnerType);
        }

        // POST: VendorPartnerTypes/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID_PARTNERTYPE,DESCRIPCION")] VendorPartnerType vendorPartnerType)
        {
            if (ModelState.IsValid)
            {
                VendorPartnerType guardar = dbVendor.VendorPartnerType.Find(vendorPartnerType.ID_PARTNERTYPE);
                guardar.DESCRIPCION = vendorPartnerType.DESCRIPCION;
                guardar.USUA_ACTUA = Usuario;
                guardar.FECH_ACTUA = Ahora;
                dbVendor.Entry(guardar).State = EntityState.Modified;
                dbVendor.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(vendorPartnerType);
        }

        // GET: VendorPartnerTypes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VendorPartnerType vendorPartnerType = dbVendor.VendorPartnerType.Find(id);
            if (vendorPartnerType == null)
            {
                return HttpNotFound();
            }
            return View(vendorPartnerType);
        }

        // POST: VendorPartnerTypes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            VendorPartnerType vendorPartnerType = dbVendor.VendorPartnerType.Find(id);
            dbVendor.VendorPartnerType.Remove(vendorPartnerType);
            dbVendor.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                dbVendor.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
