﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ControlPlanilla.Models;

namespace ControlPlanilla.Controllers
{
    public class PRESTACIONESController : AuditoriaController
    {
        private GlobalEntities db = new GlobalEntities();

        // GET: PRESTACIONES
        public ActionResult Index()
        {
            return View(db.PRESTACIONES.ToList());
        }

        // GET: PRESTACIONES/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PRESTACIONES pRESTACIONES = db.PRESTACIONES.Find(id);
            if (pRESTACIONES == null)
            {
                return HttpNotFound();
            }
            return View(pRESTACIONES);
        }

        // GET: PRESTACIONES/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: PRESTACIONES/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID_DVC,NOMBRE_PRESTACION")] PRESTACIONES pRESTACIONES)
        {
            if (ModelState.IsValid)
            {
                pRESTACIONES.FECH_CREA = Ahora;
                pRESTACIONES.USUA_CREA = Usuario;
                db.PRESTACIONES.Add(pRESTACIONES);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(pRESTACIONES);
        }

        // GET: PRESTACIONES/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PRESTACIONES pRESTACIONES = db.PRESTACIONES.Find(id);
            if (pRESTACIONES == null)
            {
                return HttpNotFound();
            }
            return View(pRESTACIONES);
        }

        // POST: PRESTACIONES/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID_DVC,NOMBRE_PRESTACION")] PRESTACIONES pRESTACIONES)
        {
            if (ModelState.IsValid)
            {
                PRESTACIONES grabar = db.PRESTACIONES.Find(pRESTACIONES.ID_DVC);
                grabar.NOMBRE_PRESTACION = pRESTACIONES.NOMBRE_PRESTACION;
                grabar.FECH_ACTU = Ahora;
                grabar.USUA_ACTU = Usuario;
                db.Entry(grabar).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(pRESTACIONES);
        }

        // GET: PRESTACIONES/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PRESTACIONES pRESTACIONES = db.PRESTACIONES.Find(id);
            if (pRESTACIONES == null)
            {
                return HttpNotFound();
            }
            return View(pRESTACIONES);
        }

        // POST: PRESTACIONES/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            PRESTACIONES pRESTACIONES = db.PRESTACIONES.Find(id);
            db.PRESTACIONES.Remove(pRESTACIONES);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
