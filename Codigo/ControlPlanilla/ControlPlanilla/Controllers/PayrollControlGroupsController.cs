﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ControlPlanilla.Models;
using PagedList;

namespace ControlPlanilla.Controllers
{
    public class PayrollControlGroupsController : AuditoriaController
    {
        private PlanillaPayControls db = new PlanillaPayControls();

        // GET: PayrollControlGroups
        public ActionResult Index()
        {
            return View(db.PayrollControlGroup.ToList());
        }

        // GET: PayrollControlGroups/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PayrollControlGroup payrollControlGroup = db.PayrollControlGroup.Find(id);
            if (payrollControlGroup == null)
            {
                return HttpNotFound();
            }
            return View(payrollControlGroup);
        }

        // GET: PayrollControlGroups/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: PayrollControlGroups/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Code,Name,PayControls,IsGroupUsed,PayControlFilter")] PayrollControlGroup payrollControlGroup)
        {
            if (ModelState.IsValid)
            {
                db.PayrollControlGroup.Add(payrollControlGroup);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(payrollControlGroup);
        }

        // GET: PayrollControlGroups/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PayrollControlGroup payrollControlGroup = db.PayrollControlGroup.Find(id);
            if (payrollControlGroup == null)
            {
                return HttpNotFound();
            }
            return View(payrollControlGroup);
        }

        // POST: PayrollControlGroups/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Code,Name,PayControls,IsGroupUsed,PayControlFilter")] PayrollControlGroup payrollControlGroup)
        {
            if (ModelState.IsValid)
            {
                db.Entry(payrollControlGroup).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(payrollControlGroup);
        }

        // GET: PayrollControlGroups/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PayrollControlGroup payrollControlGroup = db.PayrollControlGroup.Find(id);
            if (payrollControlGroup == null)
            {
                return HttpNotFound();
            }
            return View(payrollControlGroup);
        }

        // POST: PayrollControlGroups/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            PayrollControlGroup payrollControlGroup = db.PayrollControlGroup.Find(id);
            db.PayrollControlGroup.Remove(payrollControlGroup);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        #region Detalle grupos
        [AllowAnonymous]
        public static SelectList crearSelect(SelectList items)
        {
            List<AgregarSeleccioneOpcion> cmb = new List<AgregarSeleccioneOpcion>();
            cmb.Add(new AgregarSeleccioneOpcion());
            foreach (var i in items)
            {
                var ag = new AgregarSeleccioneOpcion();
                ag.Value = i.Value;
                ag.Text = i.Text;
                cmb.Add(ag);
            }


            return new SelectList(cmb, "Value", "Text"); ;
        }
        [HttpPost]
        public PartialViewResult GroupControls(string Codigo, string Nombre, int? page)
        {
            var payrollControlGroupControl = db.PayrollControlGroupControl.Where(p=> p.PayrollControlGroupCode==Codigo)
                .Include(p => p.PayrollControlMonthlySchedule)
                .Include(p => p.PayrollPostingGroup1)
                .Include(p => p.PayrollControlGroup).ToList();
            int pageNumber = (page ?? 1);
            ViewBag.Codigo = Codigo;
            ViewBag.Nombre = Nombre;
            
            //var pe = new SelectList(db.PayrollControlMonthlySchedule, "ID_CALENDARIO", "DESCRIPCION");
            //List<SelectListItem> listP = new SelectList(" ", "").ToList();
            //foreach (var item in pe)
            //{
            //    listP.Add(item);
            //}
            //var opc = new SelectList(db.PayrollPostingGroup, "Code", "EarningsAccount");
            //List<SelectListItem> list = new SelectList(" ", "").ToList();
            //foreach (var item in opc)
            //{
            //    list.Add(item);
            //}
            ViewBag.MonthlySchedule = crearSelect(new SelectList(db.PayrollControlMonthlySchedule, "ID_CALENDARIO", "DESCRIPCION"));
            ViewBag.PayrollPostingGroup = crearSelect(new SelectList(db.PayrollPostingGroup, "Code", "EarningsAccount")); ;
            ViewBag.ControlCode = crearSelect(new SelectList(db.PayrollControl, "Code", "Name"));
            ViewBag.NameControl = "";//db.PayrollControl.FirstOrDefault().Name;
            //ViewBag.PayrollControlGroupCode = new SelectList(db.PayrollControlGroup, "Code", "Name");
            var result = payrollControlGroupControl.ToPagedList(pageNumber, pageSize);
            return PartialView(result);
        }

        [HttpPost]
        public bool CrearGroup([Bind(Include = "PayrollControlGroupCode,PayControlCode,PayControlName,PayrollControlGroupName,MonthlySchedule,PayrollPostingGroup")] PayrollControlGroupControl payrollControlGroupControl)
        {
            bool result = false;
            if (ModelState.IsValid)
            {
                db.PayrollControlGroupControl.Add(payrollControlGroupControl);
                db.SaveChanges();
                result = true;
            }

            return result;
        }
        [HttpPost]
        public bool EditarGroup([Bind(Include = "PayrollControlGroupCode,PayControlCode,PayControlName,PayrollControlGroupName,MonthlySchedule,PayrollPostingGroup")] PayrollControlGroupControl payrollControlGroupControl)
        {
            bool result = false;
            if (ModelState.IsValid)
            {
                db.Entry(payrollControlGroupControl).State = EntityState.Modified;
                db.SaveChanges();
                result = true;
            }
            return result;
        }
        [HttpPost]
        public bool EliminarGroup(string GroupCode, string ControlCode)
        {
            bool result = false;
            PayrollControlGroupControl payrollControlGroupControl = db.PayrollControlGroupControl.
                Where(c=> c.PayrollControlGroupCode == GroupCode && c.PayControlCode == ControlCode).FirstOrDefault();
            
            if (payrollControlGroupControl != null)
            {
                db.PayrollControlGroupControl.Remove(payrollControlGroupControl);
                db.SaveChanges();
                result = true;
            }
            return result;
        }
        #endregion

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
