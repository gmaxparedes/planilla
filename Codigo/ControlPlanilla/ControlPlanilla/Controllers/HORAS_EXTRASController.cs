﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ControlPlanilla.Models;

namespace ControlPlanilla.Controllers
{
    public class HORAS_EXTRASController : AuditoriaController
    {
        private GlobalEntities db = new GlobalEntities();

        // GET: HORAS_EXTRAS
        public ActionResult Index()
        {
            return View(db.HORAS_EXTRAS.ToList());
        }

        // GET: HORAS_EXTRAS/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            HORAS_EXTRAS hORAS_EXTRAS = db.HORAS_EXTRAS.Find(id);
            if (hORAS_EXTRAS == null)
            {
                return HttpNotFound();
            }
            return View(hORAS_EXTRAS);
        }

        // GET: HORAS_EXTRAS/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: HORAS_EXTRAS/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "NOMBRE_PRESTACION")] HORAS_EXTRAS hORAS_EXTRAS)
        {
            if (ModelState.IsValid)
            {
                hORAS_EXTRAS.FECH_CREA = Ahora;
                hORAS_EXTRAS.USUA_CREA = Usuario;
                db.HORAS_EXTRAS.Add(hORAS_EXTRAS);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(hORAS_EXTRAS);
        }

        // GET: HORAS_EXTRAS/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            HORAS_EXTRAS hORAS_EXTRAS = db.HORAS_EXTRAS.Find(id);
            if (hORAS_EXTRAS == null)
            {
                return HttpNotFound();
            }
            return View(hORAS_EXTRAS);
        }

        // POST: HORAS_EXTRAS/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID_HORASEXTRA,NOMBRE_PRESTACION")] HORAS_EXTRAS hORAS_EXTRAS)
        {
            if (ModelState.IsValid)
            {
                HORAS_EXTRAS grabar = db.HORAS_EXTRAS.Find(hORAS_EXTRAS.ID_HORASEXTRA);
                grabar.NOMBRE_PRESTACION = hORAS_EXTRAS.NOMBRE_PRESTACION;
                grabar.FECH_ACTU = Ahora;
                grabar.USUA_ACTU = Usuario;
                db.Entry(grabar).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(hORAS_EXTRAS);
        }

        // GET: HORAS_EXTRAS/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            HORAS_EXTRAS hORAS_EXTRAS = db.HORAS_EXTRAS.Find(id);
            if (hORAS_EXTRAS == null)
            {
                return HttpNotFound();
            }
            return View(hORAS_EXTRAS);
        }

        // POST: HORAS_EXTRAS/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            HORAS_EXTRAS hORAS_EXTRAS = db.HORAS_EXTRAS.Find(id);
            db.HORAS_EXTRAS.Remove(hORAS_EXTRAS);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
