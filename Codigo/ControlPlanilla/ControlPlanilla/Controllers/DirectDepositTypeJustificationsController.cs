﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ControlPlanilla.Models;

namespace ControlPlanilla.Controllers
{
    public class DirectDepositTypeJustificationsController : AuditoriaController
    {

        // GET: DirectDepositTypeJustifications
        public ActionResult Index()
        {
            return View(dbDDL.DirectDepositTypeJustification.ToList());
        }

        // GET: DirectDepositTypeJustifications/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DirectDepositTypeJustification directDepositTypeJustification = dbDDL.DirectDepositTypeJustification.Find(id);
            if (directDepositTypeJustification == null)
            {
                return HttpNotFound();
            }
            return View(directDepositTypeJustification);
        }

        // GET: DirectDepositTypeJustifications/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: DirectDepositTypeJustifications/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "DESCRIPCION")] DirectDepositTypeJustification directDepositTypeJustification)
        {
            if (ModelState.IsValid)
            {
                directDepositTypeJustification.USUA_CREA = Usuario;
                directDepositTypeJustification.FECH_CREA = Ahora;

                dbDDL.DirectDepositTypeJustification.Add(directDepositTypeJustification);
                dbDDL.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(directDepositTypeJustification);
        }

        // GET: DirectDepositTypeJustifications/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DirectDepositTypeJustification directDepositTypeJustification = dbDDL.DirectDepositTypeJustification.Find(id);
            if (directDepositTypeJustification == null)
            {
                return HttpNotFound();
            }
            return View(directDepositTypeJustification);
        }

        // POST: DirectDepositTypeJustifications/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID_JUSTIFICATION,DESCRIPCION")] DirectDepositTypeJustification directDepositTypeJustification)
        {
            if (ModelState.IsValid)
            {
                var guardar = dbDDL.DirectDepositTypeJustification.Find(directDepositTypeJustification.ID_JUSTIFICATION);

                guardar.DESCRIPCION = directDepositTypeJustification.DESCRIPCION;
                guardar.USUA_ACTUA= Usuario;
                guardar.FECH_ACTUA = Ahora;
                dbDDL.Entry(guardar).State = EntityState.Modified;
                dbDDL.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(directDepositTypeJustification);
        }

        // GET: DirectDepositTypeJustifications/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DirectDepositTypeJustification directDepositTypeJustification = dbDDL.DirectDepositTypeJustification.Find(id);
            if (directDepositTypeJustification == null)
            {
                return HttpNotFound();
            }
            return View(directDepositTypeJustification);
        }

        // POST: DirectDepositTypeJustifications/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            DirectDepositTypeJustification directDepositTypeJustification = dbDDL.DirectDepositTypeJustification.Find(id);
            dbDDL.DirectDepositTypeJustification.Remove(directDepositTypeJustification);
            dbDDL.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                dbDDL.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
