﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ControlPlanilla.Models;

namespace ControlPlanilla.Controllers
{
    public class MOTIVO_TERMINACIONController : AuditoriaController
    {
        private GlobalEntities db = new GlobalEntities();

        // GET: MOTIVO_TERMINACION
        public ActionResult Index()
        {
            return View(db.MOTIVO_TERMINACION.ToList());
        }

        // GET: MOTIVO_TERMINACION/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MOTIVO_TERMINACION mOTIVO_TERMINACION = db.MOTIVO_TERMINACION.Find(id);
            if (mOTIVO_TERMINACION == null)
            {
                return HttpNotFound();
            }
            return View(mOTIVO_TERMINACION);
        }

        // GET: MOTIVO_TERMINACION/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: MOTIVO_TERMINACION/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID_MOV_TERMIN,NOMBRE_PRESTACION")] MOTIVO_TERMINACION mOTIVO_TERMINACION)
        {
            if (ModelState.IsValid)
            {
                mOTIVO_TERMINACION.USUA_CREA = Usuario;
                mOTIVO_TERMINACION.FECH_CREA = Ahora;
                db.MOTIVO_TERMINACION.Add(mOTIVO_TERMINACION);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(mOTIVO_TERMINACION);
        }

        // GET: MOTIVO_TERMINACION/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MOTIVO_TERMINACION mOTIVO_TERMINACION = db.MOTIVO_TERMINACION.Find(id);
            if (mOTIVO_TERMINACION == null)
            {
                return HttpNotFound();
            }
            return View(mOTIVO_TERMINACION);
        }

        // POST: MOTIVO_TERMINACION/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID_MOV_TERMIN,NOMBRE_PRESTACION")] MOTIVO_TERMINACION mOTIVO_TERMINACION)
        {
            if (ModelState.IsValid)
            {
                MOTIVO_TERMINACION grabar = db.MOTIVO_TERMINACION.Find(mOTIVO_TERMINACION.ID_MOV_TERMIN);
                grabar.NOMBRE_PRESTACION = mOTIVO_TERMINACION.NOMBRE_PRESTACION;
                grabar.USUA_ACTU = Usuario;
                grabar.FECH_ACTU = Ahora;
                db.Entry(grabar).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(mOTIVO_TERMINACION);
        }

        // GET: MOTIVO_TERMINACION/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MOTIVO_TERMINACION mOTIVO_TERMINACION = db.MOTIVO_TERMINACION.Find(id);
            if (mOTIVO_TERMINACION == null)
            {
                return HttpNotFound();
            }
            return View(mOTIVO_TERMINACION);
        }

        // POST: MOTIVO_TERMINACION/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            MOTIVO_TERMINACION mOTIVO_TERMINACION = db.MOTIVO_TERMINACION.Find(id);
            db.MOTIVO_TERMINACION.Remove(mOTIVO_TERMINACION);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
