﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ControlPlanilla.Models;

namespace ControlPlanilla.Controllers
{
    public class DEPARTAMENTOSController : AuditoriaController
    {
        //private GlobalEntities db = new GlobalEntities();

        // GET: DEPARTAMENTOS
        public ActionResult Index()
        {
            var dEPARTAMENTOS = dbMenus.DEPARTAMENTOS.Include(d => d.PAISES);
            return View(dEPARTAMENTOS.ToList());
        }

        // GET: DEPARTAMENTOS/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DEPARTAMENTOS dEPARTAMENTOS = dbMenus.DEPARTAMENTOS.Find(id);
            if (dEPARTAMENTOS == null)
            {
                return HttpNotFound();
            }
            return View(dEPARTAMENTOS);
        }

        // GET: DEPARTAMENTOS/Create
        public ActionResult Create()
        {
            ViewBag.CD_PAIS = new SelectList(dbMenus.PAISES, "CD_PAIS", "DS_NOMBREPAIS");
            return View();
        }

        // POST: DEPARTAMENTOS/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "CD_PAIS,NOMBRE_DEPTO,CODIGO_DEPTO")] DEPARTAMENTOS dEPARTAMENTOS)
        {
            if (ModelState.IsValid)
            {
                dEPARTAMENTOS.USUA_CREA = Usuario;
                dEPARTAMENTOS.FECH_CREA = Ahora;
                dbMenus.DEPARTAMENTOS.Add(dEPARTAMENTOS);

                dbMenus.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.CD_PAIS = new SelectList(dbMenus.PAISES, "CD_PAIS", "DS_NOMBREPAIS", dEPARTAMENTOS.CD_PAIS);
            return View(dEPARTAMENTOS);
        }

        // GET: DEPARTAMENTOS/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DEPARTAMENTOS dEPARTAMENTOS = dbMenus.DEPARTAMENTOS.Find(id);
            if (dEPARTAMENTOS == null)
            {
                return HttpNotFound();
            }
            ViewBag.CD_PAIS = new SelectList(dbMenus.PAISES, "CD_PAIS", "DS_NOMBREPAIS", dEPARTAMENTOS.CD_PAIS);
            return View(dEPARTAMENTOS);
        }

        // POST: DEPARTAMENTOS/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID_DEPTO,CD_PAIS,NOMBRE_DEPTO,CODIGO_DEPTO")] DEPARTAMENTOS dEPARTAMENTOS)
        {
            if (ModelState.IsValid)
            {
                DEPARTAMENTOS guardar = dbMenus.DEPARTAMENTOS.Find(dEPARTAMENTOS.ID_DEPTO);
                guardar.ID_DEPTO = dEPARTAMENTOS.ID_DEPTO;
                guardar.USUA_ACTU = Usuario;
                guardar.FECH_ACTU = Ahora;
                dbMenus.Entry(guardar).State = EntityState.Modified;
                dbMenus.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CD_PAIS = new SelectList(dbMenus.PAISES, "CD_PAIS", "DS_NOMBREPAIS", dEPARTAMENTOS.CD_PAIS);
            return View(dEPARTAMENTOS);
        }

        // GET: DEPARTAMENTOS/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DEPARTAMENTOS dEPARTAMENTOS = dbMenus.DEPARTAMENTOS.Find(id);
            if (dEPARTAMENTOS == null)
            {
                return HttpNotFound();
            }
            return View(dEPARTAMENTOS);
        }

        // POST: DEPARTAMENTOS/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            DEPARTAMENTOS dEPARTAMENTOS = dbMenus.DEPARTAMENTOS.Find(id);
            dbMenus.DEPARTAMENTOS.Remove(dEPARTAMENTOS);
            dbMenus.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                dbMenus.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
