﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ControlPlanilla.Models;

namespace ControlPlanilla.Controllers
{
    public class AFPsController : AuditoriaController
    {
        private GlobalEntities db = new GlobalEntities();

        // GET: AFPs
        public ActionResult Index()
        {
            return View(db.AFP.ToList());
        }

        // GET: AFPs/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AFP aFP = db.AFP.Find(id);
            if (aFP == null)
            {
                return HttpNotFound();
            }
            return View(aFP);
        }

        // GET: AFPs/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: AFPs/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "NOMBRE_PRESTACION")] AFP aFP)
        {
            if (ModelState.IsValid)
            {
                aFP.USUA_CREA = Usuario;
                aFP.FECH_CREA = Ahora;
                db.AFP.Add(aFP);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(aFP);
        }

        // GET: AFPs/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AFP aFP = db.AFP.Find(id);
            if (aFP == null)
            {
                return HttpNotFound();
            }
            return View(aFP);
        }

        // POST: AFPs/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID_AFP,NOMBRE_PRESTACION")] AFP aFP)
        {
            if (ModelState.IsValid)
            {
                AFP grabar = db.AFP.Find(aFP.ID_AFP);
                grabar.NOMBRE_PRESTACION = aFP.NOMBRE_PRESTACION;
                grabar.FECH_ACTU = Ahora;
                grabar.USUA_CREA = Usuario;
                db.Entry(grabar).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(aFP);
        }

        // GET: AFPs/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AFP aFP = db.AFP.Find(id);
            if (aFP == null)
            {
                return HttpNotFound();
            }
            return View(aFP);
        }

        // POST: AFPs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            AFP aFP = db.AFP.Find(id);
            db.AFP.Remove(aFP);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
