﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ControlPlanilla.Models;

namespace ControlPlanilla.Controllers
{
    public class CONTRATOesController : AuditoriaController
    {
        private GlobalEntities db = new GlobalEntities();

        // GET: CONTRATOes
        public ActionResult Index()
        {
            return View(db.CONTRATO.ToList());
        }

        // GET: CONTRATOes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CONTRATO cONTRATO = db.CONTRATO.Find(id);
            if (cONTRATO == null)
            {
                return HttpNotFound();
            }
            return View(cONTRATO);
        }

        // GET: CONTRATOes/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: CONTRATOes/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "NOMBRE_PRESTACION")] CONTRATO cONTRATO)
        {
            if (ModelState.IsValid)
            {
                cONTRATO.FECH_CREA = Ahora;
                cONTRATO.USUA_CREA = Usuario;
                db.CONTRATO.Add(cONTRATO);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(cONTRATO);
        }

        // GET: CONTRATOes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CONTRATO cONTRATO = db.CONTRATO.Find(id);
            if (cONTRATO == null)
            {
                return HttpNotFound();
            }
            return View(cONTRATO);
        }

        // POST: CONTRATOes/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID_CONTRATO,NOMBRE_PRESTACION")] CONTRATO cONTRATO)
        {
            if (ModelState.IsValid)
            {
                CONTRATO grabar = db.CONTRATO.Find(cONTRATO.ID_CONTRATO);
                grabar.NOMBRE_PRESTACION = cONTRATO.NOMBRE_PRESTACION;
                grabar.FECH_ACTU = Ahora;
                grabar.USUA_ACTU = Usuario;
                db.Entry(grabar).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(cONTRATO);
        }

        // GET: CONTRATOes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CONTRATO cONTRATO = db.CONTRATO.Find(id);
            if (cONTRATO == null)
            {
                return HttpNotFound();
            }
            return View(cONTRATO);
        }

        // POST: CONTRATOes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            CONTRATO cONTRATO = db.CONTRATO.Find(id);
            db.CONTRATO.Remove(cONTRATO);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
