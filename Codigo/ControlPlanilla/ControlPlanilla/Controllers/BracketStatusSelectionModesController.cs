﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ControlPlanilla.Models;

namespace ControlPlanilla.Controllers
{
    public class BracketStatusSelectionModesController : AuditoriaController
    {
        private PlanillaPayControls db = new PlanillaPayControls();

        // GET: BracketStatusSelectionModes
        public ActionResult Index()
        {
            return View(db.BracketStatusSelectionMode.ToList());
        }

        // GET: BracketStatusSelectionModes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BracketStatusSelectionMode bracketStatusSelectionMode = db.BracketStatusSelectionMode.Find(id);
            if (bracketStatusSelectionMode == null)
            {
                return HttpNotFound();
            }
            return View(bracketStatusSelectionMode);
        }

        // GET: BracketStatusSelectionModes/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: BracketStatusSelectionModes/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "DESCRIPCION")] BracketStatusSelectionMode bracketStatusSelectionMode)
        {
            if (ModelState.IsValid)
            {
                bracketStatusSelectionMode.USUA_CREA = Usuario;
                bracketStatusSelectionMode.FECH_CREA = Ahora;
                db.BracketStatusSelectionMode.Add(bracketStatusSelectionMode);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(bracketStatusSelectionMode);
        }

        // GET: BracketStatusSelectionModes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BracketStatusSelectionMode bracketStatusSelectionMode = db.BracketStatusSelectionMode.Find(id);
            if (bracketStatusSelectionMode == null)
            {
                return HttpNotFound();
            }

            return View(bracketStatusSelectionMode);
        }

        // POST: BracketStatusSelectionModes/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "CODE,DESCRIPCION")] BracketStatusSelectionMode bracketStatusSelectionMode)
        {
            if (ModelState.IsValid)
            {
                BracketStatusSelectionMode grabar = db.BracketStatusSelectionMode.Find(bracketStatusSelectionMode.CODE);
                grabar.DESCRIPCION = bracketStatusSelectionMode.DESCRIPCION;
                grabar.FECH_ACTUA = Ahora;
                grabar.USUA_ACTUA = Usuario;
                db.Entry(grabar).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(bracketStatusSelectionMode);
        }

        // GET: BracketStatusSelectionModes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BracketStatusSelectionMode bracketStatusSelectionMode = db.BracketStatusSelectionMode.Find(id);
            if (bracketStatusSelectionMode == null)
            {
                return HttpNotFound();
            }
            return View(bracketStatusSelectionMode);
        }

        // POST: BracketStatusSelectionModes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            BracketStatusSelectionMode bracketStatusSelectionMode = db.BracketStatusSelectionMode.Find(id);
            db.BracketStatusSelectionMode.Remove(bracketStatusSelectionMode);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
