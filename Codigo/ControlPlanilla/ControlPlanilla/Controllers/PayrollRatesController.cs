﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ControlPlanilla.Models;

namespace ControlPlanilla.Controllers
{
    public class PayrollRatesController : AuditoriaController
    {
        

        // GET: PayrollRates
        public ActionResult Index()
        {
            return View(dbPayControl.PayrollRate.ToList());
        }

        // GET: PayrollRates/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PayrollRate payrollRate = dbPayControl.PayrollRate.Find(id);
            if (payrollRate == null)
            {
                return HttpNotFound();
            }
            return View(payrollRate);
        }

        // GET: PayrollRates/Create
        public ActionResult Create()
        {
            var rate = new PayrollRate();
            ViewBag.EditStatus = new SelectList(dbStatusRate.PayRateEditStatus, "ID_EDIT", "DESCRIPCION");
            //rate.PayRateEditStatus = dbTarifas.PayRateEditStatus.ToList();
            return View(rate);
        }

        // POST: PayrollRates/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Code,Description,EditStatus,UnitofMeasureCode,BorrarPosteo")] PayrollRate payrollRate)
        {
            if (ModelState.IsValid)
            {
                dbPayControl.PayrollRate.Add(payrollRate);
                dbCiclo.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.EditStatus = new SelectList(dbStatusRate.PayRateEditStatus, "ID_EDIT", "DESCRIPCION");
            return View(payrollRate);
        }

        // GET: PayrollRates/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PayrollRate payrollRate = dbPayControl.PayrollRate.Find(id);
            if (payrollRate == null)
            {
                return HttpNotFound();
            }
            ViewBag.EditStatus = new SelectList(dbStatusRate.PayRateEditStatus, "ID_EDIT", "DESCRIPCION");
            return View(payrollRate);
        }

        // POST: PayrollRates/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Code,Description,EditStatus,UnitofMeasureCode,BorrarPosteo")] PayrollRate payrollRate)
        {
            if (ModelState.IsValid)
            {
                dbCiclo.Entry(payrollRate).State = EntityState.Modified;
                dbCiclo.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.EditStatus = new SelectList(dbStatusRate.PayRateEditStatus, "ID_EDIT", "DESCRIPCION");
            return View(payrollRate);
        }

        // GET: PayrollRates/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PayrollRate payrollRate = dbPayControl.PayrollRate.Find(id);
            if (payrollRate == null)
            {
                return HttpNotFound();
            }
            ViewBag.EditStatus = dbStatusRate.PayRateEditStatus.ToList();
            return View(payrollRate);
        }

        // POST: PayrollRates/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            PayrollRate payrollRate = dbPayControl.PayrollRate.Find(id);
            dbPayControl.PayrollRate.Remove(payrollRate);
            dbCiclo.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                dbCiclo.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
