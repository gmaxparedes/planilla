﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ControlPlanilla.Models;

namespace ControlPlanilla.Controllers
{
    public class ESTADOS_CIVILESController :AuditoriaController
    {
        private GlobalEntities db = new GlobalEntities();

        // GET: ESTADOS_CIVILES
        public ActionResult Index()
        {
            return View(db.ESTADOS_CIVILES.ToList());
        }

        // GET: ESTADOS_CIVILES/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ESTADOS_CIVILES eSTADOS_CIVILES = db.ESTADOS_CIVILES.Find(id);
            if (eSTADOS_CIVILES == null)
            {
                return HttpNotFound();
            }
            return View(eSTADOS_CIVILES);
        }

        // GET: ESTADOS_CIVILES/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: ESTADOS_CIVILES/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "DESCRIPCION")] ESTADOS_CIVILES eSTADOS_CIVILES)
        {
            if (ModelState.IsValid)
            {
                eSTADOS_CIVILES.FECH_CREA = Ahora;
                eSTADOS_CIVILES.USUA_CREA = Usuario;
                db.ESTADOS_CIVILES.Add(eSTADOS_CIVILES);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(eSTADOS_CIVILES);
        }

        // GET: ESTADOS_CIVILES/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ESTADOS_CIVILES eSTADOS_CIVILES = db.ESTADOS_CIVILES.Find(id);
            if (eSTADOS_CIVILES == null)
            {
                return HttpNotFound();
            }
            return View(eSTADOS_CIVILES);
        }

        // POST: ESTADOS_CIVILES/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID_ESTADO_CIVIL,DESCRIPCION")] ESTADOS_CIVILES eSTADOS_CIVILES)
        {
            if (ModelState.IsValid)
            {
                ESTADOS_CIVILES grabar = db.ESTADOS_CIVILES.Find(eSTADOS_CIVILES.ID_ESTADO_CIVIL);
                grabar.DESCRIPCION = eSTADOS_CIVILES.DESCRIPCION;
                grabar.USUA_ACTU = Usuario;
                grabar.FECH_ACTU = Ahora;

                db.Entry(grabar).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(eSTADOS_CIVILES);
        }

        // GET: ESTADOS_CIVILES/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ESTADOS_CIVILES eSTADOS_CIVILES = db.ESTADOS_CIVILES.Find(id);
            if (eSTADOS_CIVILES == null)
            {
                return HttpNotFound();
            }
            return View(eSTADOS_CIVILES);
        }

        // POST: ESTADOS_CIVILES/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ESTADOS_CIVILES eSTADOS_CIVILES = db.ESTADOS_CIVILES.Find(id);
            db.ESTADOS_CIVILES.Remove(eSTADOS_CIVILES);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
