﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ControlPlanilla.Models;
using PagedList;

namespace ControlPlanilla.Controllers
{
    public class FormatoPlanillasController : AuditoriaController
    {
        private PlanillaPayControls db = new PlanillaPayControls();

        // GET: FormatoPlanillas
        public ActionResult Index()
        {
            return View(db.FormatoPlanilla.ToList());
        }

        // GET: FormatoPlanillas/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FormatoPlanilla formatoPlanilla = db.FormatoPlanilla.Find(id);
            if (formatoPlanilla == null)
            {
                return HttpNotFound();
            }
            return View(formatoPlanilla);
        }

        // GET: FormatoPlanillas/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: FormatoPlanillas/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID_FORMATO,DESCRIPCION")] FormatoPlanilla formatoPlanilla)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    FormatoPlanilla guardar = new FormatoPlanilla();
                    guardar.ID_FORMATO = formatoPlanilla.ID_FORMATO;
                    guardar.DESCRIPCION = formatoPlanilla.DESCRIPCION;
                    guardar.USUA_CREA = Usuario;
                    guardar.FECH_CREA = Ahora;
                    db.FormatoPlanilla.Add(guardar);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            catch (SystemException ex)
            {
                logRepository.CapturarTodoError(ex);
            }
            return View(formatoPlanilla);
        }

        // GET: FormatoPlanillas/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FormatoPlanilla formatoPlanilla = db.FormatoPlanilla.Find(id);
            if (formatoPlanilla == null)
            {
                return HttpNotFound();
            }
            return View(formatoPlanilla);
        }

        // POST: FormatoPlanillas/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID_FORMATO,DESCRIPCION")] FormatoPlanilla formatoPlanilla)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var guardar = db.FormatoPlanilla.Find(formatoPlanilla.ID_FORMATO);
                    if (guardar != null)
                    {
                        guardar.DESCRIPCION = formatoPlanilla.DESCRIPCION;
                        guardar.USUA_ACTUA = Usuario;
                        guardar.FECH_ACTUA = Ahora;
                        db.Entry(guardar).State = EntityState.Modified;
                        db.SaveChanges();
                        return RedirectToAction("Index");
                    }
                    
                }
            }
            catch (SystemException ex)
            {
                logRepository.CapturarTodoError(ex);
            }
            
            return View(formatoPlanilla);
        }

        // GET: FormatoPlanillas/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FormatoPlanilla formatoPlanilla = db.FormatoPlanilla.Find(id);
            if (formatoPlanilla == null)
            {
                return HttpNotFound();
            }
            return View(formatoPlanilla);
        }

        // POST: FormatoPlanillas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            FormatoPlanilla formatoPlanilla = db.FormatoPlanilla.Find(id);
            db.FormatoPlanilla.Remove(formatoPlanilla);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        #region Ingresos
        [HttpPost]
        public PartialViewResult Ingresos(string IdFormato, int? page)
        {
            try
            {
                var lista = db.FormatoPlanillaEncabezadoIngresos.Where(c => c.ID_FORMATO == IdFormato).ToList();

                int numberpage = (page ?? 1);
                ViewBag.IdFormato = IdFormato;

                var result = lista.ToPagedList(numberpage, pageSize);
                return PartialView(result);
            }
            catch (SystemException ex)
            {
                logRepository.CapturarTodoError(ex);
            }
            return PartialView();
        }

        [HttpPost]
        public bool CrearEncabezadoIngresos([Bind(Include ="ID_CABECERA, ID_FORMATO,CHECK")]FormatoPlanillaEncabezadoIngresos encabezadoIngresos)
        {
            bool result = false;
            try
            {
                if (ModelState.IsValid)
                {
                    var guardar = new FormatoPlanillaEncabezadoIngresos();
                    guardar.ID_CABECERA = encabezadoIngresos.ID_CABECERA;
                    guardar.ID_FORMATO = encabezadoIngresos.ID_FORMATO;
                    guardar.CHECK = encabezadoIngresos.CHECK;
                    guardar.USUA_CREA = Usuario;
                    guardar.FECH_CREA = Ahora;

                    db.FormatoPlanillaEncabezadoIngresos.Add(guardar);
                    db.SaveChanges();
                    result = true;
                }
            }
            catch (SystemException ex)
            {
                logRepository.CapturarTodoError(ex);
            }
            return result;
        }
        [HttpPost]
        public bool EditarEncabezadoIngresos([Bind(Include = "ID_CABECERA, ID_FORMATO,CHECK")]FormatoPlanillaEncabezadoIngresos encabezadoIngresos)
        {
            bool result = false;
            try
            {
                if (ModelState.IsValid)
                {
                    var guardar = db.FormatoPlanillaEncabezadoIngresos.Find(encabezadoIngresos.ID_CABECERA);
                    if (guardar != null){
                        guardar.ID_CABECERA = encabezadoIngresos.ID_CABECERA;
                        guardar.CHECK = encabezadoIngresos.CHECK;
                        guardar.USUA_CREA = Usuario;
                        guardar.FECH_CREA = Ahora;

                        db.Entry(guardar).State = EntityState.Modified;
                        db.SaveChanges();
                        result = true;
                    }
                }
            }
            catch (SystemException ex)
            {
                logRepository.CapturarTodoError(ex);
            }
            return result;
        }
        [HttpPost]
        public bool EliminarEncabezadoIngresos([Bind(Include = "ID_CABECERA, ID_FORMATO")]FormatoPlanillaEncabezadoIngresos encabezadoIngresos)
        {
            bool result = false;
            try
            {
                if (ModelState.IsValid)
                {
                    var remove = db.FormatoPlanillaEncabezadoIngresos.Find(encabezadoIngresos.ID_CABECERA);
                    if (remove != null)
                    {

                        db.FormatoPlanillaEncabezadoIngresos.Remove(remove);
                        db.SaveChanges();
                        result = true;
                    }
                }
            }
            catch (SystemException ex)
            {
                logRepository.CapturarTodoError(ex);
            }
            return result;
        }

        [HttpPost]
        public PartialViewResult DetallesIngresos(string Id_Formato, string Id_Cabecera, int? page)
        {
            ViewBag.PayrollControl = new SelectList(db.PayrollControl, "Code", "Name");
            try
            {
                var lista = db.FormatoPlanillaDetalleIngresos.Where(c => 
                c.ID_FORMATO == Id_Formato && c.ID_CABECERA == Id_Cabecera).ToList();

                int numberpage = (page ?? 1);

                var result = lista.ToPagedList(numberpage, pageSize);
                ViewBag.IdFormato = Id_Formato;
                ViewBag.IdCabecera = Id_Cabecera;

                return PartialView(result);
            }
            catch (SystemException ex)
            {
                logRepository.CapturarTodoError(ex);
            }
            return PartialView();
        }
        [HttpPost]
        public bool CrearDetalleIngresos([Bind(Include = "ID_CABECERA, ID_FORMATO, PayControlCode")]FormatoPlanillaDetalleIngresos detalleIngresos)
        {
            bool result = false;
            try
            {
                if (ModelState.IsValid)
                {
                    var guardar = new FormatoPlanillaDetalleIngresos();
                    guardar.ID_CABECERA = detalleIngresos.ID_CABECERA;
                    guardar.ID_FORMATO = detalleIngresos.ID_FORMATO;
                    guardar.PayControlCode = detalleIngresos.PayControlCode;
                    guardar.USUA_CREA = Usuario;
                    guardar.FECH_CREA = Ahora;

                    db.Entry(guardar).State = EntityState.Added;
                    db.SaveChanges();
                    result = true;
                }
            }
            catch (SystemException ex)
            {
                logRepository.CapturarTodoError(ex);
            }
            return result;
        }
        [HttpPost]
        public bool EditarDetalleIngresos([Bind(Include = "ID, ID_CABECERA, ID_FORMATO, PayControlCode")]FormatoPlanillaDetalleIngresos detalleIngresos)
        {
            bool result = false;
            try
            {
                if (ModelState.IsValid)
                {
                    var guardar = db.FormatoPlanillaDetalleIngresos.Where(c => c.ID_CABECERA == detalleIngresos.ID_CABECERA
                    && c.ID_FORMATO == detalleIngresos.ID_FORMATO && c.ID == detalleIngresos.ID).FirstOrDefault();
                    if (guardar != null)
                    {
                        guardar.PayControlCode = detalleIngresos.PayControlCode;
                        guardar.USUA_ACTUA = Usuario;
                        guardar.FECH_ACTUA= Ahora;
                        db.Entry(guardar).State = EntityState.Modified;
                        db.SaveChanges();
                        result = true;
                    }
                    
                }
            }
            catch (SystemException ex)
            {
                logRepository.CapturarTodoError(ex);
            }
            return result;
        }
        [HttpPost]
        public bool EliminarDetalleIngresos([Bind(Include = "ID, ID_CABECERA, ID_FORMATO, PayControlCode")]FormatoPlanillaDetalleIngresos detalleIngresos)
        {
            bool result = false;
            try
            {
                if (ModelState.IsValid)
                {
                    var remove = db.FormatoPlanillaDetalleIngresos.Where(c => c.ID_CABECERA == detalleIngresos.ID_CABECERA
                    && c.ID_FORMATO == detalleIngresos.ID_FORMATO && c.ID == detalleIngresos.ID).FirstOrDefault();
                    if (remove != null)
                    {
                        db.FormatoPlanillaDetalleIngresos.Remove(remove);
                        db.SaveChanges();
                        result = true;
                    }

                }
            }
            catch (SystemException ex)
            {
                logRepository.CapturarTodoError(ex);
            }
            return result;
        }
        #endregion



        #region Egresos
        [HttpPost]
        public PartialViewResult Egresos(string IdFormato, int? page)
        {
            try
            {
                var lista = db.FormatoPlanillaEncabezadoEgresos.Where(c => c.ID_FORMATO == IdFormato).ToList();

                int numberpage = (page ?? 1);
                ViewBag.IdFormato = IdFormato;

                var result = lista.ToPagedList(numberpage, pageSize);
                return PartialView(result);
            }
            catch (SystemException ex)
            {
                logRepository.CapturarTodoError(ex);
            }
            return PartialView();
        }

        [HttpPost]
        public bool CrearEncabezadoEgresos([Bind(Include = "ID_CABECERA, ID_FORMATO,CHECK")]FormatoPlanillaEncabezadoEgresos encabezadoEngresos)
        {
            bool result = false;
            try
            {
                if (ModelState.IsValid)
                {
                    var guardar = new FormatoPlanillaEncabezadoEgresos();
                    guardar.ID_CABECERA = encabezadoEngresos.ID_CABECERA;
                    guardar.ID_FORMATO = encabezadoEngresos.ID_FORMATO;
                    guardar.CHECK = encabezadoEngresos.CHECK;
                    guardar.USUA_CREA = Usuario;
                    guardar.FECH_CREA = Ahora;

                    db.FormatoPlanillaEncabezadoEgresos.Add(guardar);
                    db.SaveChanges();
                    result = true;
                }
            }
            catch (SystemException ex)
            {
                logRepository.CapturarTodoError(ex);
            }
            return result;
        }
        [HttpPost]
        public bool EditarEncabezadoEgresos([Bind(Include = "ID_CABECERA, ID_FORMATO,CHECK")]FormatoPlanillaEncabezadoEgresos encabezadoEngresos)
        {
            bool result = false;
            try
            {
                if (ModelState.IsValid)
                {
                    var guardar = db.FormatoPlanillaEncabezadoEgresos.Find(encabezadoEngresos.ID_CABECERA);
                    if (guardar != null)
                    {
                        guardar.ID_CABECERA = encabezadoEngresos.ID_CABECERA;
                        guardar.CHECK = encabezadoEngresos.CHECK;
                        guardar.USUA_CREA = Usuario;
                        guardar.FECH_CREA = Ahora;

                        db.Entry(guardar).State = EntityState.Modified;
                        db.SaveChanges();
                        result = true;
                    }
                }
            }
            catch (SystemException ex)
            {
                logRepository.CapturarTodoError(ex);
            }
            return result;
        }
        [HttpPost]
        public bool EliminarEncabezadoEgresos([Bind(Include = "ID_CABECERA, ID_FORMATO")]FormatoPlanillaEncabezadoEgresos encabezadoEngresos)
        {
            bool result = false;
            try
            {
                if (ModelState.IsValid)
                {
                    var remove = db.FormatoPlanillaEncabezadoEgresos.Find(encabezadoEngresos.ID_CABECERA);
                    if (remove != null)
                    {

                        db.FormatoPlanillaEncabezadoEgresos.Remove(remove);
                        db.SaveChanges();
                        result = true;
                    }
                }
            }
            catch (SystemException ex)
            {
                logRepository.CapturarTodoError(ex);
            }
            return result;
        }

        [HttpPost]
        public PartialViewResult DetallesEgresos(string Id_Formato, string Id_Cabecera, int? page)
        {
            ViewBag.PayrollControl = new SelectList(db.PayrollControl, "Code", "Name");
            try
            {
                var lista = db.FormatoPlanillaDetalleEgresos.Where(c =>
                c.ID_FORMATO == Id_Formato && c.ID_CABECERA == Id_Cabecera).ToList();

                int numberpage = (page ?? 1);

                var result = lista.ToPagedList(numberpage, pageSize);
                ViewBag.IdFormato = Id_Formato;
                ViewBag.IdCabecera = Id_Cabecera;

                return PartialView(result);
            }
            catch (SystemException ex)
            {
                logRepository.CapturarTodoError(ex);
            }
            return PartialView();
        }
        [HttpPost]
        public bool CrearDetalleEgresos([Bind(Include = "ID_CABECERA, ID_FORMATO, PayControlCode")]FormatoPlanillaDetalleEgresos detalleEgresos)
        {
            bool result = false;
            try
            {
                if (ModelState.IsValid)
                {
                    var guardar = new FormatoPlanillaDetalleEgresos();
                    guardar.ID_CABECERA = detalleEgresos.ID_CABECERA;
                    guardar.ID_FORMATO = detalleEgresos.ID_FORMATO;
                    guardar.PayControlCode = detalleEgresos.PayControlCode;
                    guardar.USUA_CREA = Usuario;
                    guardar.FECH_CREA = Ahora;

                    db.Entry(guardar).State = EntityState.Added;
                    db.SaveChanges();
                    result = true;
                }
            }
            catch (SystemException ex)
            {
                logRepository.CapturarTodoError(ex);
            }
            return result;
        }
        [HttpPost]
        public bool EditarDetalleEgresos([Bind(Include = "ID, ID_CABECERA, ID_FORMATO, PayControlCode")]FormatoPlanillaDetalleEgresos detalleEgresos)
        {
            bool result = false;
            try
            {
                if (ModelState.IsValid)
                {
                    var guardar = db.FormatoPlanillaDetalleEgresos.Where(c => c.ID_CABECERA == detalleEgresos.ID_CABECERA
                    && c.ID_FORMATO == detalleEgresos.ID_FORMATO && c.ID == detalleEgresos.ID).FirstOrDefault();
                    if (guardar != null)
                    {
                        guardar.PayControlCode = detalleEgresos.PayControlCode;
                        guardar.USUA_ACTUA = Usuario;
                        guardar.FECH_ACTUA = Ahora;
                        db.Entry(guardar).State = EntityState.Modified;
                        db.SaveChanges();
                        result = true;
                    }

                }
            }
            catch (SystemException ex)
            {
                logRepository.CapturarTodoError(ex);
            }
            return result;
        }
        [HttpPost]
        public bool EliminarDetalleEgresos([Bind(Include = "ID, ID_CABECERA, ID_FORMATO, PayControlCode")]FormatoPlanillaDetalleEgresos detalleEgresos)
        {
            bool result = false;
            try
            {
                if (ModelState.IsValid)
                {
                    var remove = db.FormatoPlanillaDetalleEgresos.Where(c => c.ID_CABECERA == detalleEgresos.ID_CABECERA
                    && c.ID_FORMATO == detalleEgresos.ID_FORMATO && c.ID == detalleEgresos.ID).FirstOrDefault();
                    if (remove != null)
                    {
                        db.FormatoPlanillaDetalleEgresos.Remove(remove);
                        db.SaveChanges();
                        result = true;
                    }

                }
            }
            catch (SystemException ex)
            {
                logRepository.CapturarTodoError(ex);
            }
            return result;
        }
        #endregion
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
