﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ControlPlanilla.Models;

namespace ControlPlanilla.Controllers
{
    public class MethodStepsController : Controller
    {
        private PlanillaPayControls db = new PlanillaPayControls();
        private PlanillaEntities db1 = new PlanillaEntities();

        // GET: MethodSteps
        public ActionResult Index()
        {
            var methodStep = db.MethodStep;
            return View(methodStep.ToList());
        }

        // GET: MethodSteps/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MethodStep methodStep = db.MethodStep.Find(id);
            if (methodStep == null)
            {
                return HttpNotFound();
            }
            return View(methodStep);
        }

        // GET: MethodSteps/Create
        public ActionResult Create()
        {
            ViewBag.BracketTypeCode = new SelectList(db.BracketType, "Code", "Description");
            ViewBag.BracketStatusSelectionMode = new SelectList(db.BracketStatusSelectionMode, "CODE", "DESCRIPCION");
            ViewBag.StepClassCode = new SelectList(db1.Method_Step_Class, "Code", "Description");
            return View();
        }

        // POST: MethodSteps/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Code,Description,FunctionNo,UsesBaseAmount,BracketTypeCode,UsesPayrollRate,RequiresEmpAuthInfo,BracketStatusSelectionMode,UsesActivationCode,RateUsedType,UsesPayStructure,StepClassCode")] MethodStep methodStep)
        {
            if (ModelState.IsValid)
            {
                db.MethodStep.Add(methodStep);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.BracketTypeCode = new SelectList(db.BracketType, "Code", "Description", methodStep.BracketTypeCode);
            ViewBag.BracketStatusSelectionMode = new SelectList(db.BracketStatusSelectionMode, "CODE", "DESCRIPCION", methodStep.BracketStatusSelectionMode);
            ViewBag.StepClassCode = new SelectList(db1.Method_Step_Class, "Code", "Description", methodStep.StepClassCode);
            return View(methodStep);
        }

        // GET: MethodSteps/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MethodStep methodStep = db.MethodStep.Find(id);
            if (methodStep == null)
            {
                return HttpNotFound();
            }
            ViewBag.BracketTypeCode = new SelectList(db.BracketType, "Code", "Description", methodStep.BracketTypeCode);
            ViewBag.BracketStatusSelectionMode = new SelectList(db.BracketStatusSelectionMode, "CODE", "DESCRIPCION", methodStep.BracketStatusSelectionMode);
            ViewBag.StepClassCode = new SelectList(db1.Method_Step_Class, "Code", "Description", methodStep.StepClassCode);
            return View(methodStep);
        }

        // POST: MethodSteps/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Code,Description,FunctionNo,UsesBaseAmount,BracketTypeCode,UsesPayrollRate,RequiresEmpAuthInfo,BracketStatusSelectionMode,UsesActivationCode,RateUsedType,UsesPayStructure,StepClassCode")] MethodStep methodStep)
        {
            if (ModelState.IsValid)
            {
                db.Entry(methodStep).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.BracketTypeCode = new SelectList(db.BracketType, "Code", "Description", methodStep.BracketTypeCode);
            ViewBag.BracketStatusSelectionMode = new SelectList(db.BracketStatusSelectionMode, "CODE", "DESCRIPCION", methodStep.BracketStatusSelectionMode);
            ViewBag.StepClassCode = new SelectList(db1.Method_Step_Class, "Code", "Description", methodStep.StepClassCode);
            return View(methodStep);
        }

        // GET: MethodSteps/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MethodStep methodStep = db.MethodStep.Find(id);
            if (methodStep == null)
            {
                return HttpNotFound();
            }
            return View(methodStep);
        }

        // POST: MethodSteps/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            MethodStep methodStep = db.MethodStep.Find(id);
            db.MethodStep.Remove(methodStep);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
