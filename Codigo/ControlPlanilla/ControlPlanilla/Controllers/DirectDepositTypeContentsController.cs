﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ControlPlanilla.Models;

namespace ControlPlanilla.Controllers
{
    public class DirectDepositTypeContentsController : AuditoriaController
    {
        

        // GET: DirectDepositTypeContents
        public ActionResult Index()
        {
            return View(dbDDL.DirectDepositTypeContent.ToList());
        }

        // GET: DirectDepositTypeContents/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DirectDepositTypeContent directDepositTypeContent = dbDDL.DirectDepositTypeContent.Find(id);
            if (directDepositTypeContent == null)
            {
                return HttpNotFound();
            }
            return View(directDepositTypeContent);
        }

        // GET: DirectDepositTypeContents/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: DirectDepositTypeContents/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID_CONTENT,DESCRIPCION")] DirectDepositTypeContent directDepositTypeContent)
        {
            if (ModelState.IsValid)
            {
                directDepositTypeContent.USUA_CREA = Usuario;
                directDepositTypeContent.FECH_CREA = Ahora;

                dbDDL.DirectDepositTypeContent.Add(directDepositTypeContent);
                dbDDL.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(directDepositTypeContent);
        }

        // GET: DirectDepositTypeContents/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DirectDepositTypeContent directDepositTypeContent = dbDDL.DirectDepositTypeContent.Find(id);
            if (directDepositTypeContent == null)
            {
                return HttpNotFound();
            }
            return View(directDepositTypeContent);
        }

        // POST: DirectDepositTypeContents/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID_CONTENT,DESCRIPCION")] DirectDepositTypeContent directDepositTypeContent)
        {
            if (ModelState.IsValid)
            {
                var TypeContent = dbDDL.DirectDepositTypeContent.Find(directDepositTypeContent.ID_CONTENT);

                TypeContent.DESCRIPCION = directDepositTypeContent.DESCRIPCION;
                TypeContent.USUA_ACTUA = Usuario;
                TypeContent.FECH_ACTUA = Ahora;

                dbDDL.Entry(TypeContent).State = EntityState.Modified;
                dbDDL.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(directDepositTypeContent);
        }

        // GET: DirectDepositTypeContents/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DirectDepositTypeContent directDepositTypeContent = dbDDL.DirectDepositTypeContent.Find(id);
            if (directDepositTypeContent == null)
            {
                return HttpNotFound();
            }
            return View(directDepositTypeContent);
        }

        // POST: DirectDepositTypeContents/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            DirectDepositTypeContent directDepositTypeContent = dbDDL.DirectDepositTypeContent.Find(id);
            dbDDL.DirectDepositTypeContent.Remove(directDepositTypeContent);
            dbDDL.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                dbDDL.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
