﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ControlPlanilla.Models;
using PagedList;

namespace ControlPlanilla.Controllers
{
    public class PayrollPostingGroupsController : AuditoriaController
    {


        // GET: PayrollPostingGroups
        public ActionResult Index()
        {
            return View(dbPayControl.PayrollPostingGroup.ToList());
        }

        // GET: PayrollPostingGroups/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PayrollPostingGroup payrollPostingGroup = dbPayControl.PayrollPostingGroup.Find(id);
            if (payrollPostingGroup == null)
            {
                return HttpNotFound();
            }
            return View(payrollPostingGroup);
        }

        // GET: PayrollPostingGroups/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: PayrollPostingGroups/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Code")] PayrollPostingGroup payrollPostingGroup)
        {
            if (ModelState.IsValid)
            {
                dbPayControl.PayrollPostingGroup.Add(payrollPostingGroup);
                dbPayControl.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(payrollPostingGroup);
        }

        // GET: PayrollPostingGroups/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PayrollPostingGroup payrollPostingGroup = dbPayControl.PayrollPostingGroup.Find(id);
            if (payrollPostingGroup == null)
            {
                return HttpNotFound();
            }
            return View(payrollPostingGroup);
        }

        // POST: PayrollPostingGroups/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Code,EarningsAccount,BankAccountNo,LiabilityAccount,ExpenseAccount,DateFilter,DetailExists,CurrentEffectiveDate")] PayrollPostingGroup payrollPostingGroup)
        {
            if (ModelState.IsValid)
            {
                dbPayControl.Entry(payrollPostingGroup).State = EntityState.Modified;
                dbPayControl.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(payrollPostingGroup);
        }

        // GET: PayrollPostingGroups/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PayrollPostingGroup payrollPostingGroup = dbPayControl.PayrollPostingGroup.Find(id);
            if (payrollPostingGroup == null)
            {
                return HttpNotFound();
            }
            return View(payrollPostingGroup);
        }

        // POST: PayrollPostingGroups/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            PayrollPostingGroup payrollPostingGroup = dbPayControl.PayrollPostingGroup.Find(id);
            dbPayControl.PayrollPostingGroup.Remove(payrollPostingGroup);
            dbPayControl.SaveChanges();
            return RedirectToAction("Index");
        }

        #region GroupDetails
        [HttpPost]
        public PartialViewResult PostingDetails(string Code, int? page)
        {
            try
            {
                var detalles = dbPayControl.PayrollPostingGroupDetail.Where(c =>
                c.Code == Code).ToList();

                int pageNumber = (page ?? 1);
                var result = detalles.ToPagedList(pageNumber, pageSize);
                ViewBag.Code = Code;

                return PartialView(result);
            }
            catch (SystemException ex)
            {
                logRepository.CapturarTodoError(ex);
                return PartialView();
            }
        }

        [HttpPost]
        public bool CrearPostingDetail(PayrollPostingGroupDetail payrollPostingGroupDetail)
        {
            bool result = false;
            try
            {
                if (ModelState.IsValid)
                {
                    dbPayControl.PayrollPostingGroupDetail.Add(payrollPostingGroupDetail);
                    dbPayControl.SaveChanges();
                    result = true;
                    var editar = new PayrollPostingGroup();
                    editar.Code = payrollPostingGroupDetail.Code;
                    editar.EarningsAccount = payrollPostingGroupDetail.EarningsAccount;
                    editar.BankAccountNo = payrollPostingGroupDetail.BankAccountNo;
                    editar.LiabilityAccount = payrollPostingGroupDetail.LiabilityAccount;
                    editar.ExpenseAccount = payrollPostingGroupDetail.ExpenseAccount;
                    editar.CurrentEffectiveDate = payrollPostingGroupDetail.EffectiveDate;
                    ActualizarEncabezado(editar);
                }
            }
            catch (SystemException ex)
            {
                logRepository.CapturarTodoError(ex);
            }
            return result;
        }
        [HttpPost]
        public bool EditarPostingDetail(PayrollPostingGroupDetail payrollPostingGroupDetail)
        {
            bool result = false;
            try
            {
                if (ModelState.IsValid)
                {
                    dbPayControl.Entry(payrollPostingGroupDetail).State = EntityState.Modified;
                    dbPayControl.SaveChanges();
                    result = true;
                    var editar = new PayrollPostingGroup();
                    editar.Code = payrollPostingGroupDetail.Code;
                    editar.EarningsAccount = payrollPostingGroupDetail.EarningsAccount;
                    editar.BankAccountNo = payrollPostingGroupDetail.BankAccountNo;
                    editar.LiabilityAccount = payrollPostingGroupDetail.LiabilityAccount;
                    editar.ExpenseAccount = payrollPostingGroupDetail.ExpenseAccount;
                    editar.CurrentEffectiveDate = payrollPostingGroupDetail.EffectiveDate;
                    ActualizarEncabezado(editar);
                }
            }
            catch (SystemException ex)
            {
                logRepository.CapturarTodoError(ex);
            }
            return result;
        }
        [HttpPost]
        public bool EliminarPostingDetail([Bind(Include ="Code, EffectiveDate")] PayrollPostingGroupDetail payrollPostingGroupDetail)
        {
            bool result = false;
            try
            {
                PayrollPostingGroupDetail remove = dbPayControl.PayrollPostingGroupDetail.Where(c =>
                c.Code == payrollPostingGroupDetail.Code && c.EffectiveDate == payrollPostingGroupDetail.EffectiveDate).FirstOrDefault();
                if (remove!=null)
                {
                    dbPayControl.PayrollPostingGroupDetail.Remove(remove);
                    dbPayControl.SaveChanges();
                    result = true;
                }
            }
            catch (SystemException ex)
            {
                logRepository.CapturarTodoError(ex);
            }
            return result;
        }

        private void ActualizarEncabezado([Bind(Include = "Code, EarningsAccount, BankAccountNo, LiabilityAccount, ExpenseAccount, CurrentEffectiveDate")] PayrollPostingGroup payrollPostingGroup)
        {
            try
            {
                dbPayControl.Entry(payrollPostingGroup).State = EntityState.Modified;
                dbPayControl.SaveChanges();
            }
            catch (SystemException ex)
            {
                logRepository.CapturarTodoError(ex);
            }
        }

        #endregion

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                dbPayControl.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
