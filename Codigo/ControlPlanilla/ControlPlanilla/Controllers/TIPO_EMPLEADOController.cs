﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ControlPlanilla.Models;

namespace ControlPlanilla.Controllers
{
    public class TIPO_EMPLEADOController : AuditoriaController
    {
        private GlobalEntities db = new GlobalEntities();

        // GET: TIPO_EMPLEADO
        public ActionResult Index()
        {
            return View(db.TIPO_EMPLEADO.ToList());
        }

        // GET: TIPO_EMPLEADO/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TIPO_EMPLEADO tIPO_EMPLEADO = db.TIPO_EMPLEADO.Find(id);
            if (tIPO_EMPLEADO == null)
            {
                return HttpNotFound();
            }
            return View(tIPO_EMPLEADO);
        }

        // GET: TIPO_EMPLEADO/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: TIPO_EMPLEADO/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "NOMBRE_TIPO")] TIPO_EMPLEADO tIPO_EMPLEADO)
        {
            if (ModelState.IsValid)
            {
                tIPO_EMPLEADO.FECH_CREA = Ahora;
                tIPO_EMPLEADO.USUA_CREA = Usuario;
                db.TIPO_EMPLEADO.Add(tIPO_EMPLEADO);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(tIPO_EMPLEADO);
        }

        // GET: TIPO_EMPLEADO/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TIPO_EMPLEADO tIPO_EMPLEADO = db.TIPO_EMPLEADO.Find(id);
            if (tIPO_EMPLEADO == null)
            {
                return HttpNotFound();
            }
            return View(tIPO_EMPLEADO);
        }

        // POST: TIPO_EMPLEADO/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID_TIPO_EMPLEADO,NOMBRE_TIPO")] TIPO_EMPLEADO tIPO_EMPLEADO)
        {
            if (ModelState.IsValid)
            {
                TIPO_EMPLEADO grabar = db.TIPO_EMPLEADO.Find(tIPO_EMPLEADO.ID_TIPO_EMPLEADO);
                grabar.NOMBRE_TIPO = tIPO_EMPLEADO.NOMBRE_TIPO;
                grabar.FECH_ACTU = Ahora;
                grabar.USUA_ACTU = Usuario;
                db.Entry(grabar).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(tIPO_EMPLEADO);
        }

        // GET: TIPO_EMPLEADO/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TIPO_EMPLEADO tIPO_EMPLEADO = db.TIPO_EMPLEADO.Find(id);
            if (tIPO_EMPLEADO == null)
            {
                return HttpNotFound();
            }
            return View(tIPO_EMPLEADO);
        }

        // POST: TIPO_EMPLEADO/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            TIPO_EMPLEADO tIPO_EMPLEADO = db.TIPO_EMPLEADO.Find(id);
            db.TIPO_EMPLEADO.Remove(tIPO_EMPLEADO);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
