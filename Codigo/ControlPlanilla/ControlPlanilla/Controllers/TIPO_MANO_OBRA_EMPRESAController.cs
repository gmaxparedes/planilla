﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ControlPlanilla.Models;

namespace ControlPlanilla.Controllers
{
    public class TIPO_MANO_OBRA_EMPRESAController : AuditoriaController
    {
        //private GlobalEntities db = new GlobalEntities();

        // GET: TIPO_MANO_OBRA_EMPRESA
        public ActionResult Index()
        {
            var tIPO_MANO_OBRA_EMPRESA = dbMenus.TIPO_MANO_OBRA_EMPRESA.Include(t => t.EMPRESAS);
            return View(tIPO_MANO_OBRA_EMPRESA.ToList());
        }

        // GET: TIPO_MANO_OBRA_EMPRESA/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TIPO_MANO_OBRA_EMPRESA tIPO_MANO_OBRA_EMPRESA = dbMenus.TIPO_MANO_OBRA_EMPRESA.Find(id);
            if (tIPO_MANO_OBRA_EMPRESA == null)
            {
                return HttpNotFound();
            }
            return View(tIPO_MANO_OBRA_EMPRESA);
        }

        // GET: TIPO_MANO_OBRA_EMPRESA/Create
        public ActionResult Create()
        {
            ViewBag.CD_EMPRESA = new SelectList(dbMenus.EMPRESAS, "CD_EMPRESA", "DS_EMPRESA");
            return View();
        }

        // POST: TIPO_MANO_OBRA_EMPRESA/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "CD_EMPRESA,DESCRIPCION")] TIPO_MANO_OBRA_EMPRESA tIPO_MANO_OBRA_EMPRESA)
        {
            if (ModelState.IsValid)
            {
                tIPO_MANO_OBRA_EMPRESA.FECH_CREA = Ahora;
                tIPO_MANO_OBRA_EMPRESA.USUA_CREA = Usuario;
                dbMenus.TIPO_MANO_OBRA_EMPRESA.Add(tIPO_MANO_OBRA_EMPRESA);
                dbMenus.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.CD_EMPRESA = new SelectList(dbMenus.EMPRESAS, "CD_EMPRESA", "DS_EMPRESA", tIPO_MANO_OBRA_EMPRESA.CD_EMPRESA);
            return View(tIPO_MANO_OBRA_EMPRESA);
        }

        // GET: TIPO_MANO_OBRA_EMPRESA/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TIPO_MANO_OBRA_EMPRESA tIPO_MANO_OBRA_EMPRESA = dbMenus.TIPO_MANO_OBRA_EMPRESA.Find(id);
            if (tIPO_MANO_OBRA_EMPRESA == null)
            {
                return HttpNotFound();
            }
            ViewBag.CD_EMPRESA = new SelectList(dbMenus.EMPRESAS, "CD_EMPRESA", "DS_EMPRESA", tIPO_MANO_OBRA_EMPRESA.CD_EMPRESA);
            return View(tIPO_MANO_OBRA_EMPRESA);
        }

        // POST: TIPO_MANO_OBRA_EMPRESA/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID_MANO_OBRA,CD_EMPRESA,DESCRIPCION")] TIPO_MANO_OBRA_EMPRESA tIPO_MANO_OBRA_EMPRESA)
        {
            if (ModelState.IsValid)
            {
                TIPO_MANO_OBRA_EMPRESA grabar = dbMenus.TIPO_MANO_OBRA_EMPRESA.Find(tIPO_MANO_OBRA_EMPRESA.ID_MANO_OBRA);
                grabar.CD_EMPRESA = tIPO_MANO_OBRA_EMPRESA.CD_EMPRESA;
                grabar.DESCRIPCION = tIPO_MANO_OBRA_EMPRESA.DESCRIPCION;
                grabar.USUA_ACTU = Usuario;
                grabar.FECH_ACTU = Ahora;
                dbMenus.Entry(grabar).State = EntityState.Modified;
                dbMenus.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CD_EMPRESA = new SelectList(dbMenus.EMPRESAS, "CD_EMPRESA", "DS_EMPRESA", tIPO_MANO_OBRA_EMPRESA.CD_EMPRESA);
            return View(tIPO_MANO_OBRA_EMPRESA);
        }

        // GET: TIPO_MANO_OBRA_EMPRESA/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TIPO_MANO_OBRA_EMPRESA tIPO_MANO_OBRA_EMPRESA = dbMenus.TIPO_MANO_OBRA_EMPRESA.Find(id);
            if (tIPO_MANO_OBRA_EMPRESA == null)
            {
                return HttpNotFound();
            }
            return View(tIPO_MANO_OBRA_EMPRESA);
        }

        // POST: TIPO_MANO_OBRA_EMPRESA/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            TIPO_MANO_OBRA_EMPRESA tIPO_MANO_OBRA_EMPRESA = dbMenus.TIPO_MANO_OBRA_EMPRESA.Find(id);
            dbMenus.TIPO_MANO_OBRA_EMPRESA.Remove(tIPO_MANO_OBRA_EMPRESA);
            dbMenus.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                dbMenus.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
