﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ControlPlanilla.Models;

namespace ControlPlanilla.Controllers
{
    public class DISTRIBUCION_GASTOSController : AuditoriaController
    {
        private GlobalEntities db = new GlobalEntities();

        // GET: DISTRIBUCION_GASTOS
        public ActionResult Index()
        {
            return View(db.DISTRIBUCION_GASTOS.ToList());
        }

        // GET: DISTRIBUCION_GASTOS/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DISTRIBUCION_GASTOS dISTRIBUCION_GASTOS = db.DISTRIBUCION_GASTOS.Find(id);
            if (dISTRIBUCION_GASTOS == null)
            {
                return HttpNotFound();
            }
            return View(dISTRIBUCION_GASTOS);
        }

        // GET: DISTRIBUCION_GASTOS/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: DISTRIBUCION_GASTOS/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID_DISTRIBUCION,NOMBRE_PRESTACION")] DISTRIBUCION_GASTOS dISTRIBUCION_GASTOS)
        {
            if (ModelState.IsValid)
            {
                dISTRIBUCION_GASTOS.FECH_CREA = Ahora;
                dISTRIBUCION_GASTOS.USUA_CREA = Usuario;
                db.DISTRIBUCION_GASTOS.Add(dISTRIBUCION_GASTOS);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(dISTRIBUCION_GASTOS);
        }

        // GET: DISTRIBUCION_GASTOS/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DISTRIBUCION_GASTOS dISTRIBUCION_GASTOS = db.DISTRIBUCION_GASTOS.Find(id);
            if (dISTRIBUCION_GASTOS == null)
            {
                return HttpNotFound();
            }
            return View(dISTRIBUCION_GASTOS);
        }

        // POST: DISTRIBUCION_GASTOS/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID_DISTRIBUCION,NOMBRE_PRESTACION")] DISTRIBUCION_GASTOS dISTRIBUCION_GASTOS)
        {
            if (ModelState.IsValid)
            {
                DISTRIBUCION_GASTOS grabar = db.DISTRIBUCION_GASTOS.Find(dISTRIBUCION_GASTOS.ID_DISTRIBUCION);
                grabar.NOMBRE_PRESTACION = dISTRIBUCION_GASTOS.NOMBRE_PRESTACION;
                grabar.FECH_ACTU = Ahora;
                grabar.USUA_CREA = Usuario;
                db.Entry(grabar).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(dISTRIBUCION_GASTOS);
        }

        // GET: DISTRIBUCION_GASTOS/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DISTRIBUCION_GASTOS dISTRIBUCION_GASTOS = db.DISTRIBUCION_GASTOS.Find(id);
            if (dISTRIBUCION_GASTOS == null)
            {
                return HttpNotFound();
            }
            return View(dISTRIBUCION_GASTOS);
        }

        // POST: DISTRIBUCION_GASTOS/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            DISTRIBUCION_GASTOS dISTRIBUCION_GASTOS = db.DISTRIBUCION_GASTOS.Find(id);
            db.DISTRIBUCION_GASTOS.Remove(dISTRIBUCION_GASTOS);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
