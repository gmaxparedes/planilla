﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ControlPlanilla.Models;

namespace ControlPlanilla.Controllers
{
    public class DirectDepositLineTypesController : AuditoriaController
    {
        
        // GET: DirectDepositLineTypes
        public ActionResult Index()
        {
            return View(dbDDL.DirectDepositLineType.ToList());
        }

        // GET: DirectDepositLineTypes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DirectDepositLineType directDepositLineType = dbDDL.DirectDepositLineType.Find(id);
            if (directDepositLineType == null)
            {
                return HttpNotFound();
            }
            return View(directDepositLineType);
        }

        // GET: DirectDepositLineTypes/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: DirectDepositLineTypes/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "DESCRIPCION")] DirectDepositLineType directDepositLineType)
        {
            if (ModelState.IsValid)
            {
                DirectDepositLineType guardar = new DirectDepositLineType();

                guardar.DESCRIPCION = directDepositLineType.DESCRIPCION;
                guardar.USUA_CREA = Usuario;
                guardar.FECH_CREA = Ahora;
                dbDDL.DirectDepositLineType.Add(guardar);
                dbDDL.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(directDepositLineType);
        }

        // GET: DirectDepositLineTypes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DirectDepositLineType directDepositLineType = dbDDL.DirectDepositLineType.Find(id);
            if (directDepositLineType == null)
            {
                return HttpNotFound();
            }
            return View(directDepositLineType);
        }

        // POST: DirectDepositLineTypes/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID_LINE_TYPE, DESCRIPCION")] DirectDepositLineType directDepositLineType)
        {
            if (ModelState.IsValid)
            {
                DirectDepositLineType guardar = dbDDL.DirectDepositLineType.Find(directDepositLineType.ID_LINE_TYPE);

                guardar.DESCRIPCION = directDepositLineType.DESCRIPCION;
                guardar.USUA_ACTUA = Usuario;
                guardar.FECH_ACTUA = Ahora;
                dbDDL.Entry(guardar).State = EntityState.Modified;
                dbDDL.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(directDepositLineType);
        }

        // GET: DirectDepositLineTypes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DirectDepositLineType directDepositLineType = dbDDL.DirectDepositLineType.Find(id);
            if (directDepositLineType == null)
            {
                return HttpNotFound();
            }
            return View(directDepositLineType);
        }

        // POST: DirectDepositLineTypes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            DirectDepositLineType directDepositLineType = dbDDL.DirectDepositLineType.Find(id);
            dbDDL.DirectDepositLineType.Remove(directDepositLineType);
            dbDDL.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                dbDDL.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
