﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ControlPlanilla.Models;
using System.Data.Entity;

namespace ControlPlanilla.Controllers
{
    public class GlbOpciSistPerfilController : AuditoriaController
    {
        // GET: GlbOpciSistPerfil
        public ActionResult Index()
        {
            var model = from p in db.GLB_OPCI_SIST_PERFIL
                        join s in db.GLB_SISTEMAS on p.CD_CODI_SIST equals s.CD_CODI_SIST
                        join m in db.GLB_MENUS_SISTEMAS on p.CD_CODI_MENU equals m.CD_CODI_MENU
                        join o in db.GLB_OPCIONES_SISTEMA on p.CD_CODI_OPCI_SIST equals o.CD_CODI_OPCI_SIST
                        where p.Estado == true && s.CD_ESTADO_SISTEMA == true
                              && m.CD_ESTADO_MENU == true
                              && o.CD_ESTADO_OPCION_SISTEMA == true
                        select new { Perfil = p.CD_CODIGO_PERFIL, CD_CODI_SIST = p.CD_CODI_SIST, CD_CODI_MENU = p.CD_CODI_MENU, CD_CODI_OPCI_SIST = p.CD_CODI_OPCI_SIST, Sistema = s.DS_NOMB_SIST, Menu = m.DS_DESC_MENU, Forma = o.DS_NOMBRE_FORMA, Estado = p.Estado };
            return View(model.ToList());
        }
        ControlPlanilla.Models.PlanillaAccesoMenus db = new ControlPlanilla.Models.PlanillaAccesoMenus();
        [ValidateInput(false)]
        public ActionResult GridViewPartial()
        {
                        var model = from p in db.GLB_OPCI_SIST_PERFIL
                               join s in db.GLB_SISTEMAS on p.CD_CODI_SIST equals s.CD_CODI_SIST
                        join m in db.GLB_MENUS_SISTEMAS on p.CD_CODI_MENU equals m.CD_CODI_MENU
                        join o in db.GLB_OPCIONES_SISTEMA on p.CD_CODI_OPCI_SIST equals o.CD_CODI_OPCI_SIST
                        where p.Estado == true && s.CD_ESTADO_SISTEMA == true
                              && m.CD_ESTADO_MENU == true
                              && o.CD_ESTADO_OPCION_SISTEMA == true
                        select new { Perfil = p.CD_CODIGO_PERFIL , CD_CODI_SIST=p.CD_CODI_SIST, CD_CODI_MENU=p.CD_CODI_MENU, CD_CODI_OPCI_SIST=p.CD_CODI_OPCI_SIST, Sistema=s.DS_NOMB_SIST , Menu=m.DS_DESC_MENU , Forma=o.DS_NOMBRE_FORMA , Estado=p.Estado };
            return PartialView("_GridViewPartial", model.ToList());
        }

        [ValidateInput(false)]
        public ActionResult grdDisponiblesPartial()
        {
            var model = from p in db.GLB_CONF_OPCI_SIST
                        join s in db.GLB_SISTEMAS on p.CD_CODI_SIST equals s.CD_CODI_SIST
                        join m in db.GLB_MENUS_SISTEMAS on p.CD_CODI_MENU equals m.CD_CODI_MENU
                        join o in db.GLB_OPCIONES_SISTEMA on p.CD_CODI_OPCI_SIST equals o.CD_CODI_OPCI_SIST
                        where p.Estado == true && s.CD_ESTADO_SISTEMA == true
                              && m.CD_ESTADO_MENU == true
                              && o.CD_ESTADO_OPCION_SISTEMA == true
                        select new {  CD_CODI_SIST = p.CD_CODI_SIST, CD_CODI_MENU = p.CD_CODI_MENU, CD_CODI_OPCI_SIST = p.CD_CODI_OPCI_SIST, DS_NOMB_SIST = s.DS_NOMB_SIST, DS_DESC_MENU = m.DS_DESC_MENU, DS_NOMBRE_FORMA = o.DS_NOMBRE_FORMA };
            return PartialView("_grdDisponiblesPartial", model.ToList());
        }


        [HttpPost]
        [AllowAnonymous]
        public ActionResult Create_Menu(string CD_CODI_SIST, string CD_CODI_MENU, string CD_CODI_OPCI_SIST, string CD_CODIGO_PERFIL)
        {
            if (CD_CODI_SIST != "")
            {
                GLB_OPCI_SIST_PERFIL grabar;
                //validar si existe
                var existincia = (from e in db.GLB_OPCI_SIST_PERFIL
                                  where e.CD_CODI_SIST == CD_CODI_SIST && e.CD_CODIGO_PERFIL== CD_CODIGO_PERFIL && e.CD_CODI_MENU== CD_CODI_MENU && e.CD_CODI_OPCI_SIST== CD_CODI_OPCI_SIST
                                  select e.CD_CODI_SIST).Count();

                if (existincia > 0)
                {
                    grabar = db.GLB_OPCI_SIST_PERFIL.Find(CD_CODI_SIST, CD_CODIGO_PERFIL, CD_CODI_MENU, CD_CODI_OPCI_SIST);
                }
                else
                {
                    grabar = new GLB_OPCI_SIST_PERFIL();
                }

                grabar.CD_CODIGO_PERFIL = CD_CODIGO_PERFIL;
                grabar.CD_CODI_SIST = CD_CODI_SIST;
                grabar.CD_CODI_MENU = CD_CODI_MENU;
                grabar.CD_CODI_OPCI_SIST = CD_CODI_OPCI_SIST;
                grabar.CD_EMPRESA = CdEmpresa;
                grabar.Estado = Convert.ToBoolean("true");
                grabar.FECH_CREA = Ahora;
                grabar.USUA_CREA = Usuario;

                if (existincia > 0)
                {
                    db.Entry(grabar).State = EntityState.Modified;
                }
                else
                {
                    db.GLB_OPCI_SIST_PERFIL.Add(grabar);
                }
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult Delete_Menu(string CD_CODI_SIST, string CD_CODI_MENU, string CD_CODI_OPCI_SIST, string CD_CODIGO_PERFIL)
        {
            if (CD_CODI_SIST != "")
            {
                GLB_OPCI_SIST_PERFIL borrar = db.GLB_OPCI_SIST_PERFIL.Find(CD_CODIGO_PERFIL, CD_CODI_SIST, CD_CODI_MENU,CD_CODI_OPCI_SIST );
                db.GLB_OPCI_SIST_PERFIL.Remove(borrar);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View();
        }



    }
}