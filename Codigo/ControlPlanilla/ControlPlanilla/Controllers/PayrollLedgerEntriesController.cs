﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ControlPlanilla.Models;

namespace ControlPlanilla.Controllers
{
    public class PayrollLedgerEntriesController : AuditoriaController
    {
        private PlanillaCicloPago db = new PlanillaCicloPago();
        private EmployeeEntities dbe = new EmployeeEntities();

        // GET: PayrollLedgerEntries
        public ActionResult Index()
        {
            return View(db.PayrollLedgerEntry.ToList());
        }

        // GET: PayrollLedgerEntries/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PayrollLedgerEntry payrollLedgerEntry = db.PayrollLedgerEntry.Find(id);
            if (payrollLedgerEntry == null)
            {
                return HttpNotFound();
            }
            return View(payrollLedgerEntry);
        }

        // GET: PayrollLedgerEntries/Create
        public ActionResult Create()
        {
            ViewBag.PayrollControlCode = new SelectList(ObtenerPaycontrol(), "Code", "Name", "0");
            ViewBag.PayrollControlType = new SelectList(ObtenerPaycontrolType(), "ID_TYPE", "DESCRIPCION", 0);
            ViewBag.EmployerNo = new SelectList(ObtenerEmployer(), "No", "Name", 0);
            ViewBag.EmployeeNo = new SelectList(ObtenerEmployee(), "No", "FirstName", 0);
            ViewBag.GLPostType = new SelectList(ObtenerPayrollPostType(), "ID_POST", "DESCRIPCION", 0);
            return View();
        }

        // POST: PayrollLedgerEntries/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "EntryNo,EmployeeNo,PostingDate,DocumentType,DocumentNo,Description,RegularPay,Amount,TaxableAmount,AmountonPayCheck,PayrollPostingGroup,GlobalDimension1Code,GlobalDimension2Code,ResourceNo,UserID,SourceCode,PayrollControlCode,PayrollControlType,PayrollControlName,County,Locality,PayPeriodEndDate,GLPostType,VendorLedgerEntryNo,VendorLedgerPosting,JobNo,JobTaskNo,BurdenAllocated,Chargeable,WorkTypeCode,JournalBatchName,ReasonCode,CheckNo,PayDate,ReportingAuthorityType,ReportingAuthorityCode,EntryDate,DateWorked,PayCycleCode,PayCycleTerm,PayCyclePeriod,PayPeriodStartDate,EarnedPayCycleCode,EarnedPayCycleTerm,EarnedPayCyclePeriod,EarnedPeriodStartDate,EarnedPeriodEndDate,WorkReportingAuthorityCode,LedgerPostingDate,InsurableHours,TaxTypeCode,EmployerNo,SeasonalCode,SeasonalWages,DimensionSetID,calcReport")] PayrollLedgerEntry payrollLedgerEntry)
        {
            if (ModelState.IsValid)
            {
                db.PayrollLedgerEntry.Add(payrollLedgerEntry);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(payrollLedgerEntry);
        }

        // GET: PayrollLedgerEntries/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PayrollLedgerEntry payrollLedgerEntry = db.PayrollLedgerEntry.Find(id);
            ViewBag.PayrollControlCode = new SelectList(ObtenerPaycontrol(), "Code", "Name", payrollLedgerEntry.PayrollControlCode);
            ViewBag.PayrollControlType = new SelectList(ObtenerPaycontrolType(), "ID_TYPE", "DESCRIPCION", payrollLedgerEntry.PayrollControlType);
            ViewBag.EmployerNo = new SelectList(ObtenerEmployer(), "No", "Name", payrollLedgerEntry.EmployerNo);
            ViewBag.EmployeeNo = new SelectList(ObtenerEmployee(), "No", "FirstName", payrollLedgerEntry.EmployeeNo);
            ViewBag.GLPostType = new SelectList(ObtenerPayrollPostType(), "ID_POST", "DESCRIPCION", payrollLedgerEntry.GLPostType);
            if (payrollLedgerEntry == null)
            {
                return HttpNotFound();
            }
            return View(payrollLedgerEntry);
        }

        // POST: PayrollLedgerEntries/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "EntryNo,EmployeeNo,PostingDate,DocumentType,DocumentNo,Description,RegularPay,Amount,TaxableAmount,AmountonPayCheck,PayrollPostingGroup,GlobalDimension1Code,GlobalDimension2Code,ResourceNo,UserID,SourceCode,PayrollControlCode,PayrollControlType,PayrollControlName,County,Locality,PayPeriodEndDate,GLPostType,VendorLedgerEntryNo,VendorLedgerPosting,JobNo,JobTaskNo,BurdenAllocated,Chargeable,WorkTypeCode,JournalBatchName,ReasonCode,CheckNo,PayDate,ReportingAuthorityType,ReportingAuthorityCode,EntryDate,DateWorked,PayCycleCode,PayCycleTerm,PayCyclePeriod,PayPeriodStartDate,EarnedPayCycleCode,EarnedPayCycleTerm,EarnedPayCyclePeriod,EarnedPeriodStartDate,EarnedPeriodEndDate,WorkReportingAuthorityCode,LedgerPostingDate,InsurableHours,TaxTypeCode,EmployerNo,SeasonalCode,SeasonalWages,DimensionSetID,calcReport")] PayrollLedgerEntry payrollLedgerEntry)
        {
            if (ModelState.IsValid)
            {
                db.Entry(payrollLedgerEntry).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(payrollLedgerEntry);
        }

        // GET: PayrollLedgerEntries/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PayrollLedgerEntry payrollLedgerEntry = db.PayrollLedgerEntry.Find(id);
            if (payrollLedgerEntry == null)
            {
                return HttpNotFound();
            }
            return View(payrollLedgerEntry);
        }

        // POST: PayrollLedgerEntries/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            PayrollLedgerEntry payrollLedgerEntry = db.PayrollLedgerEntry.Find(id);
            db.PayrollLedgerEntry.Remove(payrollLedgerEntry);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
        public List<PayrollControlView> ObtenerPaycontrol()
        {
            List<PayrollControlView> listaINI = new List<PayrollControlView> {
                new PayrollControlView {  Code = "0",  Name = "Seleccione una opción" }
            };
            var listaPrincipal = dbPayControl.PayrollControl.Select(r => new PayrollControlView
                {
                    Code = r.Code,
                    Name = r.Name
            }).ToList();
            var listFinal = listaINI.Union(listaPrincipal);
            return listFinal.ToList();
        }
        public List<PayrollControlTypeView> ObtenerPaycontrolType()
        {
            List<PayrollControlTypeView> listaINI = new List<PayrollControlTypeView> {
                new PayrollControlTypeView {  ID_TYPE  = 0,   DESCRIPCION= "Seleccione una opción" }
            };
            var listaPrincipal = dbPayControl.PayrollControlType.Select(r => new PayrollControlTypeView
            {
                ID_TYPE = r.ID_TYPE,
                DESCRIPCION = r.DESCRIPCION
            }).ToList();
            var listFinal = listaINI.Union(listaPrincipal);
            return listFinal.ToList();
        }

        public List<PayrollPostTypeView> ObtenerPayrollPostType()
        {
            List<PayrollPostTypeView> listaINI = new List<PayrollPostTypeView> {
                new PayrollPostTypeView {  ID_POST  = 0,   DESCRIPCION= "Seleccione una opción" }
            };
            var listaPrincipal = dbPayControl.PayrollPostType.Select(r => new PayrollPostTypeView
            {
                ID_POST = r.ID_POST,
                DESCRIPCION = r.DESCRIPCION
            }).ToList();
            var listFinal = listaINI.Union(listaPrincipal);
            return listFinal.ToList();
        }
        public List<EmployerView> ObtenerEmployer()
        {
            List<EmployerView> listaINI = new List<EmployerView> {
                new EmployerView { No  = "0",   Name= "Seleccione una opción" }
            };
            var listaPrincipal =  dbe.Employer.Select(r => new EmployerView
            {
                No = r.No,
                Name = r.Name
            }).ToList();
            var listFinal = listaINI.Union(listaPrincipal);
            return listFinal.ToList();
        }
        public List<EmployeeView> ObtenerEmployee()
        {
            List<EmployeeView> listaINI = new List<EmployeeView> {
                new EmployeeView { No  = "0",   FirstName= "Seleccione una opción" }
            };
            var listaPrincipal = dbe.Employee.Select(r => new EmployeeView
            {
                No = r.No,
                FirstName = r.FirstName+" "+r.MiddleName+" "+r.LastName + " " +r.SecondLastName
            }).ToList();
            var listFinal = listaINI.Union(listaPrincipal);
            return listFinal.ToList();
        }




    }
}
