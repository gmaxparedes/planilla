﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ControlPlanilla.Models;


namespace ControlPlanilla.Controllers
{
    [Authorize]
    public class GlobalUsuariosController : AuditoriaController
    {
        // GET: USUARIOS
        public ActionResult Index()
        {
            //var listaEmpresas = dbe.GlobalEmpresas.Where(e => e.CD_EMPRESA == cd_empresa)
            //   .Select(c => new EmpresasView
            //   {
            //       DS_EMPRESA = c.DS_EMPRESA,
            //       CD_EMPRESA = c.CD_EMPRESA
            //   });
            //ViewBag.usuario = Session["usuario"].ToString();
            //Usuario = Session["usuario"].ToString();
            //CdEmpresa = Session["cd_empresa"].ToString();

            var globalUsers = dbe.USUARIOS
                .Select(u => new UsuariosViewModels
                {
                    CD_CODIGO_USUARIO = u.CD_CODIGO_USUARIO,
                    DS_NOMBRE_USUARIO = u.DS_NOMBRE_USUARIO,
                    CD_CLAVE_USUARIO = u.CD_CLAVE_USUARIO,
                    CD_DIRE_EMAIL = u.CD_DIRE_EMAIL,
                    CD_ESTADO = u.CD_ESTADO_USUARIO,
                    CD_EMPRESA_DEFECTO = u.CD_EMPRESA_DEFECTO
                }).ToList();



            EstadoUsuario();
            ViewBag.OpcionSistema = OpcionSistema;
            return View(globalUsers);
        }

        // GET: USUARIOS/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            USUARIOS USUARIOS = dbe.USUARIOS.Find(id);
            if (USUARIOS == null)
            {
                return HttpNotFound();
            }
            return View(USUARIOS);
        }

        // GET: USUARIOS/Create
        public ActionResult Create()
        {
            var listaPaises = dbe.PAISES.Select(a => new PaisesViewModels { CD_PAIS = a.CD_PAIS, DS_PAIS = a.DS_NOMBREPAIS }).ToList();
            ViewBag.cmbPaises = listaPaises;
            EstadoUsuario();
            return View();
        }

        // POST: USUARIOS/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(UsuariosViewModels Usuarios)
        {
            if (ModelState.IsValid)
            {
                string codigo_usuario = string.Empty;

                var users = dbe.USUARIOS.ToList();
                string letra = Usuarios.DS_NOMBRE_USUARIO.Substring(0, 1);
                var cc = dbe.USUARIOS.Where(u => u.CD_CODIGO_USUARIO.Substring(0, 1) == letra)
                    .OrderByDescending(o => o.CD_CODIGO_USUARIO)
                    .FirstOrDefault();

                if (cc == null)
                {
                    codigo_usuario = string.Concat(letra.ToUpper(), "0001");
                }
                else
                {
                    int correlativo = Convert.ToInt32(cc.CD_CODIGO_USUARIO.Substring(1, 4)) + 1;
                    codigo_usuario = string.Concat(letra.ToUpper(), correlativo.ToString().PadLeft(4, '0'));
                }


                USUARIOS USUARIOS = new USUARIOS();
                USUARIOS.CD_CODIGO_USUARIO = codigo_usuario;
                USUARIOS.DS_NOMBRE_USUARIO = Usuarios.DS_NOMBRE_USUARIO;
                USUARIOS.CD_CLAVE_USUARIO = Crypto.EnCryptString(Usuarios.CD_CLAVE_USUARIO);
                USUARIOS.CD_DIRE_EMAIL = Usuarios.CD_DIRE_EMAIL;
                USUARIOS.CD_ESTADO_USUARIO = Usuarios.CD_ESTADO;
                USUARIOS.CD_EMPRESA_DEFECTO = Usuarios.CD_EMPRESA_DEFECTO;
                USUARIOS.USUA_CREA = Usuario;
                USUARIOS.FECH_CREA = Ahora;

                dbe.USUARIOS.Add(USUARIOS);
                dbe.SaveChanges();

                bool requiereArchivos = ComprobarRequiereArchivos(OpcionSistema);

                if (requiereArchivos)
                {
                    TempData["OpcionSistema"] = OpcionSistema;
                    TempData["CdForaneo"] = codigo_usuario;
                    TempData["DsForaneo"] = Usuarios.DS_NOMBRE_USUARIO;

                    bool cumplio = ComprobarArchivosAdjuntos();

                    if (!cumplio)
                    {
                        //string opcion, string DsForaneo, string CdForaneo)
                        return RedirectToAction("SubirArchivos", "Auditoria", new { opcion = OpcionSistema, DsForaneo = Usuarios.DS_NOMBRE_USUARIO, CdForaneo = codigo_usuario });
                    }
                }

                return RedirectToAction("Index");
            }

            var listaPaises = dbe.PAISES.Select(a => new PaisesViewModels { CD_PAIS = a.CD_PAIS, DS_PAIS = a.DS_NOMBREPAIS }).ToList();
            ViewBag.cmbPaises = listaPaises;
            EstadoUsuario();
            return View(Usuarios);
        }

        // GET: USUARIOS/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UsuariosViewModels USUARIOS = dbe.USUARIOS.Where(u => u.CD_CODIGO_USUARIO == id).
                Select(u => new UsuariosViewModels
                {
                    CD_CODIGO_USUARIO = u.CD_CODIGO_USUARIO,
                    DS_NOMBRE_USUARIO = u.DS_NOMBRE_USUARIO,
                    CD_CLAVE_USUARIO = u.CD_CLAVE_USUARIO,
                    CD_DIRE_EMAIL = u.CD_DIRE_EMAIL,
                    CD_ESTADO = u.CD_ESTADO_USUARIO,
                    CD_EMPRESA_DEFECTO = u.CD_EMPRESA_DEFECTO
                }).FirstOrDefault();
            if (USUARIOS == null)
            {
                return HttpNotFound();
            }
            var listaPaises = dbe.PAISES.Select(a => new PaisesViewModels { CD_PAIS = a.CD_PAIS, DS_PAIS = a.DS_NOMBREPAIS }).ToList();
            ViewBag.cmbPaises = listaPaises;
            EstadoUsuario();
            return View(USUARIOS);
        }

        // POST: USUARIOS/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(UsuariosViewModels Usuarios)
        {
            if (ModelState.IsValid)
            {
                USUARIOS USUARIOS = dbe.USUARIOS.Where(u => u.CD_CODIGO_USUARIO == Usuarios.CD_CODIGO_USUARIO).FirstOrDefault();


                USUARIOS.CD_CODIGO_USUARIO = Usuarios.CD_CODIGO_USUARIO;
                USUARIOS.DS_NOMBRE_USUARIO = Usuarios.DS_NOMBRE_USUARIO;
                if (USUARIOS.CD_CLAVE_USUARIO != Usuarios.CD_CLAVE_USUARIO)
                {
                    USUARIOS.CD_CLAVE_USUARIO = Crypto.EnCryptString(Usuarios.CD_CLAVE_USUARIO);
                }
                
                USUARIOS.CD_DIRE_EMAIL = Usuarios.CD_DIRE_EMAIL;
                USUARIOS.CD_ESTADO_USUARIO = Usuarios.CD_ESTADO;
                USUARIOS.CD_EMPRESA_DEFECTO = Usuarios.CD_EMPRESA_DEFECTO;


                USUARIOS.FECH_ACTU = Ahora;
                USUARIOS.USUA_ACTU = (string.IsNullOrEmpty(Usuario)) ? Session["usuario"].ToString() : Usuario;

                dbe.Entry(USUARIOS).State = EntityState.Modified;
                dbe.SaveChanges();
                bool requiereArchivos = ComprobarRequiereArchivos(OpcionSistema);

                if (requiereArchivos)
                {
                    TempData["OpcionSistema"] = OpcionSistema;
                    TempData["CdForaneo"] = Usuarios.CD_CODIGO_USUARIO;
                    TempData["DsForaneo"] = "CD_CODIGO_USUARIO";

                    bool cumplio = ComprobarArchivosAdjuntos();

                    if (!cumplio)
                    {
                        return RedirectToAction("SubirArchivos", "Auditoria", new { opcion = OpcionSistema, DsForaneo = Usuarios.DS_NOMBRE_USUARIO, CdForaneo = Usuarios.CD_CODIGO_USUARIO });
                    }
                }
                TempData["MensajeOK"] = "Registro actualizado";
                return RedirectToAction("Index");
            }
            else
            {
                TempData["MensajeError"] = "Error al actualizar registro";
                if (ModelState.Values.Select(c => c.Errors).ToList().Count > 0)
                {
                    var mensajes = ModelState.Values.Where(s => s.Errors.Count > 0).Select(s => s.Errors).ToList();
                    if (mensajes.Count > 0)
                    {
                        TempData["MensajeError"] = "";
                    }
                    foreach (var item in mensajes)
                    {
                        TempData["MensajeError"] += item.Select(i => i.ErrorMessage.ToString()).FirstOrDefault() + " ";
                    }
                }
            }
            var listaPaises = dbe.PAISES.Select(a => new PaisesViewModels { CD_PAIS = a.CD_PAIS, DS_PAIS = a.DS_NOMBREPAIS }).ToList();
            ViewBag.cmbPaises = listaPaises;
            EstadoUsuario();

            return View(Usuarios);
        }

        // GET: USUARIOS/Delete/5
        //[ValidateAntiForgeryToken]
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UsuariosViewModels USUARIOS = dbe.USUARIOS.Where(u => u.CD_CODIGO_USUARIO == id).
                Select(u => new UsuariosViewModels
                {
                    CD_CODIGO_USUARIO = u.CD_CODIGO_USUARIO,
                    DS_NOMBRE_USUARIO = u.DS_NOMBRE_USUARIO,
                    CD_CLAVE_USUARIO = u.CD_CLAVE_USUARIO,
                    CD_DIRE_EMAIL = u.CD_DIRE_EMAIL,
                    CD_ESTADO = u.CD_ESTADO_USUARIO,
                    CD_EMPRESA_DEFECTO = u.CD_EMPRESA_DEFECTO
                }).FirstOrDefault();
            if (USUARIOS == null)
            {
                return HttpNotFound();
            }
            var listaPaises = dbe.PAISES.Select(a => new PaisesViewModels { CD_PAIS = a.CD_PAIS, DS_PAIS = a.DS_NOMBREPAIS }).ToList();
            ViewBag.cmbPaises = listaPaises;
            EstadoUsuario();
            return View(USUARIOS);
        }

        // POST: USUARIOS/Delete/5
        [HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            USUARIOS USUARIOS = dbe.USUARIOS.Find(id);
            dbe.USUARIOS.Remove(USUARIOS);
            dbe.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                dbe.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
