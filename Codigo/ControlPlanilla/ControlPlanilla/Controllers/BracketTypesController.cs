﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ControlPlanilla.Models;

namespace ControlPlanilla.Controllers
{
    public class BracketTypesController : AuditoriaController
    {
        private PlanillaPayControls db = new PlanillaPayControls();

        // GET: BracketTypes
        public ActionResult Index()
        {
            return View(db.BracketType.ToList());
        }

        // GET: BracketTypes/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BracketType bracketType = db.BracketType.Find(id);
            if (bracketType == null)
            {
                return HttpNotFound();
            }
            return View(bracketType);
        }

        // GET: BracketTypes/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: BracketTypes/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Code,Description,BracketPageID,UsesMultipleBracketLines,UsesFilingStatus,UsesPayFrequency,UsesAmountOver,UsesLimit,UsesTaxPercent,UsesPercent,UsesQuantity,UsesTaxAmount,UsesAmount,UsesRateMultiple,UsesMaxWH,UsesMinAmount,UsesMaxAmount,UsesPerAllowance,UsesFromAllowance,UsesMatchPercent,UsesMaxPercent,UsesActivationCode")] BracketType bracketType)
        {
            if (ModelState.IsValid)
            {
                db.BracketType.Add(bracketType);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(bracketType);
        }

        // GET: BracketTypes/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BracketType bracketType = db.BracketType.Find(id);
            if (bracketType == null)
            {
                return HttpNotFound();
            }
            return View(bracketType);
        }

        // POST: BracketTypes/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Code,Description,BracketPageID,UsesMultipleBracketLines,UsesFilingStatus,UsesPayFrequency,UsesAmountOver,UsesLimit,UsesTaxPercent,UsesPercent,UsesQuantity,UsesTaxAmount,UsesAmount,UsesRateMultiple,UsesMaxWH,UsesMinAmount,UsesMaxAmount,UsesPerAllowance,UsesFromAllowance,UsesMatchPercent,UsesMaxPercent,UsesActivationCode")] BracketType bracketType)
        {
            if (ModelState.IsValid)
            {
                db.Entry(bracketType).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(bracketType);
        }

        // GET: BracketTypes/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BracketType bracketType = db.BracketType.Find(id);
            if (bracketType == null)
            {
                return HttpNotFound();
            }
            return View(bracketType);
        }

        // POST: BracketTypes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            BracketType bracketType = db.BracketType.Find(id);
            db.BracketType.Remove(bracketType);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
