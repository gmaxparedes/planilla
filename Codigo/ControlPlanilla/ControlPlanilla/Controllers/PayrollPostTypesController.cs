﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ControlPlanilla.Models;

namespace ControlPlanilla.Controllers
{
    public class PayrollPostTypesController : AuditoriaController
    {

        // GET: PayrollPostTypes
        public ActionResult Index()
        {
            return View(dbPayControl.PayrollPostType.ToList());
        }

        // GET: PayrollPostTypes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PayrollPostType payrollPostType = dbPayControl.PayrollPostType.Find(id);
            if (payrollPostType == null)
            {
                return HttpNotFound();
            }
            return View(payrollPostType);
        }

        // GET: PayrollPostTypes/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: PayrollPostTypes/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID_POST,DESCRIPCION")] PayrollPostType payrollPostType)
        {
            if (ModelState.IsValid)
            {
                PayrollPostType guardar = new PayrollPostType();
                guardar.DESCRIPCION = payrollPostType.DESCRIPCION;
                guardar.USUA_CREA = Usuario;
                guardar.FECH_CREA = Ahora;
                dbPayControl.PayrollPostType.Add(guardar);
                dbPayControl.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(payrollPostType);
        }

        // GET: PayrollPostTypes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PayrollPostType payrollPostType = dbPayControl.PayrollPostType.Find(id);
            if (payrollPostType == null)
            {
                return HttpNotFound();
            }
            return View(payrollPostType);
        }

        // POST: PayrollPostTypes/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID_POST,DESCRIPCION")] PayrollPostType payrollPostType)
        {
            if (ModelState.IsValid)
            {
                PayrollPostType guardar = dbPayControl.PayrollPostType.Find(payrollPostType.ID_POST);
                guardar.DESCRIPCION = payrollPostType.DESCRIPCION;
                guardar.USUA_ACTUA = Usuario;
                guardar.FECH_ACTUA = Ahora;
                dbPayControl.Entry(guardar).State = EntityState.Modified;
                dbPayControl.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(payrollPostType);
        }

        // GET: PayrollPostTypes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PayrollPostType payrollPostType = dbPayControl.PayrollPostType.Find(id);
            if (payrollPostType == null)
            {
                return HttpNotFound();
            }
            return View(payrollPostType);
        }

        // POST: PayrollPostTypes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            PayrollPostType payrollPostType = dbPayControl.PayrollPostType.Find(id);
            dbPayControl.PayrollPostType.Remove(payrollPostType);
            dbPayControl.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                dbPayControl.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
