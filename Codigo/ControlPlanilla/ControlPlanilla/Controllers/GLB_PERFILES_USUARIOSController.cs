﻿using ControlPlanilla.Models;
using DevExpress.Web.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
namespace ControlPlanilla.Controllers
{
    public class GLB_PERFILES_USUARIOSController : AuditoriaController
    {
        // GET: GLB_PERFILES_USUARIOS
        static string idMenu;
        public ActionResult Index(string id)
        {
            if (id != null)
            {
                ViewBag.idseleccinado = id;
            }
            return View();
        }

        ControlPlanilla.Models.PlanillaAccesoMenus db = new ControlPlanilla.Models.PlanillaAccesoMenus();

        public JsonResult ObtenerMenus(string CD_CODIGO_PERFIL)
        {
            var listaEmployee = db.GLB_PERFILES_USUARIOS.Where(ep => ep.CD_CODIGO_PERFIL == CD_CODIGO_PERFIL).
                Select(r => new GLB_Perfiles_UsuariosView
                {
                    CD_CODIGO_PERFIL = r.CD_CODIGO_PERFIL,
                    DS_NOMBRE_PERFIL = r.DS_NOMBRE_PERFIL,
                    CD_ESTADO_PERFIL = r.CD_ESTADO_PERFIL,
                }).ToList();
            var listFinal = listaEmployee;
            return Json(listFinal, JsonRequestBehavior.AllowGet);
        }

        [ValidateInput(false)]
        public ActionResult BuscarID(string key)
        {
            idMenu = key;
            return RedirectToAction("Index", new { id = key });
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult Create_Menu(string CD_CODIGO_PERFIL, string DS_NOMBRE_PERFIL, string CD_ESTADO_PERFIL)
        {
            if (CD_CODIGO_PERFIL != "")
            {
                GLB_PERFILES_USUARIOS grabar;
                //validar si existe
                var existincia = (from e in db.GLB_PERFILES_USUARIOS
                                  where e.CD_CODIGO_PERFIL == CD_CODIGO_PERFIL
                                  select e.CD_CODIGO_PERFIL).Count();

                if (existincia > 0)
                {
                    grabar = db.GLB_PERFILES_USUARIOS.Find(CD_CODIGO_PERFIL);
                }
                else
                {
                    grabar = new GLB_PERFILES_USUARIOS();
                }

                grabar.CD_CODIGO_PERFIL = CD_CODIGO_PERFIL;
                grabar.DS_NOMBRE_PERFIL = DS_NOMBRE_PERFIL;
                grabar.CD_ESTADO_PERFIL = Convert.ToBoolean(CD_ESTADO_PERFIL);
                grabar.CD_EMPRESA = CdEmpresa;
                grabar.FECH_CREA = Ahora;
                grabar.USUA_CREA = Usuario;

                if (existincia > 0)
                {
                    db.Entry(grabar).State = EntityState.Modified;
                }
                else
                {
                    db.GLB_PERFILES_USUARIOS.Add(grabar);
                }
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View();
        }

        

        [ValidateInput(false)]
        public ActionResult GridViewPartial()
        {
            var model = db.GLB_PERFILES_USUARIOS;
            return PartialView("_GridViewPartial", model.ToList());
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult GridViewPartialAddNew([ModelBinder(typeof(DevExpressEditorsBinder))] ControlPlanilla.Models.GLB_PERFILES_USUARIOS item)
        {
            var model = db.GLB_PERFILES_USUARIOS;
            if (ModelState.IsValid)
            {
                try
                {
                    model.Add(item);
                    db.SaveChanges();
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
                ViewData["EditError"] = "Please, correct all errors.";
            return PartialView("_GridViewPartial", model.ToList());
        }
        [HttpPost, ValidateInput(false)]
        public ActionResult GridViewPartialUpdate([ModelBinder(typeof(DevExpressEditorsBinder))] ControlPlanilla.Models.GLB_PERFILES_USUARIOS item)
        {
            var model = db.GLB_PERFILES_USUARIOS;
            if (ModelState.IsValid)
            {
                try
                {
                    var modelItem = model.FirstOrDefault(it => it.CD_CODIGO_PERFIL == item.CD_CODIGO_PERFIL);
                    if (modelItem != null)
                    {
                        this.UpdateModel(modelItem);
                        db.SaveChanges();
                    }
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
                ViewData["EditError"] = "Please, correct all errors.";
            return PartialView("_GridViewPartial", model.ToList());
        }
        [HttpPost, ValidateInput(false)]
        public ActionResult GridViewPartialDelete(System.String CD_CODIGO_PERFIL)
        {
            var model = db.GLB_PERFILES_USUARIOS;
            if (CD_CODIGO_PERFIL != null)
            {
                try
                {
                    var item = model.FirstOrDefault(it => it.CD_CODIGO_PERFIL == CD_CODIGO_PERFIL);
                    if (item != null)
                        model.Remove(item);
                    db.SaveChanges();
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            return PartialView("_GridViewPartial", model.ToList());
        }
    }
}