﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ControlPlanilla.Models;

namespace ControlPlanilla.Controllers
{
    public class PayrollControlAbsenceCodesController : AuditoriaController
    {
        

        // GET: PayrollControlAbsenceCodes
        public ActionResult Index()
        {
            return View(dbPayControl.PayrollControlAbsenceCode.ToList());
        }

        // GET: PayrollControlAbsenceCodes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PayrollControlAbsenceCode payrollControlAbsenceCode = dbPayControl.PayrollControlAbsenceCode.Find(id);
            if (payrollControlAbsenceCode == null)
            {
                return HttpNotFound();
            }
            return View(payrollControlAbsenceCode);
        }

        // GET: PayrollControlAbsenceCodes/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: PayrollControlAbsenceCodes/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID_ABSENCE,DESCRIPCION")] PayrollControlAbsenceCode payrollControlAbsenceCode)
        {
            if (ModelState.IsValid)
            {
                PayrollControlAbsenceCode guardar = new PayrollControlAbsenceCode();
                guardar.DESCRIPCION = payrollControlAbsenceCode.DESCRIPCION;
                guardar.USUA_CREA = Usuario;
                guardar.FECH_CREA = Ahora;
                dbPayControl.PayrollControlAbsenceCode.Add(guardar);
                dbPayControl.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(payrollControlAbsenceCode);
        }

        // GET: PayrollControlAbsenceCodes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PayrollControlAbsenceCode payrollControlAbsenceCode = dbPayControl.PayrollControlAbsenceCode.Find(id);
            if (payrollControlAbsenceCode == null)
            {
                return HttpNotFound();
            }
            return View(payrollControlAbsenceCode);
        }

        // POST: PayrollControlAbsenceCodes/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID_ABSENCE,DESCRIPCION")] PayrollControlAbsenceCode payrollControlAbsenceCode)
        {
            if (ModelState.IsValid)
            {
                PayrollControlAbsenceCode guardar = dbPayControl.PayrollControlAbsenceCode.Find(payrollControlAbsenceCode.ID_ABSENCE);
                guardar.DESCRIPCION = payrollControlAbsenceCode.DESCRIPCION;
                guardar.USUA_ACTUA = Usuario;
                guardar.FECH_ACTUA = Ahora;
                dbPayControl.Entry(guardar).State = EntityState.Modified;
                dbPayControl.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(payrollControlAbsenceCode);
        }

        // GET: PayrollControlAbsenceCodes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PayrollControlAbsenceCode payrollControlAbsenceCode = dbPayControl.PayrollControlAbsenceCode.Find(id);
            if (payrollControlAbsenceCode == null)
            {
                return HttpNotFound();
            }
            return View(payrollControlAbsenceCode);
        }

        // POST: PayrollControlAbsenceCodes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            PayrollControlAbsenceCode payrollControlAbsenceCode = dbPayControl.PayrollControlAbsenceCode.Find(id);
            dbPayControl.PayrollControlAbsenceCode.Remove(payrollControlAbsenceCode);
            dbPayControl.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                dbPayControl.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
