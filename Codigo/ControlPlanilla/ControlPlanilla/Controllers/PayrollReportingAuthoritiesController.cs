﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ControlPlanilla.Models;

namespace ControlPlanilla.Controllers
{
    public class PayrollReportingAuthoritiesController : AuditoriaController
    {
        

        // GET: PayrollReportingAuthorities
        public ActionResult Index()
        {
            var payrollReportingAuthority = dbPayControl.PayrollReportingAuthority.Include(p => p.PayRateEditStatus).Include(p => p.PayrollControlType).Include(p => p.PayrollPostType).Include(p => p.PayrollTypeReportingAuthority);
            return View(payrollReportingAuthority.ToList());
        }

        // GET: PayrollReportingAuthorities/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PayrollReportingAuthority payrollReportingAuthority = dbPayControl.PayrollReportingAuthority.Find(id);
            if (payrollReportingAuthority == null)
            {
                return HttpNotFound();
            }
            return View(payrollReportingAuthority);
        }

        // GET: PayrollReportingAuthorities/Create
        public ActionResult Create()
        {
            ViewBag.EditStatus = new SelectList(dbPayControl.PayRateEditStatus, "ID_EDIT", "DESCRIPCION");
            ViewBag.PayrollControlTypeFilter = new SelectList(dbPayControl.PayrollControlType, "ID_TYPE", "DESCRIPCION");
            ViewBag.GLPostTypeFilter = new SelectList(dbPayControl.PayrollPostType, "ID_POST", "DESCRIPCION");
            ViewBag.Type = new SelectList(dbPayControl.PayrollTypeReportingAuthority, "ID_TYPE", "DESCRIPCION");
            return View();
        }

        // POST: PayrollReportingAuthorities/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Code,Type,Name,County,Locality,DateFilter,GlobalDimension1Filter,GlobalDimension2Filter,PayrollControlTypeFilter,GLPostTypeFilter,WorkTypeFilter,Amount,TaxableAmount,UnemploymentInsurance,StateNoCode,TaxingEntityCode,ParentRepAuthCode,EditStatus,EmployerNoFilter")] PayrollReportingAuthority payrollReportingAuthority)
        {
            if (ModelState.IsValid)
            {
                dbPayControl.PayrollReportingAuthority.Add(payrollReportingAuthority);
                dbPayControl.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.EditStatus = new SelectList(dbPayControl.PayRateEditStatus, "ID_EDIT", "DESCRIPCION", payrollReportingAuthority.EditStatus);
            ViewBag.PayrollControlTypeFilter = new SelectList(dbPayControl.PayrollControlType, "ID_TYPE", "DESCRIPCION", payrollReportingAuthority.PayrollControlTypeFilter);
            ViewBag.GLPostTypeFilter = new SelectList(dbPayControl.PayrollPostType, "ID_POST", "DESCRIPCION", payrollReportingAuthority.GLPostTypeFilter);
            ViewBag.Type = new SelectList(dbPayControl.PayrollTypeReportingAuthority, "ID_TYPE", "DESCRIPCION", payrollReportingAuthority.Type);
            return View(payrollReportingAuthority);
        }

        // GET: PayrollReportingAuthorities/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PayrollReportingAuthority payrollReportingAuthority = dbPayControl.PayrollReportingAuthority.Find(id);
            if (payrollReportingAuthority == null)
            {
                return HttpNotFound();
            }
            ViewBag.EditStatus = new SelectList(dbPayControl.PayRateEditStatus, "ID_EDIT", "DESCRIPCION", payrollReportingAuthority.EditStatus);
            ViewBag.PayrollControlTypeFilter = new SelectList(dbPayControl.PayrollControlType, "ID_TYPE", "DESCRIPCION", payrollReportingAuthority.PayrollControlTypeFilter);
            ViewBag.GLPostTypeFilter = new SelectList(dbPayControl.PayrollPostType, "ID_POST", "DESCRIPCION", payrollReportingAuthority.GLPostTypeFilter);
            ViewBag.Type = new SelectList(dbPayControl.PayrollTypeReportingAuthority, "ID_TYPE", "DESCRIPCION", payrollReportingAuthority.Type);
            return View(payrollReportingAuthority);
        }

        // POST: PayrollReportingAuthorities/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Code,Type,Name,County,Locality,DateFilter,GlobalDimension1Filter,GlobalDimension2Filter,PayrollControlTypeFilter,GLPostTypeFilter,WorkTypeFilter,Amount,TaxableAmount,UnemploymentInsurance,StateNoCode,TaxingEntityCode,ParentRepAuthCode,EditStatus,EmployerNoFilter")] PayrollReportingAuthority payrollReportingAuthority)
        {
            if (ModelState.IsValid)
            {
                dbPayControl.Entry(payrollReportingAuthority).State = EntityState.Modified;
                dbPayControl.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.EditStatus = new SelectList(dbPayControl.PayRateEditStatus, "ID_EDIT", "DESCRIPCION", payrollReportingAuthority.EditStatus);
            ViewBag.PayrollControlTypeFilter = new SelectList(dbPayControl.PayrollControlType, "ID_TYPE", "DESCRIPCION", payrollReportingAuthority.PayrollControlTypeFilter);
            ViewBag.GLPostTypeFilter = new SelectList(dbPayControl.PayrollPostType, "ID_POST", "DESCRIPCION", payrollReportingAuthority.GLPostTypeFilter);
            ViewBag.Type = new SelectList(dbPayControl.PayrollTypeReportingAuthority, "ID_TYPE", "DESCRIPCION", payrollReportingAuthority.Type);
            return View(payrollReportingAuthority);
        }

        // GET: PayrollReportingAuthorities/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PayrollReportingAuthority payrollReportingAuthority = dbPayControl.PayrollReportingAuthority.Find(id);
            if (payrollReportingAuthority == null)
            {
                return HttpNotFound();
            }
            return View(payrollReportingAuthority);
        }

        // POST: PayrollReportingAuthorities/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            PayrollReportingAuthority payrollReportingAuthority = dbPayControl.PayrollReportingAuthority.Find(id);
            dbPayControl.PayrollReportingAuthority.Remove(payrollReportingAuthority);
            dbPayControl.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                dbPayControl.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
