﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ControlPlanilla.Models;
using PagedList;

namespace ControlPlanilla.Controllers
{
    public class BracketsController : AuditoriaController
    {
        private PlanillaPayControls dbB = new PlanillaPayControls();

        // GET: Brackets
        public ActionResult Index()
        {
            var bracket = dbB.Bracket.Include(b => b.BracketType);
            return View(bracket.ToList());
        }

        // GET: Brackets/Details/5
        public ActionResult Details(string id, string Type, DateTime Fecha)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Bracket bracket = dbB.Bracket.Where(c=> c.Code==id && c.BracketTypeCode==Type && c.EffectiveDate==Fecha).FirstOrDefault();
            if (bracket == null)
            {
                return HttpNotFound();
            }
            return View(bracket);
        }

        // GET: Brackets/Create
        public ActionResult Create()
        {
            ViewBag.BracketTypeCode = new SelectList(dbB.BracketType, "Code", "Description");
            return View();
        }

        // POST: Brackets/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Code,BracketTypeCode,EffectiveDate,Description,EditStatus,BracketDetailsExist")] Bracket bracket)
        {
            if (ModelState.IsValid)
            {
                dbB.Bracket.Add(bracket);
                dbB.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.BracketTypeCode = new SelectList(dbB.BracketType, "Code", "Description", bracket.BracketTypeCode);
            return View(bracket);
        }

        // GET: Brackets/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Bracket bracket = dbB.Bracket.Where(c=> c.Code==id).FirstOrDefault();
            if (bracket == null)
            {
                return HttpNotFound();
            }
            ViewBag.BracketTypeCode = new SelectList(dbB.BracketType, "Code", "Description", bracket.BracketTypeCode);
            return View(bracket);
        }

        // POST: Brackets/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Code,BracketTypeCode,EffectiveDate,Description,EditStatus,BracketDetailsExist")] Bracket bracket)
        {
            if (ModelState.IsValid)
            {
                dbB.Entry(bracket).State = EntityState.Modified;
                dbB.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.BracketTypeCode = new SelectList(dbB.BracketType, "Code", "Description", bracket.BracketTypeCode);
            return View(bracket);
        }

        // GET: Brackets/Delete/5
        public ActionResult Delete(string id, string Type, DateTime Fecha)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Bracket bracket = dbB.Bracket.Where(c => c.Code == id && c.BracketTypeCode == Type && c.EffectiveDate == Fecha).FirstOrDefault();
            if (bracket == null)
            {
                return HttpNotFound();
            }
            return View(bracket);
        }

        // POST: Brackets/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id, string Type, DateTime Fecha)
        {
            Bracket bracket = dbB.Bracket.Where(c => c.Code == id && c.BracketTypeCode == Type && c.EffectiveDate == Fecha).FirstOrDefault();
            dbB.Bracket.Remove(bracket);
            dbB.SaveChanges();
            return RedirectToAction("Index");
        }

        //#region Direct Deposit Sections
        [AllowAnonymous]
        public static SelectList crearSelect(SelectList items)
        {
            List<AgregarSeleccioneOpcion> cmb = new List<AgregarSeleccioneOpcion>();
            cmb.Add(new AgregarSeleccioneOpcion());
            foreach (var i in items)
            {
                var ag = new AgregarSeleccioneOpcion();
                ag.Value = i.Value;
                ag.Text=i.Text;
                cmb.Add(ag);
            }
            

            return new SelectList(cmb, "Value", "Text"); ;
        }        
        [HttpPost]
        [AllowAnonymous]
        public PartialViewResult Brackets(string Code, string BracketTypeCode, DateTime EffectiveDate, int? page)
        {
            ViewBag.PayFrequency = crearSelect(new SelectList(dbCiclo.PayFrecuency, "PayFrequency", "Description"));
            ViewBag.ReportingAuthorityType = crearSelect(new SelectList(dbPayControl.PayrollTypeReportingAuthority, "ID_TYPE", "DESCRIPCION"));
            ViewBag.ReportingAuthorityCode = crearSelect(new SelectList(dbPayControl.PayrollReportingAuthority, "Code", "Name"));
            ViewBag.TypeBrackets = dbB.BracketType.Where(c => c.Code == BracketTypeCode).FirstOrDefault();
            try
            {
                var bracketDetail = dbB.BracketDetail.Where(c => c.BracketCode == Code && c.BracketTypeCode == BracketTypeCode && c.BracketEffectiveDate == EffectiveDate).ToList();
                
                ViewBag.Codigo = Code;
                ViewBag.BracketTypeCode = BracketTypeCode;
                ViewBag.EffectiveDate = EffectiveDate;

                int pageNumber = (page ?? 1);

                var result = bracketDetail.ToPagedList(pageNumber, pageSize);

                return PartialView(result);
            }
            catch (SystemException ex)
            {
                logRepository.CapturarTodoError(ex);
                return PartialView();
            }
        }
        [HttpPost]
        public bool CrearBracketDetail([Bind(Include = "BracketCode,BracketTypeCode,BracketEffectiveDate,FilingStatusCode,PayFrequency,AmountOver,Limit,TaxPercent,PercentValue,Quantity," +
            "TaxAmount,Amount,RateMultiple,MaxWH,MinAmount,MaxAmount,PerAllowance,FromAllowance,MatchPercent,MaxPercent,ReportingAuthorityType,ReportingAuthorityCode,ActivationCode")] BracketDetail bracketDetail)
        {
            bool result = false;
            try
            {
                if (ModelState.IsValid)
                {
                    var BDT = dbB.BracketDetail.Where(c => c.BracketCode == bracketDetail.BracketCode && c.BracketEffectiveDate == bracketDetail.BracketEffectiveDate && c.BracketTypeCode == bracketDetail.BracketTypeCode)
                        .OrderByDescending(c => c.LineNumber).FirstOrDefault();

                    if (BDT != null)
                    {
                        bracketDetail.LineNumber = BDT.LineNumber + 1;
                    }
                    else
                    {
                        bracketDetail.LineNumber = 1;
                    }

                    BracketDetail guardar = new BracketDetail();

                    guardar = bracketDetail;

                    dbB.BracketDetail.Add(guardar);
                    dbB.SaveChanges();
                    result = true;
                }
                ViewBag.PayFrequency = new SelectList(dbCiclo.PayFrecuency, "PayFrequency", "Description");
               // ViewBag.ReportingAuthorityType = new SelectList( , "ID_CONTENT", "DESCRIPCION", directDepositSections.SectionType);
            }
            catch (SystemException ex)
            {
                logRepository.CapturarTodoError(ex);
            }

            return result;
        }

        [HttpPost]
        public bool EditarBracketDetail([Bind(Include = "BracketCode,BracketTypeCode,BracketEffectiveDate,LineNumber,FilingStatusCode,PayFrequency,AmountOver,Limit,TaxPercent,PercentValue,Quantity," +
            "TaxAmount,Amount,RateMultiple,MaxWH,MinAmount,MaxAmount,PerAllowance,FromAllowance,MatchPercent,MaxPercent,ReportingAuthorityType,ReportingAuthorityCode,ActivationCode")] BracketDetail bracketDetail)
        {
            bool result = false;
            try
            {
                if (ModelState.IsValid)
                {
                    var guardar = dbB.BracketDetail.Where(c => c.BracketCode == bracketDetail.BracketCode && c.BracketEffectiveDate == bracketDetail.BracketEffectiveDate && c.BracketTypeCode == bracketDetail.BracketTypeCode)
                        .OrderByDescending(c => c.LineNumber).FirstOrDefault();

                    if (guardar != null)
                    {
                        guardar.FilingStatusCode = bracketDetail.FilingStatusCode;
                        guardar.PayFrequency = bracketDetail.PayFrequency;
                        guardar.AmountOver = bracketDetail.AmountOver;
                        guardar.Limit = bracketDetail.Limit;
                        guardar.TaxPercent = bracketDetail.TaxPercent;
                        guardar.PercentValue = bracketDetail.PercentValue;
                        guardar.Quantity = bracketDetail.Quantity;
                        guardar.TaxAmount = bracketDetail.TaxAmount;
                        guardar.Amount = bracketDetail.Amount;
                        guardar.RateMultiple = bracketDetail.RateMultiple;
                        guardar.MaxWH = bracketDetail.MaxWH;
                        guardar.MinAmount = bracketDetail.MinAmount;
                        guardar.MaxAmount = bracketDetail.MaxAmount;
                        guardar.PerAllowance = bracketDetail.PerAllowance;
                        guardar.FromAllowance = bracketDetail.FromAllowance;
                        guardar.MatchPercent = bracketDetail.MatchPercent;
                        guardar.MaxPercent = bracketDetail.MaxPercent;
                        guardar.ReportingAuthorityType = bracketDetail.ReportingAuthorityType;
                        guardar.ReportingAuthorityCode = bracketDetail.ReportingAuthorityCode;
                        guardar.ActivationCode = bracketDetail.ActivationCode;

                        dbB.Entry(guardar).State = EntityState.Modified;
                        dbB.SaveChanges();
                        result = true;
                    }
                }
                ViewBag.PayFrequency = crearSelect(new SelectList(dbCiclo.PayFrecuency, "PayFrequency", "Description"));
                ViewBag.ReportingAuthorityType = crearSelect(new SelectList(dbPayControl.PayrollTypeReportingAuthority, "ID_TYPE", "DESCRIPCION"));
                ViewBag.ReportingAuthorityCode = crearSelect(new SelectList(dbPayControl.PayrollReportingAuthority, "Code", "Name"));
            }
            catch (SystemException ex)
            {
                logRepository.CapturarTodoError(ex);
            }

            return result;
        }

        [HttpPost]
        public bool EliminarBracketDetail([Bind(Include = "BracketCode,BracketTypeCode,BracketEffectiveDate,LineNumber")] BracketDetail bracketDetail)
        {
            bool result = false;
            try
            {
                if (ModelState.IsValid)
                {
                    var BDT = dbB.BracketDetail.Where(c => c.BracketCode == bracketDetail.BracketCode && c.BracketEffectiveDate == bracketDetail.BracketEffectiveDate && c.BracketTypeCode == bracketDetail.BracketTypeCode && c.LineNumber==bracketDetail.LineNumber)
                        .OrderByDescending(c => c.LineNumber).FirstOrDefault();

                    if (BDT != null)
                    {
                        dbB.BracketDetail.Remove(BDT);
                        dbB.SaveChanges();
                        result = true;
                    }
                    
                    
                }
                ViewBag.PayFrequency = crearSelect(new SelectList(dbCiclo.PayFrecuency, "PayFrequency", "Description"));
                ViewBag.ReportingAuthorityType = crearSelect(new SelectList(dbPayControl.PayrollTypeReportingAuthority, "ID_TYPE", "DESCRIPCION"));
                ViewBag.ReportingAuthorityCode = crearSelect(new SelectList(dbPayControl.PayrollReportingAuthority, "Code", "Name"));
            }
            catch (SystemException ex)
            {
                logRepository.CapturarTodoError(ex);
            }

            return result;
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                dbB.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
