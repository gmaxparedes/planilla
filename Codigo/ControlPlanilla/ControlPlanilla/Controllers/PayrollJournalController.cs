﻿using DevExpress.Web.Mvc;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ControlPlanilla.Controllers
{
    public class PayrollJournalController : GlobalFunctions
    {
        // GET: PayrollJournal
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult ListaTable(string No, string codefilter, string idtemplate)
        {
            if (ModelState.IsValid)
            {
                decimal temp = MethodStep382(No, codefilter, idtemplate);
                var result = 1;
                ViewBag.OpcionConsulta = 1;
                ViewBag.employee = No;
                ViewBag.prc = codefilter;
                ViewBag.stpe1 = "ADD BASE AMOUNT";
                ViewBag.pra1 = 0;
                ViewBag.ta1 = Math.Round(MethodStep220(No, codefilter, idtemplate), 2);
                ViewBag.stpe2 = "AMOUNT IS EMP RATE";
                ViewBag.pra2 = Math.Round(MethodStep22(No, codefilter, idtemplate), 2);
                ViewBag.ta2 = 0;
                ViewBag.stpe3 = "AMOUNT TIMES QTY";
                ViewBag.pra3 = Math.Round(MethodStep255(No, codefilter, idtemplate), 2);
                ViewBag.ta3 = 0;
                ViewBag.stpe4 = "AMOUNT TIMES TAXABLE";
                ViewBag.pra4 = Math.Round(MethodStep382(No, codefilter, idtemplate), 2);
                ViewBag.ta4 = 0;
                actualizarpayrolljournalline(No, codefilter, idtemplate,  Convert.ToString( ViewBag.pra4));
                return PartialView(result);
            }

            return View();
        }


        ControlPlanilla.Models.PlanillaPayControls db = new ControlPlanilla.Models.PlanillaPayControls();
        [ValidateInput(false)]
        public ActionResult GridViewPartial1(string Employee_No)
        {
            var model = db.PayrollJournalLine.Where(s => s.EmployeeNo == Employee_No);
            if (model.Count() == 0)
            {
               
                model = db.PayrollJournalLine;
            }
            return PartialView("_GridViewPartial", model.ToList());
        }


        [HttpPost]
        [AllowAnonymous]
        public ActionResult GridViewPartial(string Employee_No)
        {
            var model = db.PayrollJournalLine.Where(s => s.EmployeeNo == Employee_No);
            if (model.Count() == 0)
            {
                model = db.PayrollJournalLine;
            }
            return PartialView("_GridViewPartial", model.ToList());
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult GridViewPartialAddNew([ModelBinder(typeof(DevExpressEditorsBinder))] ControlPlanilla.Models.PayrollJournalLine item)
        {
            var model = db.PayrollJournalLine;
            if (ModelState.IsValid)
            {
                try
                {
                    item.PayrollRunNoSeries = item.JournalTemplateName;
                    
                    model.Add(item);
                    db.SaveChanges();
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
                ViewData["EditError"] = "Please, correct all errors.";
            return PartialView("_GridViewPartial", model.ToList());
        }
        [HttpPost, ValidateInput(false)]
        public ActionResult GridViewPartialUpdate([ModelBinder(typeof(DevExpressEditorsBinder))] ControlPlanilla.Models.PayrollJournalLine item)
        {
            var model = db.PayrollJournalLine;
                            try
                {
                    var modelItem = model.FirstOrDefault(it => it.JournalTemplateName == item.JournalTemplateName &&  it.LineNumber==item.LineNumber);
                    if (modelItem != null)
                    {
                        modelItem.PayrollAmount = item.PayrollAmount;
                        modelItem.TaxableAmount = item.TaxableAmount;
                        modelItem.PayrollPostingGroup = item.PayrollPostingGroup == null ? "": item.PayrollPostingGroup;
                        db.Entry(modelItem).State = EntityState.Modified;
                        db.SaveChanges();
                    }
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            
            return PartialView("_GridViewPartial", model.ToList());
        }
        [HttpPost, ValidateInput(false)]
        public ActionResult GridViewPartialDelete(System.String JournalTemplateName)
        {
            var model = db.PayrollJournalLine;
            if (JournalTemplateName != null)
            {
                try
                {
                    var item = model.FirstOrDefault(it => it.JournalTemplateName == JournalTemplateName);
                    if (item != null)
                        model.Remove(item);
                    db.SaveChanges();
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            return PartialView("_GridViewPartial", model.ToList());
        }
    }
}