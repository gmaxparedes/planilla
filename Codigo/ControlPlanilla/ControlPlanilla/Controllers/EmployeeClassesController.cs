﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ControlPlanilla.Models;

namespace ControlPlanilla.Controllers
{
    public class EmployeeClassesController : Controller
    {
        private EmployeeEntities db = new EmployeeEntities();

        // GET: EmployeeClasses
        public ActionResult Index()
        {
            return View(db.EmployeeClass.ToList());
        }

        // GET: EmployeeClasses/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EmployeeClass employeeClass = db.EmployeeClass.Find(id);
            if (employeeClass == null)
            {
                return HttpNotFound();
            }
            return View(employeeClass);
        }

        // GET: EmployeeClasses/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: EmployeeClasses/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Code,Description")] EmployeeClass employeeClass)
        {
            if (ModelState.IsValid)
            {
                
                db.EmployeeClass.Add(employeeClass);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(employeeClass);
        }

        // GET: EmployeeClasses/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EmployeeClass employeeClass = db.EmployeeClass.Find(id);
            if (employeeClass == null)
            {
                return HttpNotFound();
            }
            return View(employeeClass);
        }

        // POST: EmployeeClasses/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Code,Description")] EmployeeClass employeeClass)
        {
            if (ModelState.IsValid)
            {
                EmployeeClass grabar = db.EmployeeClass.Find(employeeClass.Code);
                grabar.Description = employeeClass.Description;
                db.Entry(grabar).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(employeeClass);
        }

        // GET: EmployeeClasses/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EmployeeClass employeeClass = db.EmployeeClass.Find(id);
            if (employeeClass == null)
            {
                return HttpNotFound();
            }
            return View(employeeClass);
        }

        // POST: EmployeeClasses/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            EmployeeClass employeeClass = db.EmployeeClass.Find(id);
            db.EmployeeClass.Remove(employeeClass);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
