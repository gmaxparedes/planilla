﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ControlPlanilla.Models;

namespace ControlPlanilla.Controllers
{
    public class DirectDepositTypeToExportsController : AuditoriaController
    {

        // GET: DirectDepositTypeToExports
        public ActionResult Index()
        {
            return View(dbDDL.DirectDepositTypeToExport.ToList());
        }

        // GET: DirectDepositTypeToExports/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DirectDepositTypeToExport directDepositTypeToExport = dbDDL.DirectDepositTypeToExport.Find(id);
            if (directDepositTypeToExport == null)
            {
                return HttpNotFound();
            }
            return View(directDepositTypeToExport);
        }

        // GET: DirectDepositTypeToExports/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: DirectDepositTypeToExports/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "DESCRIPCION")] DirectDepositTypeToExport directDepositTypeToExport)
        {
            if (ModelState.IsValid)
            {
                DirectDepositTypeToExport guardar = new DirectDepositTypeToExport();

                guardar.DESCRIPCION = directDepositTypeToExport.DESCRIPCION;
                guardar.USUA_CREA = Usuario;
                guardar.FECH_CREA = Ahora;

                dbDDL.DirectDepositTypeToExport.Add(guardar);
                dbDDL.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(directDepositTypeToExport);
        }

        // GET: DirectDepositTypeToExports/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DirectDepositTypeToExport directDepositTypeToExport = dbDDL.DirectDepositTypeToExport.Find(id);
            if (directDepositTypeToExport == null)
            {
                return HttpNotFound();
            }
            return View(directDepositTypeToExport);
        }

        // POST: DirectDepositTypeToExports/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID_TYPE_EXPORT, DESCRIPCION")] DirectDepositTypeToExport directDepositTypeToExport)
        {
            if (ModelState.IsValid)
            {
                DirectDepositTypeToExport guardar = dbDDL.DirectDepositTypeToExport.Find(directDepositTypeToExport.ID_TYPE_EXPORT); ;

                guardar.DESCRIPCION = directDepositTypeToExport.DESCRIPCION;
                guardar.USUA_ACTUA = Usuario;
                guardar.FECH_ACTUA = Ahora;

                dbDDL.Entry(guardar).State = EntityState.Modified;
                dbDDL.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(directDepositTypeToExport);
        }

        // GET: DirectDepositTypeToExports/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DirectDepositTypeToExport directDepositTypeToExport = dbDDL.DirectDepositTypeToExport.Find(id);
            if (directDepositTypeToExport == null)
            {
                return HttpNotFound();
            }
            return View(directDepositTypeToExport);
        }

        // POST: DirectDepositTypeToExports/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            DirectDepositTypeToExport directDepositTypeToExport = dbDDL.DirectDepositTypeToExport.Find(id);
            dbDDL.DirectDepositTypeToExport.Remove(directDepositTypeToExport);
            dbDDL.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                dbDDL.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
