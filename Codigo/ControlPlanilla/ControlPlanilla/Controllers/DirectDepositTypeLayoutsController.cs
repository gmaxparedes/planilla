﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ControlPlanilla.Models;

namespace ControlPlanilla.Controllers
{
    public class DirectDepositTypeLayoutsController : AuditoriaController
    {
        

        // GET: DirectDepositTypeLayouts
        public ActionResult Index()
        {
            return View(dbDDL.DirectDepositTypeLayout.ToList());
        }

        // GET: DirectDepositTypeLayouts/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DirectDepositTypeLayout directDepositTypeLayout = dbDDL.DirectDepositTypeLayout.Find(id);
            if (directDepositTypeLayout == null)
            {
                return HttpNotFound();
            }
            return View(directDepositTypeLayout);
        }

        // GET: DirectDepositTypeLayouts/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: DirectDepositTypeLayouts/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "DESCRIPCION")] DirectDepositTypeLayout directDepositTypeLayout)
        {
            if (ModelState.IsValid)
            {
                directDepositTypeLayout.USUA_CREA = Usuario;
                directDepositTypeLayout.FECH_CREA = Ahora;

                dbDDL.DirectDepositTypeLayout.Add(directDepositTypeLayout);
                dbDDL.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(directDepositTypeLayout);
        }

        // GET: DirectDepositTypeLayouts/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DirectDepositTypeLayout directDepositTypeLayout = dbDDL.DirectDepositTypeLayout.Find(id);
            if (directDepositTypeLayout == null)
            {
                return HttpNotFound();
            }
            return View(directDepositTypeLayout);
        }

        // POST: DirectDepositTypeLayouts/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID_LAYOUT, DESCRIPCION")] DirectDepositTypeLayout directDepositTypeLayout)
        {
            if (ModelState.IsValid)
            {
                DirectDepositTypeLayout guardar = dbDDL.DirectDepositTypeLayout.Find(directDepositTypeLayout.ID_LAYOUT);
                guardar.DESCRIPCION = directDepositTypeLayout.DESCRIPCION;
                guardar.USUA_ACTUA = Usuario;
                guardar.FECH_ACTUA = Ahora;
                dbDDL.Entry(guardar).State = EntityState.Modified;
                dbDDL.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(directDepositTypeLayout);
        }

        // GET: DirectDepositTypeLayouts/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DirectDepositTypeLayout directDepositTypeLayout = dbDDL.DirectDepositTypeLayout.Find(id);
            if (directDepositTypeLayout == null)
            {
                return HttpNotFound();
            }
            return View(directDepositTypeLayout);
        }

        // POST: DirectDepositTypeLayouts/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            DirectDepositTypeLayout directDepositTypeLayout = dbDDL.DirectDepositTypeLayout.Find(id);
            dbDDL.DirectDepositTypeLayout.Remove(directDepositTypeLayout);
            dbDDL.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                dbDDL.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
