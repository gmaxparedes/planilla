﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ControlPlanilla.Models;

namespace ControlPlanilla.Controllers
{
    public class EmployeeRatesController : AuditoriaController
    {
        private PlanillaCicloPago db = new PlanillaCicloPago();
        
        private EmployeeEntities dbEmployee = new EmployeeEntities();
        

        // GET: EmployeeRates
        public ActionResult Index()
        {
            return View(db.EmployeeRate.ToList());
        }

        // GET: EmployeeRates/Details/5
        public ActionResult Details(string id, string id2)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EmployeeRate employeeRate = db.EmployeeRate.Find(id,id2);
            if (employeeRate == null)
            {
                return HttpNotFound();
            }
            return View(employeeRate);
        }

        // GET: EmployeeRates/Create
        public ActionResult Create()
        {
            ViewBag.EmployeeNo = new SelectList(ObtenerEmployee(), "No", "FirstName", 0);
            ViewBag.Employer = new SelectList(ObtenerEmployer(), "No", "Name", 0);
            ViewBag.PayrollRateCode = new SelectList(ObtenerRates(), "Code", "Description", 0);
            return View();
        }
        public List<EmployeeView> ObtenerEmployee()
        {
            List<EmployeeView> listaINI = new List<EmployeeView> {
                new EmployeeView { No  = "0",   FirstName= "Seleccione una opción" }
            };
            var listaPrincipal = dbEmployee.Employee.Select(r => new EmployeeView
            {
                No = r.No,
                FirstName = r.FirstName + " " + r.MiddleName + " " + r.LastName + " " + r.SecondLastName
            }).ToList();
            var listFinal = listaINI.Union(listaPrincipal);
            return listFinal.ToList();
        }

        public List<EmployerView> ObtenerEmployer()
        {
            List<EmployerView> listaINI = new List<EmployerView> {
                new EmployerView { No  = "0",   Name= "Seleccione una opción" }
            };
            var listaPrincipal = dbEmployee.Employer.Select(r => new EmployerView
            {
                No = r.No,
                Name = r.Name
            }).ToList();
            var listFinal = listaINI.Union(listaPrincipal);
            return listFinal.ToList();
        }
        public List<PayrollratesView> ObtenerRates()
        {
            List<PayrollratesView> listaINI = new List<PayrollratesView> {
                new PayrollratesView { Code  = "0",  Description  = "Seleccione una opción" }
            };
            var listaPrincipal = dbPayControl.PayrollRate.Select(r => new PayrollratesView
            {
                Code = r.Code,
                Description = r.Description
            }).ToList();
            var listFinal = listaINI.Union(listaPrincipal);
            return listFinal.ToList();
        }
        
        // POST: EmployeeRates/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "EmployeeNo,PayrollRateCode,EffectiveDate,Amount,UnitofMeasureCode,ActivationCode,Employer,BorrarPosteo,Referencia,PayControlRel")] EmployeeRate employeeRate)
        {
            if (ModelState.IsValid)
            {
                employeeRate.ActivationCode = "";
                db.EmployeeRate.Add(employeeRate);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(employeeRate);
        }

        // GET: EmployeeRates/Edit/5
        public ActionResult Edit(string id, string id2, string id3)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EmployeeRate employeeRate = db.EmployeeRate.Find(id, id2, Convert.ToDateTime(id3), "");
            ViewBag.EmployeeNo = new SelectList(ObtenerEmployee(), "No", "FirstName", employeeRate.EmployeeNo);
            ViewBag.Employer = new SelectList(ObtenerEmployer(), "No", "Name", employeeRate.Employer);
            ViewBag.PayrollRateCode = new SelectList(ObtenerRates(), "Code", "Description", employeeRate.PayrollRateCode);
            
            if (employeeRate == null)
            {
                return HttpNotFound();
            }
            return View(employeeRate);
        }

        // POST: EmployeeRates/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "EmployeeNo,PayrollRateCode,EffectiveDate,Amount,UnitofMeasureCode,ActivationCode,Employer,BorrarPosteo,Referencia,PayControlRel")] EmployeeRate employeeRate)
        {
            if (ModelState.IsValid)
            {
                employeeRate.ActivationCode = "";
                db.Entry(employeeRate).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(employeeRate);
        }

        // GET: EmployeeRates/Delete/5
        public ActionResult Delete(string id, string id2, string id3)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EmployeeRate employeeRate = db.EmployeeRate.Find(id,id2, Convert.ToDateTime(id3),"");
            if (employeeRate == null)
            {
                return HttpNotFound();
            }
            return View(employeeRate);
        }

        // POST: EmployeeRates/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id, string id2, string id3)
        {
            EmployeeRate employeeRate = db.EmployeeRate.Find(id,id2, Convert.ToDateTime(id3),"");
            db.EmployeeRate.Remove(employeeRate);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
