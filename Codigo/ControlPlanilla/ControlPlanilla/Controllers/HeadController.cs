﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ControlPlanilla.Models;


namespace ControlPlanilla.Controllers
{
    public class HeadController : AuditoriaController
    {
        // GET: Head
        public ActionResult Index()
        {
            //Usuario= Session["usuario"].ToString();
            //CdEmpresa = Session["cd_empresa"].ToString();
            CargarMenu(Usuario);

            if (TempData["MensajeOk"] != null)
            {
                ViewBag.MensajeOk = TempData["MensajeOk"].ToString();
            }
            if (TempData["MensajeAlerta"] != null)
            {
                ViewBag.MensajeAlerta = TempData["MensajeAlerta"].ToString();
            }
            if (TempData["MensajeError"] != null)
            {
                ViewBag.MensajeError = TempData["MensajeError"].ToString();
            }

            return View();
        }

        private void CargarMenu(string Usuario)
        {
            ListaMenusSubMenus ListaMenu = new ListaMenusSubMenus();
            GLB_USUARIOS_PERFIL p = dbe.GLB_USUARIOS_PERFIL.Where(up => up.CD_CODIGO_USUARIO == Usuario &&
            up.CD_EMPRESA == CdEmpresa).FirstOrDefault();

            if (p != null)
            {
                var Menu = dbe.GLB_OPCI_SIST_PERFIL.Where(op =>
                op.GLB_CONF_OPCI_SIST.GLB_MENUS_SISTEMAS.CD_CODI_MENU == op.CD_CODI_MENU &&
                op.CD_CODIGO_PERFIL == p.CD_CODIGO_PERFIL && op.GLB_CONF_OPCI_SIST.GLB_MENUS_SISTEMAS.CD_ESTADO_MENU &&
                op.CD_EMPRESA == CdEmpresa
                ).Select(c => new MenusViewModels
                {
                    DS_DESC_MENU = c.GLB_CONF_OPCI_SIST.GLB_MENUS_SISTEMAS.DS_DESC_MENU,
                    CD_CODI_SIST = c.CD_CODI_SIST,
                    DS_CODI_SIST = c.GLB_CONF_OPCI_SIST.GLB_SISTEMAS.DS_NOMB_SIST
                }
                ).Distinct().ToList();

                var subMenu = dbe.GLB_OPCI_SIST_PERFIL.Where(sp => sp.GLB_CONF_OPCI_SIST.GLB_OPCIONES_SISTEMA.CD_CODI_OPCI_SIST == sp.CD_CODI_OPCI_SIST
                && sp.GLB_CONF_OPCI_SIST.GLB_MENUS_SISTEMAS.CD_CODI_MENU == sp.CD_CODI_MENU
                && sp.CD_CODIGO_PERFIL == p.CD_CODIGO_PERFIL && sp.CD_EMPRESA == CdEmpresa
                && sp.Estado && sp.GLB_CONF_OPCI_SIST.GLB_MENUS_SISTEMAS.CD_ESTADO_MENU
                && sp.GLB_CONF_OPCI_SIST.GLB_OPCIONES_SISTEMA.CD_ESTADO_OPCION_SISTEMA).Select(c => new SubMenusViewModels
                {
                    DS_NOMBRE_FORMA = c.GLB_CONF_OPCI_SIST.GLB_OPCIONES_SISTEMA.DS_NOMBRE_FORMA,
                    DS_DESC_MENU = c.GLB_CONF_OPCI_SIST.GLB_MENUS_SISTEMAS.DS_DESC_MENU,
                    DS_LINK_URL = c.GLB_CONF_OPCI_SIST.GLB_OPCIONES_SISTEMA.DS_LINK_URL,
                    CD_CODI_OPCI_SIST = c.GLB_CONF_OPCI_SIST.GLB_OPCIONES_SISTEMA.CD_CODI_OPCI_SIST,
                    CD_INDI_REFRE_CAMP = c.GLB_CONF_OPCI_SIST.GLB_OPCIONES_SISTEMA.CD_INDI_REFRE_CAMP,
                    CD_CODI_SIST = c.GLB_CONF_OPCI_SIST.GLB_SISTEMAS.CD_CODI_SIST,
                    DS_CODI_SIST = c.GLB_CONF_OPCI_SIST.GLB_SISTEMAS.DS_NOMB_SIST
                }
                ).Distinct().OrderBy(v => v.DS_DESC_MENU).ThenBy(s => s.CD_CODI_OPCI_SIST).ToList();

                //Pasamos los datos de menus y sub menus
                ViewBag.PMenu = Menu.Select(s => s.DS_DESC_MENU).Distinct().ToList();
                ViewBag.Menu = Menu.ToList();
                ViewBag.SubMenu = subMenu.ToList();
                ViewBag.Perfil = p.CD_CODIGO_PERFIL;
            }
            else
            {
                ViewBag.Menu = null;
                ViewBag.SubMenu = null;
            }

        }
    }
}