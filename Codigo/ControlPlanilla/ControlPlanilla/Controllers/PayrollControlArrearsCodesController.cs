﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ControlPlanilla.Models;

namespace ControlPlanilla.Controllers
{
    public class PayrollControlArrearsCodesController : AuditoriaController
    {
        

        // GET: PayrollControlArrearsCodes
        public ActionResult Index()
        {
            return View(dbPayControl.PayrollControlArrearsCode.ToList());
        }

        // GET: PayrollControlArrearsCodes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PayrollControlArrearsCode payrollControlArrearsCode = dbPayControl.PayrollControlArrearsCode.Find(id);
            if (payrollControlArrearsCode == null)
            {
                return HttpNotFound();
            }
            return View(payrollControlArrearsCode);
        }

        // GET: PayrollControlArrearsCodes/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: PayrollControlArrearsCodes/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID_ARREARS,DESCRIPCION")] PayrollControlArrearsCode payrollControlArrearsCode)
        {
            if (ModelState.IsValid)
            {
                PayrollControlArrearsCode guardar = new PayrollControlArrearsCode();
                guardar.DESCRIPCION = payrollControlArrearsCode.DESCRIPCION;
                guardar.USUA_CREA = Usuario;
                guardar.FECH_CREA = Ahora;
                dbPayControl.PayrollControlArrearsCode.Add(guardar);
                dbPayControl.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(payrollControlArrearsCode);
        }

        // GET: PayrollControlArrearsCodes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PayrollControlArrearsCode payrollControlArrearsCode = dbPayControl.PayrollControlArrearsCode.Find(id);
            if (payrollControlArrearsCode == null)
            {
                return HttpNotFound();
            }
            return View(payrollControlArrearsCode);
        }

        // POST: PayrollControlArrearsCodes/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID_ARREARS,DESCRIPCION")] PayrollControlArrearsCode payrollControlArrearsCode)
        {
            if (ModelState.IsValid)
            {
                PayrollControlArrearsCode guardar = dbPayControl.PayrollControlArrearsCode.Find(payrollControlArrearsCode.ID_ARREARS);
                guardar.DESCRIPCION = payrollControlArrearsCode.DESCRIPCION;
                guardar.USUA_ACTUA= Usuario;
                guardar.FECH_ACTUA = Ahora;
                dbPayControl.Entry(guardar).State = EntityState.Modified;
                dbPayControl.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(payrollControlArrearsCode);
        }

        // GET: PayrollControlArrearsCodes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PayrollControlArrearsCode payrollControlArrearsCode = dbPayControl.PayrollControlArrearsCode.Find(id);
            if (payrollControlArrearsCode == null)
            {
                return HttpNotFound();
            }
            return View(payrollControlArrearsCode);
        }

        // POST: PayrollControlArrearsCodes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            PayrollControlArrearsCode payrollControlArrearsCode = dbPayControl.PayrollControlArrearsCode.Find(id);
            dbPayControl.PayrollControlArrearsCode.Remove(payrollControlArrearsCode);
            dbPayControl.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                dbPayControl.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
