﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ControlPlanilla.Models;

namespace ControlPlanilla.Controllers
{
    public class AREAS_EMPRESAController : AuditoriaController
    {
        private GlobalEntities db = new GlobalEntities();

        // GET: AREAS_EMPRESA
        public ActionResult Index()
        {
            return View(dbMenus.AREAS_EMPRESA.ToList());
        }

        // GET: AREAS_EMPRESA/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AREAS_EMPRESA aREAS_EMPRESA = dbMenus.AREAS_EMPRESA.Find(id);
            if (aREAS_EMPRESA == null)
            {
                return HttpNotFound();
            }
            return View(aREAS_EMPRESA);
        }

        // GET: AREAS_EMPRESA/Create
        public ActionResult Create()
        {
            var listaPaises = dbMenus.PAISES.Select(a => new PaisesViewModels { CD_PAIS = a.CD_PAIS, DS_PAIS = a.DS_NOMBREPAIS }).ToList();
            var listaDepartamentos = dbMenus.DEPARTAMENTOS.Select(a => new DepartamentosView  { ID_DEPTO = a.ID_DEPTO, NOMBRE_DEPTO = a.NOMBRE_DEPTO }).ToList();
            ViewBag.cmbPaises = listaPaises;
            ViewBag.cmbDepartamentos = listaDepartamentos;
            return View();
        }

        // POST: AREAS_EMPRESA/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID_AREA,CD_EMPRESA,DESCRIPCION")] AREAS_EMPRESA aREAS_EMPRESA)
        {
            if (ModelState.IsValid)
            {
                aREAS_EMPRESA.USUA_CREA = Usuario;
                aREAS_EMPRESA.FECH_CREA = Ahora;
                dbMenus.AREAS_EMPRESA.Add(aREAS_EMPRESA);
                dbMenus.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(aREAS_EMPRESA);
        }

        // GET: AREAS_EMPRESA/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AREAS_EMPRESA aREAS_EMPRESA = dbMenus.AREAS_EMPRESA.Find(id);
            if (aREAS_EMPRESA == null)
            {
                return HttpNotFound();
            }
            else {
                var listaPais = from ar in dbMenus.AREAS_EMPRESA
                                    join rl in dbMenus.RELACION_EMPRESA_PAIS on ar.CD_EMPRESA equals rl.CD_EMPRESA
                                    where ar.ID_AREA == id
                                    select new { CD_PAIS = rl.CD_PAIS};
                var infon = listaPais.First();
                ViewBag.CD_PAIS = infon.CD_PAIS;
                var listaPaises = dbMenus.PAISES.Select(a => new PaisesViewModels { CD_PAIS = a.CD_PAIS, DS_PAIS = a.DS_NOMBREPAIS }).ToList();
                var listaEmpresas = from rl1 in dbMenus.RELACION_EMPRESA_PAIS
                                  join emp in dbMenus.EMPRESAS on rl1.CD_EMPRESA equals emp.CD_EMPRESA
                                  where rl1.CD_EMPRESA == aREAS_EMPRESA.CD_EMPRESA
                                  select new EmpresasView {CD_EMPRESA = emp.CD_EMPRESA,DS_EMPRESA = emp.DS_EMPRESA };
                ViewBag.cmbPaises = listaPaises;
                ViewBag.cmbEMPRESA = listaEmpresas.ToList();
            }
            return View(aREAS_EMPRESA);
        }

        // POST: AREAS_EMPRESA/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID_AREA,CD_EMPRESA,DESCRIPCION")] AREAS_EMPRESA aREAS_EMPRESA)
        {
            if (ModelState.IsValid)
            {
                AREAS_EMPRESA grabar = dbMenus.AREAS_EMPRESA.Find(aREAS_EMPRESA.ID_AREA);
                grabar.FECH_ACTU = Ahora;
                grabar.DESCRIPCION = aREAS_EMPRESA.DESCRIPCION;
                grabar.CD_EMPRESA = aREAS_EMPRESA.CD_EMPRESA;
                grabar.USUA_ACTU = Usuario;
                dbMenus.Entry(grabar).State = EntityState.Modified;
                dbMenus.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(aREAS_EMPRESA);
        }

        // GET: AREAS_EMPRESA/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AREAS_EMPRESA aREAS_EMPRESA = dbMenus.AREAS_EMPRESA.Find(id);
            if (aREAS_EMPRESA == null)
            {
                return HttpNotFound();
            }
            return View(aREAS_EMPRESA);
        }

        // POST: AREAS_EMPRESA/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            AREAS_EMPRESA aREAS_EMPRESA = dbMenus.AREAS_EMPRESA.Find(id);
            dbMenus.AREAS_EMPRESA.Remove(aREAS_EMPRESA);
            dbMenus.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        
        public JsonResult ObtenerEmpresas(string cd_pais)
        {

            var listaEmpresas = from rl1 in dbMenus.RELACION_EMPRESA_PAIS
                                join emp in dbMenus.EMPRESAS on rl1.CD_EMPRESA equals emp.CD_EMPRESA
                                where rl1.CD_PAIS == cd_pais
                                select new { CD_EMPRESA = emp.CD_EMPRESA, DS_EMPRESA = emp.DS_EMPRESA };

            return Json(listaEmpresas, JsonRequestBehavior.AllowGet);
        }
    }
}
