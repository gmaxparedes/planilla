﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ControlPlanilla.Models;

namespace ControlPlanilla.Controllers
{
    public class PayrollControlTypesController : AuditoriaController
    {
        

        // GET: PayrollControlTypes
        public ActionResult Index()
        {
            return View(dbPayControl.PayrollControlType.ToList());
        }

        // GET: PayrollControlTypes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PayrollControlType payrollControlType = dbPayControl.PayrollControlType.Find(id);
            if (payrollControlType == null)
            {
                return HttpNotFound();
            }
            return View(payrollControlType);
        }

        // GET: PayrollControlTypes/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: PayrollControlTypes/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "DESCRIPCION")] PayrollControlType payrollControlType)
        {
            if (ModelState.IsValid)
            {
                PayrollControlType guardar = new PayrollControlType();
                guardar.DESCRIPCION = payrollControlType.DESCRIPCION;
                guardar.USUA_CREA = Usuario;
                guardar.FECH_CREA = Ahora;
                dbPayControl.PayrollControlType.Add(guardar);
                dbPayControl.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(payrollControlType);
        }

        // GET: PayrollControlTypes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PayrollControlType payrollControlType = dbPayControl.PayrollControlType.Find(id);
            if (payrollControlType == null)
            {
                return HttpNotFound();
            }
            return View(payrollControlType);
        }

        // POST: PayrollControlTypes/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID_TYPE,DESCRIPCION")] PayrollControlType payrollControlType)
        {
            if (ModelState.IsValid)
            {
                PayrollControlType guardar = dbPayControl.PayrollControlType.Find(payrollControlType.ID_TYPE);
                guardar.DESCRIPCION = payrollControlType.DESCRIPCION;
                guardar.USUA_ACTUA = Usuario;
                guardar.FECH_ACTUA = Ahora;
                dbPayControl.Entry(guardar).State = EntityState.Modified;
                dbPayControl.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(payrollControlType);
        }

        // GET: PayrollControlTypes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PayrollControlType payrollControlType = dbPayControl.PayrollControlType.Find(id);
            if (payrollControlType == null)
            {
                return HttpNotFound();
            }
            return View(payrollControlType);
        }

        // POST: PayrollControlTypes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            PayrollControlType payrollControlType = dbPayControl.PayrollControlType.Find(id);
            dbPayControl.PayrollControlType.Remove(payrollControlType);
            dbPayControl.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                dbPayControl.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
