﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ControlPlanilla.Models;

namespace ControlPlanilla.Controllers
{
    public class PayrollControlNetPayTestsController : AuditoriaController
    {
        

        // GET: PayrollControlNetPayTests
        public ActionResult Index()
        {
            return View(dbPayControl.PayrollControlNetPayTest.ToList());
        }

        // GET: PayrollControlNetPayTests/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PayrollControlNetPayTest payrollControlNetPayTest = dbPayControl.PayrollControlNetPayTest.Find(id);
            if (payrollControlNetPayTest == null)
            {
                return HttpNotFound();
            }
            return View(payrollControlNetPayTest);
        }

        // GET: PayrollControlNetPayTests/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: PayrollControlNetPayTests/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID_NETPAYTEST,DESCRIPCION")] PayrollControlNetPayTest payrollControlNetPayTest)
        {
            if (ModelState.IsValid)
            {
                PayrollControlNetPayTest guardar = new PayrollControlNetPayTest();
                guardar.DESCRIPCION = payrollControlNetPayTest.DESCRIPCION;
                guardar.USUA_CREA = Usuario;
                guardar.FECH_CREA = Ahora;
                dbPayControl.PayrollControlNetPayTest.Add(guardar);
                dbPayControl.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(payrollControlNetPayTest);
        }

        // GET: PayrollControlNetPayTests/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PayrollControlNetPayTest payrollControlNetPayTest = dbPayControl.PayrollControlNetPayTest.Find(id);
            if (payrollControlNetPayTest == null)
            {
                return HttpNotFound();
            }
            return View(payrollControlNetPayTest);
        }

        // POST: PayrollControlNetPayTests/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID_NETPAYTEST,DESCRIPCION")] PayrollControlNetPayTest payrollControlNetPayTest)
        {
            if (ModelState.IsValid)
            {
                PayrollControlNetPayTest guardar = dbPayControl.PayrollControlNetPayTest.Find(payrollControlNetPayTest.ID_NETPAYTEST);
                guardar.DESCRIPCION = payrollControlNetPayTest.DESCRIPCION;
                guardar.USUA_ACTUA= Usuario;
                guardar.FECH_ACTUA = Ahora;
                dbPayControl.Entry(guardar).State = EntityState.Modified;
                dbPayControl.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(payrollControlNetPayTest);
        }

        // GET: PayrollControlNetPayTests/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PayrollControlNetPayTest payrollControlNetPayTest = dbPayControl.PayrollControlNetPayTest.Find(id);
            if (payrollControlNetPayTest == null)
            {
                return HttpNotFound();
            }
            return View(payrollControlNetPayTest);
        }

        // POST: PayrollControlNetPayTests/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            PayrollControlNetPayTest payrollControlNetPayTest = dbPayControl.PayrollControlNetPayTest.Find(id);
            dbPayControl.PayrollControlNetPayTest.Remove(payrollControlNetPayTest);
            dbPayControl.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                dbPayControl.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
