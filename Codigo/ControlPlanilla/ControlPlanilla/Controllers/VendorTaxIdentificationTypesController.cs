﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ControlPlanilla.Models;

namespace ControlPlanilla.Controllers
{
    public class VendorTaxIdentificationTypesController : AuditoriaController
    {
        

        // GET: VendorTaxIdentificationTypes
        public ActionResult Index()
        {
            return View(dbVendor.VendorTaxIdentificationType.ToList());
        }

        // GET: VendorTaxIdentificationTypes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VendorTaxIdentificationType vendorTaxIdentificationType = dbVendor.VendorTaxIdentificationType.Find(id);
            if (vendorTaxIdentificationType == null)
            {
                return HttpNotFound();
            }
            return View(vendorTaxIdentificationType);
        }

        // GET: VendorTaxIdentificationTypes/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: VendorTaxIdentificationTypes/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID_TAXTYPE,DESCRIPCION")] VendorTaxIdentificationType vendorTaxIdentificationType)
        {
            if (ModelState.IsValid)
            {
                VendorTaxIdentificationType guardar = new VendorTaxIdentificationType();
                guardar.DESCRIPCION = vendorTaxIdentificationType.DESCRIPCION;
                guardar.USUA_CREA = Usuario;
                guardar.FECH_CREA = Ahora;
                dbVendor.VendorTaxIdentificationType.Add(guardar);
                dbVendor.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(vendorTaxIdentificationType);
        }

        // GET: VendorTaxIdentificationTypes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VendorTaxIdentificationType vendorTaxIdentificationType = dbVendor.VendorTaxIdentificationType.Find(id);
            if (vendorTaxIdentificationType == null)
            {
                return HttpNotFound();
            }
            return View(vendorTaxIdentificationType);
        }

        // POST: VendorTaxIdentificationTypes/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID_TAXTYPE,DESCRIPCION")] VendorTaxIdentificationType vendorTaxIdentificationType)
        {
            if (ModelState.IsValid)
            {
                VendorTaxIdentificationType guardar = dbVendor.VendorTaxIdentificationType.Find(vendorTaxIdentificationType.ID_TAXTYPE);
                guardar.DESCRIPCION = vendorTaxIdentificationType.DESCRIPCION;
                guardar.USUA_ACTUA = Usuario;
                guardar.FECH_ACTUA = Ahora;
                dbVendor.Entry(guardar).State = EntityState.Modified;
                dbVendor.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(vendorTaxIdentificationType);
        }

        // GET: VendorTaxIdentificationTypes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VendorTaxIdentificationType vendorTaxIdentificationType = dbVendor.VendorTaxIdentificationType.Find(id);
            if (vendorTaxIdentificationType == null)
            {
                return HttpNotFound();
            }
            return View(vendorTaxIdentificationType);
        }

        // POST: VendorTaxIdentificationTypes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            VendorTaxIdentificationType vendorTaxIdentificationType = dbVendor.VendorTaxIdentificationType.Find(id);
            dbVendor.VendorTaxIdentificationType.Remove(vendorTaxIdentificationType);
            dbVendor.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                dbVendor.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
