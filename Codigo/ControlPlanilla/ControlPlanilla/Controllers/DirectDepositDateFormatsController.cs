﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ControlPlanilla.Models;

namespace ControlPlanilla.Controllers
{
    public class DirectDepositDateFormatsController : AuditoriaController
    {

        // GET: DirectDepositDateFormats
        public ActionResult Index()
        {
            return View(dbDDL.DirectDepositDateFormat.ToList());
        }

        // GET: DirectDepositDateFormats/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DirectDepositDateFormat directDepositDateFormat = dbDDL.DirectDepositDateFormat.Find(id);
            if (directDepositDateFormat == null)
            {
                return HttpNotFound();
            }
            return View(directDepositDateFormat);
        }

        // GET: DirectDepositDateFormats/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: DirectDepositDateFormats/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID_DATE_FORMAT,DESCRIPCION")] DirectDepositDateFormat directDepositDateFormat)
        {
            if (ModelState.IsValid)
            {
                directDepositDateFormat.USUA_CREA = Usuario;
                directDepositDateFormat.FECH_CREA = Ahora;
                dbDDL.DirectDepositDateFormat.Add(directDepositDateFormat);
                dbDDL.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(directDepositDateFormat);
        }

        // GET: DirectDepositDateFormats/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DirectDepositDateFormat directDepositDateFormat = dbDDL.DirectDepositDateFormat.Find(id);
            if (directDepositDateFormat == null)
            {
                return HttpNotFound();
            }
            return View(directDepositDateFormat);
        }

        // POST: DirectDepositDateFormats/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID_DATE_FORMAT,DESCRIPCION")] DirectDepositDateFormat directDepositDateFormat)
        {
            if (ModelState.IsValid)
            {
                var guardar = dbDDL.DirectDepositDateFormat.Find(directDepositDateFormat.ID_DATE_FORMAT);
                guardar.DESCRIPCION = directDepositDateFormat.DESCRIPCION;
                guardar.USUA_ACTUA = Usuario;
                guardar.FECH_ACTUA = Ahora;

                dbDDL.Entry(guardar).State = EntityState.Modified;
                dbDDL.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(directDepositDateFormat);
        }

        // GET: DirectDepositDateFormats/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DirectDepositDateFormat directDepositDateFormat = dbDDL.DirectDepositDateFormat.Find(id);
            if (directDepositDateFormat == null)
            {
                return HttpNotFound();
            }
            return View(directDepositDateFormat);
        }

        // POST: DirectDepositDateFormats/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            DirectDepositDateFormat directDepositDateFormat = dbDDL.DirectDepositDateFormat.Find(id);
            dbDDL.DirectDepositDateFormat.Remove(directDepositDateFormat);
            dbDDL.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                dbDDL.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
