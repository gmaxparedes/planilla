﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ControlPlanilla.Models;
using PagedList;

namespace ControlPlanilla.Controllers
{
    public class CalculationOrdersController : AuditoriaController
    {
        private PlanillaPayControls db = new PlanillaPayControls();

        // GET: CalculationOrders
        public ActionResult Index()
        {
            return View(db.CalculationOrder.ToList());
        }

        // GET: CalculationOrders/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CalculationOrder calculationOrder = db.CalculationOrder.Find(id);
            if (calculationOrder == null)
            {
                return HttpNotFound();
            }
            return View(calculationOrder);
        }

        // GET: CalculationOrders/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: CalculationOrders/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Code,Description,MasterOrder,PayrollControls,SynchRequired")] CalculationOrder calculationOrder)
        {
            if (ModelState.IsValid)
            {
                db.CalculationOrder.Add(calculationOrder);
                db.SaveChanges();
                TempData["MensajeOK"] = "Registro creado";
                return RedirectToAction("Index");
            }

            return View(calculationOrder);
        }

        // GET: CalculationOrders/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CalculationOrder calculationOrder = db.CalculationOrder.Find(id);
            if (calculationOrder == null)
            {
                return HttpNotFound();
            }
            return View(calculationOrder);
        }

        // POST: CalculationOrders/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Code,Description,MasterOrder,PayrollControls,SynchRequired")] CalculationOrder calculationOrder)
        {
            if (ModelState.IsValid)
            {
                db.Entry(calculationOrder).State = EntityState.Modified;
                db.SaveChanges();
                TempData["MensajeOK"] = "Registro actualizado";
                return RedirectToAction("Index");
            }
            return View(calculationOrder);
        }

        // GET: CalculationOrders/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CalculationOrder calculationOrder = db.CalculationOrder.Find(id);
            if (calculationOrder == null)
            {
                return HttpNotFound();
            }
            return View(calculationOrder);
        }

        // POST: CalculationOrders/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            CalculationOrder calculationOrder = db.CalculationOrder.Find(id);
            db.CalculationOrder.Remove(calculationOrder);
            db.SaveChanges();
            TempData["MensajeOK"] = "Registro eliminado";
            return RedirectToAction("Index");
        }

        [HttpPost]
        public PartialViewResult CalculationOrderControls(string Code, int? page)
        {
            try
            {
                var calculationOrderControl = db.CalculationOrderControl.Where(c => c.CalculationOrderCode == Code)
                    .Include(c => c.PayrollControl).ToList();
                int pageNumber = (page ?? 1);
                var result = calculationOrderControl.ToPagedList(pageNumber, pageSize);
                ViewBag.Code = Code;
                ViewBag.PayrollControl = new SelectList(db.PayrollControl, "Code", "Name");
                return PartialView(result);
            }
            catch (SystemException ex)
            {
                logRepository.CapturarTodoError(ex);
            }
            return PartialView();
        }

        [HttpPost]
        public bool CrearOrder([Bind(Include = "CalculationOrderCode, PayrollControlCode, AnotherOrderUses, MasterOrder")] CalculationOrderControl calculationOrderControl)
        {
            bool result = false;
            try
            {
                if (ModelState.IsValid)
                {
                    var registro = db.CalculationOrderControl.Where(c => c.CalculationOrderCode == calculationOrderControl.CalculationOrderCode).ToList();
                    int Num = 1;
                    int Min = 0;
                    int Max = 0;
                    if (registro.Count > 0 )
                    {
                        Min= registro.Select(c => c.OrderNo).Min();
                        Max = registro.Select(c => c.OrderNo).Max();
                        var arreglo = registro.Select(c => c.OrderNo).ToList();
                        for (int i = 0; i < arreglo.Count; i++)
                        {
                            if ((i+1) != arreglo[i])
                            {
                                Num = i+1;
                                break;
                            }
                            else
                            {
                                Num= Max = registro.Select(c => c.OrderNo).Max()+1;
                            }
                        }
                        
                    }
                    var guardar = calculationOrderControl;
                    guardar.OrderNo = Num;
                    db.CalculationOrderControl.Add(guardar);
                    db.SaveChanges();
                    var upt = db.CalculationOrder.Where(c => c.Code == calculationOrderControl.CalculationOrderCode).FirstOrDefault();
                    if (upt != null)
                    {
                        upt.PayrollControls = Num;
                        db.Entry(upt).State = EntityState.Modified;
                        db.SaveChanges();
                    }
                    result = true;
                }


            }
            catch (SystemException ex)
            {
                logRepository.CapturarTodoError(ex);
            }


            return result;
        }

        [HttpPost]
        public bool EditarOrder([Bind(Include = "CalculationOrderCode, OrderNo, PayrollControlCode, AnotherOrderUses, MasterOrder")] CalculationOrderControl calculationOrderControl)
        {
            bool result = false;
            try
            {
                if (ModelState.IsValid)
                {
                    var guardar = db.CalculationOrderControl
                        .Where(c => c.CalculationOrderCode == calculationOrderControl.CalculationOrderCode
                        && c.OrderNo==calculationOrderControl.OrderNo).FirstOrDefault();
                    if (guardar != null)
                    {
                        guardar.PayrollControlCode = calculationOrderControl.PayrollControlCode;
                        guardar.AnotherOrderUses = calculationOrderControl.AnotherOrderUses;
                        guardar.MasterOrder = calculationOrderControl.MasterOrder;

                        db.Entry(guardar).State = EntityState.Modified;
                        db.SaveChanges();
                        result = true;
                    }
                    
                }


            }
            catch (SystemException ex)
            {
                logRepository.CapturarTodoError(ex);
            }
            return result;
        }
        public bool EliminarOrder([Bind(Include = "CalculationOrderCode, OrderNo")] CalculationOrderControl calculationOrderControl)
        {
            bool result = false;
            try
            {
                if (ModelState.IsValid)
                {
                    var remove = db.CalculationOrderControl
                        .Where(c => c.CalculationOrderCode == calculationOrderControl.CalculationOrderCode
                        && c.OrderNo == calculationOrderControl.OrderNo).FirstOrDefault();
                    if (remove != null)
                    {

                        db.CalculationOrderControl.Remove(remove);
                        db.SaveChanges();
                        result = true;
                        var registro = db.CalculationOrderControl.Where(c => c.CalculationOrderCode == calculationOrderControl.CalculationOrderCode).ToList();
                        int Num = 0;
                        if (registro.Count > 0)
                        {
                            Num = registro.Select(c => c.OrderNo).Max();
                        }
                        var upt = db.CalculationOrder.Where(c => c.Code == calculationOrderControl.CalculationOrderCode).FirstOrDefault();
                        if (upt != null)
                        {
                            upt.PayrollControls = Num;
                            db.Entry(upt).State = EntityState.Modified;
                            db.SaveChanges();
                        }
                    }

                }


            }
            catch (SystemException ex)
            {
                logRepository.CapturarTodoError(ex);
            }
            return result;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
