﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ControlPlanilla.Models;
using DevExpress.Data.ODataLinq.Helpers;
using DevExpress.Web.Internal;

namespace ControlPlanilla.Controllers
{
    public class ZONAS_PAISController : AuditoriaController
    {
        private GlobalEntities db = new GlobalEntities();
        private EmployeeEntities dbE = new EmployeeEntities();
        // GET: AREAS_EMPRESA
        public ActionResult Index()
        {
            return View(dbE.ZONA_SERVICIO.ToList());
        }

        // GET: AREAS_EMPRESA/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ZONA_SERVICIO aREAS_EMPRESA = dbE.ZONA_SERVICIO.Find(id);
            if (aREAS_EMPRESA == null)
            {
                return HttpNotFound();
            }
            return View(aREAS_EMPRESA);
        }

        // GET: AREAS_EMPRESA/Create
        public ActionResult Create()
        {
            var listaPaises = dbMenus.PAISES.Select(a => new PaisesViewModels { CD_PAIS = a.CD_PAIS, DS_PAIS = a.DS_NOMBREPAIS }).ToList();
            ViewBag.cmbPaises = listaPaises;
            return View();
        }

        // POST: AREAS_EMPRESA/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "CD_ZONA,CD_PAIS,DS_ZONA")] ZONA_SERVICIO  zONA_SERVICIO)
        {
            if (ModelState.IsValid)
            {

                zONA_SERVICIO.USUA_CREA = Usuario;
                zONA_SERVICIO.FECH_CREA = Ahora;
                dbE.ZONA_SERVICIO.Add(zONA_SERVICIO);
                dbE.SaveChanges();

                RELACION_ZONA_PAIS grabar = dbE.RELACION_ZONA_PAIS.Where(b => b.CD_ZONA.Equals(zONA_SERVICIO.CD_ZONA) && b.CD_PAIS.Equals(zONA_SERVICIO.CD_PAIS)).FirstOrDefault();
                if (grabar == null)
                {
                    grabar = new RELACION_ZONA_PAIS();
                    grabar.CD_PAIS = zONA_SERVICIO.CD_PAIS;
                    grabar.CD_ZONA = zONA_SERVICIO.CD_ZONA;
                    grabar.FECH_CREA = Ahora;
                    grabar.USUA_CREA = Usuario;
                    dbE.RELACION_ZONA_PAIS.Add(grabar);
                    dbE.SaveChanges();
                }

                return RedirectToAction("Index");

            }

            return View(zONA_SERVICIO);
        }

        // GET: AREAS_EMPRESA/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ZONA_SERVICIO aREAS_EMPRESA = dbE.ZONA_SERVICIO.Find(id);
            if (aREAS_EMPRESA == null)
            {
                return HttpNotFound();
            }
            else {
                var listaPais = from ar in dbE.ZONA_SERVICIO
                                    join rl in dbE.RELACION_ZONA_PAIS on ar.CD_ZONA equals rl.CD_ZONA
                                    where ar.CD_ZONA == id
                                    select new { CD_PAIS = rl.CD_PAIS};
                var infon = listaPais.First();
                ViewBag.CD_PAIS = infon.CD_PAIS;
                var listaPaises = dbMenus.PAISES.Select(a => new PaisesViewModels { CD_PAIS = a.CD_PAIS, DS_PAIS = a.DS_NOMBREPAIS }).ToList();
                ViewBag.cmbPaises = listaPaises;
            }
            return View(aREAS_EMPRESA);
        }

        // POST: AREAS_EMPRESA/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "CD_ZONA,CD_PAIS,DS_ZONA")] ZONA_SERVICIO aREAS_EMPRESA)
        {
            if (ModelState.IsValid)
            {
                ZONA_SERVICIO grabar = dbE.ZONA_SERVICIO.Find(aREAS_EMPRESA.CD_ZONA);
                grabar.FECH_ACTU = Ahora;
                grabar.DS_ZONA = aREAS_EMPRESA.DS_ZONA;
                grabar.USUA_ACTU = Usuario;
                dbE.Entry(grabar).State = EntityState.Modified;
                dbE.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(aREAS_EMPRESA);
        }

        // GET: AREAS_EMPRESA/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ZONA_SERVICIO aREAS_EMPRESA = dbE.ZONA_SERVICIO.Find(id);
            if (aREAS_EMPRESA == null)
            {
                return HttpNotFound();
            }
            return View(aREAS_EMPRESA);
        }

        // POST: AREAS_EMPRESA/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ZONA_SERVICIO aREAS_EMPRESA = dbE.ZONA_SERVICIO.Find(id);

            RELACION_ZONA_PAIS grabar1 = dbE.RELACION_ZONA_PAIS.Where(b => b.CD_ZONA.Equals(aREAS_EMPRESA.CD_ZONA) && b.CD_PAIS.Equals(aREAS_EMPRESA.CD_PAIS)).FirstOrDefault();
            if (grabar1 != null) {
                dbE.RELACION_ZONA_PAIS.Remove(grabar1);
            }
            dbE.ZONA_SERVICIO.Remove(aREAS_EMPRESA);
            dbE.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        
        public JsonResult ObtenerEmpresas(string cd_pais)
        {

            var listaEmpresas = from rl1 in dbMenus.RELACION_EMPRESA_PAIS
                                join emp in dbMenus.EMPRESAS on rl1.CD_EMPRESA equals emp.CD_EMPRESA
                                where rl1.CD_PAIS == cd_pais
                                select new { CD_EMPRESA = emp.CD_EMPRESA, DS_EMPRESA = emp.DS_EMPRESA };

            return Json(listaEmpresas, JsonRequestBehavior.AllowGet);
        }
    }
}
