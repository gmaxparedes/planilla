﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ControlPlanilla.Models;

namespace ControlPlanilla.Controllers
{
    public class DEPTO_EMPRESAController : AuditoriaController
    {
        //private GlobalEntities db = new GlobalEntities();

        // GET: DEPTO_EMPRESA
        public ActionResult Index()
        {
            var dEPTO_EMPRESA = dbMenus.DEPTO_EMPRESA.Include(d => d.AREAS_EMPRESA).Include(d => d.EMPRESAS);
            return View(dEPTO_EMPRESA.ToList());
        }

        // GET: DEPTO_EMPRESA/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DEPTO_EMPRESA dEPTO_EMPRESA = dbMenus.DEPTO_EMPRESA.Find(id);
            if (dEPTO_EMPRESA == null)
            {
                return HttpNotFound();
            }
            return View(dEPTO_EMPRESA);
        }

        // GET: DEPTO_EMPRESA/Create
        public ActionResult Create()
        {
            var listaPaises = dbMenus.PAISES.Select(a => new PaisesViewModels { CD_PAIS = a.CD_PAIS, DS_PAIS = a.DS_NOMBREPAIS }).ToList();
            ViewBag.cmbPaises = listaPaises;
            
            return View();
        }

        // POST: DEPTO_EMPRESA/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID_AREA,CD_EMPRESA,DESCRIPCION")] DEPTO_EMPRESA dEPTO_EMPRESA)
        {
            if (ModelState.IsValid)
            {
                dEPTO_EMPRESA.USUA_CREA = Usuario;
                dEPTO_EMPRESA.FECH_CREA = Ahora;
                dbMenus.DEPTO_EMPRESA.Add(dEPTO_EMPRESA);
                dbMenus.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ID_AREA = new SelectList(dbMenus.AREAS_EMPRESA, "ID_AREA", "CD_EMPRESA", dEPTO_EMPRESA.ID_AREA);
            ViewBag.CD_EMPRESA = new SelectList(dbMenus.EMPRESAS, "CD_EMPRESA", "DS_EMPRESA", dEPTO_EMPRESA.CD_EMPRESA);
            return View(dEPTO_EMPRESA);
        }

        // GET: DEPTO_EMPRESA/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DEPTO_EMPRESA dEPTO_EMPRESA = dbMenus.DEPTO_EMPRESA.Find(id);
            if (dEPTO_EMPRESA == null)
            {
                return HttpNotFound();
            }
            var listaPaises = dbMenus.PAISES.Select(a => new PaisesViewModels { CD_PAIS = a.CD_PAIS, DS_PAIS = a.DS_NOMBREPAIS }).ToList();
            ViewBag.cmbPaises = listaPaises;
            var listaPais = from ar in dbMenus.AREAS_EMPRESA
                            join rl in dbMenus.RELACION_EMPRESA_PAIS on ar.CD_EMPRESA equals rl.CD_EMPRESA
                            where ar.ID_AREA == dEPTO_EMPRESA.ID_AREA
                            select new { CD_PAIS = rl.CD_PAIS };
            var infon = listaPais.First();
            ViewBag.CD_PAIS = infon.CD_PAIS;

            ViewBag.cmbEMPRESA = ObtenerEmpresasL(infon.CD_PAIS);
            ViewBag.ID_AREA = new SelectList(dbMenus.AREAS_EMPRESA, "ID_AREA", "DESCRIPCION", dEPTO_EMPRESA.ID_AREA);
            
            return View(dEPTO_EMPRESA);
        }

        // POST: DEPTO_EMPRESA/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID_DEPTO,ID_AREA,CD_EMPRESA,DESCRIPCION")] DEPTO_EMPRESA dEPTO_EMPRESA)
        {
            if (ModelState.IsValid)
            {
                DEPTO_EMPRESA grabar = dbMenus.DEPTO_EMPRESA.Find(dEPTO_EMPRESA.ID_DEPTO);
                grabar.ID_AREA = dEPTO_EMPRESA.ID_AREA;
                grabar.CD_EMPRESA = dEPTO_EMPRESA.CD_EMPRESA;
                grabar.DESCRIPCION = dEPTO_EMPRESA.DESCRIPCION;
                grabar.FECH_ACTU = Ahora;
                grabar.USUA_ACTU = Usuario;
                dbMenus.Entry(grabar).State = EntityState.Modified;
                dbMenus.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ID_AREA = new SelectList(dbMenus.AREAS_EMPRESA, "ID_AREA", "CD_EMPRESA", dEPTO_EMPRESA.ID_AREA);
            ViewBag.CD_EMPRESA = new SelectList(dbMenus.EMPRESAS, "CD_EMPRESA", "DS_EMPRESA", dEPTO_EMPRESA.CD_EMPRESA);
            return View(dEPTO_EMPRESA);
        }

        // GET: DEPTO_EMPRESA/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DEPTO_EMPRESA dEPTO_EMPRESA = dbMenus.DEPTO_EMPRESA.Find(id);
            if (dEPTO_EMPRESA == null)
            {
                return HttpNotFound();
            }
            return View(dEPTO_EMPRESA);
        }

        // POST: DEPTO_EMPRESA/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            DEPTO_EMPRESA dEPTO_EMPRESA = dbMenus.DEPTO_EMPRESA.Find(id);
            dbMenus.DEPTO_EMPRESA.Remove(dEPTO_EMPRESA);
            dbMenus.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                dbMenus.Dispose();
            }
            base.Dispose(disposing);
        }

        public JsonResult ObtenerAreas(string cd_empresa)
        {

            var listaEmpresas = from  ar in dbMenus.AREAS_EMPRESA
                                where ar.CD_EMPRESA == cd_empresa
                                select new { ID_AREA = ar.ID_AREA, DESCRIPCION = ar.DESCRIPCION };

            return Json(listaEmpresas, JsonRequestBehavior.AllowGet);
        }
        public JsonResult ObtenerEmpresas(string cd_pais)
        {

            var listaEmpresas = from rl1 in dbMenus.RELACION_EMPRESA_PAIS
                                join emp in dbMenus.EMPRESAS on rl1.CD_EMPRESA equals emp.CD_EMPRESA
                                where rl1.CD_PAIS == cd_pais
                                select new { CD_EMPRESA = emp.CD_EMPRESA, DS_EMPRESA = emp.DS_EMPRESA };

            return Json(listaEmpresas, JsonRequestBehavior.AllowGet);
        }
        public List<EmpresasView> ObtenerEmpresasL(string cd_pais)
        {

            var listaEmpresas = from rl1 in dbMenus.RELACION_EMPRESA_PAIS
                                join emp in dbMenus.EMPRESAS on rl1.CD_EMPRESA equals emp.CD_EMPRESA
                                where rl1.CD_PAIS == cd_pais
                                select new EmpresasView { CD_EMPRESA = emp.CD_EMPRESA, DS_EMPRESA = emp.DS_EMPRESA };
            return listaEmpresas.ToList();

        }
    }
}
