﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ControlPlanilla.Models;
using System.Globalization;

namespace ControlPlanilla.Controllers
{
    public class HomeController : AuditoriaController
    {

        public ActionResult Index()
        {
            logRepository.Capturatodo("Se levanta");

            var listaPaises = dbe.PAISES.Select(a => new PaisesViewModels { CD_PAIS = a.CD_PAIS, DS_PAIS = a.DS_NOMBREPAIS }).ToList();
            //ViewBag.cmbPaises = listaPaises;
            AccederViewModels acceder = new AccederViewModels();

            acceder.PAISES = listaPaises;

            if (TempData["usuario"] != null)
            {
                acceder.CD_CODIGO_USUARIO = TempData["usuario"].ToString();
            }
            if (TempData["errorMensaje"] != null)
            {
                ViewBag.MensajeError = TempData["errorMensaje"];
                ModelState.AddModelError("CD_CLAVE_USUARIO", TempData["errorMensaje"].ToString());
            }
            if (TempData["Error"] != null)
            {
                ViewBag.MensajeError = TempData["Error"];
            }
            if (!String.IsNullOrEmpty(Usuario))
            {
                CerrarSesion();
            }

            return View(acceder);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult Inicio()
        {
            var nombre = dbe.USUARIOS.Where(c => c.CD_CODIGO_USUARIO == Usuario).FirstOrDefault();
            if (nombre != null) 
            {
                ViewBag.Usuario = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(nombre.DS_NOMBRE_USUARIO.ToString());
            }
            else
            {
                return RedirectToAction("Index");
            }
            return View();
        }

        public ActionResult ValidarCodigo(VerificarCodigo verificar)
        {
            if (TempData["errorMensaje"] != null)
            {
                ViewBag.MensajeError = TempData["errorMensaje"];
                ModelState.AddModelError("CodigoGeneraddo", "Código de validación incorrecto");
            }
            return View(verificar);
        }


        public JsonResult ObtenerEmpresas(string cd_pais)
        {
            //var listaEmpresas = dbe.GlobalEmpresas.Select(e => 
            //new { Text = e.DS_EMPRESA, Value = e.CD_EMPRESA})
            //    .Where(e=> e.CD_EMPRESA==cd_empresa);
            var listaEmpresas = dbe.RELACION_EMPRESA_PAIS.Where(ep => ep.CD_PAIS == cd_pais).
                Select(r => new EmpresasView
                {
                    CD_EMPRESA = r.EMPRESAS.CD_EMPRESA,
                    DS_EMPRESA = r.EMPRESAS.DS_EMPRESA
                }).ToList();

            return Json(listaEmpresas, JsonRequestBehavior.AllowGet);
        }

    }
}