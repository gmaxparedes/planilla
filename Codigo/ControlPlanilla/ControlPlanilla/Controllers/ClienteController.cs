﻿using System.Web.Mvc;
using Microsoft.Reporting.WebForms;
using DevExpress.Web.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ControlPlanilla.Models;
using System.Data.Entity;
using System.Web.UI.WebControls;

namespace ControlPlanilla.Controllers
{

    public class ClienteController : AuditoriaController
    {
        static string idclient;
        private EmployeeEntities dbE = new EmployeeEntities();
        ControlPlanilla.Models.EmployeeEntities db = new ControlPlanilla.Models.EmployeeEntities();
        public JsonResult ObtenerEmployee(string No)
        {
            var listaEmployee = dbE.Employee.Where(ep => ep.No == No).
                Select(r => new EmployeeView
                {
                    No = r.No,
                    FirstName = r.FirstName,
                    MiddleName = r.MiddleName == null ? "" : r.MiddleName,
                    LastName = r.LastName == null ? "" : r.LastName,
                    SecondLastName = r.SecondLastName,
                    MarriedLastname = r.MarriedLastname,
                    MiddleInitial = r.MiddleInitial == null ? "" : r.MiddleInitial,
                    Suffix = r.Suffix,
                    Initials = r.Initials,
                    SearchName = r.SearchName,
                    Address = r.Address,
                    Address2 = r.Address2,
                    CountryRegionCode = r.CountryRegionCode,
                    Departamento = r.Departamento,
                    City = r.City,
                    PostCode = r.PostCode,
                    ID_CARGO = r.ID_CARGO,
                    Blocked = r.Blocked,
                    LastDateModified = r.LastDateModified,
                    //tab 2
                    Dpto = r.Dpto,
                    Area = r.Area,
                    //seccion documentos
                    DUI = r.DUI,
                    SocialSecurityNo = r.SocialSecurityNo,
                    NomDUI = r.NomDUI,
                    SocialSecurityname = r.SocialSecurityname,
                    LugExpTI = r.LugExpTI,
                    NUP = r.NUP,
                    ProfDUI = r.ProfDUI,
                    NUPName = r.NUPName,
                    TILetras = r.TILetras,
                    NIT = r.NIT,
                    FExpTILetras = r.FExpTILetras,
                    NITName = r.NITName,
                    PhoneNo = r.PhoneNo,
                    HomePhoneNo = r.HomePhoneNo,
                    Extension = r.Extension,
                    MobilePhoneNo = r.MobilePhoneNo,
                    CompanyEMail = r.CompanyEMail,
                    EMail = r.EMail
                }).ToList();
            var listFinal = listaEmployee;
            return Json(listFinal, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ObtenerCliente(string No)
        {
            var listaCliente = dbE.Customer.Where(ep => ep.No == No).
                Select(r => new CustomerView
                {
                    No = r.No,
                    Contact = r.Contact,
                    PhoneNo = r.PhoneNo,
                    ClientePOS = r.ClientePOS,
                    SaldoTemporadaNormal = r.SaldoTemporadaNormal,
                    CreditLimitLCY = r.CreditLimitLCY,
                    SalespersonCode = r.SalespersonCode,
                    ResponsibilityCenter = r.ResponsibilityCenter,
                    CD_ZONA = r.CD_ZONA,
                    Blocked = r.Blocked,
                    LastDateModified = r.LastDateModified,
                    NoOrdenCompExenta = r.NoOrdenCompExenta,
                    NoConstanciaRegExonerado = r.NoConstanciaRegExonerado,
                    NoRegistroSecretaria = r.NoRegistroSecretaria,
                    TelexNo=r.TelexNo,
                    FaxNo=r.FaxNo,
                    EMail=r.EMail,
                    HomePage=r.HomePage,
                    GLN=r.GLN,
                    InvoiceCopies=r.InvoiceCopies
                }).ToList();
            var listFinal = listaCliente;
            return Json(listFinal, JsonRequestBehavior.AllowGet);
        }
        //
        [HttpPost]
        [AllowAnonymous]
        public ActionResult Create_Contacto(string No, string TelexNo, string FaxNo, string EMail, string HomePage)
        {
            if (ModelState.IsValid)
            {
                Customer grabar = dbE.Customer.Find(No);
                grabar.TelexNo = TelexNo;
                grabar.FaxNo = FaxNo;
                grabar.EMail = EMail;
                grabar.HomePage = HomePage;
                dbE.Entry(grabar).State = EntityState.Modified;
                dbE.SaveChanges();
                return RedirectToAction("Index");
            }
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult Create_Factura(string No, string GLN, int InvoiceCopies)
        {
            if (ModelState.IsValid)
            {
                Customer grabar = dbE.Customer.Find(No);
                grabar.GLN = GLN;
                grabar.InvoiceCopies = InvoiceCopies;
                
                dbE.Entry(grabar).State = EntityState.Modified;
                dbE.SaveChanges();
                return RedirectToAction("Index");
            }
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult Create_Cliente(string No, string Contact, string PhoneNo, string ClientePOS, decimal SaldoTemporadaNormal, decimal CreditLimitLCY, string SalespersonCode, string ResponsibilityCenter, int CD_ZONA, string Blocked, string NoOrdenCompExenta, string NoConstanciaRegExonerado, string NoRegistroSecretaria)
        {
            if (No != "")
            {
                Customer grabar;
                //validar si existe
                var existincia = (from e in dbE.Customer
                                  where e.No == No
                                  select e.No).Count();

                if (existincia > 0)
                {
                    grabar = dbE.Customer.Find(No);
                }
                else
                {
                    grabar = new Customer();
                }

                grabar.No = No;
                grabar.Contact = Contact;
                grabar.PhoneNo = PhoneNo;
                grabar.ClientePOS = Convert.ToByte(ClientePOS == "on" ? "1" : "0");
                grabar.SaldoTemporadaNormal = SaldoTemporadaNormal;
                grabar.CreditLimitLCY = CreditLimitLCY;
                grabar.SalespersonCode = SalespersonCode;
                grabar.ResponsibilityCenter = ResponsibilityCenter;
                grabar.CD_ZONA = CD_ZONA;
                grabar.Blocked = Convert.ToInt16(ClientePOS == "on" ? "1" : "0");
                grabar.LastDateModified = Ahora;
                grabar.NoOrdenCompExenta = NoOrdenCompExenta;
                grabar.NoConstanciaRegExonerado = NoConstanciaRegExonerado;
                grabar.NoRegistroSecretaria = NoRegistroSecretaria;
                
                if (existincia > 0)
                {
                    dbE.Entry(grabar).State = EntityState.Modified;
                }
                else
                {
                    dbE.Customer.Add(grabar);
                }
                dbE.SaveChanges();
                return RedirectToAction("Index");
            }

            return View();
        }
        //


        public ActionResult Index(string id)
        {
            if (id != null)
            {
                ViewBag.idseleccinado = id;
                var listaEmployee = dbE.Employee.Where(ep => ep.No == id).FirstOrDefault();

                var listazona = (from ar in dbE.ZONA_SERVICIO
                                 join rl in dbE.RELACION_ZONA_PAIS on ar.CD_ZONA equals rl.CD_ZONA
                                 where ar.CD_PAIS == listaEmployee.CountryRegionCode
                                 select new Zona_ServicioView { CD_ZONA = ar.CD_ZONA, DS_ZONA = ar.DS_ZONA }).ToList();

                ViewBag.cmbServicio = listazona;
            }
            else {
                var listazona = (from ar in dbE.ZONA_SERVICIO
                                 join rl in dbE.RELACION_ZONA_PAIS on ar.CD_ZONA equals rl.CD_ZONA
                                 where ar.CD_PAIS == "0"
                                 select new Zona_ServicioView { CD_ZONA = ar.CD_ZONA, DS_ZONA = ar.DS_ZONA }).ToList();
                ViewBag.cmbServicio = listazona;
            }

            var listaPaises = dbMenus.PAISES.Select(a => new PaisesViewModels { CD_PAIS = a.CD_PAIS, DS_PAIS = a.DS_NOMBREPAIS }).ToList();
            ViewBag.cmbPaises = listaPaises;
            var listaEmployer = dbE.Employer.Select(a => new EmployerView { No = a.No, Name = a.Name }).ToList();
            ViewBag.cmbEmployer = listaEmployer;
            var listaCentroResponsabilidad = dbE.CENTRO_RESPONSABILIDAD.Select(a => new CENTRO_RESPONSABILIDADView { ID_CENTRO = a.ID_CENTRO, NOMBRE = a.NOMBRE }).ToList();
            ViewBag.cmbCentroResponsabilidad = listaCentroResponsabilidad;
            idclient = id;

            return View();
        }


        [ValidateInput(false)]
        public ActionResult BuscarID(string key)
        {
            idclient = key;
            return RedirectToAction("Index", new { id = key });
        }
        
        [ValidateInput(false)]
        public ActionResult Reporte(string IdCliente ) {
            var listaCliente = (from ar in dbE.Employee
                             join rl in dbE.Customer on ar.No equals rl.No
                             where ar.No == idclient
                             select new { Nombre = ar.FirstName +" " + ar.MiddleName + " " + ar.LastName + " " + ar.SecondLastName, saldo =rl.SaldoTemporadaNormal, limite=rl.CreditLimitLCY }).ToList();

           if (listaCliente.Count>0) { 
            ReportViewer reportes = new ReportViewer();

            ReportParameter[] rp = new ReportParameter[] {
                new ReportParameter("Cliente",listaCliente[0].Nombre),
                new ReportParameter("Codigo",idclient),
                new ReportParameter("Limite",listaCliente[0].limite.ToString()),
                new ReportParameter("Saldo",listaCliente[0].saldo.ToString())
            };
            reportes.LocalReport.EnableExternalImages = true;
            reportes.LocalReport.DataSources.Clear();
            reportes.ProcessingMode = ProcessingMode.Local;
            reportes.LocalReport.ReportPath = "rpts/rptNPE.rdlc";
            reportes.SizeToReportContent = true;
            reportes.Width = Unit.Percentage(100);
            reportes.Height = Unit.Percentage(100);
            reportes.LocalReport.Refresh();
            reportes.LocalReport.SetParameters(rp);
            reportes.Visible = true;
            ViewBag.ReportViewer = reportes;
            }
            return PartialView("Reporte");
        }

        ControlPlanilla.Models.EmployeeEntities db1 = new ControlPlanilla.Models.EmployeeEntities();

        [ValidateInput(false)]
        public ActionResult GridViewPartial()
        {
            var model = db1.GridCustomer;
            return PartialView("_GridViewPartial", model.ToList());
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult GridViewPartialAddNew([ModelBinder(typeof(DevExpressEditorsBinder))] ControlPlanilla.Models.GridCustomer item)
        {
            var model = db1.GridCustomer;
            if (ModelState.IsValid)
            {
                try
                {
                    model.Add(item);
                    db1.SaveChanges();
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
                ViewData["EditError"] = "Please, correct all errors.";
            return PartialView("_GridViewPartial", model.ToList());
        }
        [HttpPost, ValidateInput(false)]
        public ActionResult GridViewPartialUpdate([ModelBinder(typeof(DevExpressEditorsBinder))] ControlPlanilla.Models.GridCustomer item)
        {
            var model = db1.GridCustomer;
            if (ModelState.IsValid)
            {
                try
                {
                    var modelItem = model.FirstOrDefault(it => it.NO == item.NO);
                    if (modelItem != null)
                    {
                        this.UpdateModel(modelItem);
                        db1.SaveChanges();
                    }
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
                ViewData["EditError"] = "Please, correct all errors.";
            return PartialView("_GridViewPartial", model.ToList());
        }
        [HttpPost, ValidateInput(false)]
        public ActionResult GridViewPartialDelete(System.String NO)
        {
            var model = db1.GridCustomer;
            if (NO != null)
            {
                try
                {
                    var item = model.FirstOrDefault(it => it.NO == NO);
                    if (item != null)
                        model.Remove(item);
                    db1.SaveChanges();
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            return PartialView("_GridViewPartial", model.ToList());
        }
    }
}