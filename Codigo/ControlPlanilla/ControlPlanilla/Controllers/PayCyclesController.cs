﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ControlPlanilla.Models;
using PagedList;

namespace ControlPlanilla.Controllers
{
    public class PayCyclesController : AuditoriaController
    {
        // GET: PayCycles
        public ActionResult Index(string SearchString, int? page)
        {
            var PayC = dbCiclo.PayCycle.ToList();
            if (!string.IsNullOrEmpty(SearchString))
            {
                PayC = PayC.Where(c => c.Code.ToUpper().Contains(SearchString.ToUpper())).ToList();

                foreach (var item in PayC)
                {
                    item.PayFrecuencies = dbCiclo.PayFrecuency.ToList();
                    item.PayFactorAnuals = dbCiclo.PayFactorAnual.ToList();
                }
            }
            else
            {
                foreach (var item in PayC)
                {
                    item.PayFrecuencies = dbCiclo.PayFrecuency.ToList();
                    item.PayFactorAnuals = dbCiclo.PayFactorAnual.ToList();
                }

            }
            //int pageSize = 1;
            //int pageSize = Convert.ToInt32(ConfigurationManager.AppSettings["DefaultPageSize"]);
            int pageNumber = (page ?? 1);

            var result = PayC.ToPagedList(pageNumber, pageSize);
            return View(result);
        }

        // GET: PayCycles/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PayCycle dboPayCycle = dbCiclo.PayCycle.Find(id);
            if (dboPayCycle == null)
            {
                return HttpNotFound();
            }
            dboPayCycle.PayFrecuencies = dbCiclo.PayFrecuency.ToList();
            dboPayCycle.PayFactorAnuals = dbCiclo.PayFactorAnual.ToList();
            return View(dboPayCycle);
        }

        // GET: PayCycles/Create
        public ActionResult Create()
        {
            PayCycle PayC = new PayCycle();
            PayC.PayFrecuencies = dbCiclo.PayFrecuency.ToList();
            PayC.PayFactorAnuals = dbCiclo.PayFactorAnual.ToList();
            return View(PayC);
        }

        // POST: PayCycles/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Code,Description,PayFrequency,PaymentDelay,AnnualizingFactor,MonthlyFactor,Format,Caso1,Caso2,Caso3,Caso4")] PayCycle dboPayCycle)
        {
            if (ModelState.IsValid)
            {
                dbCiclo.PayCycle.Add(dboPayCycle);
                dbCiclo.SaveChanges();
                return RedirectToAction("Index");
            }
            dboPayCycle.PayFrecuencies = dbCiclo.PayFrecuency.ToList();
            dboPayCycle.PayFactorAnuals = dbCiclo.PayFactorAnual.ToList();
            return View(dboPayCycle);
        }

        // GET: PayCycles/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PayCycle dboPayCycle = dbCiclo.PayCycle.Find(id);
            PayCycle PayC = new PayCycle();
            PayC.PayFrecuencies = dbCiclo.PayFrecuency.ToList();
            if (dboPayCycle == null)
            {
                return HttpNotFound();
            }
            dboPayCycle.PayFrecuencies = dbCiclo.PayFrecuency.ToList();
            dboPayCycle.PayFactorAnuals = dbCiclo.PayFactorAnual.ToList();
            return View(dboPayCycle);
        }

        // POST: PayCycles/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Code,Description,PayFrequency,PaymentDelay,AnnualizingFactor,MonthlyFactor,Format,Caso1,Caso2,Caso3,Caso4")] PayCycle dboPayCycle)
        {
            if (ModelState.IsValid)
            {
                dbCiclo.Entry(dboPayCycle).State = EntityState.Modified;
                dbCiclo.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(dboPayCycle);
        }

        // GET: PayCycles/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PayCycle dboPayCycle = dbCiclo.PayCycle.Find(id);
            if (dboPayCycle == null)
            {
                return HttpNotFound();
            }
            dboPayCycle.PayFrecuencies = dbCiclo.PayFrecuency.ToList();
            dboPayCycle.PayFactorAnuals = dbCiclo.PayFactorAnual.ToList();
            return View(dboPayCycle);
        }

        // POST: PayCycles/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            PayCycle dboPayCycle = dbCiclo.PayCycle.Find(id);
            dbCiclo.PayCycle.Remove(dboPayCycle);
            dbCiclo.SaveChanges();
            return RedirectToAction("Index");
        }


        /*CRUD para los terminos del ciclo de pago*/
        [HttpPost]
        [AllowAnonymous]
        public ActionResult AsociarTerminos(string PayCycleCode, int? page)
        {

            try
            {
                //int pageSize = 1;
                //int pageSize = Convert.ToInt32(ConfigurationManager.AppSettings["DefaultPageSize"]);
                int pageNumber = (page ?? 1);

                var termns = dbCiclo.PayCycleTerm.Where(t => t.PayCycleCode == PayCycleCode)
                    .OrderByDescending(x => x.Term).ToList();

                var result = termns.ToPagedList(pageNumber, pageSize);
                ViewBag.PayCycleCode = PayCycleCode;
                return PartialView("AsociarTerminos", result);
            }
            catch (SystemException ex)
            {
                logRepository.CapturarTodoError(ex);
                throw;
            }
        }

        [HttpPost]
        public bool CreateTerm([Bind(Include = "Term,PayCycleCode,DefaultPeriods,PeriodsGenerated")] PayCycleTerm dboPayCycleTerm)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    dbCiclo.PayCycleTerm.Add(dboPayCycleTerm);
                    dbCiclo.SaveChanges();
                    return true;
                }

                return false;
            }
            catch (SystemException ex)
            {
                logRepository.CapturarTodoError(ex);
                return false;
                throw;
            }

        }
        [HttpPost]
        public bool EditTerm([Bind(Include = "Term,PayCycleCode,DefaultPeriods,PeriodsGenerated")] PayCycleTerm dboPayCycleTerm)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    dbCiclo.Entry(dboPayCycleTerm).State = EntityState.Modified;
                    dbCiclo.SaveChanges();
                    return true;
                }
                return false;
            }
            catch (SystemException ex)
            {
                logRepository.CapturarTodoError(ex);
                return false;
            }

        }

        [HttpPost]
        public bool DeleteTerm(string id, string PayCycle)
        {
            try
            {
                PayCycleTerm dboPayCycleTerm = dbCiclo.PayCycleTerm.Where(c=> c.Term==id && c.PayCycleCode==PayCycle).FirstOrDefault();
                dbCiclo.PayCycleTerm.Remove(dboPayCycleTerm);
                dbCiclo.SaveChanges();
                return true;
            }
            catch (SystemException ex)
            {
                logRepository.CapturarTodoError(ex);
                return false;
            }

        }



        /* Crud para periodos de pagos  */
        [HttpPost]
        [AllowAnonymous]
        public PartialViewResult AsociarPeriodos(string Term, string PayCycle, int? page)
        {
            try
            {
                if (string.IsNullOrEmpty(Term))
                {
                    if (!string.IsNullOrEmpty(TempData["termino"].ToString()))
                    {
                        Term = TempData["termino"].ToString();
                    }
                }
                //int pageSize = 1;
                //int pageSize = Convert.ToInt32(ConfigurationManager.AppSettings["DefaultPageSize"]);
                int pageNumber = (page ?? 1);

                var perds = dbCiclo.PayCyclePeriod.Where(c => c.PayCycleTerm == Term && c.PayCycleCode==PayCycle)
                    .OrderBy(o => o.Period).ToList();

                var result = perds.ToPagedList(pageNumber, pageSize);
                ViewBag.Term = Term;
                ViewBag.PayCycleCode=PayCycle;

                return PartialView(result);
            }
            catch (SystemException ex)
            {
                logRepository.CapturarTodoError(ex);
                throw;
            }
        }

        [HttpPost]
        public bool CreatePer([Bind(Include = "Period,PayCycleCode,PayCycleTerm,StartDate,EndDate,PayDate")] PayCyclePeriod dboPayCyclePeriod)
        {
            TempData["termino"] = dboPayCyclePeriod.PayCycleTerm.ToString();
            try
            {
                if (ModelState.IsValid)
                {
                    dbCiclo.PayCyclePeriod.Add(dboPayCyclePeriod);
                    dbCiclo.SaveChanges();
                    return true;
                }

                return false;
            }
            catch (SystemException ex)
            {
                logRepository.CapturarTodoError(ex);
                return false;
                throw;
            }

        }
        [HttpPost]
        public bool EditPer([Bind(Include = "Period,PayCycleCode,PayCycleTerm,StartDate,EndDate,PayDate")] PayCyclePeriod dboPayCyclePeriod)
        {
            bool result = false;
            TempData["termino"] = dboPayCyclePeriod.PayCycleTerm.ToString();
            try
            {
                if (ModelState.IsValid)
                {
                    dbCiclo.Entry(dboPayCyclePeriod).State = EntityState.Modified;
                    dbCiclo.SaveChanges();
                    result = true;

                }
                return result;
            }
            catch (SystemException ex)
            {
                logRepository.CapturarTodoError(ex);
                return result;
                throw;
            }

        }

        [HttpPost]
        public bool DeletePer(string id, string PayCycleCode, string Term)
        {

            try
            {
                PayCyclePeriod dboPayCyclePeriod = dbCiclo.PayCyclePeriod.Where(c=>
                c.PayCycleCode==PayCycleCode && c.PayCycleTerm==Term && c.Period==id).FirstOrDefault();
                
                dbCiclo.PayCyclePeriod.Remove(dboPayCyclePeriod);
                dbCiclo.SaveChanges();
                return true;
            }
            catch (SystemException ex)
            {
                logRepository.CapturarTodoError(ex);
                return false;
                throw;
            }

        }



        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                dbCiclo.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
