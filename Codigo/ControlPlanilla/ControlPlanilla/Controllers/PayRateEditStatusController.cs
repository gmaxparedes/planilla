﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ControlPlanilla.Models;

namespace ControlPlanilla.Controllers
{
    public class PayRateEditStatusController : AuditoriaController
    {
        

        // GET: PayRateEditStatus
        public ActionResult Index()
        {
            return View(dbStatusRate.PayRateEditStatus.ToList());
        }

        // GET: PayRateEditStatus/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PayRateEditStatus payRateEditStatus = dbStatusRate.PayRateEditStatus.Find(id);
            if (payRateEditStatus == null)
            {
                return HttpNotFound();
            }
            return View(payRateEditStatus);
        }

        // GET: PayRateEditStatus/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: PayRateEditStatus/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID_EDIT,DESCRIPCION")] PayRateEditStatus payRateEditStatus)
        {
            if (ModelState.IsValid)
            {
                PayRateEditStatus guardar = new PayRateEditStatus();
                guardar.DESCRIPCION = payRateEditStatus.DESCRIPCION;
                guardar.USUA_CREA = Usuario;
                guardar.FECH_CREA = Ahora;
                dbStatusRate.PayRateEditStatus.Add(guardar);
                dbStatusRate.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(payRateEditStatus);
        }

        // GET: PayRateEditStatus/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PayRateEditStatus payRateEditStatus = dbStatusRate.PayRateEditStatus.Find(id);
            if (payRateEditStatus == null)
            {
                return HttpNotFound();
            }
            return View(payRateEditStatus);
        }

        // POST: PayRateEditStatus/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID_EDIT,DESCRIPCION")] PayRateEditStatus payRateEditStatus)
        {
            if (ModelState.IsValid)
            {
                PayRateEditStatus payStatus = dbStatusRate.PayRateEditStatus.Find(payRateEditStatus.ID_EDIT);

                payStatus.DESCRIPCION = payRateEditStatus.DESCRIPCION;
                payStatus.USUA_ACTUA = Usuario;
                payStatus.FECH_ACTUA = Ahora;

                dbStatusRate.Entry(payStatus).State = EntityState.Modified;
                dbStatusRate.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(payRateEditStatus);
        }

        // GET: PayRateEditStatus/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PayRateEditStatus payRateEditStatus = dbStatusRate.PayRateEditStatus.Find(id);
            if (payRateEditStatus == null)
            {
                return HttpNotFound();
            }
            return View(payRateEditStatus);
        }

        // POST: PayRateEditStatus/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            PayRateEditStatus payRateEditStatus = dbStatusRate.PayRateEditStatus.Find(id);
            dbStatusRate.PayRateEditStatus.Remove(payRateEditStatus);
            dbStatusRate.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                dbStatusRate.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
