﻿using DevExpress.Web.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ControlPlanilla.Models;
using System.Data.Entity;

namespace ControlPlanilla.Controllers
{
    public class GLB_MENUS_SISTEMASController : AuditoriaController
    {
        static string idMenu;
        // GET: GLB_MENUS_SISTEMAS
        public ActionResult Index(string id)
        {
            if (id != null)
            {
                ViewBag.idseleccinado = id;
            }
                return View();
        }
        
        private ControlPlanilla.Models.PlanillaAccesoMenus db = new ControlPlanilla.Models.PlanillaAccesoMenus();

        public JsonResult ObtenerMenus(string CD_CODI_MENU)
        {
            var listaEmployee = db.GLB_MENUS_SISTEMAS.Where(ep => ep.CD_CODI_MENU == CD_CODI_MENU).
                Select(r => new GLB_MENUS_SISTEMASView
                {
                    CD_CODI_MENU = r.CD_CODI_MENU,
                    CD_EMPRESA = r.CD_EMPRESA,
                    CD_ESTADO_MENU = r.CD_ESTADO_MENU,
                    DS_DESC_MENU = r.DS_DESC_MENU,
                }).ToList();
            var listFinal = listaEmployee;
            return Json(listFinal, JsonRequestBehavior.AllowGet);
        }
        
        [ValidateInput(false)]
        public ActionResult BuscarID(string key)
        {
            idMenu = key;
            return RedirectToAction("Index", new { id = key });
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult Create_Menu(string CD_CODI_MENU,  string DS_DESC_MENU, string CD_ESTADO_MENU)
        {
            if (CD_CODI_MENU != "")
            {
                GLB_MENUS_SISTEMAS   grabar;
                //validar si existe
                var existincia = (from e in db.GLB_MENUS_SISTEMAS
                                  where e.CD_CODI_MENU == CD_CODI_MENU
                                  select e.CD_CODI_MENU).Count();

                if (existincia > 0)
                {
                    grabar = db.GLB_MENUS_SISTEMAS.Find(CD_CODI_MENU);
                }
                else
                {
                    grabar = new GLB_MENUS_SISTEMAS();
                }

                grabar.CD_CODI_MENU = CD_CODI_MENU;
                grabar.CD_EMPRESA = CdEmpresa;
                grabar.CD_ESTADO_MENU = Convert.ToBoolean(CD_ESTADO_MENU );
                grabar.DS_DESC_MENU = DS_DESC_MENU;
                grabar.FECH_CREA = Ahora;
                grabar.USUA_CREA = Usuario;

                if (existincia > 0)
                {
                    db.Entry(grabar).State = EntityState.Modified;
                }
                else
                {
                    db.GLB_MENUS_SISTEMAS.Add(grabar);
                }
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View();
        }



        [ValidateInput(false)]
        public ActionResult GridView1Partial()
        {
            var model = db.GLB_MENUS_SISTEMAS;
            return PartialView("_GridView1Partial", model.ToList());
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult GridView1PartialAddNew([ModelBinder(typeof(DevExpressEditorsBinder))] ControlPlanilla.Models.GLB_MENUS_SISTEMAS item)
        {
            var model = db.GLB_MENUS_SISTEMAS;
            if (ModelState.IsValid)
            {
                try
                {
                    model.Add(item);
                    db.SaveChanges();
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
                ViewData["EditError"] = "Please, correct all errors.";
            return PartialView("_GridView1Partial", model.ToList());
        }
        [HttpPost, ValidateInput(false)]
        public ActionResult GridView1PartialUpdate([ModelBinder(typeof(DevExpressEditorsBinder))] ControlPlanilla.Models.GLB_MENUS_SISTEMAS item)
        {
            var model = db.GLB_MENUS_SISTEMAS;
            if (ModelState.IsValid)
            {
                try
                {
                    var modelItem = model.FirstOrDefault(it => it.CD_CODI_MENU == item.CD_CODI_MENU);
                    if (modelItem != null)
                    {
                        this.UpdateModel(modelItem);
                        db.SaveChanges();
                    }
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
                ViewData["EditError"] = "Please, correct all errors.";
            return PartialView("_GridView1Partial", model.ToList());
        }
        [HttpPost, ValidateInput(false)]
        public ActionResult GridView1PartialDelete(System.String CD_CODI_MENU)
        {
            var model = db.GLB_MENUS_SISTEMAS;
            if (CD_CODI_MENU != null)
            {
                try
                {
                    var item = model.FirstOrDefault(it => it.CD_CODI_MENU == CD_CODI_MENU);
                    if (item != null)
                        model.Remove(item);
                    db.SaveChanges();
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            return PartialView("_GridView1Partial", model.ToList());
        }
    }
}