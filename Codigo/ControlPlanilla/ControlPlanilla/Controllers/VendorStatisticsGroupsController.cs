﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ControlPlanilla.Models;

namespace ControlPlanilla.Controllers
{
    public class VendorStatisticsGroupsController : AuditoriaController    {
        

        // GET: VendorStatisticsGroups
        public ActionResult Index()
        {
            return View(dbVendor.VendorStatisticsGroup.ToList());
        }

        // GET: VendorStatisticsGroups/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VendorStatisticsGroup vendorStatisticsGroup = dbVendor.VendorStatisticsGroup.Find(id);
            if (vendorStatisticsGroup == null)
            {
                return HttpNotFound();
            }
            return View(vendorStatisticsGroup);
        }

        // GET: VendorStatisticsGroups/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: VendorStatisticsGroups/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID_STATISCS,DESCRIPCION")] VendorStatisticsGroup vendorStatisticsGroup)
        {
            if (ModelState.IsValid)
            {
                VendorStatisticsGroup guardar = new VendorStatisticsGroup();
                guardar.DESCRIPCION = vendorStatisticsGroup.DESCRIPCION;
                guardar.USUA_CREA = Usuario;
                guardar.FECH_CREA = Ahora;
                dbVendor.VendorStatisticsGroup.Add(guardar);
                dbVendor.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(vendorStatisticsGroup);
        }

        // GET: VendorStatisticsGroups/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VendorStatisticsGroup vendorStatisticsGroup = dbVendor.VendorStatisticsGroup.Find(id);
            if (vendorStatisticsGroup == null)
            {
                return HttpNotFound();
            }
            return View(vendorStatisticsGroup);
        }

        // POST: VendorStatisticsGroups/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID_STATISCS,DESCRIPCION")] VendorStatisticsGroup vendorStatisticsGroup)
        {
            if (ModelState.IsValid)
            {
                VendorStatisticsGroup guardar = dbVendor.VendorStatisticsGroup.Where(c=> c.ID_STATISCS== vendorStatisticsGroup.ID_STATISCS).FirstOrDefault();
                if (guardar != null)
                {
                    guardar.DESCRIPCION = vendorStatisticsGroup.DESCRIPCION;
                    guardar.USUA_ACTUA = Usuario;
                    guardar.FECH_ACTUA = Ahora;
                    dbVendor.Entry(guardar).State = EntityState.Modified;
                    dbVendor.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            return View(vendorStatisticsGroup);
        }

        // GET: VendorStatisticsGroups/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VendorStatisticsGroup vendorStatisticsGroup = dbVendor.VendorStatisticsGroup.Find(id);
            if (vendorStatisticsGroup == null)
            {
                return HttpNotFound();
            }
            return View(vendorStatisticsGroup);
        }

        // POST: VendorStatisticsGroups/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            VendorStatisticsGroup vendorStatisticsGroup = dbVendor.VendorStatisticsGroup.Find(id);
            dbVendor.VendorStatisticsGroup.Remove(vendorStatisticsGroup);
            dbVendor.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                dbVendor.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
