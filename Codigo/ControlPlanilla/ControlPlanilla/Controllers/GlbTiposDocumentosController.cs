﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ControlPlanilla.Models;

namespace ControlPlanilla.Controllers
{
    public class GlbTiposDocumentosController : AuditoriaController
    {


        // GET: GlbTiposDocumentos
        public ActionResult Index()
        {
            return View(dbMenus.GLB_TIPO_DOCUMENTOS.ToList());
        }

        // GET: GlbTiposDocumentos/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            GLB_TIPO_DOCUMENTOS gLB_TIPO_DOCUMENTOS = dbMenus.GLB_TIPO_DOCUMENTOS.Find(id);
            if (gLB_TIPO_DOCUMENTOS == null)
            {
                return HttpNotFound();
            }
            return View(gLB_TIPO_DOCUMENTOS);
        }

        // GET: GlbTiposDocumentos/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: GlbTiposDocumentos/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "CD_TIPO_DOCUMENTO, DS_TIPO_DOCUMENTO")] GLB_TIPO_DOCUMENTOS gLB_TIPO_DOCUMENTOS)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    gLB_TIPO_DOCUMENTOS.CD_EMPRESA = CdEmpresa;
                    gLB_TIPO_DOCUMENTOS.USUA_CREA = Usuario;
                    gLB_TIPO_DOCUMENTOS.FECH_CREA = Ahora;

                    dbMenus.GLB_TIPO_DOCUMENTOS.Add(gLB_TIPO_DOCUMENTOS);
                    dbMenus.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            catch (SystemException ex)
            {
                logRepository.CapturarTodoError(ex);
                throw;
            }
            return View(gLB_TIPO_DOCUMENTOS);
        }

        // GET: GlbTiposDocumentos/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            GLB_TIPO_DOCUMENTOS gLB_TIPO_DOCUMENTOS = dbMenus.GLB_TIPO_DOCUMENTOS.Find(id);
            if (gLB_TIPO_DOCUMENTOS == null)
            {
                return HttpNotFound();
            }
            return View(gLB_TIPO_DOCUMENTOS);
        }

        // POST: GlbTiposDocumentos/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "CD_TIPO_DOCUMENTO, DS_TIPO_DOCUMENTO")] GLB_TIPO_DOCUMENTOS gLB_TIPO_DOCUMENTOS)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    GLB_TIPO_DOCUMENTOS doc = dbMenus.GLB_TIPO_DOCUMENTOS.Find(gLB_TIPO_DOCUMENTOS.CD_TIPO_DOCUMENTO);

                    doc.CD_TIPO_DOCUMENTO = gLB_TIPO_DOCUMENTOS.CD_TIPO_DOCUMENTO;
                    doc.DS_TIPO_DOCUMENTO = gLB_TIPO_DOCUMENTOS.DS_TIPO_DOCUMENTO;
                    doc.USUA_ACTU = Usuario;
                    doc.FECH_ACTU = Ahora;

                    dbMenus.Entry(doc).State = EntityState.Modified;
                    dbMenus.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            catch (SystemException ex)
            {
                logRepository.CapturarTodoError(ex);
                throw;
            }

            return View(gLB_TIPO_DOCUMENTOS);
        }

        // GET: GlbTiposDocumentos/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            GLB_TIPO_DOCUMENTOS gLB_TIPO_DOCUMENTOS = dbMenus.GLB_TIPO_DOCUMENTOS.Find(id);
            if (gLB_TIPO_DOCUMENTOS == null)
            {
                return HttpNotFound();
            }
            return View(gLB_TIPO_DOCUMENTOS);
        }

        // POST: GlbTiposDocumentos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            try
            {
                GLB_TIPO_DOCUMENTOS gLB_TIPO_DOCUMENTOS = dbMenus.GLB_TIPO_DOCUMENTOS.Find(id);
                dbMenus.GLB_TIPO_DOCUMENTOS.Remove(gLB_TIPO_DOCUMENTOS);
                dbMenus.SaveChanges();
            }
            catch (SystemException ex)
            {
                logRepository.CapturarTodoError(ex);
                throw;
            }
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                dbMenus.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
