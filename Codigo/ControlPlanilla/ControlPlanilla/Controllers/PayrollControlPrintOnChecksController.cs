﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ControlPlanilla.Models;

namespace ControlPlanilla.Controllers
{
    public class PayrollControlPrintOnChecksController : AuditoriaController
    {

        // GET: PayrollControlPrintOnChecks
        public ActionResult Index()
        {
            return View(dbPayControl.PayrollControlPrintOnCheck.ToList());
        }

        // GET: PayrollControlPrintOnChecks/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PayrollControlPrintOnCheck payrollControlPrintOnCheck = dbPayControl.PayrollControlPrintOnCheck.Find(id);
            if (payrollControlPrintOnCheck == null)
            {
                return HttpNotFound();
            }
            return View(payrollControlPrintOnCheck);
        }

        // GET: PayrollControlPrintOnChecks/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: PayrollControlPrintOnChecks/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID_PRINT,DESCRIPCION")] PayrollControlPrintOnCheck payrollControlPrintOnCheck)
        {
            if (ModelState.IsValid)
            {
                PayrollControlPrintOnCheck guardar = new PayrollControlPrintOnCheck();
                guardar.DESCRIPCION = payrollControlPrintOnCheck.DESCRIPCION;
                guardar.USUA_CREA = Usuario;
                guardar.FECH_CREA = Ahora;
                dbPayControl.PayrollControlPrintOnCheck.Add(guardar);
                dbPayControl.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(payrollControlPrintOnCheck);
        }

        // GET: PayrollControlPrintOnChecks/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PayrollControlPrintOnCheck payrollControlPrintOnCheck = dbPayControl.PayrollControlPrintOnCheck.Find(id);
            if (payrollControlPrintOnCheck == null)
            {
                return HttpNotFound();
            }
            return View(payrollControlPrintOnCheck);
        }

        // POST: PayrollControlPrintOnChecks/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID_PRINT,DESCRIPCION")] PayrollControlPrintOnCheck payrollControlPrintOnCheck)
        {
            if (ModelState.IsValid)
            {
                PayrollControlPrintOnCheck guardar = dbPayControl.PayrollControlPrintOnCheck.Find(payrollControlPrintOnCheck.ID_PRINT);
                guardar.DESCRIPCION = payrollControlPrintOnCheck.DESCRIPCION;
                guardar.USUA_ACTUA = Usuario;
                guardar.FECH_ACTUA = Ahora;
                dbPayControl.Entry(guardar).State = EntityState.Modified;
                dbPayControl.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(payrollControlPrintOnCheck);
        }

        // GET: PayrollControlPrintOnChecks/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PayrollControlPrintOnCheck payrollControlPrintOnCheck = dbPayControl.PayrollControlPrintOnCheck.Find(id);
            if (payrollControlPrintOnCheck == null)
            {
                return HttpNotFound();
            }
            return View(payrollControlPrintOnCheck);
        }

        // POST: PayrollControlPrintOnChecks/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            PayrollControlPrintOnCheck payrollControlPrintOnCheck = dbPayControl.PayrollControlPrintOnCheck.Find(id);
            dbPayControl.PayrollControlPrintOnCheck.Remove(payrollControlPrintOnCheck);
            dbPayControl.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                dbPayControl.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
