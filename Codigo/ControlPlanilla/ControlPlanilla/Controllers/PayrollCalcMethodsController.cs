﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ControlPlanilla.Models;
using PagedList;

namespace ControlPlanilla.Controllers
{
    public class PayrollCalcMethodsController : AuditoriaController
    {
        private PlanillaPayControls db = new PlanillaPayControls();

        // GET: PayrollCalcMethods
        public ActionResult Index()
        {
            var payrollCalcMethod = db.PayrollCalcMethod.Include(p => p.PayrollControl);
            return View(payrollCalcMethod.ToList());
        }

        // GET: PayrollCalcMethods/Details/5
        public ActionResult Details(string id, DateTime fecha)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PayrollCalcMethod payrollCalcMethod = db.PayrollCalcMethod.Where(c => c.PayrollControlCode == id && c.EffectiveDate == fecha).FirstOrDefault();
            if (payrollCalcMethod == null)
            {
                return HttpNotFound();
            }
            return View(payrollCalcMethod);
        }

        // GET: PayrollCalcMethods/Create
        public ActionResult Create()
        {
            ViewBag.PayrollControlCode = new SelectList(db.PayrollControl, "Code", "Name");
            return View();
        }

        // POST: PayrollCalcMethods/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "PayrollControlCode,Description,EffectiveDate")] PayrollCalcMethod payrollCalcMethod)
        {
            if (ModelState.IsValid)
            {
                db.PayrollCalcMethod.Add(payrollCalcMethod);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.PayrollControlCode = new SelectList(db.PayrollControl, "Code", "Name", payrollCalcMethod.PayrollControlCode);
            return View(payrollCalcMethod);
        }

        // GET: PayrollCalcMethods/Edit/5
        public ActionResult Edit(string id, DateTime fecha)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PayrollCalcMethod payrollCalcMethod = db.PayrollCalcMethod.Where(c=> c.PayrollControlCode==id && c.EffectiveDate==fecha).FirstOrDefault();
            if (payrollCalcMethod == null)
            {
                return HttpNotFound();
            }
            ViewBag.PayrollControlCode = new SelectList(db.PayrollControl, "Code", "Name", payrollCalcMethod.PayrollControlCode);
            return View(payrollCalcMethod);
        }

        // POST: PayrollCalcMethods/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "PayrollControlCode,Description,EffectiveDate")] PayrollCalcMethod payrollCalcMethod)
        {
            if (ModelState.IsValid)
            {
                db.Entry(payrollCalcMethod).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.PayrollControlCode = new SelectList(db.PayrollControl, "Code", "Name", payrollCalcMethod.PayrollControlCode);
            return View(payrollCalcMethod);
        }

        // GET: PayrollCalcMethods/Delete/5
        public ActionResult Delete(string id, DateTime fecha)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PayrollCalcMethod payrollCalcMethod = db.PayrollCalcMethod.Where(c => c.PayrollControlCode == id && c.EffectiveDate == fecha).FirstOrDefault();
            if (payrollCalcMethod == null)
            {
                return HttpNotFound();
            }
            return View(payrollCalcMethod);
        }

        // POST: PayrollCalcMethods/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id, DateTime fecha)
        {
            PayrollCalcMethod payrollCalcMethod = db.PayrollCalcMethod.Where(c=> c.PayrollControlCode==id && c.EffectiveDate== fecha).FirstOrDefault();
            db.PayrollCalcMethod.Remove(payrollCalcMethod);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        #region cal lines
        [HttpPost]
        public PartialViewResult CaclLines(string PayrollControlCode, DateTime EffectiveDate, int? page)
        {
            ViewBag.BaseAmountCode = new SelectList(db.BaseAmount, "Code", "Name");
            ViewBag.BracketCode = new SelectList(db.Bracket, "Code", "Description");
            ViewBag.BracketTypeCode = new SelectList(db.BracketType, "Code", "Description");
            ViewBag.MethodStepCode = new SelectList(db.MethodStep, "Code", "Description");
            ViewBag.PayrollControlCode = new SelectList(db.PayrollCalcMethod, "PayrollControlCode", "Description");
            ViewBag.PayrollControlCode = new SelectList(db.PayrollCalcMethod, "PayrollControlCode", "Description");
            ViewBag.PayrollRateCode = new SelectList(db.PayrollRate, "Code", "Description");
            try
            {
                var payrollCalcMethodLine = db.PayrollCalcMethodLine
                    .Where(p => p.PayrollControlCode == PayrollControlCode && p.EffectiveDate == EffectiveDate)
                    .Include(p => p.BaseAmount).Include(p => p.Bracket).Include(p => p.BracketType)
                    .Include(p => p.MethodStep).Include(p => p.PayrollCalcMethod)
                    .Include(p => p.PayrollCalcMethod1).Include(p => p.PayrollRate).ToList();

                int pagenumber = (page ?? 1);

                var result = payrollCalcMethodLine.ToPagedList(pagenumber, pageSize);
                ViewBag.PayCode = PayrollControlCode;
                ViewBag.EffectiveDate = EffectiveDate;

                return PartialView(result);
            }
            catch (SystemException ex)
            {
                logRepository.CapturarTodoError(ex);
            }
            return PartialView();
        }
        [HttpGet]
        public JsonResult TraerEffectiveDateBase(string Code)
        {
            var fechas = new SelectList(db.BaseAmount.Where(c => c.Code == Code), "EffectiveDate", "EffectiveDate");

            return Json(fechas, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public JsonResult TraerBracketCode(string Type)
        {
            var bracket = new SelectList(db.Bracket.Where(c => c.BracketTypeCode == Type), "Code", "Description");

            return Json(bracket, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public JsonResult TraerEffectiveDateBrackets(string Code, string Type)
        {
            var fechas = new SelectList(db.Bracket.Where(c => c.BracketTypeCode == Type && c.Code == Code), "EffectiveDate", "EffectiveDate");

            return Json(fechas, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public bool CrearLineMethod([Bind(Include = "MethodStepCode,PayrollControlCode,EffectiveDate,BaseAmountCode,PayrollRateCode,BracketCode,BracketTypeCode,UnitofMeasureCode,ActivationCode,PayStructureCode,Comment,BracketTypeCodeBraket,EffectiveDateBreaket,EffectiveDateBaseAmount")] PayrollCalcMethodLine payrollCalcMethodLine)
        {
            bool result = false;
            try
            {
                payrollCalcMethodLine.EffectiveDateBaseAmount = payrollCalcMethodLine.EffectiveDate;
                payrollCalcMethodLine.EffectiveDateBreaket = payrollCalcMethodLine.EffectiveDate;
                if (ModelState.IsValid)
                {
                    var registro = db.PayrollCalcMethodLine.Where(
                        c => c.PayrollControlCode == payrollCalcMethodLine.PayrollControlCode &&
                        c.EffectiveDate == payrollCalcMethodLine.EffectiveDate).ToList();
                    int Num = 1;
                    int Min = 0;
                    int Max = 0;
                    if (registro.Count > 0)
                    {
                        Min = registro.Select(c => c.LineNumber).Min();
                        Max = registro.Select(c => c.LineNumber).Max();
                        var arreglo = registro.Select(c => c.LineNumber).ToList();
                        for (int i = 0; i < arreglo.Count; i++)
                        {
                            if ((i + 1) != arreglo[i])
                            {
                                Num = i + 1;
                                break;
                            }
                            else
                            {
                                Num = Max = registro.Select(c => c.LineNumber).Max() + 1;
                            }
                        }
                        

                    }
                    var guardar = payrollCalcMethodLine;
                    guardar.LineNumber = Num;
                    db.PayrollCalcMethodLine.Add(guardar);
                    db.SaveChanges();
                    result = true;
                }
            }
            catch (SystemException ex)
            {
                logRepository.CapturarTodoError(ex);
            }

            return result;
        }
        [HttpPost]
        public bool EditarLineMethod([Bind(Include = "MethodStepCode,PayrollControlCode,EffectiveDate,LineNumber,BaseAmountCode,PayrollRateCode,BracketCode,BracketTypeCode,UnitofMeasureCode,ActivationCode,PayStructureCode,Comment,BracketTypeCodeBraket,EffectiveDateBreaket,EffectiveDateBaseAmount")] PayrollCalcMethodLine payrollCalcMethodLine)
        {
            bool result = false;
            try
            {
                if (ModelState.IsValid)
                {
                    db.Entry(payrollCalcMethodLine).State = EntityState.Modified;
                    db.SaveChanges();
                    result = true;

                }
            }

            catch (SystemException ex)
            {
                logRepository.CapturarTodoError(ex);
            }

            return result;
        }
        [HttpPost]
        public bool EliminarLineMethod([Bind(Include = "PayrollControlCode,EffectiveDate,LineNumber")] PayrollCalcMethodLine payrollCalcMethodLine)
        {
            bool result = false;
            try
            {
                if (ModelState.IsValid)
                {
                    var remove = db.PayrollCalcMethodLine.Where(c => c.PayrollControlCode == payrollCalcMethodLine.PayrollControlCode
                    && c.EffectiveDate == payrollCalcMethodLine.EffectiveDate && c.LineNumber == payrollCalcMethodLine.LineNumber).FirstOrDefault();
                    if (remove != null)
                    {
                        db.PayrollCalcMethodLine.Remove(remove);
                        db.SaveChanges();
                    }

                    result = true;

                }
            }

            catch (SystemException ex)
            {
                logRepository.CapturarTodoError(ex);
            }

            return result;
        }

        #endregion
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
