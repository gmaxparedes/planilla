﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ControlPlanilla.Models;

namespace ControlPlanilla.Controllers
{
    public class PayFrecuenciesController : AuditoriaController
    {
        

        // GET: PayFrecuencies
        public ActionResult Index()
        {
            ViewBag.OpcionSistema = OpcionSistema;
            return View(dbCiclo.PayFrecuency.ToList());
        }

        // GET: PayFrecuencies/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PayFrecuency payFrecuency = dbCiclo.PayFrecuency.Find(id);
            if (payFrecuency == null)
            {
                return HttpNotFound();
            }
            return View(payFrecuency);
        }

        // GET: PayFrecuencies/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: PayFrecuencies/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Description")] PayFrecuency payFrecuency)
        {
            if (ModelState.IsValid)
            {
                payFrecuency.USUA_CREA = Usuario;
                payFrecuency.FECH_CREA = Ahora;
                dbCiclo.PayFrecuency.Add(payFrecuency);
                dbCiclo.SaveChanges();

                PayFrecuency payF = dbCiclo.PayFrecuency.Where(c => c.Description == payFrecuency.Description && c.USUA_CREA==Usuario).OrderBy(d=> d.PayFrequency).FirstOrDefault();

                bool requiereArchivos = ComprobarRequiereArchivos(OpcionSistema);

                if (requiereArchivos)
                {
                    TempData["OpcionSistema"] = OpcionSistema;
                    TempData["CdForaneo"] = payF.PayFrequency;
                    TempData["DsForaneo"] = "PayFrequency";
                    bool cumplio = ComprobarArchivosAdjuntos();

                    if (!cumplio)
                    {
                        //string opcion, string DsForaneo, string CdForaneo)
                        return RedirectToAction("SubirArchivos", "Auditoria", new { opcion = OpcionSistema, DsForaneo = payF.Description, CdForaneo = payF.PayFrequency });
                    }
                }

                return RedirectToAction("Index");
            }

            return View(payFrecuency);
        }

        // GET: PayFrecuencies/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PayFrecuency payFrecuency = dbCiclo.PayFrecuency.Find(id);
            if (payFrecuency == null)
            {
                return HttpNotFound();
            }
            return View(payFrecuency);
        }

        // POST: PayFrecuencies/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "PayFrequency,Description")] PayFrecuency payFrecuency)
        {
            if (ModelState.IsValid)
            {
                PayFrecuency payF = dbCiclo.PayFrecuency.Find(payFrecuency.PayFrequency);

                payF.Description = payFrecuency.Description;
                payF.USUA_ACTU = Usuario;
                payF.FECH_ACTU = Ahora;

                dbCiclo.Entry(payF).State = EntityState.Modified;
                dbCiclo.SaveChanges();

                bool requiereArchivos = ComprobarRequiereArchivos(OpcionSistema);

                if (requiereArchivos)
                {
                    TempData["OpcionSistema"] = OpcionSistema;
                    TempData["CdForaneo"] = payF.PayFrequency;
                    TempData["DsForaneo"] = payF.Description;

                    bool cumplio = ComprobarArchivosAdjuntos();

                    if (!cumplio)
                    {
                        //string opcion, string DsForaneo, string CdForaneo)
                        return RedirectToAction("SubirArchivos", "Auditoria", new { opcion = OpcionSistema, DsForaneo = payF.Description, CdForaneo = payF.PayFrequency });
                    }
                }

                return RedirectToAction("Index");
            }
            return View(payFrecuency);
        }

        // GET: PayFrecuencies/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PayFrecuency payFrecuency = dbCiclo.PayFrecuency.Find(id);
            if (payFrecuency == null)
            {
                return HttpNotFound();
            }
            return View(payFrecuency);
        }

        // POST: PayFrecuencies/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            PayFrecuency payFrecuency = dbCiclo.PayFrecuency.Find(id);
            dbCiclo.PayFrecuency.Remove(payFrecuency);
            dbCiclo.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                dbCiclo.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
