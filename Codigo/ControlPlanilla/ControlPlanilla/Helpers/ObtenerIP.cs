﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;

namespace ControlPlanilla.Helpers
{
    public class ObtenerIP
    {
        public string DevolverIP()
        {
            IPHostEntry host;

            string localIP = "";
            host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (IPAddress ip in host.AddressList)
            {
                if (ip.AddressFamily.ToString() == "InterNetwork")
                {
                    localIP = "ip: " + ip.ToString() + " ** " + Dns.GetHostName();
                }
                //break;
            }
            //localIP = "ip " + ip[1] + " ** " + host;
            return localIP;
        }
    }
}