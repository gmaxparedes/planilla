﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ControlPlanilla.Helpers
{
    public class VariablesGlobales
    {
        public string CdOpcionSistema { get; set; }
        public string CdForaneo { get; set; }
        public string DsForane { get; set; }
    }
}