﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ControlPlanilla.Helpers
{
    public class Constantes
    {
        
        public const string NombreCookie = "xc_target";
        public const string NombreCodigoCookie = "xc_stat_sh";//hash

        public const string NombreUsuarioCookie = "xc_us";//user
        public const string NombreCompletoUsuarioCookie = "xc_usfn";//nombre completo del usuario
        public const string NombreEmpresaCookie = "xc_cny";

        public const string NombreUsuarioNoAutenticado = "UNAUTHORIZED_USER";//MIGRACION

    }
}