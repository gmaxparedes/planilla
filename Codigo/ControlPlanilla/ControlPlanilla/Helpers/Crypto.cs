﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Security;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Configuration;

namespace ControlPlanilla.Helpers
{
    public class Crypto
    {
        //Logger logger = LogManager.GetCurrentClassLogger();
        private RijndaelManaged aesAlg;
        private byte[] _password;
        private ICryptoTransform _encryptor;
        public string EnCryptString(string input)
        {
            return CryptString(input, false);
        }

        public string DeCryptString(string input)
        {
            return CryptString(input, true);
        }

        #region Rijndael Encryption

        /// <summary>
        /// Encrypt the given text and give the byte array back as a BASE64 string
        /// </summary>
        /// <param name="text" />The text to encrypt
        /// <param name="salt" />The pasword salt
        /// <returns>The encrypted text</returns>
        public string CryptString(string inputString, bool decrypt)
        {
            //string salt = "syntepro";
            SecureString theSecureString = new NetworkCredential(String.Empty, inputString).SecurePassword;
            string salt = WebConfigurationManager.AppSettings["KSA"].ToString();
            aesAlg = new RijndaelManaged();
            if (!decrypt)
            {
                try
                {
                    if (string.IsNullOrEmpty(new NetworkCredential(String.Empty, theSecureString).Password)) throw new ArgumentNullException("inputString");

                    if (salt == null) throw new ArgumentNullException("salt");
                    var saltBytes = Encoding.ASCII.GetBytes(salt);
                    var key = new Rfc2898DeriveBytes(WebConfigurationManager.AppSettings["KSAP"].ToString(), saltBytes);
                    aesAlg.Key = key.GetBytes(aesAlg.KeySize / 8);
                    aesAlg.IV = key.GetBytes(aesAlg.BlockSize / 8);
                    _password = Encoding.ASCII.GetBytes(new NetworkCredential(String.Empty, theSecureString).Password);
                    _encryptor = aesAlg.CreateEncryptor(aesAlg.Key, aesAlg.IV);
                    var d = Convert.ToBase64String(_encryptor.TransformFinalBlock(_password, 0, _password.Length));
                    return Convert.ToBase64String(_encryptor.TransformFinalBlock(_password, 0, _password.Length));

                }
                catch (Exception)
                {
                    //logger.Error(ex, "Error al encriptar");
                    //throw;
                }
                finally
                {
                    if (aesAlg != null)
                    {
                        aesAlg.Clear();
                        aesAlg.Dispose();
                    }
                }
                return null;

            }
            else
            {
                try
                {
                    string text;
                    if (salt == null) throw new ArgumentNullException("salt");
                    var saltBytes = Encoding.ASCII.GetBytes(salt);
                    var key = new Rfc2898DeriveBytes(WebConfigurationManager.AppSettings["KSAP"].ToString(), saltBytes);
                    aesAlg.Key = key.GetBytes(aesAlg.KeySize / 8);
                    aesAlg.IV = key.GetBytes(aesAlg.BlockSize / 8);

                    var decryptor = aesAlg.CreateDecryptor(aesAlg.Key, aesAlg.IV);
                    var cipher = Convert.FromBase64String(new NetworkCredential(String.Empty, theSecureString).Password);

                    using (var msDecrypt = new MemoryStream(cipher))
                    {
                        using (var csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                        {
                            using (var srDecrypt = new StreamReader(csDecrypt))
                            {
                                text = srDecrypt.ReadToEnd();
                            }
                        }
                    }
                    return text;
                }
                catch (Exception)
                {

                    //logger.Error(ex, "Error al Desencriptar");
                    //throw;
                }
                finally
                {
                    if (aesAlg != null)
                    {
                        aesAlg.Clear();
                        aesAlg.Dispose();
                    }
                }
                return null;
            }
        }
        #endregion
    }

}