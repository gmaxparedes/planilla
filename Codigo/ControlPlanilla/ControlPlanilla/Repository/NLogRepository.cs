﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NLog;
using ControlPlanilla.Models;
using ControlPlanilla.Helpers;
using System.Configuration;


namespace ControlPlanilla.Repository
{
    public class NLogRepository
    {
        protected PlanillaAccesoMenus context { get; set; }
        Logger logWcf = LogManager.GetCurrentClassLogger();
        ObtenerIP ip = new ObtenerIP();
        string DevIp = string.Empty;
        public NLogRepository(PlanillaAccesoMenus context)
        {
            this.context = context;
            LogManager.Configuration.Variables["IP"].Text = ip.DevolverIP();
            //LogManager.Configuration.Variables["Cadena"].Text = context.Database.Connection.ConnectionString.ToString();
            //LogManager.Configuration.Variables["Cadena"].Text = ConfigurationManager.ConnectionStrings["Nlog"].ConnectionString.ToString();
            //var d = LogManager.Configuration.Variables["Cadena"].Text;

        }
        public void Capturatodo(string texto)
        {
            logWcf.Info(texto);
        }
        public void CapturarTodoError(SystemException ex)
        {
            logWcf.Error(ex, ex.Message);
        }
        public void Entrar(string Usuario)
        {
            logWcf.Info("Usuario: " + Usuario +" inició sesion");
        }
        public void Salir(string Usuario)
        {
            logWcf.Info("Usuario: " + Usuario + " terminó sesion");
        }
    }
}